<?php

namespace Business\Enums;

class WidgetsEnum extends BaseEnum {

	const ImportantBlock = 1;
	const Testimonials = 3;
	const Gallery = 4;
	const Events = 5;
	const Faq = 7;
	const PressKit = 9;
	const People = 10;
	const Programs = 11;
	const Map = 12;
	const Timeline = 13;
	const PressKitGallery = 14;
	const SidebarMenu = 15;
	const SidebarMenuMobileController = 16;
	const PressKitArchive = 17;
	const Document = 18;
	const Coaches = 19;
	const Categories = 20;
	const Players = 21;
	const Sponsors = 22;
	const Title2 = 23;
	const PromoBox = 24;
	const SocialIcon = 25;
	const SkillsDropdown = 26;
	const Divider = 27;
	const PlayerBox = 28;
	const NewMember = 29;
	const CentralBlock = 30;
	const TitleCenter = 31;
	const ContactBox = 32;
	const ContactForm = 33;
	const Video = 34;
	const People2 = 35;
	const VideoFull = 36;
	const Tabs = 37;
	const FeaturedBox = 38;
	const Trainings = 39;
	const BackgroundYoutubeVideo = 40;
	const HeadingBlock = 41;
	const Offers = 42;
	const OffersMap = 43;
	const LatestBlog = 44;
	const Booking = 45;
	const PageList = 46;
	const Search = 47;
	const Posts = 48;
	const Category = 49;
	const AdSense = 50;
	const ContentAd = 51;


	public function __construct() {
		$this->Descriptions = [
			1 => "Important Block",
			3 => "Testimonials",
			4 => "Gallery",
			5 => "Events",
			7 => "Faq",
			9 => "Press Kit",
			10 => "People",
			11 => "Programs",
			12 => "Map",
			13 => "Timeline",
			14 => "Press Kit Gallery",
			15 => "Sidebar Menu",
			16 => "Sidebar Menu Mobile Controller",
			17 => "Press Kit Archive",
			24 => "Promo Box",
			25 => "Social Icon",
			26 => "Skills Dropdown",
			28 => "Player Box",
			29 => "New Member",
			30 => "Central Block",
			31 => "TitleCenter",
			32 => "Contact Box",
			33 => "Contact Form",
			34 => "Video",
			35 => "People 2",
			36 => "Video Full",
			38 => "Featured Box",
			40 => "Background Youtube Video",
			41 => "Heading Block",
			42 => "Offers",
			43 => "Offers Map",
			44 => "Latest Blogs",
			45 => "Booking",
			46 => "Page List",
			50 => "AdSense",
			51 => "Content Ad",


		];
	}
}