<?php
/**
 * Created by PhpStorm.
 * User: nikola
 * Date: 15.6.16.
 * Time: 19.08
 */

namespace Business\Enums;


class LanguagesEnum extends BaseEnum {

    const English = 1;
    const Serbian = 2;

}