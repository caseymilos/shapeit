<?php

namespace Business\Enums;

class BuilderLayoutsEnum extends BaseEnum {

    const RowFull = 1;
    const Row50_50 = 2;
    const Row66_33 = 3;
    const Row33_66 = 4;
    const Row75_25 = 5;
    const Row25_75 = 6;
    const Row33_33_33 = 7;
    const Row25_25_25_25 = 8;
    const Row40_60 = 9;
    const StickySidebar = 10;

    public function __construct() {
        $this->Descriptions = [
            1 => "1 column",
            2 => "2 columns",
            3 => "2 columns",
            4 => "2 columns",
            5 => "2 columns",
            6 => "2 columns",
            7 => "3 columns",
            8 => "4 columns",
            9 => "2 columns",
            10 => "Sticky Sidebar",
        ];
    }

    public static function Subtitle($id) {
        switch ($id) {
            case 1:
                return "100%";
            case 2:
                return "50% + 50%";
            case 3:
                return "2/3 + 1/3";
            case 4:
                return "1/3 + 2/3";
            case 5:
                return "3/4 + 1/4";
            case 6:
                return "1/4 + 3/4";
            case 7:
                return "1/3 + 1/3 + 1/3";
            case 8:
                return "1/4 + 1/4 + 1/4 + 1/4";
            case 9:
                return "40% + 60%";
            case 10:
                return "1/3 + 2/3";
        }
    }

}