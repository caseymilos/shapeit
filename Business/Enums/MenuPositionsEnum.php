<?php

namespace Business\Enums;


class MenuPositionsEnum extends BaseEnum {

    const TopMenu = 1;
    const FooterMenu = 2;
    const HeaderMenu = 3;
    const NoPosition = 5;

    public function __construct() {
        $this->descriptions = [
            1 => "Top Menu",
            2 => "Footer Menu",
            3 => "Header Menu",
            5 => "No position",
        ];
    }

}