<?php

namespace Business\Enums;


class MenuItemTypesEnum extends BaseEnum {
    const Homepage = 1;
    const Faculty = 2;
    const Login = 4;
    const Page = 5;
    const External = 6;
    const Program = 7;
    const Nothing = 8;
}