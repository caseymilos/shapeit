<?php

namespace Business\Enums;

class AdPositionsEnum extends BaseEnum {

	const Sidebar = 1;
	const BetweenPosts = 2;

	public function __construct() {
		$this->descriptions = [
		];
	}
}