<?php

namespace Business\Enums;


class PermissionsEnum extends BaseEnum {

    const AccessSystem = 1;

    // Users
    const ViewUsers = 200;
    const CreateUser = 201;
    const EditUser = 202;
    const DeleteUser = 203;
    const InviteUser = 204;
    const ViewRoles = 205;
    const EditRole = 206;
    const DeleteRole = 207;
    const CreateRole = 208;

    // Coaches
    const ViewCoaches = 10;
    const EditCoaches = 11;
    const DeleteCoaches = 12;
    const CreateCoaches = 13;



    // Categories
    const ViewCategories = 40;
    const EditCategory = 41;
    const DeleteCategory = 42;


    // Menus
    const ViewMenus = 80;
    const EditMenu = 81;
    const DeleteMenu = 82;
    const CreateMenu = 83;


    // Security
    const Login = 110;
    const Logout = 111;




}