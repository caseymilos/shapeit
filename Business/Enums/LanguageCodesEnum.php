<?php
/**
 * Created by PhpStorm.
 * User: nikola
 * Date: 16.6.16.
 * Time: 11.17
 */

namespace Business\Enums;


class LanguageCodesEnum extends BaseEnum {
    
    const en = 1;
    const rs = 2;

}