<?php

namespace Business\Enums;

class MediaTypesEnum extends BaseEnum {

    const Picture = 1;
    const Video = 2;
    const Music = 3;
    const Document = 4;

}