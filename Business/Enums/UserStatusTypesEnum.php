<?php

namespace Business\Enums;

class UserStatusTypesEnum extends BaseEnum {
    
    const Active = 1;
    const Inactive = 2;
    const Blocked = 3;
    const Pending = 4;


    public $Descriptions = [];

    public function __construct() {
        $this->Descriptions = [

            1 => "Active",
            2 => "Inactive",
            3 => "Blocked",
            4 => "Pending",

        ];
    }

}