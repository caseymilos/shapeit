<?php

namespace Business\Enums;

class PostTypesEnum extends BaseEnum {

	const Health = 1;
	const Fitness = 2;
	const Beauty = 3;
	const Fashion = 4;
	const Disclaimer = 5;
	const Hygiene = 6;
	const Lifestyle = 7;


	public function __construct() {
		$this->descriptions = [
		];
	}
}