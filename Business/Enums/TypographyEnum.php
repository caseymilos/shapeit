<?php

namespace Business\Enums;

class TypographyEnum extends BaseEnum {

    const Title = 101;
    const TextArea = 102;
    const Button = 103;
    const Quote = 106;
    const Accordion = 108;
    const EmptySpace = 109;

    public function __construct() {
        $this->Descriptions = [
            101 => "Title",
            102 => "Text Area",
            103 => "Button",
            106 => "Quote",
            108 => "Accordion",
            109 => "Empty Space",
        ];
    }
}