<?php

namespace Business\DTO;

class LogFileDTO
{

    public $File;
    public $Content;
    public $Type;
    public $Level;

    /**
     * LogFileDTO constructor.
     * @param $file
     * @param $content
     * @param $type
     * @param $level
     */
    public function __construct($file, $content, $type, $level)
    {
        $this->File = $file;
        $this->Content = $content;
        $this->Type = $type;
        $this->Level = $level;
    }


}