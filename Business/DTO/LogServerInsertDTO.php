<?php

namespace Business\DTO;


class LogServerInsertDTO
{

    public $Session;
    public $Entry;
    public $Files = [];

}