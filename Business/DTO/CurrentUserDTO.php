<?php

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 13.5.2016.
 * Time: 17.04
 */
namespace Business\DTO;

class CurrentUserDTO {

    public $UserId;
    public $Username;
    public $Picture;
    public $Roles = [];
    public $Email;
    public $Permissions = [];
    public $FirstName;
    public $LastName;

}

