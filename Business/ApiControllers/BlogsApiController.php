<?php

namespace Business\ApiControllers;

use Data\DataManagers\BlogsDataManager;
use Data\Models\Blog;

class BlogsApiController {
	/**
	 * @return Blog[]
	 */
	public static function GetBlogs($excludedBlogs = []) {
		return BlogsDataManager::GetBlogs($excludedBlogs);
	}
	/**
	 * @return Blog[]
	 */
	public static function GetLatestBlogs($limit) {
		return BlogsDataManager::GetLatestBlogs($limit);
	}

	public static function DeleteBlog($id) {
		return BlogsDataManager::DeleteBlog($id);
	}

	/**
	 * @param $id
	 * @return Blog
	 */
	public static function GetBlog($id) {
		return BlogsDataManager::GetBlog($id);
	}

	public static function SaveBlog($model) {
		return BlogsDataManager::SaveBlog($model);
	}

	public static function GetBlogsByListOfIds($offers) {
		return BlogsDataManager::GetBlogsByListOfIds($offers);
	}

	public static function GetBlogBySlug($slug) {
		return BlogsDataManager::GetBlogBySlug($slug);
	}

	public static function AddRelation($relation) {
		return BlogsDataManager::AddRelation($relation);

	}

	public static function DeleteRelation($parentId, $childId) {
		return BlogsDataManager::DeleteRelation($parentId, $childId);
	}

}