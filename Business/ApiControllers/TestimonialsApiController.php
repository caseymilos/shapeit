<?php

namespace Business\ApiControllers;

use Data\DataManagers\TestimonialsDataManager;
use Data\Models\Testimonial;

class TestimonialsApiController
{

    public static function SaveTestimonial($model)
    {
        return TestimonialsDataManager::SaveTestimonial($model);
    }

    public static function DeleteTestimonial($testimonialId)
    {
        return TestimonialsDataManager::DeleteTestimonial($testimonialId);
    }


    /**
     * @return Testimonial[]
     */
    public static function GetTestimonials()
    {
        return TestimonialsDataManager::GetTestimonials();
    }

    /**
     * @return Testimonial[]
     */
    public static function GetActiveTestimonialsOrderedByWeight()
    {
        return TestimonialsDataManager::GetActiveTestimonialsOrderedByWeight();
    }

    /**
     * @param  $testimonialId
     * @return Testimonial
     */
    public static function GetTestimonial($testimonialId)
    {
        return TestimonialsDataManager::GetTestimonial($testimonialId);
    }

    /**
     * @param $weight
     * @return Testimonial
     */
    public static function GetTestimonialByWeight($weight)
    {
        return TestimonialsDataManager::GetTestimonialByWeight($weight);
    }

    public static function UpdateTestimonialWeight($weights) {
        return TestimonialsDataManager::UpdateWeight($weights);

    }


    //Testimonial description
    public static function SaveTestimonialDescription($model)
    {
        return TestimonialsDataManager::SaveTestimonialDescription($model);
    }


} 