<?php

namespace Business\ApiControllers;

use Data\DataManagers\AdsDataManager;
use Data\Models\Ad;

class AdsApiController {
	/**
	 * @return Ad[]
	 */
	public static function GetAds($position = null) {
		return AdsDataManager::GetAds($position);
	}

	public static function DeleteAd($id) {
		return AdsDataManager::DeleteAd($id);
	}

	/**
	 * @param $id
	 * @return Ad
	 */
	public static function GetAd($id) {
		return AdsDataManager::GetAd($id);
	}

	public static function SaveAd($model) {
		return AdsDataManager::SaveAd($model);
	}

}