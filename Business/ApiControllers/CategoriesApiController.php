<?php
/**
 * Created by PhpStorm.
 * User: Marko
 * Date: 04/10/2016
 * Time: 14:11
 */

namespace Business\ApiControllers;

use Data\DataManagers\CategoriesDataManager;
use Data\Models\Category;


class CategoriesApiController
{
    public static function SaveCategory($model)
    {
        return CategoriesDataManager::SaveCategory($model);
    }

    public static function DeleteCategory($categoryId)
    {
        return CategoriesDataManager::DeleteCategory($categoryId);
    }

    /**
     * @param  $categoryId
     * @return Category
     */
    public static function GetCategory($categoryId)
    {
        return CategoriesDataManager::GetCategory($categoryId);
    }


    //Category details
    public static function SaveCategoryDetails($model)
    {
        return CategoriesDataManager::SaveCategoryDetails($model);
    }

    /**
     * @return Category[]
     */
    public static function GetCategories()
    {
        return CategoriesDataManager::GetCategories();
    }



}