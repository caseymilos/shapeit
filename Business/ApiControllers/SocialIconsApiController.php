<?php

namespace Business\ApiControllers;

use Data\DataManagers\SocialIconsDataManager;
use Data\Models\SocialIcon;

class SocialIconsApiController
{

    public static function SaveSocialIcon($model)
    {
        return SocialIconsDataManager::SaveSocialIcon($model);
    }

    public static function DeleteSocialIcon($socialIconId)
    {
        return SocialIconsDataManager::DeleteSocialIcon($socialIconId);
    }


    /**
     * @return SocialIcon[]
     */
    public static function GetSocialIcons()
    {
        return SocialIconsDataManager::GetSocialIcons();
    }

    /**
     * @return SocialIcon[]
     */
    public static function GetActiveSocialIconsOrderedByWeight()
    {
        return SocialIconsDataManager::GetActiveSocialIconsOrderedByWeight();
    }

    /**
     * @param  $socialIconId
     * @return SocialIcon
     */
    public static function GetSocialIcon($socialIconId)
    {
        return SocialIconsDataManager::GetSocialIcon($socialIconId);
    }

    /**
     * @param $weight
     * @return SocialIcon
     */
    public static function GetSocialIconByWeight($weight)
    {
        return SocialIconsDataManager::GetSocialIconByWeight($weight);
    }

    public static function UpdateSocialIconWeight($weights) {
        return SocialIconsDataManager::UpdateWeight($weights);

    }

} 