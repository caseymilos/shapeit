<?php

namespace Business\ApiControllers;

use Data\DataManagers\PagesDataManager;
use Data\Models\Page;

/**
 * Class PagesApiController
 * @package Business\ApiControllers
 */
class PagesApiController {

    /**
     * @return Page[]
     */
    public static function GetPages() {
        return PagesDataManager::GetPages();
    }

    /**
     * @param string $slug
     * @param integer $language
     * @return Page
     */
    public static function GetPageBySlug($slug, $language = null) {
        return PagesDataManager::GetPageBySlug($slug, $language);
    }

    public static function CheckSlug($slug) {
        return PagesDataManager::CheckSlug($slug);
    }

    public static function DeletePage($id) {
        return PagesDataManager::DeletePage($id);
    }

    /**
     * @param $id
     * @return Page
     */
    public static function GetPage($id) {
        return PagesDataManager::GetPage($id);
    }

    public static function SavePage($page) {
        return PagesDataManager::SavePage($page);
    }

    public static function SaveDetails($details) {
        return PagesDataManager::SaveDetails($details);
    }

    public static function GetPagesByListOfIds($pages) {
        return PagesDataManager::GetPagesByListOfIds($pages);
    }

    public static function TranslateSlug($slug, $language) {
        return PagesDataManager::TranslateSlug($slug, $language);
    }
}