<?php

namespace Business\ApiControllers;

use Data\DataManagers\MediaDataManager;
use Data\Models\Media;
use Data\Models\MediaDescription;

/**
 * Class MediaApiController
 * @package Business\ApiControllers
 */
class MediaApiController {
    public static function GetAllMedia() {
        return MediaDataManager::GetAllMedia();
    }

    public static function Save(Media $media) {
        return MediaDataManager::SaveMedia($media);
    }

    public static function SaveDescription(MediaDescription $description) {
        return MediaDataManager::SaveDescription($description);
    }

    public static function DeleteMedia($id) {
        return MediaDataManager::DeleteMedia($id);
    }

    public static function GetGalleryMedia($ids) {
        $gallery = MediaDataManager::GetGalleryMedia($ids);
        $orderedGallery = [];
        // reorder images
        if (count($gallery) > 0 && count($ids) > 0) {
            foreach ($ids as $key => $id) {
                foreach ($gallery as $media) {
                    if ($id == $media->MediaId) {
                        $orderedGallery[] = $media;
                        break;
                    }
                }
            }
        }

        return $orderedGallery;
    }

    /**
     * @param $id
     * @return Media
     */
    public static function GetMedia($id) {
        return MediaDataManager::GetOne($id);
    }

    public static function BatchDeleteMedia($ids) {
        return MediaDataManager::BatchDeleteMedia($ids);
    }
}