<?php


namespace Business\ApiControllers;

use Data\DataManagers\CommentsDataManager;
use Data\Models\Comment;

class CommentsApiController {

	/**
	 * @return Comment[]
	 */
	public static function GetCommentsByPostId($postId) {
		return CommentsDataManager::GetCommentsByPostId($postId);
	}

	public static function SaveComment($model){
		return CommentsDataManager::SaveComment($model);
	}

}