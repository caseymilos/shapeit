<?php

namespace Business\ApiControllers;


use Data\DataManagers\PostsDataManager;
use Data\Models\Post;
use Data\Models\RelatedPosts;

class PostsApiController {
	/**
	 * @return Post[]
	 */
	public static function GetPosts() {
		return PostsDataManager::GetPosts();
	}

	public static function DeletePost($id) {
		return PostsDataManager::DeletePost($id);
	}

	/**
	 * @param $id
	 * @return Post
	 */
	public static function GetPost($id) {
		return PostsDataManager::GetPost($id);
	}

	public static function SavePost($model) {
		return PostsDataManager::SavePost($model);
	}

	public static function GetPostsByListOfIds($posts) {
		return PostsDataManager::GetPostsByListOfIds($posts);
	}

	public static function GetPostBySlug($slug) {
		return PostsDataManager::GetPostBySlug($slug);
	}

	public static function RemoveRelation($parentId, $childId) {
		return PostsDataManager::RemoveRelation($parentId, $childId);
	}

	public static function GetPostsByPostTypeId($postTypeId = null, $offset = null, $limit = null, $count = false) {
		return PostsDataManager::GetPostsByPostTypeId($postTypeId, $offset, $limit, $count);
	}


	/**
	 * @param RelatedPosts $relation
	 * @return boolean
	 */
	public static function AddRelation($relation) {
		return PostsDataManager::AddRelation($relation);
	}

}