<?php

namespace Business\ApiControllers;


use Data\DataManagers\BookingsDataManager;
use Data\Models\Booking;

class BookingsApiController {

	public static function GetBookings() {
		return BookingsDataManager::GetBookings();
	}

	public static function DeleteBooking($id) {
		return BookingsDataManager::DeleteBooking($id);
	}

	/**
	 * @param $id
	 * @return Booking
	 */
	public static function GetBooking($id) {
		return BookingsDataManager::GetBooking($id);
	}

	public static function SaveBooking($model) {
		return BookingsDataManager::SaveBooking($model);
	}

}