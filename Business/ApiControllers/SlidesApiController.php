<?php

namespace Business\ApiControllers;

use Data\DataManagers\SlidesDataManager;
use Data\Models\Slide;

class SlidesApiController
{

    public static function SaveSlide($model)
    {
        return SlidesDataManager::SaveSlide($model);
    }

    public static function DeleteSlide($slideId)
    {
        return SlidesDataManager::DeleteSlide($slideId);
    }


    /**
     * @return Slide[]
     */
    public static function GetSlides()
    {
        return SlidesDataManager::GetSlides();
    }

    /**
     * @return Slide[]
     */
    public static function GetActiveSlidesOrderedByWeight()
    {
        return SlidesDataManager::GetActiveSlidesOrderedByWeight();
    }

    /**
     * @param  $slideId
     * @return Slide
     */
    public static function GetSlide($slideId)
    {
        return SlidesDataManager::GetSlide($slideId);
    }

    /**
     * @param $weight
     * @return Slide
     */
    public static function GetSlideByWeight($weight)
    {
        return SlidesDataManager::GetSlideByWeight($weight);
    }

    public static function UpdateSlideWeight($weights) {
        return SlidesDataManager::UpdateWeight($weights);

    }


    //Slide description
    public static function SaveSlideDescription($model)
    {
        return SlidesDataManager::SaveSlideDescription($model);
    }


} 