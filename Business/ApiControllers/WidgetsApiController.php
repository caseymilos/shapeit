<?php

namespace Business\ApiControllers;

use Data\DataManagers\WidgetsDataManager;
use Data\Models\Widget;

class WidgetsApiController {

    public static function GetWidget($id) {
        return WidgetsDataManager::GetWidget($id);
    }

    public static function SaveWidget(Widget $widget) {
        return WidgetsDataManager::SaveWidget($widget);
    }

    public static function DeleteWidget($id) {
        return WidgetsDataManager::DeleteWidget($id);
    }

}