<?php

namespace Business\Helper;

use Business\Utilities\Config\Config;
use Cloudflare\Api;
use Cloudflare\Zone\Cache;

class CloudFlareHelper {

    private $Email;
    private $ApiKey;
    private $Zone;

    public function __construct() {
        $config = new Config();

        $this->Email = $config->getInstance()->cloudflare->email;
        $this->ApiKey = $config->getInstance()->cloudflare->apiKey;
        $this->Zone = $config->getInstance()->cloudflare->zone;
    }

    public function PurgeFiles($files) {
        $client = new Api($this->Email, $this->ApiKey);
        $cache = new Cache($client);
        $cache->purge_files($this->Zone, $files);
    }

}