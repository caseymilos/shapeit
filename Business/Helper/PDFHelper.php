<?php
namespace Business\Helper;

// set_time_limit(2000);

use mPDF;

class PDFHelper {
	const RocketAPIkey = '6a55b585-718c-46f4-8c28-97a0c5bf5134';

	public static function GeneratePdf($content, $baseUrl = "", $url = null) {

		return static::_mPdf($content);

		if (false or $pdf = static::_freePdf($content, $baseUrl)) {
			return $pdf;
		}
		else {
			if ($url != null) {
				return static::_rocketPdf($url);
			}
			else {
				return static::_rocketPdf($content);
			}
		}
	}

	private static function _freePdf($content, $baseUrl) {
		$url = 'http://freehtmltopdf.com';
		$data = array('convert' => '',
			'html' => $content,
			'baseurl' => $baseUrl);

		// use key 'http' even if you send the request to https://...
		$options = array(
			'http' => array(
				'header' => "Content-type: application/x-www-form-urlencoded\r\n",
				'method' => 'POST',
				'content' => http_build_query($data),
			),
		);
		$context = stream_context_create($options);
		$result = file_get_contents($url, false, $context);

		if (!$result) {
			return false;
		}
		else {
			return $result;
		}
	}

	private static function _rocketPdf($content) {
		$postdata = http_build_query(
			array(
				'apikey' => self::RocketAPIkey,
				'value' => $content,
				'DisableShrinking' => "true"
			)
		);
		$opts = array('http' =>
			array(
				'method' => 'POST',
				'header' => 'Content-type: application/x-www-form-urlencoded',
				'content' => $postdata
			)
		);
		$context = stream_context_create($opts);

		$result = file_get_contents('http://api.html2pdfrocket.com/pdf', false, $context);

		return $result;
	}

	private static function _mPdf($content) {

		include(__DIR__ . "/../Lib/mpdf60/mpdf.php");

		//A4 paper
		$mpdf = new mPDF('utf-8', 'A4', 0, '', 0, 0, 0, 0, 0, 0);
		$mpdf->SetDisplayMode('fullpage');

		$mpdf->WriteHTML($content);
		$content = $mpdf->Output('', 'S');
		return $content;

	}

}
