<?php
namespace Business\Helper;

use Logger;
use PayPal\Exception\PayPalConnectionException;

include(ROOT_PATH . '/Business/Lib/log4php/Logger.php');

class LogHelper {

	public static function Log(\Exception $e) {

		$logId = uniqid("log_");


		Logger::configure(array(
			'rootLogger' => array(
				'appenders' => array('file', 'mail'),
			),
			'appenders' => array(
				'file' => array(
					'class' => 'LoggerAppenderSMTPMailEvent',
					'layout' => array(
						'class' => 'LoggerLayoutSimple'
					),
					'params' => array(
						'to' => 'exceptions@gentlemandev.com',
						'from' => SYSTEM_SMTP_USERNAME,
						'smtpHost' => SYSTEM_SMTP_HOST . ':' . SYSTEM_SMTP_PORT,
						'username' => SYSTEM_SMTP_USERNAME,
						'password' => SYSTEM_SMTP_PASSWORD,
						'subject' => '[Innes Vienna][Exception] - ' . $logId
					)
				),
				'mail' => array(
					'class' => 'LoggerAppenderFile',
					'layout' => array(
						'class' => 'LoggerLayoutHtml'
					),
					'params' => array(
						'file' => CDN_PATH . "Logs/" . date("Ymd") . "/" . $logId . '.html',
						'append' => true
					)
				)
			)
		));

		$logger = Logger::getLogger("main");


		$logger->error($e);

		if ($e instanceof PayPalConnectionException) {
			$logger->info($e->getData());
		}

		$logger->info($_REQUEST);
		$logger->info($_SESSION);
		$logger->info($_SERVER);
	}

}