<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 05-Aug-15
 * Time: 12:34
 */

namespace Business\Helper;

use Business\DTO\RecipientDTO;
use Business\DTO\SenderDTO;
use PHPMailer\PHPMailer;
use Business\Utilities\Config\Config;
use StringAttachmentDTO;

class NotificationHelper {

    /**
     * @param string $subject Subject of email
     * @param string $body Body of email
     * @param string $text Description for admin area
     * @param array $attachments Attachments for mail
     * @param SenderDTO[] $senders List of senders
     * @param RecipientDTO[] $recipients List of recipients
     * @param RecipientDTO[] $cc List of CC recipients
     * @param RecipientDTO[] $bcc List of BCC recipients
     * @param StringAttachmentDTO[] $stringAttachments List of string attachments
     * @throws \PHPMailer\PHPMailerException
     * @throws \Exception
     */
    public static function Send($subject, $body, $text, $senders, $recipients, $attachments = [], $cc = [], $bcc = [], $stringAttachments = []) {
        // Saving to DB
        /**
         * $model = new MailViewModel();
         * $model->Sender = $senders;
         * $model->Text = $text;
         * $model->Subject = $subject;
         * $model->Recipients = $recipients;
         * $model->Attachments = $attachments;
         * $model->CC = $cc;
         * $model->BCC = $bcc;
         * $model->DateSent = date("Y-m-d H:i:s");
         * NotificationsApiController::InsertMessage($model);
         **/


        // Sending mail
        $mail = new PHPMailer();
        $mail->CharSet = "UTF-8";

        if (count($recipients) > 0) {
            foreach ($recipients as $recipient) {
                $mail->addAddress($recipient->Address, $recipient->Name);
            }
        }
        if (count($cc) > 0) {
            foreach ($cc as $recipient) {
                $mail->addCC($recipient->Address, $recipient->Name);
            }
        }
        if (count($bcc) > 0) {
            foreach ($bcc as $recipient) {
                $mail->addBCC($recipient->Address, $recipient->Name);
            }
        }

        $mail->Subject = $subject;

        if (count($senders) > 0) {
            foreach ($senders as $sender) {
                $mail->setFrom($sender->Address, $sender->Name);
            }
        }
        $mail->isHTML(true);

        $body = htmlspecialchars_decode(htmlentities($body, ENT_NOQUOTES, 'UTF-8'), ENT_NOQUOTES);

        $mail->Body = $body;

        if (count($stringAttachments) > 0) {
            foreach ($stringAttachments as $stringAttachment) {
                $mail->addStringAttachment($stringAttachment->Content, $stringAttachment->Name);
            }
        }


        // SMTP settings
        $config = Config::GetInstance();
        if ($config->smtp->use == true) {
            $mail->isSMTP();
            $mail->Host = $config->smtp->host;
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = "tls";
            $mail->Port = $config->smtp->port;
            $mail->AuthType = "PLAIN";
            $mail->Username = $config->smtp->username;
            $mail->Password = $config->smtp->password;
        }

        return $mail->send();
    }
}