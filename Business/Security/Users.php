<?php

namespace Business\Security;
use DateTime;
use Gdev\UserManagement\ApiControllers\RolePermissionsApiController;
use Gdev\UserManagement\ApiControllers\UserAccessTokensApiController;
use Gdev\UserManagement\ApiControllers\UserRolesApiController;
use Gdev\UserManagement\ApiControllers\UsersApiController;
use Gdev\UserManagement\DataManagers\RolePermissionsDataManager;
use Gdev\UserManagement\DataManagers\UserAccessTokensDataManager;
use Gdev\UserManagement\DataManagers\UserRolesDataManager;
use Gdev\UserManagement\DataManagers\UsersDataManager;
use Gdev\UserManagement\Models\User;
use Gdev\UserManagement\Models\UserAccessToken;
use Gdev\UserManagement\Repositories\UserAccessTokensRepository;
use Gdev\UserManagement\Repositories\UsersRepository;

/**
 * Class Users
 * @package Security
 *
 * Handles user management tasks
 */
class Users {


    /**
     * Login based on username and password. Returns complete user data.
     *
     * @param string $email
     * @param string $password
     *
     * @return User|null
     */
    public static function Login($username, $password) {
        $user = UsersApiController::GetUserByUserName($username);
        if (!empty($user) && $user->Active == 1 && $user->Approved == 1) {
            if (Crypt::CheckPassword($password, $user->Password)) {
                // Successful login

                // Populating permissions
                /*$userRoles = UserRolesApiController::GetUserRoles($user->UserId);

                $user->Permissions = RolePermissionsApiController::GetRolePermissions(current($userRoles)->RoleId);*/

                // Saving new UserAccessToken to database

                $token = new UserAccessToken();
                $token->Token = Tokens::CreateToken();
                $token->UserId = $user->UserId;
                $token->StartDate = new DateTime();
                $token->EndDate = null;

                UserAccessTokensApiController::CreateUserAccessToken($token);

                return $user;
            }
        }

        return null;
    }

    /**
     * Logs in user with access token. Returns complete user data.
     *
     * @param $token
     * @return User|null
     */
    public static function LoginWithToken($token) {
        $token = UserAccessTokensApiController::GetToken($token);
        if ($token instanceof UserAccessToken && $token->IsActive()) {
            $user = UsersApiController::GetUserById($token->UserId);
            $userRoles = UserRolesDataManager::GetUserRoles($user->UserId);

            $user->Permissions = RolePermissionsDataManager::GetRolePermissions(current($userRoles)->RoleId);
            return $user;
        }
        return null;

    }


    /**
     * Creates new User, assigns new created UserId and returns User object. Returns false if unsuccessful.
     *
     * @param User $user
     *
     * @return bool|User
     */
   /* public static function Register(UserModel $user) {
        $userId = UsersRepository::Insert([
            UsersRepository::COLUMN_USERNAME => $user->Username,
            UsersRepository::COLUMN_PASSWORD => Crypt::HashPassword($user->Password),
            UsersRepository::COLUMN_FIRST_NAME => $user->FirstName,
            UsersRepository::COLUMN_LAST_NAME => $user->LastName,
            UsersRepository::COLUMN_PICTURE => $user->Picture,
            UsersRepository::COLUMN_ROLE_ID => $user->RoleId,
            UsersRepository::COLUMN_USER_STATUS_ID => $user->UserStatusId,
            UsersRepository::COLUMN_EMAIL => $user->Email
        ]);

        if ($userId === false) {
            return false;
        }

        $user->UserId = $userId;
        return $user;
    }*/


}