<?php

namespace Business\Security;

class Token
{

    const METHOD = 'AES-256-CBC';

    public $Token;
    public $Data;
    public $Key;

    public function Generate($data, $key)
    {
        $this->Data = $data;
        $this->Key = $key;
        $this->Token = openssl_encrypt($data, self::METHOD, hex2bin($key), 0, "45gh354645gh3546");
    }

    public function Decode($token, $key)
    {
        $this->Token = $token;
        $this->Key = $key;
        $this->Data = openssl_decrypt($token, self::METHOD, hex2bin($key), 0, "45gh354645gh3546");
    }

}