<?php

namespace Business\Lib\Logs;

use Business\DTO\LogFileDTO;
use Data\Models\LogEntry;
use Data\Models\LogSession;
use DateTime;
use Logger;


/**
 * Class Log
 * @package Business\Lib\Logs
 *
 * @static LogSession Session
 */
class Log
{

    CONST LEVEL_TRACE = 1;
    CONST LEVEL_DEBUG = 2;
    CONST LEVEL_INFO = 3;
    CONST LEVEL_WARN = 4;
    CONST LEVEL_ERROR = 5;
    CONST LEVEL_FATAL = 6;

    CONST TYPE_TEXT = 1;
    CONST TYPE_JSON = 2;
    CONST TYPE_XML = 3;
    CONST TYPE_BINARY = 4;


    static $Logger = null;
    static $Session = null;
    static $Entry = null;
    static $Files = [];
    static $Path = null;

    public static function Init($sessionCode, $action, $server = null)
    {
        static::SetSession($sessionCode);
        $entry = new LogEntry();
        $entry->Action = $action;
        $entry->Server = $server ? $server : $_SERVER['SERVER_NAME'];
        $entry->StartDateTime = new DateTime();
        static::$Entry = $entry;
    }

    public static function SetSession($sessionCode)
    {
        $session = new LogSession();
        $session->Session = $sessionCode;
        static::$Session = $session;
    }

    public static function SetPath($path)
    {
        static::$Path = $path;
    }

    public static function Add($file, $content, $type = self::TYPE_BINARY, $level = self::LEVEL_INFO)
    {
        $fileDto = new LogFileDTO($file, $content, $type, $level);
        static::$Files[] = $fileDto;
    }

    public static function Save()
    {

        foreach (static::$Files as $file) {
            if (is_null(static::$Logger)) {
                static::Configure($file->File, $file->Type);
                static::$Logger = Logger::getLogger("rootLogger");
            }
            static::Configure($file->File, $file->Type);


            static::$Logger->info($file->Content);
        }
    }

    public static function InsertLogs() {

    }

    public static function Configure($file, $type)
    {

        $fileName = static::$Session->Session . "_" . $file . static::Extension($type);

        Logger::configure([
            'rootLogger' => [
                'appenders' => [
                    'default'
                ]
            ],
            'appenders' => [
                'default' => [
                    'class' => 'LoggerAppenderFile',
                    'layout' => array(
                        'class' => 'LoggerLayoutPattern',
                        'params' => [
                            'conversionPattern' => '%m'
                        ]
                    ),
                    'params' => array(
                        'file' => static::$Path . $fileName,
                        'append' => true
                    )
                ]
            ]
        ]);
    }

    private static function Extension($type)
    {
        switch ($type) {
            case self::TYPE_TEXT:
                return ".txt";
            case self::TYPE_JSON:
                return ".json";
            case self::TYPE_XML:
                return ".xml";
            case self::TYPE_BINARY:
                return "";
            default:
                return "";
        }
    }

}