<?php
/**
 * Created by PhpStorm.
 * User: nikola
 * Date: 19.5.16.
 * Time: 18.32
 */

namespace Business\Utilities\Config;

/**
 * Class Config
 * @package Business\Utilities\Config
 */
class Config {

    /** @var \Configula\Config */
    protected static $_config = null;

    public static function GetInstance() {
        if (static::$_config == null) {
            static::$_config = new \Configula\Config(ROOT_PATH . "/config");
        }
        return static::$_config;
    }
}