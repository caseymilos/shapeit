<?php

namespace Business\Utilities;

/**
 * Class XmlBuilder
 * @package Business\Utilities
 * @author Borislav Peev <borislav.asdf@gmail.com>
 */
class XmlBuilder {

    /**
     * Builds an XML string from a PHP structure.
     *
     * @param array
     * @return string
     */
    static function build($xmlObj) {
        $ret = '';
        self::_build($xmlObj, 0, $ret);
        return $ret;
    }

    private static function _isEmpty($value) {
        return $value === null ||
        (is_array($value) &&
            count($value) === 1 &&
            !empty($value['@']));
    }

    private static function _buildAttribs($attribs, &$ret) {
        foreach ($attribs as $attrName => $attrValue) {
            $ret .= ' ' . $attrName . '="' . htmlspecialchars((string)$attrValue) . '"';
        }
    }

    private static function _needsEols($value) {
        if (is_array($value)) {
            $l1 = count($value);
            if ($l1 > 2) {
                return true;
            }
            else if ($l1 === 2) {
                if (!empty($value['@'])) {
                    foreach ($value as $l2key => $l2value) {
                        if ($l2key !== '@') {
                            return self::_needsEols($l2value);
                        }
                    }
                }
                else {
                    return true;
                }
            }
            else if ($l1 === 1) {
                if (empty($value['@'])) {
                    foreach ($value as $l2key => $l2value) {
                        if ($l2key[0] == '#') {
                            return false;
                        }
                        return true;
                        // return !self::_isEmpty( $l2value );
                    }
                }
            }
        }
        return false;
    }

    private static function _buildPi($name, $value, &$ret) {
        $ret .= '<' . $name;
        if (is_array($value) && !empty($value['@'])) {
            self::_buildAttribs($value['@'], $ret);
        }
        if (self::_isEmpty($value)) {
            $ret .= ' ?>';
        }
        else {
            $ret .= ' ';
            self::_build($value, 0, $ret);
            $ret .= ' ?>';
        }
    }

    private static function _buildElement($name, $value, $indent, &$ret) {
        $ret .= '<' . $name;
        if (is_array($value) && !empty($value['@'])) {
            self::_buildAttribs($value['@'], $ret);
        }
        if (self::_isEmpty($value)) {
            $ret .= ' />';
        }
        else {
            $needEols = self::_needsEols($value);
            $ret .= '>' . ($needEols ? "\n" : '');
            self::_build($value, $needEols ? $indent + 1 : 0, $ret);
            if ($needEols) {
                $ret .= "\n";
                if ($indent > 0) {
                    $ret .= str_repeat("\t", $indent);
                }
            }
            $ret .= '</' . $name . '>';
        }
    }

    private static function _buildValue($value, $indent, &$ret, $escape) {
        if ($indent > 0) {
            $ret .= str_repeat("\t", $indent);
        }
        $ret .= $escape ? htmlspecialchars((string)$value) : (string)$value;

    }

    static function _build($xml, $indent = 0, &$ret, $escape = true) {

        if (!is_array($xml)) {
            self::_buildValue($xml, $indent, $ret, $escape);
            return;
        }

        $last = count($xml);
        if ($last === 1 && !empty($xml['@'])) {
            return;
        }
        foreach ($xml as $name => $value) {
            --$last;

            // skip attribs
            if ($name === '@') {
                continue;
            }

            // if we have array special handling
            if (is_int($name)) {
                self::_build($value, $indent, $ret);
                if ($last > 0) {
                    $ret .= "\n";
                }
                continue;
            }

            if ($indent > 0) {
                $ret .= str_repeat("\t", $indent);
            }

            if ($name[0] == '#') {
                if ($name === '#cdata') {
                    $ret .= '<![CDATA[';
                    self::_build($value, 0, $ret, false);
                    $ret .= ']]>';
                }
                else if ($name === '#comment') {
                    $ret .= '<!--';
                    self::_build($value, 0, $ret);
                    $ret .= '-->';
                }
            }
            else {
                if ($name[0] == '?') {
                    self::_buildPi($name, $value, $ret);
                }
                else {
                    self::_buildElement($name, $value, $indent, $ret);
                }
                if ($last > 0) {
                    $ret .= "\n";
                }
            }
        }

    }

}

?>