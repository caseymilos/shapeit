<?php
define("ROOT_PATH", dirname(__FILE__));
define("CDN_PATH", dirname(__FILE__) . "/Web/CDN/");
define('PROJECT_DIR', realpath('./'));
define('LOCALE_DIR', PROJECT_DIR . '/Locale');

define('DEBUG_MODE_ADMIN', true);
define('USE_CACHE_ADMIN', true);