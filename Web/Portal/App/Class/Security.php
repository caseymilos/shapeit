<?php

use Business\Enums\PermissionsEnum;

class Security {

    public static function CheckPermissions(array $permissions) {
        $currentUser = static::GetCurrentUser();
        if (empty($currentUser)) {
            return false;
        }
        //ToDo: Proveriti azurirane permisije pri svakom pozivu
        $userPermissions = static::GetCurrentUser()->Permissions;
        
        if(!is_array($userPermissions)) {
            return false;
        }
        
        
        if (count(array_intersect($userPermissions, $permissions))) {
            return true;
        }
        else {
            return false;
        }
    }
    
    /**
 * @return Business\DTO\CurrentUserDTO|bool
 */
    public static function GetCurrentUser() {
        return Session::Get("CurrentUser");
    }
    /**
     * @return int
     */
    public static function GetCurrentUserId() {
        return Session::Get("CurrentUser")->UserId;
    }

    /**
     * @param Business\DTO\CurrentUserDTO $user
     */
    public static function SetCurrentUser($user) {
        Session::Set("CurrentUser", $user);
    }

    public static function IsSuperAdmin() {
        return in_array(PermissionsEnum::SuperAdmin, Security::GetCurrentUser()->Permissions);

    }

} 