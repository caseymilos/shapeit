<?php

use Business\Enums\LanguageCodesEnum;
use Business\Utilities\Config\Config;
use Libraries\HTTPQuest\Requests;

/**
 * Class Router
 *
 * @property string $Controller
 * @property string $Action
 * @property string $Path
 * @property array $Parameters
 * @property string $RequestType
 * @property string $URL
 * @property RouteDTO[] $Routes
 * @property string $Language
 * @property string $Route
 */
class Router
{
    public $Controller;
    public $Action;
    public $Path;
    public $Parameters = array();
    public $RequestType = null;
    public $Language = "en";
    public $URL;
    public $Route;


    public static function GetInstance()
    {
        static $instance = null;
        if ($instance == null) {
            $instance = new Router();
        }
        return $instance;
    }

    public function __construct()
    {
        $config = Config::GetInstance();

        $this->Routes = RoutesConfig::GetRoutes();
        $this->Route = RoutesConfig::DefaultRoutePath();

        /** Getting request method */
        $this->RequestType = $_SERVER['REQUEST_METHOD'];

        /** Getting url path */
        if (isset($_GET['path'])) {
            $path = $_GET['path'];
        } else {
            $path = false;
        }

        $lang = strtok($path, '/');
        if (in_array($lang, ["en", "de"])) {
            $this->Language = $lang;
            $path = preg_replace(sprintf('/%s/', $lang), '', $path, 1);
        } else {
            $this->Language = $config->portal->language;
        }

        $path = trim($path, "/");
        $this->Path = $path;

    }

    public function Route()
    {

        $foundRoute = false;

        $path = $this->Path;
        if ($path == '') {
            $this->Controller = $this->Routes[RoutesConfig::DefaultRoute()]->Controller;
            $this->Action = $this->Routes[RoutesConfig::DefaultRoute()]->Action;
            $foundRoute = true;
        } else {
            /** Checking if there is page with specific slug */
            if (class_exists("\\Business\\ApiControllers\\PagesApiController")
                && (class_exists("\\Data\\Models\\PageDetails"))
            ) {
                $page = \Business\ApiControllers\PagesApiController::CheckSlug($path);
                if ($page instanceof \Data\Models\PageDetails) {
                    $foundRoute = true;
                    $this->Controller = "Pages";
                    $this->Action = "Page";
                    $this->Parameters = ["slug" => $path];
                    $this->Route = "page";
                }
            }

            if ($foundRoute == false) {
                /** Finding route */

                foreach ($this->Routes as $routeName => $route) {
                    if ($routeName == "page") {
                        continue;
                    }
                    $pattern = $route->Pattern;

                    /* Converting URL pattern to regex */
                    preg_match_all('/\{(.*?)\}/', $pattern, $match);

                    $params = array();
                    foreach ($match[1] as $key => $param) {
                        list($paramName, $paramType) = explode(":", $param);

                        /** Preparing array for storing parameters */
                        $params[$paramName] = "";

                        switch ($paramType) {
                            case "integer" :
                                $rule = "[0-9]+";
                                break;
                            //ToDO: prodiskutovati rule za string
                            case "string" :
                                $rule = "[a-zA-Z0-9-._]+";
                                break;
                            default:
                                $rule = "[a-zA-Z0-9]+";
                                break;
                        }

                        $regexPart = "(?P<" . $paramName . ">" . $rule . ")";
                        $pattern = str_replace($match[0][$key], $regexPart, $pattern);
                    }

                    $regex = "/^" . str_replace("/", "\\/", $pattern) . "$/";

                    if (preg_match_all($regex, $path, $urlParams)) {

                        $foundRoute = true;

                        $this->Route = $routeName;
                        foreach ($params as $key => $value) {
                            $params[$key] = $urlParams[$key][0];
                        }

                        $this->Controller = $route->Controller;
                        $this->Action = $route->Action;
                        $this->Parameters = $params;
                        if (count($route->Params)) {
                            $this->Parameters = array_merge($this->Parameters, $route->Params);
                        }
                        break;
                    }
                }
            }
        }

        if ($foundRoute == false) {
            throw new NotExistingException($this->Path . " route not found");
        }
        $this->URL = static::CurrentUrl();
        $this->Controller = (is_readable("Controller/" . $this->Controller . "Controller.php") ? $this->Controller : $this->Routes[RoutesConfig::DefaultRoute()]->Controller);

        $controllerName = $this->Controller . "Controller";
        $controller = new $controllerName();

        return $controller->InvokeAction();
    }

    /**
     * @param $name
     * @param array $params
     * @param bool $render
     * @param null $language
     * @return string
     * @throws Exception
     */
    public static function Create($name, $params = array(), $render = false, $language = null)
    {
        $config = Config::GetInstance();

        $router = Router::GetInstance();

        if ($name == "page" && $language != null && $language != $router->Language) {
            $page = \Business\ApiControllers\PagesApiController::TranslateSlug($params["slug"], LanguageCodesEnum::GetConstant($language));
            if ($page instanceof \Data\Models\PageDetails) {
                $params["slug"] = $page->Slug;
            } else {
                $name = "home";
            }
        }

        $routes = $router->Routes;

        if (isset($routes[$name])) {
            $route = $routes[$name];
            $pattern = $route->Pattern;

            if (count($params)) {

                /* Converting URL pattern to regex */
                preg_match_all('/\{(.*?)\}/', $pattern, $match);

                if (count($match[1])) {
                    foreach ($match[1] as $key => $param) {
                        list($paramName, $paramType) = explode(":", $param);
                        $pattern = str_replace($match[0][$key], $params[$paramName], $pattern);
                    }
                }
            }
        } else {
            return "";
            //ToDo: Throw new router exception
        }

        if ($language == null) {
            $language = $router->Language;
        }

        if ($language == $config->portal->language) {
            $language = '';
        }

        $url = $config->portal->url . $language . ($language != '' ? "/" : '') . $pattern;

        if (true == $render) {
            echo $url;
            return true;
        }
        return $url;
    }

    public static function Redirect($name, $params = array())
    {
        $path = static::Create($name, $params, false);
        header("Location: " . $path);
        die();
    }

    public static function CurrentUrl()
    {
        $router = Router::GetInstance();
        return static::Create($router->Route, $router->Parameters, false, $router->Language);
    }

    public static function GetLanguage()
    {
        if (isset($_GET['path'])) {
            $path = $_GET['path'];
        } else {
            $path = false;
        }

        $lang = strtok($path, '/');

        if (class_exists("\Business\Enums\LocalesEnum")) {
            $languageCode = \Business\Enums\LocalesEnum::GetConstant($lang);
            if (is_null($languageCode)) {
                $config = Config::GetInstance();
                return $config->portal->language;
            }

            return $languageCode;
        }
        return false;
    }


}