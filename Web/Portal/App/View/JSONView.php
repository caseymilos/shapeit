<?php

namespace View;

use NotificationDTO;
use NotificationTypesEnum;
use ResponseCodesEnum;
use ViewModel\MVCViewModel;

class JSONView extends MVCView
{
    private $Body = null;

    /**
     * JSONView constructor.
     * @param MVCViewModel $viewModel
     * @param int $responseCode
     * @internal param null $Body
     */

    public function __construct(MVCViewModel $viewModel,$responseCode = ResponseCodesEnum::OK)
    {
        $this->ViewModel = $viewModel;
        $this->ResponseCode = $responseCode;
    }
    //ToDo: Prodiskutovati ovo
    public function GenerateUnauthorizedResponse($message = null) {
        $message = $message ? $message : ResponseCodesEnum::Description(ResponseCodesEnum::Unauthorized);
        $this->ViewModel->Messages = [new NotificationDTO($message, NotificationTypesEnum::Error)];
        $this->ViewModel->Code = ResponseCodesEnum::Unauthorized;
        $this->ResponseCode = ResponseCodesEnum::Unauthorized;
    }
    public function GenerateInternalErrorResponse($message = null) {
        $message = $message ? $message : ResponseCodesEnum::Description(ResponseCodesEnum::InternalServerError);
        $this->ViewModel->Messages = [new NotificationDTO($message, NotificationTypesEnum::Error)];
        $this->ViewModel->Code = ResponseCodesEnum::InternalServerError;
        $this->ResponseCode = ResponseCodesEnum::InternalServerError;
    }

    public function Validate() {
        if (empty($this->Body)){
            throw new \Exception("Json format is invalid");
        }
       parent::Validate();

    }

    public final function Render($render = true) {
        $this->Body = json_encode($this->ViewModel,JSON_PRETTY_PRINT);
        $this->Validate();

        header('Content-Type: application/json; charset=utf-8');
        header($this->createHTTPResponse());

        echo $this->Body;


    }

    public function createHTTPResponse()
    {
        if (!empty($this->ViewModel->Code)) {
            $this->ResponseCode = $this->ViewModel->Code;
        }
        return sprintf("HTTP/1.0 %s %s", $this->ResponseCode, ResponseCodesEnum::Description($this->ResponseCode));
    }

}