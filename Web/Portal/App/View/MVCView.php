<?php
use ViewModel\MVCViewModel;

/**
 * Created by PhpStorm.
 * User: Nenad
 * Date: 25.7.2017.
 * Time: 14.37
 * @property  MVCViewModel $ViewModel
 * @property  integer[] $Permissions
 */
namespace View;

use Exception;
use Security;

abstract class MVCView
{
    public $ViewModel;
    public  $Permissions = [];
    public $ResponseCode = \ResponseCodesEnum::OK;

    public abstract function Render($render = true);

    protected  function Validate() {

        if (!empty($this->Permissions) && !Security::CheckPermissions($this->Permissions)) {
            return false;
        }
        /*
         * Todo: This check is temporarily removed. Check if it is needed.
        $calledClassArray = explode("\\",get_called_class());
        $calledClassArray[0] = "ViewModel";
        $calledClass = implode("\\",$calledClassArray);

        $viewModelClass = $calledClass . "Model";

        if (!($this->ViewModel instanceof $viewModelClass)) {
            throw new Exception("Viewmodel class is invalid");
        }
        */
        return true;
    }

    protected function ValidatePermissions() {
        return (Security::CheckPermissions($this->Permissions));
    }

}