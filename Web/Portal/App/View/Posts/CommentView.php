<?php

namespace View\Posts;

use View\TwigView;
// use ViewModel\Blogs\CommentViewModel;
/*
 * @property CommentViewModel $ViewModel
 */
class CommentView extends TwigView
{
    const TEMPLATE = "Posts/Comment";
}