<?php

namespace View\Posts;

use View\TwigView;
// use ViewModel\Blogs\BlogViewModel;
/*
 * @property BlogViewModel $ViewModel
 */
class PostView extends TwigView
{
    const TEMPLATE = "Posts/Post";
}