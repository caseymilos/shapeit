<?php

namespace View\Blogs;

use View\TwigView;
// use ViewModel\Blogs\CommentViewModel;
/*
 * @property CommentViewModel $ViewModel
 */
class CommentView extends TwigView
{
    const TEMPLATE = "Blogs/Comment";
}