<?php

namespace View\Blogs;

use View\TwigView;
// use ViewModel\Blogs\BlogViewModel;
/*
 * @property BlogViewModel $ViewModel
 */
class BlogView extends TwigView
{
    const TEMPLATE = "Blogs/Blog";
}