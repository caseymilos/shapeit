<?php

namespace View\Offers;

use View\TwigView;
use ViewModel\Widgets\SearchViewModel;
/*
 * @property OffersViewModel $ViewModel
 */
class SearchResultsView extends TwigView
{
    const TEMPLATE = "Offers/SearchResults";
}