<?php

namespace View\Offers;

use View\TwigView;
// use ViewModel\Offers\OfferViewModel;
/*
 * @property OfferViewModel $ViewModel
 */
class OfferView extends TwigView
{
    const TEMPLATE = "Offers/Offer";
}