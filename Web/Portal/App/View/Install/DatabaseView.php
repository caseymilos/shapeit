<?php

namespace View\Install;

use View\TwigView;

class DatabaseView extends TwigView
{
    const TEMPLATE = "Install/Database";
}