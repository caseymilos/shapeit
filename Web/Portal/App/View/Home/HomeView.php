<?php
/**
 * Created by PhpStorm.
 * User: Nenad
 * Date: 25.7.2017.
 * Time: 14.37
 */

namespace View\Home;

use View\TwigView;
use ViewModel\Home\HomeViewModel;
/*
 * @property HomeViewModel $ViewModel
 */
class HomeView extends TwigView
{
    const TEMPLATE = "Home/Index";
}