<?php

namespace View\Category;

use View\TwigView;
// use ViewModel\Blogs\BlogViewModel;
/*
 * @property BlogViewModel $ViewModel
 */
class CategoryView extends TwigView
{
    const TEMPLATE = "Category/Category";
}