<?php
namespace View;

use HtmlHelper;
use Router;
use SelectableHelper;
use Twig_Extension_Debug;
use Twig_SimpleFunction;
use TwigFiltersHelper;
use TwigHelper;
use ViewModel\MVCViewModel;

/**
 * Class TwigView
 * @package View
 */
class TwigView extends MVCView
{
    const TEMPLATE = null;

    /**
     * TwigView constructor.
     * @param MVCViewModel $viewModel
     * @param integer[] $permissions
     */
    public function __construct(MVCViewModel $viewModel, $permissions = [])
    {
        $this->ViewModel = $viewModel;
        $this->Permissions = $permissions;

    }

    function Validate()
    {
        if (!parent::Validate()) {
            return false;
        };
        if (empty(static::TEMPLATE)) {
            //ToDo: internal exception here
            throw new \Exception('Template file not specified in ' . get_called_class());
        }
        return true;
    }

    public final function Render($render = true)
    {
        $parameters = array("model" => $this->ViewModel);

        if (!$this->Validate()) {
            return false;
        }

        require_once 'Lib/Twig/Autoloader.php';
        \Twig_Autoloader::register();

        $loader = new \Twig_Loader_Filesystem('Template');
        $twig = new \Twig_Environment($loader, array(
            //ToDo:: read from config
            'cache' => null,
			'debug' => true
        ));

        $router = Router::GetInstance();

        $twig->addGlobal('Router', $router);
        $twig->addGlobal('Config', \Business\Utilities\Config\Config::GetInstance());
        //ToDo: videti kako moze lepse da se napravi
        $twig->addGlobal('Helper', new TwigHelper());
        $twig->addGlobal('HtmlHelper', new HtmlHelper());
        $twig->addGlobal('SelectableHelper', new SelectableHelper());

        $function = new Twig_SimpleFunction('__', function ($string) {
            return __($string);
        });
        $twig->addFunction($function);
		$twig->addExtension(new Twig_Extension_Debug());

        // custom filters
        $twig->addExtension(new TwigFiltersHelper());

        // ToDo: fallback na default folder
        $config = \Business\Utilities\Config\Config::GetInstance();
        $theme = $config->portal->theme;


        $template = $twig->loadTemplate($theme . "/" . static::TEMPLATE . '.twig');

        if (!$render) {
            return $template->render($parameters);
        }

        echo $template->render($parameters);
        return true;
    }

}