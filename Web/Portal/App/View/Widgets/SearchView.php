<?php

namespace View\Widgets;

use View\TwigView;
use ViewModel\Widgets\SearchViewModel;
/*
 * @property OffersViewModel $ViewModel
 */
class SearchView extends TwigView
{
    const TEMPLATE = "Widgets/Search";
}