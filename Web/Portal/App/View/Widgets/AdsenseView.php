<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 22.1.2018.
 * Time: 13.42
 */

namespace View\Widgets;
use View\TwigView;

class AdsenseView extends TwigView {
	const TEMPLATE = "Widgets/Adsense";
}