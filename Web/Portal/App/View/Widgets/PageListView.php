<?php

namespace View\Widgets;


use View\TwigView;

class PageListView extends TwigView {
	const TEMPLATE = "Widgets/PageList";

}