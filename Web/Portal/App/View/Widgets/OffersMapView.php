<?php

namespace View\Widgets;

use View\TwigView;
use ViewModel\Widgets\OffersViewModel;
/*
 * @property OffersViewModel $ViewModel
 */
class OffersMapView extends TwigView
{
    const TEMPLATE = "Widgets/OffersMap";
}