<?php

namespace View\Widgets;

use View\TwigView;
use ViewModel\Widgets\BlogsViewModel;
/*
 * @property BlogsViewModel $ViewModel
 */
class LatestBlogView extends TwigView
{
    const TEMPLATE = "Widgets/LatestBlog";
}