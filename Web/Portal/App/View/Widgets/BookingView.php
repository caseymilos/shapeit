<?php

namespace View\Widgets;

use View\TwigView;
// use ViewModel\Widgets\BookingViewModel;
/*
 * @property BookingViewModel $ViewModel
 */
class BookingView extends TwigView
{
    const TEMPLATE = "Widgets/Booking";
}