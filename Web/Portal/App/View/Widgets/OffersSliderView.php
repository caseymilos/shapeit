<?php
/**
 * Created by PhpStorm.
 * User: Nenad
 * Date: 25.7.2017.
 * Time: 14.37
 */

namespace View\Widgets;

use View\TwigView;
use ViewModel\Widgets\OffersViewModel;

/*
 * @property OffersViewModel $ViewModel
 */

class OffersSliderView extends TwigView {
	const TEMPLATE = "Widgets/OffersSlider";
}