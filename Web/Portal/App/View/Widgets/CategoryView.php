<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 16.1.2018.
 * Time: 18.13
 */

namespace View\Widgets;
use View\TwigView;

class CategoryView extends TwigView {
	const TEMPLATE = "Widgets/Category";
}