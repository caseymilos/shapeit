<?php

namespace View\Widgets;


use View\TwigView;

class GalleryView extends TwigView {
	const TEMPLATE = "Widgets/Gallery";

}