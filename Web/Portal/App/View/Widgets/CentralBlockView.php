<?php

namespace View\Widgets;


use View\TwigView;

class CentralBlockView extends TwigView {
	const TEMPLATE = "Widgets/CentralBlock";

}