<?php

namespace View\Pages;

use View\TwigView;
use ViewModel\Page\PageViewModel;
/*
 * @property HomeViewModel $ViewModel
 */
class PageView extends TwigView
{
    const TEMPLATE = "Pages/Templates/Default";
}