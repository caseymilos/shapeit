function partialLoaderRun(selector) {
    selector.each(function () {
        var $this = $(this);

        // show loading label
        var loader = $(this).data("loader");
        if (loader != "none") {
            $this.html('<div class="text-center"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>');
        }

        var url = $this.data('partialLoad');

        if (isNumber(url)) {
            url = "/widget/" + url;
        }

        var data = null;
        if ($(this).attr("data-parameters")) {
            data = $(this).data("parameters");
        }

        console.log(data);

        $.ajax({
            url: url,
            data: data,
            success: function (content) {
                $this.html(content);
                if ($this.data("callback")) {
                    window[$this.data("callback")]();
                }
            }
        });

    });
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

$(document).ready(function () {
    partialLoaderRun($("[data-partial-load]"));
});
