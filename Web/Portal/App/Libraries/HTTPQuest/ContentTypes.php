<?php

namespace Libraries\HTTPQuest;

class ContentTypes
{
    const PUTFORMDATA = 'PUTmultipart/form-data';
    const FORMDATA = 'multipart/form-data';
    const X_WWW_FORM_URLENCODED = 'application/x-www-form-urlencoded';
    const JSON = 'application/json';
}