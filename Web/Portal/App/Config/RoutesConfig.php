<?php

/**
 * Class RoutesConfig
 */
class RoutesConfig {

	/**
	 * @return RouteDTO[]
	 */
	public static function GetRoutes() {
		$routes = array(
			//Homepage
			"home" => new RouteDTO("home", "Home", "Index"),

			// Pages
			"page" => new RouteDTO("{slug:string}", "Pages", "Page"),
			"page-content" => new RouteDTO("page-content/{id:integer}", "Pages", "PageContent"),
			"contact" => new RouteDTO("contact", "Pages", "Contact"),
			"widget" => new RouteDTO("widget/{id:integer}", "Widgets", "Widget"),

			// Offers
			"offer" => new RouteDTO("travel/{slug:string}", "Offers", "Offer"),
			"offer-search" => new RouteDTO("travel/search/{country:string}/{city:string}", "Offers", "SearchResults"),
			"book-offer" => new RouteDTO("travel/book/{id:integer}", "Offers", "BookOffer"),

			// Blog
			"blog" => new RouteDTO("blog/{slug:string}", "Blogs", "Blog"),

			// Posts
			"post" => new RouteDTO("post/{slug:string}", "Posts", "Post"),


			// Search
			"search" => new RouteDTO("search", "Search", "Search"),

			// Comments
			"post-comment" => new RouteDTO("post-comment", "Blogs", "AddComment"),

			// CSS
			"css" => new RouteDTO("Theme/PBS/css/{name:string}.css", "Frontend", "Css"),
			"fonts-css" => new RouteDTO("Theme/PBS/fonts/{name:string}.css", "Frontend", "Css"),
			"javascript" => new RouteDTO("Theme/PBS/js/{name:string}.js", "Frontend", "Javascript"),
		);
		return $routes;
	}

	public static function DefaultRoute() {
		return "home";
	}

	public static function DefaultRoutePath() {
		return RoutesEnum::Description(RoutesEnum::Home);
	}

}