<?php

class RestApiAuthorizationFailedException extends MVCException {
    public function DisplayError() {
        HtmlHelper::RenderView("Errors/RestApiAuthorizationFailed");
    }
}