<?php

class NotExistingException extends MVCException {

    public function DisplayError() {
		header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
        HtmlHelper::RenderView("Errors/NotExisting");
    }
}