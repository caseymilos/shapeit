<?php

use Business\ApiControllers\CommentsApiController;
use Business\ApiControllers\PostsApiController;
use Data\Models\Comment;
use View\Posts\CommentView;
use View\Posts\PostView;
use ViewModel\Posts\CommentViewModel;
use ViewModel\Posts\PostViewModel;


class PostsController extends MVCController {

	public function GetPostsByPostTypeId($postTypeId) {
		$model = new \ViewModel\Posts\PostViewModel();

		$post = \Business\ApiControllers\PostsApiController::GetPostsByPostTypeId($postTypeId);
		if (empty($post)) {
			throw new NotExistingException();
		}

		$model->Post = $post;
		$view = new PostView($model);

		return $view;
	}

	public function GetPost($slug) {
		$model = new PostViewModel();

		$post = PostsApiController::GetPostBySlug($slug);
		if (empty($post)) {
			throw new NotExistingException();
		}

		$model->Post = $post;
		$view = new PostView($model);

		return $view;
	}

	public function GetCommentsByPostId($postId) {

		$model = new PostViewModel();

		$comments = CommentsApiController::GetCommentsByPostId($postId);

		$model->Comments = $comments;
		$view = new PostView($model);
		return $view;
	}

	public function PostAddComment($postId, $comment, $author, $authorEmail) {

		$commentItem = new Comment();

		$commentItem->PostId = $postId;
		$commentItem->Comment = $comment;
		$commentItem->Author = $author;
		$commentItem->AuthorEmail = $authorEmail;
		$commentItem->Date = new DateTime();

		CommentsApiController::SaveComment($commentItem);

		$model = new CommentViewModel();
		$model->Comment = $commentItem;
		return new CommentView($model);
	}

}