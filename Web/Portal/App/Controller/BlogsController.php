<?php

use Business\ApiControllers\BlogsApiController;
use Business\ApiControllers\CommentsApiController;
use Data\Models\Comment;
use View\Blogs\BlogView;
use View\Blogs\CommentView;
use ViewModel\Blogs\PostViewModel;
use ViewModel\Comments\CommentViewModel;

class BlogsController extends MVCController {

	public function GetBlog($slug) {
		$model = new PostViewModel();

		$blog = BlogsApiController::GetBlogBySlug($slug);

		$model->Blog = $blog;
		$view = new BlogView($model);

		return $view;
	}



}