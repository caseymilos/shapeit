<?php

use Business\ApiControllers\BlogsApiController;
use Business\ApiControllers\PostsApiController;
use Business\ApiControllers\PagesApiController;
use Business\ApiControllers\PressKitsApiController;
use Business\ApiControllers\WidgetsApiController;
use Doctrine\Common\Collections\ArrayCollection;
use Gdev\MenuManagement\ApiControllers\MenusApiController;
use View\Posts\PostView;
use View\Widgets\AdsenseView;
use View\Widgets\BookingView;
use View\Widgets\CategoryView;
use View\Widgets\ContentAdView;
use View\Widgets\GalleryView;
use View\Widgets\LatestBlogView;
use View\Widgets\OffersMapView;
use View\Widgets\OffersSliderView;
use View\Widgets\OffersView;
use View\Widgets\PageListView;
use View\Widgets\PostsView;
use ViewModel\MVCViewModel;
use ViewModel\Widgets\AdsenseViewModel;
use ViewModel\Widgets\BlogsViewModel;
use ViewModel\Widgets\GalleryViewModel;
use ViewModel\Widgets\PostsViewModel;
use ViewModel\Widgets\SearchViewModel;

class WidgetsController extends MVCController
{

    public function GetWidget($id)
    {
        $widget = WidgetsApiController::GetWidget($id);
        if ($widget instanceof \Data\Models\Widget) {
            if ($widget->Type > 100) {
                $enum = new \Business\Enums\TypographyEnum();
            } else {
                $enum = new \Business\Enums\WidgetsEnum();
            }
            return $this->{$enum->Caption($widget->Type)}($widget->Options);
        }
    }

    private function Offers($options)
    {
        $offers = PostsApiController::GetOffersByListOfIds($options->offers);
        $orderedOffers = [];
        if (count($options->offers)) {
            foreach ($options->offers as $offerId) {
                foreach ($offers as $offer) {
                    if ($offer->OfferId == $offerId) {
                        $orderedOffers[] = $offer;
                        continue;
                    }
                }
            }
        }

        $model = new \ViewModel\Widgets\OffersViewModel();
        $model->Offers = $orderedOffers;

        if ($options->mode == "grid") {
            $view = new OffersView($model);
        } else {
            $view = new OffersSliderView($model);
        }

        return $view;
    }

    private function OffersMap($options)
    {
        $offers = PostsApiController::GetOffersByListOfIds($options->offers);
        $orderedOffers = [];
        if (count($options->offers)) {
            foreach ($options->offers as $offerId) {
                foreach ($offers as $offer) {
                    if ($offer->OfferId == $offerId) {
                        $orderedOffers[] = $offer;
                        continue;
                    }
                }
            }
        }

        $model = new \ViewModel\Widgets\OffersViewModel();
        $model->Offers = $orderedOffers;
        $model->Options = $options;
        $view = new OffersMapView($model);
        return $view;
    }

    private function Posts($options)
    {
        $posts = PostsApiController::GetPostsByListOfIds($options->posts);
        $orderedPosts = [];
        if (count($options->posts)) {
            foreach ($options->posts as $postId) {
                foreach ($posts as $post) {
                    if ($post->PostId == $postId) {
                        $orderedPosts[] = $post;
                        continue;
                    }
                }
            }
        }

        $model = new \ViewModel\Widgets\PostsViewModel();
        $model->Posts = $orderedPosts;
        $model->Options = $options;
        $view = new PostsView($model);
        return $view;
    }

    private function Category($options)
    {

        if (array_key_exists("page", $_GET)) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        if ($page > 2) {
            $pageStart = $page - 2;
        } elseif ($page == 2) {
            $pageStart = $page - 1;
        } else {
            $pageStart = $page;
        }

        if (array_key_exists("currentUrl", $_GET)) {
            $currentUrl = $_GET['currentUrl'];
        } else {
            $currentUrl = "/home";
        }

        $limit = 10;
        $offset = ($page - 1) * $limit;

        if (empty($options) || !property_exists($options, "postTypeId") || empty($options->postTypeId)) {
            $postTypeId = null;
        } else {
            $postTypeId = $options->postTypeId;
        }
        $posts = PostsApiController::GetPostsByPostTypeId($postTypeId, $offset, $limit);
        $count = PostsApiController::GetPostsByPostTypeId($postTypeId, null, null, true);

        $maxPages = ceil($count / $limit);
        if ($maxPages == 0) {
            $maxPages = 1;
        }

        $end = $maxPages - 1;
        $start = 2;
        $ellipsisStart = false;
        $ellipsisEnd = false;
        if ($maxPages > 10) {
            if ($page > 6) {
                if ($page < $maxPages - 5) {
                    $ellipsisEnd = true;
                    $end = $page + 5;
                }
                $ellipsisStart = true;
                $start = $page - 5;
            }
        }

        $model = new \ViewModel\Widgets\PostsViewModel();
        $model->Posts = $posts;
        $model->Ads = \Business\ApiControllers\AdsApiController::GetAds(\Business\Enums\AdPositionsEnum::BetweenPosts);
        $model->Options = $options;
        $model->CurrentUrl = $currentUrl;
        $model->Pagination = (object)[
            "Start" => $start,
            "End" => $end,
            "StartEllipsis" => $ellipsisStart,
            "EndEllipsis" => $ellipsisEnd,
        ];
        $model->MaxPages = $maxPages;
        $model->Page = $pageStart;
        $view = new CategoryView($model);
        return $view;
    }

    private function LatestBlog($options)
    {
        $model = new BlogsViewModel();
        $model->Options = $options;
        if (empty($options) || !property_exists($options, "number") || empty($options->number)) {
            $model->Blogs = BlogsApiController::GetBlogs();
        } else {
            $model->Blogs = BlogsApiController::GetLatestBlogs($options->number);
        }
        $view = new LatestBlogView($model);
        return $view;
    }

    private function Adsense($options)
    {
        $model = new AdsenseViewModel();
        if (!empty($options) && property_exists($options, "adId")) {
            $model->Ad = \Business\ApiControllers\AdsApiController::GetAd($options->adId);
        } else {
            $model->Ad = null;
        }
        $model->Options = $options;
        $view = new AdsenseView($model);
        return $view;
    }

    private function ContentAd()
    {
        $view = new ContentAdView(new MVCViewModel());
        return $view;
    }

    private function Booking()
    {
        $view = new BookingView(new MVCViewModel());
        return $view;
    }

    private function ImportantBlock($options)
    {
        $image = \Business\ApiControllers\MediaApiController::GetMedia($options->picture);
        return $this->RenderView("Widgets/ImportantBlock", ["options" => $options, "image" => $image], true);
    }

    private function Gallery($options)
    {
        $model = new ViewModel\Widgets\MediaViewModel();
        $ids = [];
        if (property_exists($options, "media") && count($options->media) > 0) {
            foreach ($options->media as $media) {
                $ids[] = (int)$media->mediaId;
            }

            $model->Media = \Business\ApiControllers\MediaApiController::GetGalleryMedia($ids);
        }

        $model->Options = $options;

        $view = new GalleryView($model);
        return $view;
    }

    private function PressKitGallery($options)
    {
        $model = new MediaViewModel();
        $ids = [];
        if (property_exists($options, "media") && count($options->media) > 0) {
            foreach ($options->media as $media) {
                $ids[] = (int)$media->mediaId;
            }

            $model->Media = \Business\ApiControllers\MediaApiController::GetGalleryMedia($ids);
        }

        return $this->RenderView("Widgets/PressKitGallery", ["options" => $options, "model" => $model], true);
    }

    private function Testimonials($options)
    {
        $testimonials = \Business\ApiControllers\TestimonialsApiController::GetActiveTestimonialsOrderedByWeight();
        return $this->RenderView("Widgets/Testimonials", ["testimonials" => $testimonials], true);
    }

    private function Events($options)
    {
        $model = \Business\ApiControllers\EventsApiController::GetActiveEvents();
        return $this->RenderView("Widgets/Events", ["model" => $model], true);
    }

    private function History()
    {
        $model = \Business\ApiControllers\HistoryApiController::GetHistory()->execute();
        return $this->RenderView("Widgets/History", ["model" => $model], true);
    }

    private function Faq($options)
    {
        $model = new FaqViewModel();
        $model->Faq = \Business\ApiControllers\FaqsApiController::GetActiveFaqGroupsOrderByWeight();

        return $this->RenderView("Widgets/Faq", ["model" => $model], true);
    }

    private function Search($options)
    {
        $model = new SearchViewModel();
        $model->Options = $options;
        $view = new \View\Widgets\SearchView($model);
        return $view;
    }

    private function Document($options)
    {
        $document = \Data\Repositories\DocumentsRepository::getInstance()->get($options->document);
        return $this->RenderView("Widgets/Document", ["options" => $options, "document" => $document], true);
    }

    private function TitleNavigation($options)
    {
        $pages = PagesApiController::GetPages($options->pages);

        $iterator = $pages->entities();
        uasort($iterator, function ($a, $b) use ($options) {
            return (array_search($a->PageId, $options->pages) < array_search($b->PageId, $options->pages)) ? -1 : 1;
        });
        $pages = new ArrayCollection($iterator);

        $previousPage = $currentPage = $nextPage = null;
        if (count($pages) > 0) {
            foreach ($pages as $page) {
                if ($page->PageId == end($options->pages)) {
                    $previousPage = $page;
                } elseif ($page->PageId == $options->pages[1]) {
                    $nextPage = $page;
                } elseif ($page->PageId == reset($options->pages)) {
                    $currentPage = $page;
                }
            }
        }

        $pagesController = new PagesController(false);
        $currentContent = $pagesController->_build($currentPage->GetDescription(MainHelper::GetLanguage())->PageDetailsId);

        return $this->RenderView("Widgets/TitleNavigation", ["options" => $options, "previousPage" => $previousPage, "nextPage" => $nextPage, "currentPage" => $currentPage, "allPages" => $pages, "currentContent" => $currentContent], true);
    }

    private function PressKit($options)
    {
        $pressKits = PressKitsApiController::GetPressKits($options->items);

        $orderedItems = [];
        if (count($options->items)) {
            foreach ($options->items as $itemId) {
                foreach ($pressKits as $pressKit) {
                    if ($pressKit->PressKitId == $itemId) {
                        $orderedItems[] = $pressKit;
                        continue;
                    }
                }
            }
        }

        $model = new PressKitViewModel();
        $model->PressKit = $orderedItems;

        return $this->RenderView("Widgets/PressKit", ["model" => $model, "options" => $options], true);
    }

    private function PressKitArchive($options)
    {
        $model = new PressKitViewModel();
        $items = PressKitsApiController::GetPressKits();
        if (count($items) > 0) {
            foreach ($items as $item) {
                if ($item->Archive) {
                    if (!array_key_exists($item->Archive, $model->PressKit)) {
                        $model->PressKit[$item->Archive] = [];
                    }
                    $model->PressKit[$item->Archive][] = $item;
                }
            }
        }

        return $this->RenderView("Widgets/PressKitArchive", ["model" => $model, "options" => $options], true);
    }

    private function Accordion($options)
    {
        return $this->RenderView("Widgets/Accordion", ["options" => $options], true);
    }

    private function People($options)
    {
        $image = \Business\ApiControllers\MediaApiController::GetMedia($options->picture);
        return $this->RenderView("Widgets/People", ["options" => $options, "image" => $image], true);
    }

    private function FeaturedBox($options)
    {
        $image = \Business\ApiControllers\MediaApiController::GetMedia($options->picture);
        return $this->RenderView("Widgets/FeaturedBox", ["options" => $options, "image" => $image], true);
    }

    private function Trainings($options)
    {
        $image = \Business\ApiControllers\MediaApiController::GetMedia($options->picture);
        return $this->RenderView("Widgets/Trainings", ["options" => $options, "image" => $image], true);
    }

    private function People2($options)
    {
        $image = \Business\ApiControllers\MediaApiController::GetMedia($options->picture);
        return $this->RenderView("Widgets/People2", ["options" => $options, "image" => $image], true);
    }

    private function CentralBlock($options)
    {
        $image = \Business\ApiControllers\MediaApiController::GetMedia($options->picture);
        $model = new \ViewModel\Widgets\CentralBlockViewModel();
        $model->Media = $image;
        $model->Options = $options;
        $view = new \View\Widgets\CentralBlockView($model);
        return $view;
    }

    private function PlayerBox($options)
    {
        $image = \Business\ApiControllers\MediaApiController::GetMedia($options->picture);
        return $this->RenderView("Widgets/PlayerBox", ["options" => $options, "image" => $image], true);
    }

    private function NewMember($options)
    {
        $image = \Business\ApiControllers\MediaApiController::GetMedia($options->picture);
        return $this->RenderView("Widgets/NewMember", ["options" => $options, "image" => $image], true);
    }

    private function Quote($options)
    {
        $image = \Business\ApiControllers\MediaApiController::GetMedia($options->picture);
        return $this->RenderView("Widgets/Quote", ["options" => $options, "image" => $image], true);
    }

    private function Coaches($options)
    {
        $coaches = \Business\ApiControllers\CoachesApiController::GetCoaches();
        return $this->RenderView("Widgets/Coaches", ["options" => $options, "coaches" => $coaches], true);
    }

    private function Categories($options)
    {
        $categories = \Business\ApiControllers\CategoriesApiController::GetCategories();
        return $this->RenderView("Widgets/Categories", ["options" => $options, "categories" => $categories], true);
    }

    private function Players($options)
    {
        $image = \Business\ApiControllers\MediaApiController::GetMedia($options->picture);
        return $this->RenderView("Widgets/Players", ["options" => $options, "image" => $image], true);
    }

    private function Sponsors($options)
    {
        $sponsorGroup = \Business\ApiControllers\SponsorsApiController::GetSponsorGroup($options->sponsorGroup);
        $sponsors = \Business\ApiControllers\SponsorsApiController::GetSponsors($options->sponsorGroup);
        return $this->RenderView("Widgets/Sponsors", ["options" => $options, "sponsors" => $sponsors, "group" => $sponsorGroup], true);
    }

    private function Timeline($options)
    {
        $ids = [];
        $media = new MediaViewModel();
        if (property_exists($options, "media") && count($options->media) > 0) {
            foreach ($options->media as $media) {
                $ids[] = (int)$media->mediaId;
            }

            $media->Media = \Business\ApiControllers\MediaApiController::GetGalleryMedia($ids);
        }

        return $this->RenderView("Widgets/Timeline", ["options" => $options, "media" => $media], true);
    }

    private function BlogView($options)
    {
        $pages = PagesApiController::GetPagesByListOfIds($options->pages);
        $orderedPages = [];
        if (count($options->pages)) {
            foreach ($options->pages as $pageId) {
                foreach ($pages as $page) {
                    if ($page->PageId == $pageId) {
                        $orderedPages[] = $page;
                        continue;
                    }
                }
            }
        }

        // prepare column name
        switch ($options->items) {
            case "1":
                $class = "12 only-one-item";
                break;
            case "2":
                $class = "6 only-two-items";
                break;
            case "3":
                $class = "4";
                break;
            case "4":
                $class = "3";
                break;
            case "6":
                $class = "2";
                break;
            default:
                $options->items = 3;
                $class = "4";
        }

        return $this->RenderView("Widgets/BlogView", ["options" => $options, "pages" => $orderedPages, "class" => $class], true);
    }

    private function PageList($options)
    {
        $pages = PagesApiController::GetPagesByListOfIds($options->pages);
        $orderedPages = [];
        if (count($options->pages)) {
            foreach ($options->pages as $pageId) {
                foreach ($pages as $page) {
                    if ($page->PageId == $pageId) {
                        $orderedPages[] = $page;
                        continue;
                    }
                }
            }
        }

        // prepare column name
        switch ($options->items) {
            case "5":
                $class = "portfolio-5";
                break;
            case "6":
                $class = "portfolio-6";
                break;
            default:
                $class = "";
        }
        $model = new \ViewModel\Widgets\PageListViewModel();
        $model->Pages = $orderedPages;
        $model->Options = $options;
        $model->Class = $class;
        $view = new PageListView($model);
        return $view;

    }

    private function Jobs($options)
    {
        return $this->RenderView("Widgets/Jobs", ["options" => $options], true);
    }

    private function Programs($options)
    {
        $model = \Business\ApiControllers\ProgramsApiController::GetPrograms();
        return $this->RenderView("Widgets/Programs", ["model" => $model], true);
    }

    public function SidebarMenu($options)
    {
        if (!((is_object($options) && property_exists($options, "menu")) || array_key_exists("menu", $_REQUEST))) {
            return "";
        }

        if (isset($_REQUEST["menu"])) {
            $menuId = $_REQUEST["menu"];
        } else {
            $menuId = $options->menu;
        }
        $model = MenusApiController::GetTree($menuId);
        $callerPage = null;

        if (isset($_REQUEST['pageUrl'])) {
            $callerPage = $_REQUEST['pageUrl'];
        }
        return $this->RenderView("Widgets/SidebarMenu", ["model" => $model, 'options' => $options, 'url' => $callerPage], true);
    }

    public function SidebarMenuMobileController($options)
    {
        return $this->RenderView("Builder/Widget/SidebarMenuMobileController", ["options" => $options], true);
    }

}