<?php

use View\Home\BlogView;
use View\Home\HomeView;

class HomeController extends MVCController
{

    public function GetIndex()
    {
    	Router::Redirect("page", ["slug" => "home"]);
        $viewModel = new \ViewModel\Home\HomeViewModel();
        return new HomeView($viewModel);

    }



    public function PostContact() {

	}

}

