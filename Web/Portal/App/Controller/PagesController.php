<?php

use Business\ApiControllers\PagesApiController;
use Business\DTO\RecipientDTO;
use Business\DTO\SenderDTO;
use Business\Enums\BuilderLayoutsEnum;
use Business\Enums\WidgetsEnum;
use Business\Helper\NotificationHelper;
use Data\Cache\MemcachedService;
use Data\Repositories\LayoutsRepository;
use ViewModel\Pages\PageViewModel;

class PagesController extends MVCController {

	public function GetPage($slug) {

		$model = new PageViewModel();

		$page = PagesApiController::GetPageBySlug($slug, MainHelper::GetLanguage());
		if (false == $page) {
			throw new NotExistingException();
		}

		if ($page->Active == false) {
			throw new NotExistingException();
		}

		$model->Page = $page;
		$model->Details = $model->Page->GetDescription(MainHelper::GetLanguage());
		$model->Details->Description = $this->_build($model->Details->PageDetailsId);

		$view = new \View\Pages\PageView($model);

		return $view;
	}

	public function GetPageContent($id) {
		$model = new PageViewModel();

		$page = PagesApiController::GetPage($id);
		if (false == $page) {
			throw new NotExistingException();
		}

		$model->Page = $page;
		$model->Details = $model->Page->GetDescription(MainHelper::GetLanguage());
		$model->Details->Description = $this->_build($model->Details->PageDetailsId);

		echo $model->Details->Description;
	}

	public function PostContact($name, $email, $message) {
		$error = null;

		if ($email != null) {

			$config = \Business\Utilities\Config\Config::GetInstance();

			$success = NotificationHelper::Send(
				"New message from BSA website contact form",
				$this->RenderView("Mail/ContactUsMail", ["model" => $message], true),
				"",
				[new SenderDTO($name, $email)],
				[new RecipientDTO($config->contact->recipient->name, $config->contact->recipient->email)]
			);
		} else {
			$success = false;
			$error = "You did not enter email!";
		}

		$model = (object)[
			"success" => $success,
			"error" => $error
		];

		header('Content-type: application/json');
		echo json_encode($model);
	}

	public function GetFlashNews() {
		$discardedIds = Cookie::Get("discardedFlashNews");
		$discardedIds = explode(",", $discardedIds);

		$model = new FlashNewsViewModel();
		$model->FlashNews = \Business\ApiControllers\FlashArticlesApiController::GetActiveFlashArticles($discardedIds);

		$this->RenderView(null, ["model" => $model]);
	}

	public function GetDiscardFlashNews() {
		$discardedIds = Cookie::Get("discardedFlashNews");
		$ids = explode(",", $discardedIds);
		$activeNews = \Business\ApiControllers\FlashArticlesApiController::GetActiveFlashArticles($ids);
		if (count($activeNews) > 0) {
			foreach ($activeNews as $article) {
				$ids[] = $article->FlashArticleId;
			}
		}
		$ids = array_unique($ids);
		Cookie::Set("discardedFlashNews", implode(",", $ids));
	}

	public function GetSeatmap($code) {
		$model = new \Data\Models\HallConfiguration($code);

		$this->RenderView(null, ['model' => $model]);
	}


	public function _build($pageDetailsId) {
		$layoutsData = MemcachedService::getInstance()->get("PageLayouts" . $pageDetailsId);
		if (is_array($layoutsData) && count($layoutsData) > 0) {
			$layouts = new \Spot\Entity\Collection();
			foreach ($layoutsData as $layoutData) {
				$layout = new \Data\Models\Layout($layoutData);
				$widgetsCollection = new \Spot\Entity\Collection();
				if (count($layoutData["Widgets"]) > 0) {
					foreach ($layoutData["Widgets"] as $widgetData) {
						$widgetsCollection->add(new \Data\Models\Widget($widgetData));
					}
				}

				$layout->Widgets = $widgetsCollection;
				$layouts->add($layout);
			}
		} else {
			$layouts = LayoutsRepository::getInstance()->where(["PageDetailsId" => $pageDetailsId])->order(["Weight" => "ASC"])->with(["Widgets"]);
			MemcachedService::getInstance()->set("PageLayouts" . $pageDetailsId, $layouts->toArray());
		}

		$content = "";

		$layoutsEnum = new BuilderLayoutsEnum();
		$widgetsEnum = new WidgetsEnum();
		$typographyEnum = new \Business\Enums\TypographyEnum();

		if (count($layouts) > 0) {
			foreach ($layouts as $layout) {
				/** @var \Data\Models\Layout $layout */
				$layoutContent = [];
				if (count($layout->Widgets) > 0) {
					foreach ($layout->Widgets as $widget) {
						/** @var \Data\Models\Widget $widget */
						if ($widget->Type <= 100) {
							$caption = $widgetsEnum->Caption($widget->Type);
						} else {
							$caption = $typographyEnum->Caption($widget->Type);
						}
						if (!array_key_exists($widget->Position, $layoutContent)) {
							$layoutContent[$widget->Position] = "";
						}
						$layoutContent[$widget->Position] .= HtmlHelper::RenderView("Builder/Widget/" . $caption, ['model' => $widget], true);

					}
				}

				$layoutContent["options"] = $layout->Options;
				$content .= HtmlHelper::RenderView("Builder/Layout/" . $layoutsEnum->Caption($layout->LayoutTypeId), $layoutContent, true);
			}
		}

		return $content;
	}


}