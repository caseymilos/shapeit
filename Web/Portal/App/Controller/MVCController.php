<?php

use Libraries\HTTPQuest\HTTPQuest;
use Libraries\HTTPQuest\Requests;
use View\MVCView;

abstract class MVCController
{
    const SINGLE_LANGUAGE = "single-language";

    /* Properties */

    /* Constructor */
    public function InvokeAction($callFunc = true)
    {
        if ($callFunc) {
            $router = Router::GetInstance();
            if (!$router->Action) {
                $router->Action = "Index";
            }

            $functionParameters = array();
            $parameters = [];
            $files = [];
            $httpQuest = new HTTPQuest();

            switch ($router->RequestType) {
                case Requests::GET:
                    $parameters = $_GET;
                    $files = [];
                    break;
                case Requests::DELETE:
                    parse_str(file_get_contents("php://input"), $parameters);
                    array_merge($parameters, $_GET);
                    $files = [];
                    break;
                case Requests::POST:
                    if (!$httpQuest->HasContentType()) {
                        $parameters = $this->DecodeParameters();
                        $files = $this->DecodeFiles();
                    } else {
                        $httpQuest->decode($parameters, $files);
                    }
                    break;
                case Requests::PUT:
                    $httpQuest->decode($parameters, $files);
                    break;
                case Requests::PATCH:
                    if (!$httpQuest->HasContentType()) {
                        $parameters = $this->DecodeParameters();
                        $files = $this->DecodeFiles();
                    } else {
                        $httpQuest->decode($parameters, $files);
                    }
                    break;
                default:
                    $parameters = $_REQUEST;
                    $files = [];
            }
            $parameterCollection = array_merge($parameters, $files, $router->Parameters);

            $reflector = new ReflectionClass($router->Controller . "Controller");
            //ucfirst($router->RequestType) set request Type in format so that only first letter is capitalized
            $reflectionMethod = $reflector->getMethod(ucfirst(strtolower($router->RequestType)) . $router->Action);
            $parameters = $reflectionMethod->getParameters();

            foreach ($parameters as $parameter) {
                $paramName = $parameter->name;
                if ($paramName === "permissions") {
                    $permissions = $parameter->getDefaultValue();
                    continue;
                }

                if (!isset($parameterCollection[$paramName])) {
                    if ($parameter->isDefaultValueAvailable()) {
                        $functionParameters[$paramName] = $parameter->getDefaultValue();
                    } else {
                        throw new Exception("Missing required parameter: " . $paramName);
                    }
                } else {
                    if ($paramClass = $parameter->getClass()) {
                        $param = new $paramClass->name();
                        if (is_array($parameterCollection[$paramName])) {
                            foreach ($parameterCollection[$paramName] as $paramPropertyName => $paramPropertyValue) {
                                $param->$paramPropertyName = $paramPropertyValue;
                            }
                        }
                        $functionParameters[$paramName] = $param;
                    } else {
                        $functionParameters[$paramName] = $parameterCollection[$paramName];
                    }
                }
            }

            if (isset($permissions)) {
                if (!Security::CheckPermissions($permissions)) {
                    Router::Redirect(RoutesEnum::Login);

                    return;
                }
            }
            $actionResult = $reflectionMethod->invokeArgs($this, $functionParameters);
            if (!($actionResult instanceof MVCView)) {
                //ToDo: Internal Exception
                throw new Exception('Controller response is not instance of MVCView');
            }
            return $actionResult;
        }
    }

    private function DecodeParameters()
    {
        $parameters = [];
        $array = count($_POST) > 0 ? $_POST : $_REQUEST;
        foreach ($array as $name => $parameter) {
            $exploded = explode("-", $name, 2);
            if (count($exploded) > 1) {
                $language = $exploded[0];
                $paramName = $exploded[1];
                if (!array_key_exists($paramName, $parameters)) {
                    $parameters[$paramName] = [];
                }
                $parameters[$paramName][$language] = $parameter;
            } else {
                $parameters[$name] = $parameter;
            }
        }

        return $parameters;
    }

    private function DecodeFiles()
    {
        $files = [];
        if (count($_FILES) > 0) {
            foreach ($_FILES as $name => $file) {
                $exploded = explode("-", $name, 2);
                if (count($exploded) > 1) {
                    $language = $exploded[0];
                    $paramName = $exploded[1];
                    if (!array_key_exists($paramName, $files)) {
                        $parameters[$paramName] = [];
                    }
                    $files[$paramName][$language] = $file;
                } else {
                    $files[$name] = $file;

                }
            }
        }
        return $files;
    }


    public function RenderView($template = null, $parameters = [], $return = false, $theme = null)
    {
        if (is_null($template)) {
            $template = self::_getCaller();
        }

        return HtmlHelper::RenderView($template, $parameters, $return, $theme);
    }

    private function _getCaller()
    {
        $trace = debug_backtrace();
        $controller = preg_replace('/Controller$/', '', $trace[2]['class']);
        $method = preg_replace('/^Get|^Post/', '', $trace[2]['function']);
        return ($controller . "/" . $method);
    }
}