<?php

use Business\ApiControllers\BookingsApiController;
use Business\ApiControllers\CommentsApiController;
use Business\ApiControllers\PostsApiController;
use Data\Models\Booking;
use Data\Models\Comment;
use View\Posts\CommentView;
use View\Offers\BookOfferView;
use View\Offers\OfferView;
use View\Offers\SearchResultsView;
use View\Posts\PostView;
use ViewModel\Blogs\CommentViewModel;
use ViewModel\Offers\BookOfferViewModel;
use ViewModel\Offers\SearchResultsViewModel;


class OffersController extends MVCController {

	public function GetOffer($slug) {
		$model = new \ViewModel\Offers\OfferViewModel();

		$offer = PostsApiController::GetOfferBySlug($slug);
		if (empty($offer)) {
			throw new NotExistingException();
		}

		$model->Offer = $offer;
		$view = new OfferView($model);

		return $view;
	}

	public function GetSearchResults($country, $city) {

		$result = PostsApiController::GetOffers($country, $city);

		$model = new SearchResultsViewModel();
		$model->Offers = $result;
		$model->Country = $country;
		$model->City = $city;

		$view = new SearchResultsView($model);
		return $view;

	}

	public function PostBookOffer($id, $firstName, $lastName, $email, $message, $dateFrom, $dateTo, $phoneNumber, $adults) {

		$booking = new Booking();

		$booking->OfferId = $id;
		$booking->FirstName = $firstName;
		$booking->LastName = $lastName;
		$booking->Email = $email;
		$booking->PhoneNumber = $phoneNumber;
		$booking->Adults = $adults;
		$booking->DateFrom = DateTime::createFromFormat("Y-m-d", $dateFrom);
		$booking->DateTo = DateTime::createFromFormat("Y-m-d", $dateTo);
		$booking->Message = $message;

		BookingsApiController::SaveBooking($booking);



		$model = new BookOfferViewModel();
		$model->Booking = $booking;
		$model->Status = true;

		$view = new BookOfferView($model);

		return $view;
	}




}

