<?php

use Business\Enums\PermissionsEnum;
use View\MVCView;

class InstallController extends MVCController
{

    public function GetExportRoutes($permissions = [PermissionsEnum::AccessSystem])
    {
        $routes = RoutesConfig::GetRoutes();
        $fp = fopen('Config/routes.json', 'w');
        $c = fwrite($fp, json_encode($routes,JSON_PRETTY_PRINT));
        fclose($fp);

        $routesEnum = RoutesEnum::enum();
        $fp1 = fopen('Theme/BizBot/js/classes/routesEnum.js', 'w');
        $routesEnumJson = json_encode($routesEnum,JSON_PRETTY_PRINT);
        $routesEnumJson = str_replace('"',"",$routesEnumJson);
        $routesEnumJson = sprintf("routesEnum = %s;", $routesEnumJson);
        $c = fwrite($fp1, $routesEnumJson);
        fclose($fp1);


        //return new MVCView();
    }

}

