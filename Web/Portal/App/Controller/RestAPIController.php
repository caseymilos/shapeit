<?php

use Business\ApiControllers\ApplicationsApiController;
use Business\Security\Token;
use Data\Models\Application;

class RestAPIController extends MVCController
{

    private $Application;

    public function __construct()
    {
        $config = \Business\Utilities\Config\Config::GetInstance();

        // Todo: remove this, it is just for testing
        $token = new Token();
        $dateTime = new DateTime("-5 minutes");
        $requestObject = (object)[
            "Key" => "123456789",
            "DateTime" => $dateTime->format("Y-m-d H:i:s")
        ];
        $token->Generate(json_encode($requestObject), $config->portal->key);

        // Read authorization token
        $headers = apache_request_headers();
        $accessToken = null;

        if (array_key_exists('Authorization', $headers)) {
            $accessToken = str_replace("Bearer: ", "", $headers['Authorization']);
        }

        if (is_null($accessToken)) {
            // Todo: different exception for missing authorization key (?)
            throw new RestApiAuthorizationFailedException();
        }

        // Decode token, get timestamp and user secret key
        $token = new Token();
        $token->Decode($accessToken, $config->portal->key);

        // Get application by key
        $data = json_decode($token->Data);
        $app = ApplicationsApiController::GetApplicationByKey($data->Key);
        $this->Application = $app;
        if (!($app instanceof Application)) {
            throw new RestApiAuthorizationFailedException();
        }

        // Check if token is not older than allowed
        $accessTokenDateTime = new DateTime($data->DateTime);
        $now = new DateTime();
        $tokenLimit = new DateTime("-30 minutes");
        if ($tokenLimit > $accessTokenDateTime || $accessTokenDateTime > $now) {
            // Todo: different exception for token expired
            throw new RestApiAuthorizationFailedException();
        }

        // Todo: check application status
    }

    protected function CheckAccessToBot($botId)
    {
        // Check if application has access to Bot
        $accessBot = false;
        foreach ($this->Application->Business->Bots as $bot) {
            if ($bot->BotId == $botId) {
                $accessBot = true;
            }
        }
        if (false === $accessBot) {
            // Todo: different exception for wrong bot
            throw new RestApiAuthorizationFailedException();
        }
    }

}