<?php

namespace ViewModel\Blogs;

use Data\Models\Blog;
use Data\Models\Comment;
use ViewModel\MVCViewModel;

/**
 * Class BlogViewModel
 * @package ViewModel\Blogs
 *
 * @property Blog Blog
 * @property Comment[] Offers
 */
class BlogViewModel extends MVCViewModel {

	public $Blog;
	public $Comments = [];

}