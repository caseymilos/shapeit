<?php
namespace ViewModel\Pages;

use Data\Models\Page;
use Data\Models\PageDetails;
use ViewModel\MVCViewModel;

/**
 * Class PageViewModel
 *
 * @property Page Page
 * @property  PageDetails Details
 */
class PageViewModel extends MVCViewModel {

    public $Page;
    public $Details;

}