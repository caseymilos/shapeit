<?php

namespace ViewModel\Widgets;

use Data\Models\Page;
use ViewModel\MVCViewModel;

/**
 * Class PageListViewModel
 * @package ViewModel\Widgets
 *
 * @property Page[] Pages
 * @property [] Options
 * @property string Class
 */
class PageListViewModel extends MVCViewModel {

	public $Pages = [];
	public $Options;
	public $Class;

}