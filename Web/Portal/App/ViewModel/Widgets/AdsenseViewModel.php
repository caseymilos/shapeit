<?php

namespace ViewModel\Widgets;

use Data\Models\Ad;
use ViewModel\MVCViewModel;

/**
 * Class AdsenseViewModel
 * @package ViewModel\Widgets
 *
 * @property Ad[] Ad
 */
class AdsenseViewModel extends MVCViewModel
{

    public $Ad;
    public $Options;

}