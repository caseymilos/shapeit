<?php

namespace ViewModel\Widgets;

use Data\Models\Media;
use ViewModel\MVCViewModel;

/**
 * Class MediaViewModel
 * @package ViewModel\Widgets
 *
 * @property Media[] Media
 * @property [] Options
 */
class MediaViewModel extends MVCViewModel {

	public $Media = [];
	public $Options ;


}