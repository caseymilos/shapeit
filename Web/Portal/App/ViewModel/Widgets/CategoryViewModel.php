<?php

namespace ViewModel\Widgets;

use Data\Models\Post;
use ViewModel\MVCViewModel;

/**
 * Class CategoryViewModel
 * @package ViewModel\Widgets
 *
 * @property Post[] Category
 */
class CategoryViewModel extends MVCViewModel {

	public $Category = [];
	public $Options;

}