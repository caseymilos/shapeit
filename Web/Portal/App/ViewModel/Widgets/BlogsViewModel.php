<?php

namespace ViewModel\Widgets;

use Data\Models\Post;
use ViewModel\MVCViewModel;

/**
 * Class BlogsViewModel
 * @package ViewModel\Widgets
 *
 * @property Post[] Blogs
 */
class BlogsViewModel extends MVCViewModel {

	public $Blogs = [];
	public $Options;

}