<?php

namespace ViewModel\Widgets;

use Data\Models\Post;
use ViewModel\MVCViewModel;

/**
 * Class SearchViewModel
 * @package ViewModel\Widgets
 *
 * @property string[] Countries
 * @property string[] Cities
 */
class SearchViewModel extends MVCViewModel {

	public $Countries = [];
	public $Cities = [];
	public $Options;

}