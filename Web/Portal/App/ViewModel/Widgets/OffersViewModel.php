<?php

namespace ViewModel\Widgets;

use Data\Models\Post;
use ViewModel\MVCViewModel;

/**
 * Class OffersViewModel
 * @package ViewModel\Widgets
 *
 * @property Post[] Offers
 */
class OffersViewModel extends MVCViewModel {

	public $Offers = [];
	public $Options;

}