<?php

namespace ViewModel\Widgets;

use Data\Models\Media;
use ViewModel\MVCViewModel;

/**
 * Class CentralBlockViewModel
 * @package ViewModel\Widgets
 *
 * @property Media Media
 * @property [] Options
 */
class CentralBlockViewModel extends MVCViewModel {

	public $Media;
	public $Options ;


}