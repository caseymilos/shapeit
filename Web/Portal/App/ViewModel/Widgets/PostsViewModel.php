<?php

namespace ViewModel\Widgets;

use Data\Models\Post;
use ViewModel\MVCViewModel;

/**
 * Class PostsViewModel
 * @package ViewModel\Widgets
 *
 * @property Post[] Posts
 */
class PostsViewModel extends MVCViewModel {

	public $Posts = [];
	public $Ads = [];
	public $Options;

	public $CurrentUrl;
	public $Page;

}