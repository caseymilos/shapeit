<?php

namespace ViewModel\Posts;

use Data\Models\Post;
use Data\Models\Comment;
use ViewModel\MVCViewModel;

/**
 * Class PostViewModel
 * @package ViewModel\Posts
 *
 * @property Post Post
 * @property Comment[] Offers
 */
class PostViewModel extends MVCViewModel {

	public $Post;
	public $Comments = [];

}