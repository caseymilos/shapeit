<?php

namespace ViewModel\Posts;

use Data\Models\Comment;
use ViewModel\MVCViewModel;

/**
 * Class CommentViewModel
 * @package ViewModel\Comments
 *
 * @property Comment Comment
 */
class CommentViewModel extends MVCViewModel {

	public $Comment;

}