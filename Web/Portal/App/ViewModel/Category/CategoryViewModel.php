<?php

namespace ViewModel\Category;

use Data\Models\Post;
use ViewModel\MVCViewModel;

/**
 * Class CategoryViewModel
 * @package ViewModel\Category
 *
 * @property Post Category
 */
class CategoryViewModel extends MVCViewModel {

	public $Category;

}