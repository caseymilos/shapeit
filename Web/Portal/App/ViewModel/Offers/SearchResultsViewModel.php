<?php

namespace ViewModel\Offers;

use Data\Models\Post;
use ViewModel\MVCViewModel;

/**
 * Class SearchResultsViewModel
 * @package ViewModel\Offers
 *
 * @property Post[] Offers
 * @property string Country
 * @property string City
 */
class SearchResultsViewModel extends MVCViewModel {

	public $Offers = [];
	public $Country;
	public $City;

}