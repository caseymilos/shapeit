<?php

namespace ViewModel\Offers;

use Data\Models\Booking;
use ViewModel\MVCViewModel;

/**
 * Class BookOfferViewModel
 * @package ViewModel\Offers
 *
 * @property Booking Booking
 * @property boolean Status
 */
class BookOfferViewModel extends MVCViewModel {

	public $Booking;
	public $Status;

}