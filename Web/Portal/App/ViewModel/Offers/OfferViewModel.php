<?php

namespace ViewModel\Offers;

use Data\Models\Post;
use ViewModel\MVCViewModel;

/**
 * Class OfferViewModel
 * @package ViewModel\Offers
 *
 * @property Post Offer
 */
class OfferViewModel extends MVCViewModel {

	public $Offer;

}