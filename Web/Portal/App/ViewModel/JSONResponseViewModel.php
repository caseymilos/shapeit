<?php
/*
 * @property string[] $Messages
 * @property int $Code;
 * @property array $Data
 */
namespace ViewModel;

class JSONResponseViewModel extends MVCViewModel {
    public $Messages =[];
    public $Code;
    public $Data = [];

    /**
     * JSONResponseViewModel constructor.
     * @param $Messages
     * @param $Code
     * @param array $Data
     */
    public function __construct($Messages = null, $Code = null, array $Data = [])
    {
        $this->Messages = $Messages;
        $this->Code = $Code;
        $this->Data = $Data;
    }



}
