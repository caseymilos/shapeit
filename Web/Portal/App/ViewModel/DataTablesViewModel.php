<?php
/**
 * Created by PhpStorm.
 * User: offic
 * Date: 10/6/2017
 * Time: 3:36 PM
 */

namespace ViewModel;
use Business\DTO\DTDataDTO;




/**
 * Class DataTablesViewModel
 * @package ViewModel
 * @property int $draw
 * @property int $recordsTotal;
 * @property int $recordsFiltered;
 * @property array $data;
 */
class DataTablesViewModel extends MVCViewModel
{
    public $draw;
    public $recordsTotal;
    public $recordsFiltered;
    public $data;

    /**
     * DataTablesViewModel constructor.
     * @param int $recordsTotal|null
     * @param int $recordsFiltered|null
     * @param array $data
     * @param $draw|null
     */
    public function __construct($recordsTotal=null, $recordsFiltered=null, array $data=[], $draw=null)
    {
        $this->recordsTotal = $recordsTotal;
        $this->recordsFiltered = $recordsFiltered;
        $this->data = $data;
        $this->draw = $draw;
    }

    /**
     * @param DTDataDTO $dto
     * @return DataTablesViewModel
     */
    public static function CreateFromDtDataDTO(DTDataDTO $dto) {
        $vm = new DataTablesViewModel();
        $vm->data = $dto->Data;
        $vm->recordsTotal = $dto->TotalRecords;
        $vm->recordsFiltered = $dto->FilteredRecords;
        return $vm;
    }

}