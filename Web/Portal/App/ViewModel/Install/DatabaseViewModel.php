<?php

namespace ViewModel\Install;

use ViewModel\MVCViewModel;

class DatabaseViewModel extends MVCViewModel
{

    public $Success;

    /**
     * DatabaseViewModel constructor.
     *
     * @param boolean $success
     */
    public function __construct($success)
    {
        $this->Success = $success;
    }
}