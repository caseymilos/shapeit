<?php


use Business\Lib\Logs\Log;

try {

    $session = strtoupper(uniqid());

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    include('../../../GlobalConfig.php');
    include('../../../GlobalAutoload.php');
    include('Components/Autoload.php');

    Session::Start();

    // initiate logging
    // Log::SetSession($session);
    // Log::SetPath(ROOT_PATH . "/Web/API/Logs/");

    $router = Router::GetInstance();
    $view = $router->Route();

    $view->Render();

    /*
    if (!$view instanceof MVCView) {
        throw new Exception("Not valid view");
    }
    */

} catch (MVCException $e) {
    $e->DisplayError();
} catch (Exception $e)  {
    var_dump($e);
    die();
} finally {
    if (isset($e) && $e instanceof \Exception) {
        // Log::Add("ERROR", $e);
    }

    // Log::Save();
}