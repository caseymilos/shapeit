<?php

class RoutesEnum extends BaseEnum
{

    const Home = 1;
    const ChatsStarted = 2;
    const SaveChatData = 3;

    const NotFound = 404;



    /**
     * RoutesEnum constructor.
     */
    public function __construct()

    {
        $this->Descriptions = [
            //translated routes
            //RoutesEnum::Home => __("home"),
            RoutesEnum::Home => "home",
            RoutesEnum::ChatsStarted => "chat-started",
            RoutesEnum::SaveChatData => "save-data",
            RoutesEnum::NotFound => "404"

        ];
    }


}