<?php

use Business\ApiControllers\AdsApiController;
use Business\Enums\LanguageCodesEnum;
use Gdev\MenuManagement\ApiControllers\MenusApiController;

class MainHelper {

	public static function GetLanguage() {
		global $router;
		return LanguageCodesEnum::GetConstant($router->Language);
	}

	public static function GetMenuTree($position, $language) {
		$menu = MenusApiController::GetMenuByPosition($position, $language);
		if (count($menu) == 0) {
			return [];
		}
		return MenusApiController::GetTree($menu[0]->MenuId);
	}

	public static function GetAd($id) {

		return AdsApiController::GetAd($id);
	}

}