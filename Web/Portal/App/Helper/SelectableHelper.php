<?php
use Business\ApiControllers\EventsApiController;
use Business\Enums\GendersEnum;
use Business\Enums\LanguagesEnum;
use Business\Enums\MenuItemTypesEnum;
use Business\Enums\MenuPositionsEnum;
use Business\Enums\UserStatusTypesEnum;
use Gdev\MenuManagement\ApiControllers\MenuItemsApiController;
use Gdev\UserManagement\ApiControllers\PermissionsApiController;
use Gdev\UserManagement\ApiControllers\RolesApiController;

/**
 * Created by PhpStorm.
 * User: Nino
 * Date: 15.6.2016
 * Time: 14:07
 */
class SelectableHelper {

    public static function UserRoles() {
        $items = [];
        $roleDescriptions = RolesApiController::GetRoles();
        foreach ($roleDescriptions as $roleDescription) {
            $items[$roleDescription->RoleId] = $roleDescription->Name;
        }
        return $items;
    }

    public static function Permissions() {
        $items = [];
        $permissions = PermissionsApiController::GetPermissions();
        foreach ($permissions as $permission) {
            $items[$permission->PermissionId] = $permission->Description;
        }
        return $items;
    }

    public static function UserStatusTypes() {
        $items = [];
        foreach (UserStatusTypesEnum::enum() as $key => $value) {
            $items[$value] = UserStatusTypesEnum::Description($value);
        }
        return $items;
    }

    public static function Genders() {
        $items = [];
        foreach (GendersEnum::enum() as $key => $value) {
            $items[$value] = GendersEnum::Description($value);
        }

        return $items;
    }

    public static function MenuPositions() {
        $items = [];
        foreach (MenuPositionsEnum::enum() as $key => $value) {
            $items[$value] = MenuPositionsEnum::Description($value);
        }

        return $items;
    }

    public static function MenuItemTypes() {
        $items = [];
        foreach (MenuItemTypesEnum::enum() as $key => $value) {
            $items[$value] = MenuItemTypesEnum::Description($value);
        }

        return $items;
    }

    public static function MenuItems($menuId) {
        $menuItems = MenuItemsApiController::GetMenuItems($menuId);

        $items = [];
        foreach ($menuItems as $value) {
            $items[$value->MenuItemId] = $value->Caption;
        }

        return $items;
    }

    public static function Languages() {
        $items = [];
        foreach (LanguagesEnum::enum() as $key => $value) {
            $items[$value] = LanguagesEnum::Description($value);
        }

        return $items;
    }

    public static function Locations() {
        $items = [];
        $rooms = EventsApiController::GetLocations();
        if (count($rooms) > 0) {
            foreach ($rooms as $room) {
                $items[$room->EventRoomId] = $room->Name;
            }
        }

        return $items;
    }

    public static function Genres() {
        $items = [];
        $data = EventsApiController::GetGenres();
        if (count($data) > 0) {
            foreach ($data as $item) {
                $items[$item->GenreId] = $item->GetDescription(MainHelper::GetLanguage())->Name;
            }
        }

        return $items;
    }

    public static function Categories() {
        $items = [];
        $data = EventsApiController::GetCategories();
        if (count($data) > 0) {
            foreach ($data as $item) {
                $items[$item->CategoryId] = $item->GetDescription(MainHelper::GetLanguage())->Name;
            }
        }

        return $items;
    }

    public static function Events() {
        $items = [];
        $data = EventsApiController::GetEvents();
        if (count($data) > 0) {
            foreach ($data as $item) {
                $items[$item->EventId] = $item->GetDescription(MainHelper::GetLanguage())->Name;
            }
        }

        return $items;
    }

}