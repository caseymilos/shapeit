<?php

use Business\Enums\MenuItemTypesEnum;

class MenuHelper {

    /**
     * @param \Gdev\MenuManagement\Models\MenuItem $menuItem
     * @return string
     */
    public static function GetItemUrl($menuItem) {
        switch ($menuItem->MenuItemTypeId) {
            case MenuItemTypesEnum::Homepage:
                return Router::Create("home", [], false);
            case MenuItemTypesEnum::Faculty:
                return Router::Create("faculty", [], false);
            case MenuItemTypesEnum::Page:
                return Router::Create("page", ["slug" => $menuItem->Page->GetDescription(MainHelper::GetLanguage())->Slug], false);
            case MenuItemTypesEnum::Program:
                return Router::Create("program", ["slug" => $menuItem->Program->GetDetails(MainHelper::GetLanguage())->Slug], false);
            case MenuItemTypesEnum::External:
                return $menuItem->Url;
            case MenuItemTypesEnum::Nothing:
                return "";
            default:
                return "#";
        }
    }

    /**
     * @param \Gdev\MenuManagement\Models\MenuItem $menuItem
     * @return string
     */
    public static function IsItemUrl($menuItem, $url = null) {
        if ($url == null) {
            $url = Router::CurrentUrl();
        }
        if ($url == static::GetItemUrl($menuItem)) {
            return true;
        }

        return false;
    }

}