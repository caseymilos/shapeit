<?php

use Business\Enums\StaticTaskStatusesEnum;
use Gdev\MenuManagement\Models\MenuItem;

/**
 * Created by PhpStorm.
 * User: nikola
 * Date: 8.5.16.
 * Time: 23.16
 */
class TwigHelper {

	public static function ActiveButton($controller, $action = false, $class = "active") {
		return HtmlHelper::ActiveButton($controller, $action, $class);
	}

	public static function IsLoggedIn() {
		return Security::GetCurrentUser() == false ? false : true;
	}

	public static function GetCurrentUser() {
		return Security::GetCurrentUser();
	}

	public static function Enum($enum, $value) {
		$type = "Business\\Enums" . '\\' . $enum;
		return $type::Description($value);
	}

	public static function EnumCaption($enum, $value) {
		$type = "Business\\Enums" . '\\' . $enum;
		$enum = new $type();
		return $enum->Caption($value);
	}

	public static function EnumConstant($enum, $constant) {
		$type = "Business\\Enums" . '\\' . $enum;
		return $type::GetConstant($constant);
	}

	public static function GetLanguage() {
		return MainHelper::GetLanguage();
	}

	public static function GetLanguageCode() {
		$enum = new \Business\Enums\LanguageCodesEnum();
		return $enum->Caption(static::GetLanguage());
	}

	public static function GetSponsors() {
		// cached output
		$output = \Data\Cache\MemcachedService::getInstance()->get("FooterSponsors");
		if (!$output) {
			$output = HtmlHelper::RenderView("Shared/Footer/Sponsors", ["sponsors" => \Business\ApiControllers\SponsorsApiController::GetSponsors()], true);
			\Data\Cache\MemcachedService::getInstance()->set("FooterSponsors", $output, \Data\Cache\MemcachedService::Lifetime);
		}
		return $output;
	}

	public static function GetMenu($position) {
		// cached output
		$language = MainHelper::GetLanguage();
		$output = \Data\Cache\MemcachedService::getInstance()->get("MenuOutput" . $position . $language);
		if (!$output) {
			$enum = new \Business\Enums\MenuPositionsEnum();
			$output = HtmlHelper::RenderView("Shared/Menu/" . $enum->Caption($position), ["menu" => MainHelper::GetMenuTree($position, $language)], true);
			\Data\Cache\MemcachedService::getInstance()->set("MenuOutput" . $position . $language, $output, \Data\Cache\MemcachedService::Lifetime);
		}
		return $output;
	}

	public static function GetItemUrl(MenuItem $menuItem) {
		return MenuHelper::GetItemUrl($menuItem);
	}

	public static function IsItemUrl(MenuItem $menuItem, $url = null) {
		return MenuHelper::IsItemUrl($menuItem, $url);
	}

	public static function GetPageUrl(\Data\Models\Page $page) {
		return Router::Create("page", ["slug" => $page->GetDescription(MainHelper::GetLanguage())->Slug], false);
	}


	public static function TranslateDateTime(DateTime $datetime) {
		if (MainHelper::GetLanguage() == \Business\Enums\LanguagesEnum::English) {
			return $datetime->format("D, jS F, H:i");
		}
		return strftime("%a, %d. %B, %H:%M", $datetime->getTimestamp()) . " Uhr";
	}

	/**
	 * @return \Data\Models\SocialIcon[]
	 */
	public static function GetSocialIcons() {
		return \Business\ApiControllers\SocialIconsApiController::GetActiveSocialIconsOrderedByWeight();
	}

	public static function EventMessage($event) {
		return EventsHelper::GetMessage($event);
	}

	public static function EventUrl($event) {
		if (is_null($event)) {
			return "";
		}

		if ($event instanceof \Spot\Relation\BelongsTo) {
			$event = $event->execute();
		}
		return EventsHelper::GetUrl($event);
	}

	public function GetPage() {
		if (!array_key_exists("page", $_GET)) {
			return 1;
		}
		return $_GET['page'];
	}

	public static function GetAd($id) {

		return MainHelper::GetAd($id);
	}

}