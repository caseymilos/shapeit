<?php

require_once 'Lib/Twig/Autoloader.php';

class TwigFiltersHelper extends Twig_Extension {

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('indexes', array($this, 'GetIndexes')),
            new \Twig_SimpleFilter('insertAds', array($this, 'InsertAds')),
        );
    }

    public function GetIndexes($items, $property) {
        $result = [];
        if (count($items) > 0) {
            foreach ($items as $item) {
                $result[] = $item->{$property};
            }
        }

        return $result;
    }

    public function InsertAds($text) {
        $matches = [];
        preg_match_all("/\{ad\|([^}]+)\}/", $text, $matches);

        foreach($matches[1] as $id) {
            $ad = \Business\ApiControllers\AdsApiController::GetAd($id);
            $text = str_replace("{ad|".$id."}", $ad->Code, $text);
        }

        return $text;
    }

    public function getName() {
        return 'gdev_twig_filters';
    }

}