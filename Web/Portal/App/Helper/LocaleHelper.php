<?php

class LocaleHelper {

	public static function Translate($string) {
		return __($string);
	}


	/**
	 * Only serves to enable translation for javascript labels
	 */
	private function Translations() {
		$array = [
			__("months"),
			__("days"),
			__("hours"),
			__("minutes"),
			__("seconds"),
			__("/the-opening-week"),
			__("List View"),
			__("Calendar View"),
			__("Your name"),
			__("E-mail"),
			__("Your message"),
			__("Search for the artists"),
			__("Search for the composer"),
		];
	}

}