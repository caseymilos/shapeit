<?php

/* Admin/Offers/Offers.twig */
class __TwigTemplate_d8e486fb86dc8f58d5d11e0665e5fec78d7bab85e62c053120484c12114c1292 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Master.twig", "Admin/Offers/Offers.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Master.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "List of offers";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "
\t<ol class=\"breadcrumb breadcrumb-quirk\">
\t\t<li>
\t\t\t<a href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "home"), "method"), "html", null, true);
        echo "\">
\t\t\t\t<i class=\"fa fa-home mr5\"></i>
\t\t\t\tHome
\t\t\t</a>
\t\t</li>
\t\t<li>
\t\t\t<a href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "offers-list"), "method"), "html", null, true);
        echo "\">Offers</a>
\t\t</li>
\t\t<li class=\"active\">Offers Directory</li>
\t</ol>


\t<div class=\"panel\">
\t\t<div class=\"panel-heading\">
\t\t\t<h4 class=\"panel-title\">Offers List</h4>
\t\t</div>
\t\t<div class=\"panel-body\">
\t\t\t<div class=\"table-responsive\">
\t\t\t\t<table class=\"datatable table table-bordered table-striped-col\">
\t\t\t\t\t<thead>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th style=\"width: 100px;\">Image</th>
\t\t\t\t\t\t\t<th>Title</th>
\t\t\t\t\t\t\t<th>Price</th>
\t\t\t\t\t\t\t<th class=\"text-center\" style=\"width: 80px;\">Actions</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>

\t\t\t\t\t<tfoot>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>Image</th>
\t\t\t\t\t\t\t<th>Title</th>
\t\t\t\t\t\t\t<th>Price</th>
\t\t\t\t\t\t\t<th class=\"text-center\" style=\"width: 80px;\">Actions</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</tfoot>

\t\t\t\t\t<tbody>
\t\t\t\t\t\t";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Offers", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["offer"]) {
            // line 50
            echo "\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td><img src=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["offer"], "PictureSource", array(0 => "Small"), "method"), "html", null, true);
            echo "\"/></td>
\t\t\t\t\t\t\t\t<td>";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($context["offer"], "Title", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td>";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["offer"], "Price", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<ul class=\"table-options\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "offers-thumbnail-crop", 1 => array("id" => $this->getAttribute($context["offer"], "OfferId", array()), "size" => "medium")), "method"), "html", null, true);
            echo "\"
\t\t\t\t\t\t\t\t\t\t\t   title=\"Crop thumbnail\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-crop\"></i>
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "update-offer", 1 => array("id" => $this->getAttribute($context["offer"], "OfferId", array()))), "method"), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a class=\"confirm\" style=\"cursor: pointer\"
\t\t\t\t\t\t\t\t\t\t\t   data-href=\"";
            // line 69
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "delete-offer", 1 => array("id" => $this->getAttribute($context["offer"], "OfferId", array()))), "method"), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-trash\"></i>
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['offer'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "\t\t\t\t\t</tbody>

\t\t\t\t</table>
\t\t\t</div>
\t\t</div>
\t</div><!-- panel -->

";
    }

    public function getTemplateName()
    {
        return "Admin/Offers/Offers.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 77,  127 => 69,  118 => 63,  109 => 57,  102 => 53,  98 => 52,  94 => 51,  91 => 50,  87 => 49,  52 => 17,  43 => 11,  38 => 8,  35 => 7,  29 => 5,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Master.twig" %}*/
/* */
/* {# @var model \PagesViewModel #}*/
/* */
/* {% block title %}List of offers{% endblock %}*/
/* */
/* {% block content %}*/
/* */
/* 	<ol class="breadcrumb breadcrumb-quirk">*/
/* 		<li>*/
/* 			<a href="{{ Router.Create("home") }}">*/
/* 				<i class="fa fa-home mr5"></i>*/
/* 				Home*/
/* 			</a>*/
/* 		</li>*/
/* 		<li>*/
/* 			<a href="{{ Router.Create("offers-list") }}">Offers</a>*/
/* 		</li>*/
/* 		<li class="active">Offers Directory</li>*/
/* 	</ol>*/
/* */
/* */
/* 	<div class="panel">*/
/* 		<div class="panel-heading">*/
/* 			<h4 class="panel-title">Offers List</h4>*/
/* 		</div>*/
/* 		<div class="panel-body">*/
/* 			<div class="table-responsive">*/
/* 				<table class="datatable table table-bordered table-striped-col">*/
/* 					<thead>*/
/* 						<tr>*/
/* 							<th style="width: 100px;">Image</th>*/
/* 							<th>Title</th>*/
/* 							<th>Price</th>*/
/* 							<th class="text-center" style="width: 80px;">Actions</th>*/
/* 						</tr>*/
/* 					</thead>*/
/* */
/* 					<tfoot>*/
/* 						<tr>*/
/* 							<th>Image</th>*/
/* 							<th>Title</th>*/
/* 							<th>Price</th>*/
/* 							<th class="text-center" style="width: 80px;">Actions</th>*/
/* 						</tr>*/
/* 					</tfoot>*/
/* */
/* 					<tbody>*/
/* 						{% for offer in model.Offers %}*/
/* 							<tr>*/
/* 								<td><img src="{{ offer.PictureSource("Small") }}"/></td>*/
/* 								<td>{{ offer.Title }}</td>*/
/* 								<td>{{ offer.Price }}</td>*/
/* 								<td>*/
/* 									<ul class="table-options">*/
/* 										<li>*/
/* 											<a href="{{ Router.Create("offers-thumbnail-crop", {id: offer.OfferId, size: 'medium'}) }}"*/
/* 											   title="Crop thumbnail">*/
/* 												<i class="fa fa-crop"></i>*/
/* 											</a>*/
/* 										</li>*/
/* 										<li>*/
/* 											<a href="{{ Router.Create("update-offer", {id: offer.OfferId}) }}">*/
/* 												<i class="fa fa-pencil"></i>*/
/* 											</a>*/
/* 										</li>*/
/* 										<li>*/
/* 											<a class="confirm" style="cursor: pointer"*/
/* 											   data-href="{{ Router.Create("delete-offer", {id: offer.OfferId}) }}">*/
/* 												<i class="fa fa-trash"></i>*/
/* 											</a>*/
/* 										</li>*/
/* 									</ul>*/
/* 								</td>*/
/* 							</tr>*/
/* 						{% endfor %}*/
/* 					</tbody>*/
/* */
/* 				</table>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div><!-- panel -->*/
/* */
/* {% endblock %}*/
