<?php

/* Admin/Pages/WidgetOptions/HeadingBlock.twig */
class __TwigTemplate_0098d3be7e99a831d4c4360ab27f59ed34f61233f6c672a60d5d4c8764daa1ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-group\">
    <label>Title</label>
    <input type=\"text\" placeholder=\"Title\" name=\"title\" class=\"form-control\"
           value=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "title", array()), "html", null, true);
        echo "\"/>
</div>

<div class=\"form-group\">
    <label>Description</label>
    <input type=\"text\" placeholder=\"Description\" name=\"description\" class=\"form-control\"
           value=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "description", array()), "html", null, true);
        echo "\"/>
</div>

<div class=\"form-group\">
    <label>Icon</label>
    <input type=\"text\" placeholder=\"Icon\" name=\"icon\" class=\"form-control\"
           value=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "icon", array()), "html", null, true);
        echo "\"/>
</div>";
    }

    public function getTemplateName()
    {
        return "Admin/Pages/WidgetOptions/HeadingBlock.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 16,  33 => 10,  24 => 4,  19 => 1,);
    }
}
/* <div class="form-group">*/
/*     <label>Title</label>*/
/*     <input type="text" placeholder="Title" name="title" class="form-control"*/
/*            value="{{ widget.Options.title }}"/>*/
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label>Description</label>*/
/*     <input type="text" placeholder="Description" name="description" class="form-control"*/
/*            value="{{ widget.Options.description }}"/>*/
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label>Icon</label>*/
/*     <input type="text" placeholder="Icon" name="icon" class="form-control"*/
/*            value="{{ widget.Options.icon }}"/>*/
/* </div>*/
