<?php

/* Admin/Pages/ThumbnailCrop.twig */
class __TwigTemplate_dcff1acd0c4a043a00c339f94f35e5047d850e97e0f4dd6f457c92c43b1cf03b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Master.twig", "Admin/Pages/ThumbnailCrop.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'post_scripts' => array($this, 'block_post_scripts'),
            'additional_styles' => array($this, 'block_additional_styles'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Master.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Crop Thumbnail";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "
    <ol class=\"breadcrumb breadcrumb-quirk\">
        <li>
            <a href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "home"), "method"), "html", null, true);
        echo "\">
                <i class=\"fa fa-home mr5\"></i>
                Home
            </a>
        </li>
        <li>
            <a href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "pages-list"), "method"), "html", null, true);
        echo "\">Pages</a>
        </li>
        <li class=\"active\">Crop Thumbnail</li>
    </ol>

    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel\">
                <div class=\"panel-heading nopaddingbottom\">
                    <h4 class=\"panel-title\">Crop Thumbnail</h4>
                </div>
                <div class=\"panel-body\">
                    <hr>

                    <div id=\"event-thumbnail\">
                    </div>

                    <hr>

                    <a href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "pages-thumbnail-crop", 1 => array("id" => $this->getAttribute($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Page", array()), "PageId", array()), "size" => (isset($context["size"]) ? $context["size"] : null))), "method"), "html", null, true);
        echo "\"
                       class=\"btn btn-primary\" id=\"save-thumbnail\">Save
                    </a>
                </div>
            </div>
        </div>
    </div>


";
    }

    // line 47
    public function block_post_scripts($context, array $blocks = array())
    {
        // line 48
        echo "    <script src=\"Theme/Admin/lib/Croppie/croppie.min.js\"></script>
    <script type=\"text/javascript\">
        \$('#event-thumbnail').croppie({
            url: '";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Page", array()), "PictureSource", array(), "method"), "html", null, true);
        echo "',
            viewport: {
                width: ";
        // line 53
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dimensions"]) ? $context["dimensions"] : null), 0, array()), "html", null, true);
        echo ",
                height: ";
        // line 54
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dimensions"]) ? $context["dimensions"] : null), 1, array()), "html", null, true);
        echo ",
                type: 'square'
            },
            boundary: {
                width: ";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dimensions"]) ? $context["dimensions"] : null), 2, array()), "html", null, true);
        echo ",
                height: ";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dimensions"]) ? $context["dimensions"] : null), 3, array()), "html", null, true);
        echo "
            }
        });

        \$(\"#save-thumbnail\").on(\"click\", function (e) {
            e.preventDefault();
            var data = \$(\"#event-thumbnail\").croppie('get');
            \$(\"body\").addClass(\"loading\");

            \$.ajax({
                url: \$(this).attr(\"href\"),
                type: \"POST\",
                data: {
                    points: data.points
                },
                complete: function () {
                    \$(\"body\").removeClass(\"loading\");
                }
            })
        });
    </script>
";
    }

    // line 82
    public function block_additional_styles($context, array $blocks = array())
    {
        // line 83
        echo "    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/Croppie/croppie.css\">
";
    }

    public function getTemplateName()
    {
        return "Admin/Pages/ThumbnailCrop.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 83,  144 => 82,  118 => 59,  114 => 58,  107 => 54,  103 => 53,  98 => 51,  93 => 48,  90 => 47,  76 => 36,  54 => 17,  45 => 11,  40 => 8,  37 => 7,  31 => 5,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Master.twig" %}*/
/* */
/* {# @var model \PageViewModel #}*/
/* */
/* {% block title %}Crop Thumbnail{% endblock %}*/
/* */
/* {% block content %}*/
/* */
/*     <ol class="breadcrumb breadcrumb-quirk">*/
/*         <li>*/
/*             <a href="{{ Router.Create("home") }}">*/
/*                 <i class="fa fa-home mr5"></i>*/
/*                 Home*/
/*             </a>*/
/*         </li>*/
/*         <li>*/
/*             <a href="{{ Router.Create("pages-list") }}">Pages</a>*/
/*         </li>*/
/*         <li class="active">Crop Thumbnail</li>*/
/*     </ol>*/
/* */
/*     <div class="row">*/
/*         <div class="col-sm-12">*/
/*             <div class="panel">*/
/*                 <div class="panel-heading nopaddingbottom">*/
/*                     <h4 class="panel-title">Crop Thumbnail</h4>*/
/*                 </div>*/
/*                 <div class="panel-body">*/
/*                     <hr>*/
/* */
/*                     <div id="event-thumbnail">*/
/*                     </div>*/
/* */
/*                     <hr>*/
/* */
/*                     <a href="{{ Router.Create("pages-thumbnail-crop", {"id": model.Page.PageId, "size": size}) }}"*/
/*                        class="btn btn-primary" id="save-thumbnail">Save*/
/*                     </a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/* */
/* {% endblock %}*/
/* */
/* {% block post_scripts %}*/
/*     <script src="Theme/Admin/lib/Croppie/croppie.min.js"></script>*/
/*     <script type="text/javascript">*/
/*         $('#event-thumbnail').croppie({*/
/*             url: '{{ model.Page.PictureSource() }}',*/
/*             viewport: {*/
/*                 width: {{ dimensions.0 }},*/
/*                 height: {{ dimensions.1 }},*/
/*                 type: 'square'*/
/*             },*/
/*             boundary: {*/
/*                 width: {{ dimensions.2 }},*/
/*                 height: {{ dimensions.3 }}*/
/*             }*/
/*         });*/
/* */
/*         $("#save-thumbnail").on("click", function (e) {*/
/*             e.preventDefault();*/
/*             var data = $("#event-thumbnail").croppie('get');*/
/*             $("body").addClass("loading");*/
/* */
/*             $.ajax({*/
/*                 url: $(this).attr("href"),*/
/*                 type: "POST",*/
/*                 data: {*/
/*                     points: data.points*/
/*                 },*/
/*                 complete: function () {*/
/*                     $("body").removeClass("loading");*/
/*                 }*/
/*             })*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
/* */
/* {% block additional_styles %}*/
/*     <link rel="stylesheet" href="Theme/Admin/lib/Croppie/croppie.css">*/
/* {% endblock %}*/
