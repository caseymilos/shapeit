<?php

/* Admin/Builder/Layout/Master/Row.twig */
class __TwigTemplate_d5c15f12c79d7fcd7e5f66a6a86ee4e8319f7f3e7d2ba6be9a10f1441cac1680 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Builder/Layout/Master/Base.twig", "Admin/Builder/Layout/Master/Row.twig", 1);
        $this->blocks = array(
            'element' => array($this, 'block_element'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Builder/Layout/Master/Base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_element($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"row\">
        ";
        // line 5
        $this->displayBlock('content', $context, $blocks);
        // line 6
        echo "    </div>
";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "Admin/Builder/Layout/Master/Row.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 5,  37 => 6,  35 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Builder/Layout/Master/Base.twig" %}*/
/* */
/* {% block element %}*/
/*     <div class="row">*/
/*         {% block content %}{% endblock %}*/
/*     </div>*/
/* {% endblock %}*/
/* */
