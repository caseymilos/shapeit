<?php

/* Admin/Builder/Layout/Options/Options.twig */
class __TwigTemplate_597e7d7f801556f7c649ccb41386c691f2b00d078d1a61bbe6b2e662139de10b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-group\">
    <label>Width</label>
    <div class=\"row\">
        <div class=\"col-md-12\">
            ";
        // line 5
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "Radio", array(0 => array("boxed" => "Boxed", "fullWidth" => "Full Width"), 1 => "width", 2 => $this->getAttribute((isset($context["options"]) ? $context["options"] : null), "width", array())), "method");
        echo "
        </div>
    </div>
</div>


<div class=\"form-group\">
    <label>Background Color</label>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <input type=\"text\" id=\"colorpicker1\" class=\"form-control\" name=\"backgroundColor\" value=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["options"]) ? $context["options"] : null), "backgroundColor", array()), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["options"]) ? $context["options"] : null), "backgroundColor", array()), "html", null, true);
        echo "\">
        </div>
    </div>
</div>

<div class=\"row\">
    <div class=\"col-md-6\">
        <div class=\"form-group\">
            <label>Padding Top</label>
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <input type=\"text\" class=\"form-control\" name=\"paddingTop\" value=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["options"]) ? $context["options"] : null), "paddingTop", array()), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["options"]) ? $context["options"] : null), "paddingTop", array()), "html", null, true);
        echo "\">
                </div>
            </div>
        </div>
    </div>
    <div class=\"col-md-6\">
        <div class=\"form-group\">
            <label>Padding Bottom</label>
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <input type=\"text\" class=\"form-control\" name=\"paddingBottom\" value=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["options"]) ? $context["options"] : null), "paddingBottom", array()), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["options"]) ? $context["options"] : null), "paddingBottom", array()), "html", null, true);
        echo "\">
                </div>
            </div>
        </div>
    </div>
</div>

<script type=\"text/javascript\">
\t\$('#colorpicker1').colorpicker();
</script>";
    }

    public function getTemplateName()
    {
        return "Admin/Builder/Layout/Options/Options.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 36,  54 => 26,  38 => 15,  25 => 5,  19 => 1,);
    }
}
/* <div class="form-group">*/
/*     <label>Width</label>*/
/*     <div class="row">*/
/*         <div class="col-md-12">*/
/*             {{ HtmlHelper.Radio({boxed: "Boxed", fullWidth: "Full Width"}, "width", options.width)|raw }}*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* */
/* <div class="form-group">*/
/*     <label>Background Color</label>*/
/*     <div class="row">*/
/*         <div class="col-md-12">*/
/*             <input type="text" id="colorpicker1" class="form-control" name="backgroundColor" value="{{ options.backgroundColor }}" placeholder="{{ options.backgroundColor }}">*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* <div class="row">*/
/*     <div class="col-md-6">*/
/*         <div class="form-group">*/
/*             <label>Padding Top</label>*/
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <input type="text" class="form-control" name="paddingTop" value="{{ options.paddingTop }}" placeholder="{{ options.paddingTop }}">*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="col-md-6">*/
/*         <div class="form-group">*/
/*             <label>Padding Bottom</label>*/
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <input type="text" class="form-control" name="paddingBottom" value="{{ options.paddingBottom }}" placeholder="{{ options.paddingBottom }}">*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* <script type="text/javascript">*/
/* 	$('#colorpicker1').colorpicker();*/
/* </script>*/
