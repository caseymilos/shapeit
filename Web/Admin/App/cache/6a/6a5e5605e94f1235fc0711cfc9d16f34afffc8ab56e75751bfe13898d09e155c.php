<?php

/* Admin/Builder/Layout/Master/Base.twig */
class __TwigTemplate_34e0ce798f3059dba9c3f05198a0f4105cb4633ac62bc512b9d651160dfc401c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'element' => array($this, 'block_element'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["uid"] = (twig_date_format_filter($this->env, "now", "U") . twig_random($this->env));
        // line 2
        echo "
<div class=\"layout-element\" data-layout=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "LayoutId", array()), "html", null, true);
        echo "\" data-type=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "LayoutTypeId", array()), "html", null, true);
        echo "\"
     data-layout-uid=\"";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["uid"]) ? $context["uid"] : null), "html", null, true);
        echo "\">
    <a title=\"Options\" data-edit-layout class=\"btn btn-xs btn-info\">
        <i class=\"fa fa-pencil\"></i>
    </a>
    <a title=\"Delete layout\" data-delete-layout class=\"btn btn-xs btn-danger\">
        <i class=\"fa fa-times\"></i>
    </a>
    ";
        // line 11
        $this->displayBlock('element', $context, $blocks);
        // line 12
        echo "</div>


<script type=\"text/javascript\">
    \$(document).ready(function () {
        \$(\"[data-layout-uid=";
        // line 17
        echo twig_escape_filter($this->env, (isset($context["uid"]) ? $context["uid"] : null), "html", null, true);
        echo "]\").data(\"options\", ";
        echo twig_jsonencode_filter($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Options", array()));
        echo ");
    });
</script>";
    }

    // line 11
    public function block_element($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "Admin/Builder/Layout/Master/Base.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 11,  50 => 17,  43 => 12,  41 => 11,  31 => 4,  25 => 3,  22 => 2,  20 => 1,);
    }
}
/* {% set uid = "now"|date('U') ~ random() %}*/
/* */
/* <div class="layout-element" data-layout="{{ model.LayoutId }}" data-type="{{ model.LayoutTypeId }}"*/
/*      data-layout-uid="{{ uid }}">*/
/*     <a title="Options" data-edit-layout class="btn btn-xs btn-info">*/
/*         <i class="fa fa-pencil"></i>*/
/*     </a>*/
/*     <a title="Delete layout" data-delete-layout class="btn btn-xs btn-danger">*/
/*         <i class="fa fa-times"></i>*/
/*     </a>*/
/*     {% block element %}{% endblock %}*/
/* </div>*/
/* */
/* */
/* <script type="text/javascript">*/
/*     $(document).ready(function () {*/
/*         $("[data-layout-uid={{ uid }}]").data("options", {{ model.Options|json_encode|raw }});*/
/*     });*/
/* </script>*/
