<?php

/* Admin/Users/Shared/UserBox.twig */
class __TwigTemplate_3898a50afc03ed0327e2454e428b72bb8f6d58dc0a85596a31cc55a47ff80f30 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<div class=\"col-md-3 col-lg-3\">
    <div class=\"panel panel-profile grid-view\">
        <div class=\"panel-heading\">
            <div class=\"text-center\">
                <a href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "user-update-details", 1 => array("username" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "UserName", array()))), "method"), "html", null, true);
        echo "\"
                   class=\"panel-profile-photo\">
                    <img class=\"img-circle user-image\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Details", array()), "PictureSource", array(0 => "Small"), "method"), "html", null, true);
        echo "\" alt=\"\">
                </a>
                <a href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "user-update-details", 1 => array("username" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "UserName", array()))), "method"), "html", null, true);
        echo "\">
                    <h4 class=\"panel-profile-name\">";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Details", array()), "FirstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Details", array()), "LastName", array()), "html", null, true);
        echo "</h4>
                </a>
                <p class=\"media-usermeta\">
                    <i class=\"glyphicon glyphicon-briefcase\"></i>
                    ";
        // line 15
        echo twig_escape_filter($this->env, twig_join_filter($this->env->getExtension('gdev_twig_filters')->GetIndexes($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Roles", array()), "Name"), ", "), "html", null, true);
        echo "
                </p>
            </div>
            <ul class=\"panel-options\">
                <li>
                    <a class=\"tooltips\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "user-update-details", 1 => array("username" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "UserName", array()))), "method"), "html", null, true);
        echo "\"
                       data-toggle=\"tooltip\" title=\"Edit\">
                        <i class=\"glyphicon glyphicon-pencil\"></i>
                    </a>
                </li>
                <li>
                    <a class=\"tooltips confirm\"
                       data-href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "user-delete", 1 => array("username" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "UserName", array()))), "method"), "html", null, true);
        echo "\"
                       title=\"Delete\">
                        <i class=\"fa fa-trash\"></i>
                    </a>
                </li>
            </ul>
        </div><!-- panel-heading -->
        <div class=\"panel-body people-info\">

            <div class=\"info-group\">
                <label>Email</label>
                ";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Email", array()), "html", null, true);
        echo "
            </div>

            <div class=\"info-group last\">
                <label>Date of Birth</label>
                ";
        // line 43
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Details", array()), "DateOfBirth", array())) ? ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Details", array()), "DateOfBirth", array()), "format", array(0 => "jS F, Y"), "method")) : ("-")), "html", null, true);
        echo "
            </div>
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "Admin/Users/Shared/UserBox.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 43,  80 => 38,  66 => 27,  56 => 20,  48 => 15,  39 => 11,  35 => 10,  30 => 8,  25 => 6,  19 => 2,);
    }
}
/* {# @var user \Gdev\UserManagement\Models\User #}*/
/* <div class="col-md-3 col-lg-3">*/
/*     <div class="panel panel-profile grid-view">*/
/*         <div class="panel-heading">*/
/*             <div class="text-center">*/
/*                 <a href="{{ Router.Create("user-update-details", {username: user.UserName}) }}"*/
/*                    class="panel-profile-photo">*/
/*                     <img class="img-circle user-image" src="{{ user.Details.PictureSource("Small") }}" alt="">*/
/*                 </a>*/
/*                 <a href="{{ Router.Create("user-update-details", {username: user.UserName}) }}">*/
/*                     <h4 class="panel-profile-name">{{ user.Details.FirstName }} {{ user.Details.LastName }}</h4>*/
/*                 </a>*/
/*                 <p class="media-usermeta">*/
/*                     <i class="glyphicon glyphicon-briefcase"></i>*/
/*                     {{ user.Roles|indexes("Name")|join(", ") }}*/
/*                 </p>*/
/*             </div>*/
/*             <ul class="panel-options">*/
/*                 <li>*/
/*                     <a class="tooltips" href="{{ Router.Create("user-update-details", {username: user.UserName}) }}"*/
/*                        data-toggle="tooltip" title="Edit">*/
/*                         <i class="glyphicon glyphicon-pencil"></i>*/
/*                     </a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a class="tooltips confirm"*/
/*                        data-href="{{ Router.Create("user-delete", {username: user.UserName}) }}"*/
/*                        title="Delete">*/
/*                         <i class="fa fa-trash"></i>*/
/*                     </a>*/
/*                 </li>*/
/*             </ul>*/
/*         </div><!-- panel-heading -->*/
/*         <div class="panel-body people-info">*/
/* */
/*             <div class="info-group">*/
/*                 <label>Email</label>*/
/*                 {{ user.Email }}*/
/*             </div>*/
/* */
/*             <div class="info-group last">*/
/*                 <label>Date of Birth</label>*/
/*                 {{ user.Details.DateOfBirth ? user.Details.DateOfBirth.format('jS F, Y') : "-" }}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
