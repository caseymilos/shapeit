<?php

/* Admin/Master/Base.twig */
class __TwigTemplate_8d881ed5a96fe3abf804676c1d19e52758d03bd16fe74fa4a7c577130ef17d52 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'doctype' => array($this, 'block_doctype'),
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body_class' => array($this, 'block_body_class'),
            'body' => array($this, 'block_body'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $this->displayBlock('doctype', $context, $blocks);
        // line 5
        echo "
<html lang=\"en\">

    <head>
        ";
        // line 9
        $this->displayBlock('head', $context, $blocks);
        // line 30
        echo "    </head>

    <body class=\"";
        // line 32
        $this->displayBlock('body_class', $context, $blocks);
        echo "\">
        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        $this->displayBlock('scripts', $context, $blocks);
        // line 35
        echo "    </body>
</html>";
    }

    // line 2
    public function block_doctype($context, array $blocks = array())
    {
        // line 3
        echo "<!DOCTYPE html>
";
    }

    // line 9
    public function block_head($context, array $blocks = array())
    {
        // line 10
        echo "            <base href=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["Config"]) ? $context["Config"] : null), "admin", array()), "url", array()), "html", null, true);
        echo "\">
            <title>";
        // line 11
        $this->displayBlock('title', $context, $blocks);
        echo " - Travelinsider</title>
            <meta charset=\"utf-8\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0\">
            <meta name=\"description\" content=\"\">
            <meta name=\"author\" content=\"\">
            <!--<link rel=\"shortcut icon\" href=\"../images/favicon.png\" type=\"image/png\">-->
            <link rel=\"stylesheet\" href=\"Theme/Admin/lib/Hover/hover.css\">
            <link rel=\"stylesheet\" href=\"Theme/Admin/lib/fontawesome/css/font-awesome.css\">
            <link rel=\"stylesheet\" href=\"Theme/Admin/lib/weather-icons/css/weather-icons.css\">
            <link rel=\"stylesheet\" href=\"Theme/Admin/lib/ionicons/css/ionicons.css\">

            <script src=\"Theme/Admin/lib/modernizr/modernizr.js\"></script>

            <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!--[if lt IE 9]>
            <script src=\"Theme/Admin/lib/html5shiv/html5shiv.js\"></script>
            <script src=\"Theme/Admin/lib/respond/respond.src.js\"></script>
            <![endif]-->
        ";
    }

    public function block_title($context, array $blocks = array())
    {
    }

    // line 32
    public function block_body_class($context, array $blocks = array())
    {
    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
    }

    // line 34
    public function block_scripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "Admin/Master/Base.twig";
    }

    public function getDebugInfo()
    {
        return array (  106 => 34,  101 => 33,  96 => 32,  69 => 11,  64 => 10,  61 => 9,  56 => 3,  53 => 2,  48 => 35,  45 => 34,  43 => 33,  39 => 32,  35 => 30,  33 => 9,  27 => 5,  25 => 2,);
    }
}
/* {# Twig master for common master elements #}*/
/* {% block doctype %}*/
/* <!DOCTYPE html>*/
/* {% endblock %}*/
/* */
/* <html lang="en">*/
/* */
/*     <head>*/
/*         {% block head %}*/
/*             <base href="{{ Config.admin.url }}">*/
/*             <title>{% block title %}{% endblock %} - Travelinsider</title>*/
/*             <meta charset="utf-8">*/
/*             <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">*/
/*             <meta name="description" content="">*/
/*             <meta name="author" content="">*/
/*             <!--<link rel="shortcut icon" href="../images/favicon.png" type="image/png">-->*/
/*             <link rel="stylesheet" href="Theme/Admin/lib/Hover/hover.css">*/
/*             <link rel="stylesheet" href="Theme/Admin/lib/fontawesome/css/font-awesome.css">*/
/*             <link rel="stylesheet" href="Theme/Admin/lib/weather-icons/css/weather-icons.css">*/
/*             <link rel="stylesheet" href="Theme/Admin/lib/ionicons/css/ionicons.css">*/
/* */
/*             <script src="Theme/Admin/lib/modernizr/modernizr.js"></script>*/
/* */
/*             <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->*/
/*             <!--[if lt IE 9]>*/
/*             <script src="Theme/Admin/lib/html5shiv/html5shiv.js"></script>*/
/*             <script src="Theme/Admin/lib/respond/respond.src.js"></script>*/
/*             <![endif]-->*/
/*         {% endblock %}*/
/*     </head>*/
/* */
/*     <body class="{% block body_class %}{% endblock %}">*/
/*         {% block body %}{% endblock %}*/
/*         {% block scripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
