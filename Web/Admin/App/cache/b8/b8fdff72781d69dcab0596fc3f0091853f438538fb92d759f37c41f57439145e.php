<?php

/* Admin/Blog/Update.twig */
class __TwigTemplate_c7318ed7b1585bde8fa757809e2bf5d982da96a03ff6e09a4c9b051fe22e7918 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("Admin/Master/Master.twig", "Admin/Blog/Update.twig", 3);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'post_scripts' => array($this, 'block_post_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Master.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Edit Blog ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Blog", array()), "Name", array()), "html", null, true);
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "    <ol class=\"breadcrumb breadcrumb-quirk\">
        <li>
            <a href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "home"), "method"), "html", null, true);
        echo "\">
                <i class=\"fa fa-home mr5\"></i>
                Home
            </a>
        </li>
        <li>
            <a href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "blogs-list"), "method"), "html", null, true);
        echo "\">Blogs</a>
        </li>
        <li class=\"active\">Edit blog ";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Blog", array()), "Title", array()), "html", null, true);
        echo "</li>
    </ol>

    ";
        // line 21
        $this->loadTemplate("Admin/Blog/Partial/BlogForm.twig", "Admin/Blog/Update.twig", 21)->display(array_merge($context, array("model" => (isset($context["model"]) ? $context["model"] : null))));
    }

    // line 24
    public function block_post_scripts($context, array $blocks = array())
    {
        // line 25
        echo "
";
    }

    public function getTemplateName()
    {
        return "Admin/Blog/Update.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 25,  68 => 24,  64 => 21,  58 => 18,  53 => 16,  44 => 10,  40 => 8,  37 => 7,  30 => 5,  11 => 3,);
    }
}
/* {# @var model \BlogViewModel #}*/
/* */
/* {% extends "Admin/Master/Master.twig" %}*/
/* */
/* {% block title %}Edit Blog {{ model.Blog.Name }}{% endblock %}*/
/* */
/* {% block content %}*/
/*     <ol class="breadcrumb breadcrumb-quirk">*/
/*         <li>*/
/*             <a href="{{ Router.Create("home") }}">*/
/*                 <i class="fa fa-home mr5"></i>*/
/*                 Home*/
/*             </a>*/
/*         </li>*/
/*         <li>*/
/*             <a href="{{ Router.Create("blogs-list") }}">Blogs</a>*/
/*         </li>*/
/*         <li class="active">Edit blog {{ model.Blog.Title }}</li>*/
/*     </ol>*/
/* */
/*     {% include "Admin/Blog/Partial/BlogForm.twig" with {'model': model} %}*/
/* {% endblock %}*/
/* */
/* {% block post_scripts %}*/
/* */
/* {% endblock %}*/
/* */
