<?php

/* Admin/Media/GalleryWidgetBrowse.twig */
class __TwigTemplate_78dc9855faec213ca1e7c4e1b9f20d77b2fd71a337501a24d05700412bc00fb0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<div class=\"modal-header\">
    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
    <h4 class=\"modal-title\">Add Media</h4>
</div>
<div class=\"modal-body\">

    <div class=\"media-widget-media-holder\" style=\"height: 280px; overflow: auto;\">
        <div class=\"container-fluid\">
            ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_array_batch($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Media", array()), 4));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 11
            echo "                <div class=\"row\">
                    ";
            // line 12
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["row"]);
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["media"]) {
                // line 13
                echo "                        <div class=\"col-md-3\">
                            ";
                // line 14
                $this->loadTemplate((("Admin/Media/Browse/Items/" . $this->getAttribute($this->getAttribute($context["media"], "MediaType", array()), "Caption", array())) . ".twig"), "Admin/Media/GalleryWidgetBrowse.twig", 14)->display(array_merge($context, array("media" => $context["media"])));
                // line 15
                echo "                        </div>
                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['media'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "                </div>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "        </div>
    </div>

</div>
<div class=\"modal-footer\">
    <button type=\"button\" class=\"btn btn-default close-modal-button\">Close</button>
    <button type=\"button\" id=\"add-media-to-widget-button\" class=\"btn btn-primary\">Add Media</button>
</div>

<script type=\"text/javascript\">
    \$(\".close-modal-button\").on(\"click\", function () {
        \$(this).closest(\".modal\").modal(\"hide\");
    });

    \$(\"#add-media-to-widget-button\").on(\"click\", function () {
        var selectedImages = \$(\".media-widget-media-holder\").find(\".image-checkbox:checked\");
        selectedImages.each(function () {
            var mediaId = \$(this).val();
            var mediaSource = \$(this).next(\"img\").attr(\"src\");
            var mediaTitle = \$(this).next(\"img\").attr(\"alt\");

            var template = \$(\"#gallery-widget-template\").find(\".widget-sortable-unit\").first().clone();
            template.find(\"[data-media-id-input]\").val(mediaId);
            template.find(\"[data-media-picture-input]\").val(mediaSource);
            template.find(\"[data-media-title-input]\").val(mediaTitle);
            template.find(\"[data-media-image-img]\").attr(\"src\", mediaSource);
            template.find(\"[data-media-description-div]\").text(mediaTitle);
            \$(\"#media-list-holder\").append(template);
        });

        mediaWidgetReorganizeIndexes();

        \$(this).closest(\".modal\").modal(\"hide\");
    });
</script>

<div id=\"gallery-widget-template\" class=\"hidden\">
    <div class=\"container-fluid widget-sortable-unit\">
        <div class=\"hidden\">
            <input type=\"text\" value=\"\" data-media-id-input
                   name=\"\">
            <input type=\"text\" value=\"\" data-media-picture-input
                   name=\"\">
            <input type=\"text\" value=\"\" data-media-title-input
                   name=\"\">
        </div>
        <div class=\"row\">
            <div class=\"col-xs-2\">
                <img class=\"img-responsive\" data-media-image-img src=\"\" alt=\"\">
            </div>
            <div class=\"col-xs-8\" data-media-description-div>
            </div>
            <div class=\"col-xs-2 text-right\">
                <a class=\"btn btn-danger widget-media-delete\">
                    <i class=\"fa fa-times\"></i>
                </a>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "Admin/Media/GalleryWidgetBrowse.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 19,  86 => 17,  71 => 15,  69 => 14,  66 => 13,  49 => 12,  46 => 11,  29 => 10,  19 => 2,);
    }
}
/* {# @var model \MediaViewModel #}*/
/* <div class="modal-header">*/
/*     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>*/
/*     <h4 class="modal-title">Add Media</h4>*/
/* </div>*/
/* <div class="modal-body">*/
/* */
/*     <div class="media-widget-media-holder" style="height: 280px; overflow: auto;">*/
/*         <div class="container-fluid">*/
/*             {% for row in model.Media|batch(4) %}*/
/*                 <div class="row">*/
/*                     {% for media in row %}*/
/*                         <div class="col-md-3">*/
/*                             {% include "Admin/Media/Browse/Items/" ~ media.MediaType.Caption ~ ".twig" with {media: media} %}*/
/*                         </div>*/
/*                     {% endfor %}*/
/*                 </div>*/
/*             {% endfor %}*/
/*         </div>*/
/*     </div>*/
/* */
/* </div>*/
/* <div class="modal-footer">*/
/*     <button type="button" class="btn btn-default close-modal-button">Close</button>*/
/*     <button type="button" id="add-media-to-widget-button" class="btn btn-primary">Add Media</button>*/
/* </div>*/
/* */
/* <script type="text/javascript">*/
/*     $(".close-modal-button").on("click", function () {*/
/*         $(this).closest(".modal").modal("hide");*/
/*     });*/
/* */
/*     $("#add-media-to-widget-button").on("click", function () {*/
/*         var selectedImages = $(".media-widget-media-holder").find(".image-checkbox:checked");*/
/*         selectedImages.each(function () {*/
/*             var mediaId = $(this).val();*/
/*             var mediaSource = $(this).next("img").attr("src");*/
/*             var mediaTitle = $(this).next("img").attr("alt");*/
/* */
/*             var template = $("#gallery-widget-template").find(".widget-sortable-unit").first().clone();*/
/*             template.find("[data-media-id-input]").val(mediaId);*/
/*             template.find("[data-media-picture-input]").val(mediaSource);*/
/*             template.find("[data-media-title-input]").val(mediaTitle);*/
/*             template.find("[data-media-image-img]").attr("src", mediaSource);*/
/*             template.find("[data-media-description-div]").text(mediaTitle);*/
/*             $("#media-list-holder").append(template);*/
/*         });*/
/* */
/*         mediaWidgetReorganizeIndexes();*/
/* */
/*         $(this).closest(".modal").modal("hide");*/
/*     });*/
/* </script>*/
/* */
/* <div id="gallery-widget-template" class="hidden">*/
/*     <div class="container-fluid widget-sortable-unit">*/
/*         <div class="hidden">*/
/*             <input type="text" value="" data-media-id-input*/
/*                    name="">*/
/*             <input type="text" value="" data-media-picture-input*/
/*                    name="">*/
/*             <input type="text" value="" data-media-title-input*/
/*                    name="">*/
/*         </div>*/
/*         <div class="row">*/
/*             <div class="col-xs-2">*/
/*                 <img class="img-responsive" data-media-image-img src="" alt="">*/
/*             </div>*/
/*             <div class="col-xs-8" data-media-description-div>*/
/*             </div>*/
/*             <div class="col-xs-2 text-right">*/
/*                 <a class="btn btn-danger widget-media-delete">*/
/*                     <i class="fa fa-times"></i>*/
/*                 </a>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
