<?php

/* Admin/Builder/Widget/TextArea.twig */
class __TwigTemplate_7640f47ec3484b53b7f94eed939ed823f2105c1b9c251d847bb27c440b28a72d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Builder/Widget/Master/Panel.twig", "Admin/Builder/Widget/TextArea.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Builder/Widget/Master/Panel.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Text Area";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        $context["textareaId"] = twig_random($this->env);
        // line 7
        echo "    <textarea class=\"textarea\" id=\"textarea-";
        echo twig_escape_filter($this->env, (isset($context["textareaId"]) ? $context["textareaId"] : null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Options", array()), "content", array()), "html", null, true);
        echo "</textarea>

    <script type=\"text/javascript\">
        \$(document).ready(function () {
            \$(\"#textarea-";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["textareaId"]) ? $context["textareaId"] : null), "html", null, true);
        echo "\").summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['link', 'hr']],
                    ['misc', ['fullscreen', 'codeview', 'undo', 'redo']]
                ],
                callbacks: {
                    onBlur: function (e) {
                        var options = {
                            content: \$(\"#textarea-";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["textareaId"]) ? $context["textareaId"] : null), "html", null, true);
        echo "\").summernote('code')
                        };

                        \$(\"#textarea-";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["textareaId"]) ? $context["textareaId"] : null), "html", null, true);
        echo "\").closest(\"[data-widget]\").data(\"options\", options);
                    }
                }
            });
        });

    </script>
";
    }

    public function getTemplateName()
    {
        return "Admin/Builder/Widget/TextArea.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 28,  68 => 25,  51 => 11,  41 => 7,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Builder/Widget/Master/Panel.twig" %}*/
/* */
/* {% block title %}Text Area{% endblock %}*/
/* */
/* {% block content %}*/
/*     {% set textareaId = random() %}*/
/*     <textarea class="textarea" id="textarea-{{ textareaId }}">{{ model.Options.content }}</textarea>*/
/* */
/*     <script type="text/javascript">*/
/*         $(document).ready(function () {*/
/*             $("#textarea-{{ textareaId }}").summernote({*/
/*                 toolbar: [*/
/*                     ['style', ['bold', 'italic', 'underline', 'clear']],*/
/*                     ['font', ['strikethrough', 'superscript', 'subscript']],*/
/*                     ['fontsize', ['fontsize']],*/
/*                     ['color', ['color']],*/
/*                     ['para', ['ul', 'ol', 'paragraph']],*/
/*                     ['height', ['height']],*/
/*                     ['insert', ['link', 'hr']],*/
/*                     ['misc', ['fullscreen', 'codeview', 'undo', 'redo']]*/
/*                 ],*/
/*                 callbacks: {*/
/*                     onBlur: function (e) {*/
/*                         var options = {*/
/*                             content: $("#textarea-{{ textareaId }}").summernote('code')*/
/*                         };*/
/* */
/*                         $("#textarea-{{ textareaId }}").closest("[data-widget]").data("options", options);*/
/*                     }*/
/*                 }*/
/*             });*/
/*         });*/
/* */
/*     </script>*/
/* {% endblock %}*/
