<?php

/* Admin/Media/Browse/Radio/Picture.twig */
class __TwigTemplate_07fde84766147463be5067112bb321e915dd35118476bbdc5a03239e566b36cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<label class=\"gallery-widget-label\">
    <input type=\"radio\" name=\"media-widget-item\" class=\"image-checkbox hidden\" value=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["media"]) ? $context["media"] : null), "MediaId", array()), "html", null, true);
        echo "\">
    <img class=\"img-responsive\" src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["media"]) ? $context["media"] : null), "PictureSource", array(0 => "Medium"), "method"), "html", null, true);
        echo "\"
         alt=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["media"]) ? $context["media"] : null), "GetDescription", array(0 => $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "GetLanguage", array(), "method")), "method"), "Title", array()), "html", null, true);
        echo "\">
</label>
";
    }

    public function getTemplateName()
    {
        return "Admin/Media/Browse/Radio/Picture.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  26 => 3,  22 => 2,  19 => 1,);
    }
}
/* <label class="gallery-widget-label">*/
/*     <input type="radio" name="media-widget-item" class="image-checkbox hidden" value="{{ media.MediaId }}">*/
/*     <img class="img-responsive" src="{{ media.PictureSource("Medium") }}"*/
/*          alt="{{ media.GetDescription(Helper.GetLanguage()).Title }}">*/
/* </label>*/
/* */
