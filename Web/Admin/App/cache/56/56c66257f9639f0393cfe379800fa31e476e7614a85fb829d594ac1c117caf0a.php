<?php

/* Admin/Menus/Partial/Tree.twig */
class __TwigTemplate_4d936084a53cb3e62ff54ab60d541ca08d8938a249eafcd8259bf71924b749f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<li data-menu-item-id=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "MenuItemId", array()), "html", null, true);
        echo "\">
    <a class=\"confirm\" data-href=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "menus-delete-item", 1 => array("id" => $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "MenuItemId", array()))), "method"), "html", null, true);
        echo "\">
        <span class=\"label label-danger\"><i class=\"fa fa-times\"></i></span>
    </a>
    <a href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "menus-update-item", 1 => array("menuItemId" => $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "MenuItemId", array()))), "method"), "html", null, true);
        echo "\">
        <span class=\"label label-primary\"><i class=\"fa fa-pencil\"></i></span>
    </a>
    <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "menus-update-item", 1 => array("menuItemId" => $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "MenuItemId", array()))), "method"), "html", null, true);
        echo "\">
        ";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Caption", array()), "html", null, true);
        echo "
    </a>
</li>
";
        // line 12
        if (twig_length_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Children", array()))) {
            // line 13
            echo "
    <ul class=\"fa-ul menu-item-list\">
        ";
            // line 15
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Children", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 16
                echo "            ";
                $this->loadTemplate("Admin/Menus/Partial/Tree.twig", "Admin/Menus/Partial/Tree.twig", 16)->display(array_merge($context, array("model" => $context["child"])));
                // line 17
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 18
            echo "    </ul>
";
        }
        // line 20
        echo "
";
    }

    public function getTemplateName()
    {
        return "Admin/Menus/Partial/Tree.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 20,  86 => 18,  72 => 17,  69 => 16,  52 => 15,  48 => 13,  46 => 12,  40 => 9,  36 => 8,  30 => 5,  24 => 2,  19 => 1,);
    }
}
/* <li data-menu-item-id="{{ model.MenuItemId }}">*/
/*     <a class="confirm" data-href="{{ Router.Create("menus-delete-item", {id: model.MenuItemId}) }}">*/
/*         <span class="label label-danger"><i class="fa fa-times"></i></span>*/
/*     </a>*/
/*     <a href="{{ Router.Create("menus-update-item", {menuItemId: model.MenuItemId}) }}">*/
/*         <span class="label label-primary"><i class="fa fa-pencil"></i></span>*/
/*     </a>*/
/*     <a href="{{ Router.Create("menus-update-item", {menuItemId: model.MenuItemId}) }}">*/
/*         {{ model.Caption }}*/
/*     </a>*/
/* </li>*/
/* {% if model.Children|length %}*/
/* */
/*     <ul class="fa-ul menu-item-list">*/
/*         {% for child in model.Children %}*/
/*             {% include "Admin/Menus/Partial/Tree.twig" with {model: child} %}*/
/*         {% endfor %}*/
/*     </ul>*/
/* {% endif %}*/
/* */
/* */
