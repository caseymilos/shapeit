<?php

/* Admin/Bookings/Bookings.twig */
class __TwigTemplate_31b7d1509c63f6ae2a0fd03ed9064c73863740e93a92e8645aa433e2f5644a20 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Master.twig", "Admin/Bookings/Bookings.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'additional_styles' => array($this, 'block_additional_styles'),
            'post_scripts' => array($this, 'block_post_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Master.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "List of Bookings";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "
\t";
        // line 8
        echo "
\t<ol class=\"breadcrumb breadcrumb-quirk\">
\t\t<li>
\t\t\t<a href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "home"), "method"), "html", null, true);
        echo "\">
\t\t\t\t<i class=\"fa fa-home mr5\"></i>
\t\t\t\tHome
\t\t\t</a>
\t\t</li>
\t\t<li>
\t\t\t<a href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "booking-list"), "method"), "html", null, true);
        echo "\">Bookings</a>
\t\t</li>
\t\t<li class=\"active\">Bookings Directory</li>
\t</ol>

\t<div class=\"panel\">
\t\t<div class=\"panel-heading\">
\t\t\t<h4 class=\"panel-title\">Bookings List</h4>
\t\t</div>
\t\t<div class=\"panel-body\">
\t\t\t<div class=\"table-responsive\">
\t\t\t\t<table class=\"datatable table table-bordered table-striped-col\">
\t\t\t\t\t<thead>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>Offer</th>
\t\t\t\t\t\t\t<th>First Name</th>
\t\t\t\t\t\t\t<th>Last Name</th>
\t\t\t\t\t\t\t<th>Email</th>
\t\t\t\t\t\t\t<th>Phone number</th>
\t\t\t\t\t\t\t<th>Adults</th>
\t\t\t\t\t\t\t<th>Message</th>
\t\t\t\t\t\t\t<th>Departure</th>
\t\t\t\t\t\t\t<th>Return</th>
\t\t\t\t\t\t\t<th class=\"text-center\" style=\"width: 100px;\">Actions</th>

\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>

\t\t\t\t\t<tfoot>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>Offer</th>
\t\t\t\t\t\t\t<th>First Name</th>
\t\t\t\t\t\t\t<th>Last Name</th>
\t\t\t\t\t\t\t<th>Email</th>
\t\t\t\t\t\t\t<th>Phone number</th>
\t\t\t\t\t\t\t<th>Adults</th>
\t\t\t\t\t\t\t<th>Message</th>
\t\t\t\t\t\t\t<th>Departure</th>
\t\t\t\t\t\t\t<th>Return</th>
\t\t\t\t\t\t\t<th class=\"text-center\" style=\"width: 100px;\">Actions</th>

\t\t\t\t\t\t</tr>
\t\t\t\t\t</tfoot>

\t\t\t\t\t<tbody>
\t\t\t\t\t\t";
        // line 62
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Bookings", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["booking"]) {
            // line 63
            echo "
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td>";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["booking"], "Offer", array()), "Title", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td>";
            // line 66
            echo twig_escape_filter($this->env, $this->getAttribute($context["booking"], "FirstName", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td>";
            // line 67
            echo twig_escape_filter($this->env, $this->getAttribute($context["booking"], "LastName", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td>";
            // line 68
            echo twig_escape_filter($this->env, $this->getAttribute($context["booking"], "Email", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td>";
            // line 69
            echo twig_escape_filter($this->env, $this->getAttribute($context["booking"], "PhoneNumber", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td>";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute($context["booking"], "Adults", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td>";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute($context["booking"], "Message", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td>";
            // line 72
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["booking"], "DateFrom", array()), "format", array(0 => "Y-m-d"), "method"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td>";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["booking"], "DateTo", array()), "format", array(0 => "Y-m-d"), "method"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<ul class=\"table-options\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 77
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "bookings-thumbnail-crop", 1 => array("id" => $this->getAttribute($context["booking"], "BookingId", array()), "size" => "medium")), "method"), "html", null, true);
            echo "\"
\t\t\t\t\t\t\t\t\t\t\t   title=\"Crop thumbnail\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-crop\"></i>
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 83
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "update-booking", 1 => array("id" => $this->getAttribute($context["booking"], "BookingId", array()))), "method"), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a class=\"confirm\" style=\"cursor: pointer\"
\t\t\t\t\t\t\t\t\t\t\t   data-href=\"";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "delete-booking", 1 => array("id" => $this->getAttribute($context["booking"], "BookingId", array()))), "method"), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-trash\"></i>
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['booking'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 97
        echo "\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>
\t\t</div>
\t</div><!-- panel -->

";
    }

    // line 105
    public function block_additional_styles($context, array $blocks = array())
    {
        // line 106
        echo "\t<link rel=\"stylesheet\" href=\"Theme/PBS/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css\">
\t<link rel=\"stylesheet\" href=\"Theme/PBS/lib/select2/select2.css\">
";
    }

    // line 110
    public function block_post_scripts($context, array $blocks = array())
    {
        // line 111
        echo "\t<script src=\"Theme/PBS/lib/datatables/jquery.dataTables.js\"></script>
\t<script src=\"Theme/PBS/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js\"></script>
\t<script src=\"Theme/PBS/lib/select2/select2.js\"></script>

\t<script type=\"text/javascript\">
\t\t\$(document).ready(function () {
\t\t\t'use strict';
\t\t\t\$('.datatable').DataTable();

\t\t\t\$('select').select2();
\t\t});
\t</script>
";
    }

    public function getTemplateName()
    {
        return "Admin/Bookings/Bookings.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  206 => 111,  203 => 110,  197 => 106,  194 => 105,  184 => 97,  170 => 89,  161 => 83,  152 => 77,  145 => 73,  141 => 72,  137 => 71,  133 => 70,  129 => 69,  125 => 68,  121 => 67,  117 => 66,  113 => 65,  109 => 63,  105 => 62,  57 => 17,  48 => 11,  43 => 8,  40 => 6,  37 => 5,  31 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Master.twig" %}*/
/* */
/* {% block title %}List of Bookings{% endblock %}*/
/* */
/* {% block content %}*/
/* */
/* 	{# @var model \BookingsViewModel #}*/
/* */
/* 	<ol class="breadcrumb breadcrumb-quirk">*/
/* 		<li>*/
/* 			<a href="{{ Router.Create("home") }}">*/
/* 				<i class="fa fa-home mr5"></i>*/
/* 				Home*/
/* 			</a>*/
/* 		</li>*/
/* 		<li>*/
/* 			<a href="{{ Router.Create("booking-list") }}">Bookings</a>*/
/* 		</li>*/
/* 		<li class="active">Bookings Directory</li>*/
/* 	</ol>*/
/* */
/* 	<div class="panel">*/
/* 		<div class="panel-heading">*/
/* 			<h4 class="panel-title">Bookings List</h4>*/
/* 		</div>*/
/* 		<div class="panel-body">*/
/* 			<div class="table-responsive">*/
/* 				<table class="datatable table table-bordered table-striped-col">*/
/* 					<thead>*/
/* 						<tr>*/
/* 							<th>Offer</th>*/
/* 							<th>First Name</th>*/
/* 							<th>Last Name</th>*/
/* 							<th>Email</th>*/
/* 							<th>Phone number</th>*/
/* 							<th>Adults</th>*/
/* 							<th>Message</th>*/
/* 							<th>Departure</th>*/
/* 							<th>Return</th>*/
/* 							<th class="text-center" style="width: 100px;">Actions</th>*/
/* */
/* 						</tr>*/
/* 					</thead>*/
/* */
/* 					<tfoot>*/
/* 						<tr>*/
/* 							<th>Offer</th>*/
/* 							<th>First Name</th>*/
/* 							<th>Last Name</th>*/
/* 							<th>Email</th>*/
/* 							<th>Phone number</th>*/
/* 							<th>Adults</th>*/
/* 							<th>Message</th>*/
/* 							<th>Departure</th>*/
/* 							<th>Return</th>*/
/* 							<th class="text-center" style="width: 100px;">Actions</th>*/
/* */
/* 						</tr>*/
/* 					</tfoot>*/
/* */
/* 					<tbody>*/
/* 						{% for booking in model.Bookings %}*/
/* */
/* 							<tr>*/
/* 								<td>{{ booking.Offer.Title }}</td>*/
/* 								<td>{{ booking.FirstName }}</td>*/
/* 								<td>{{ booking.LastName }}</td>*/
/* 								<td>{{ booking.Email }}</td>*/
/* 								<td>{{ booking.PhoneNumber }}</td>*/
/* 								<td>{{ booking.Adults }}</td>*/
/* 								<td>{{ booking.Message }}</td>*/
/* 								<td>{{ booking.DateFrom.format("Y-m-d") }}</td>*/
/* 								<td>{{ booking.DateTo.format("Y-m-d") }}</td>*/
/* 								<td>*/
/* 									<ul class="table-options">*/
/* 										<li>*/
/* 											<a href="{{ Router.Create("bookings-thumbnail-crop", {id: booking.BookingId, size: 'medium'}) }}"*/
/* 											   title="Crop thumbnail">*/
/* 												<i class="fa fa-crop"></i>*/
/* 											</a>*/
/* 										</li>*/
/* 										<li>*/
/* 											<a href="{{ Router.Create("update-booking", {id: booking.BookingId}) }}">*/
/* 												<i class="fa fa-pencil"></i>*/
/* 											</a>*/
/* 										</li>*/
/* 										<li>*/
/* 											<a class="confirm" style="cursor: pointer"*/
/* 											   data-href="{{ Router.Create("delete-booking", {id: booking.BookingId}) }}">*/
/* 												<i class="fa fa-trash"></i>*/
/* 											</a>*/
/* 										</li>*/
/* 									</ul>*/
/* 								</td>*/
/* 							</tr>*/
/* 						{% endfor %}*/
/* 					</tbody>*/
/* 				</table>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div><!-- panel -->*/
/* */
/* {% endblock %}*/
/* */
/* {% block additional_styles %}*/
/* 	<link rel="stylesheet" href="Theme/PBS/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css">*/
/* 	<link rel="stylesheet" href="Theme/PBS/lib/select2/select2.css">*/
/* {% endblock %}*/
/* */
/* {% block post_scripts %}*/
/* 	<script src="Theme/PBS/lib/datatables/jquery.dataTables.js"></script>*/
/* 	<script src="Theme/PBS/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>*/
/* 	<script src="Theme/PBS/lib/select2/select2.js"></script>*/
/* */
/* 	<script type="text/javascript">*/
/* 		$(document).ready(function () {*/
/* 			'use strict';*/
/* 			$('.datatable').DataTable();*/
/* */
/* 			$('select').select2();*/
/* 		});*/
/* 	</script>*/
/* {% endblock %}*/
