<?php

/* Admin/Shared/Header/Right/Languages.twig */
class __TwigTemplate_1940f1c59371bde6ee4c8590ecdb1d1be5c7ea224a41960daf60bed1208ce9f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"languagesPanel\" class=\"btn-group\">
    ";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "Languages", array(), "method"));
        foreach ($context['_seq'] as $context["id"] => $context["language"]) {
            // line 3
            echo "        <a ";
            if (($this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "GetLanguage", array(), "method") == $context["id"])) {
                echo "class=\"active\"";
            }
            echo " href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Route", array()), 1 => $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Parameters", array()), 2 => false, 3 => $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "enum", array(0 => "LanguageCodesEnum", 1 => $context["id"]), "method")), "method"), "html", null, true);
            echo "\">
            <img src=\"Theme/Admin/images/languages/";
            // line 4
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "enum", array(0 => "LanguageCodesEnum", 1 => $context["id"]), "method"), "html", null, true);
            echo ".png\"
                 style=\"width: 21px;\"/>
        </a>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "Admin/Shared/Header/Right/Languages.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 8,  35 => 4,  26 => 3,  22 => 2,  19 => 1,);
    }
}
/* <div id="languagesPanel" class="btn-group">*/
/*     {% for id, language in SelectableHelper.Languages() %}*/
/*         <a {% if Helper.GetLanguage() == id %}class="active"{% endif %} href="{{ Router.Create(Router.Route, Router.Parameters, false, Helper.enum("LanguageCodesEnum", id)) }}">*/
/*             <img src="Theme/Admin/images/languages/{{ Helper.enum("LanguageCodesEnum", id) }}.png"*/
/*                  style="width: 21px;"/>*/
/*         </a>*/
/*     {% endfor %}*/
/* </div>*/
/* */
