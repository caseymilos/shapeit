<?php

/* Admin/Builder/Builder.twig */
class __TwigTemplate_be92fd040eaf0cedd0499d68fc34598ee88c28a6f1c57809e67159a0b1ca7223 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Master.twig", "Admin/Builder/Builder.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'post_scripts' => array($this, 'block_post_scripts'),
            'additional_styles' => array($this, 'block_additional_styles'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Master.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Page Builder";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "
    <script type=\"text/javascript\">
        var TWIG_OPTIONS = {
            layoutsUrl: \"";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "layout-options"), "method"), "html", null, true);
        echo "\"
        }
    </script>

    ";
        // line 13
        $context["page"] = $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Page", array());
        // line 14
        echo "
    <form action=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "page-builder-copy"), "method"), "html", null, true);
        echo "\" method=\"POST\">
        <div class=\"row\">
            <div class=\"col-md-6\">
                <div class=\"panel panel-danger\">
                    <div class=\"panel-heading\">
                        <h4 class=\"panel-title\">Copy content</h4>
                    </div>
                    <div class=\"panel-body\">
                        <label>Copy content from other version of this page</label>
                        <div class=\"row\">
                            <div class=\"col-xs-9\">
                                <select class=\"form-control select2\" name=\"language\">
                                    ";
        // line 27
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "Details", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 28
            echo "                                        ";
            if (($this->getAttribute($context["p"], "LanguageId", array()) != $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "GetLanguage", array(), "method"))) {
                // line 29
                echo "                                            <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "LanguageId", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "Enum", array(0 => "LanguagesEnum", 1 => $this->getAttribute($context["p"], "LanguageId", array())), "method"), "html", null, true);
                echo "</option>
                                        ";
            }
            // line 31
            echo "                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "                                </select>
                            </div>
                            <div class=\"col-xs-3 text-right\">
                                <button class=\"btn btn-success\">Copy</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input type=\"hidden\" name=\"pageId\" value=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "PageId", array()), "html", null, true);
        echo "\"/>
    </form>

    <div id=\"page-id\" class=\"hidden\">";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "PageId", array()), "html", null, true);
        echo "</div>
    <form id=\"page-basic-data\" enctype=\"multipart/form-data\" action=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "page-builder-save"), "method"), "html", null, true);
        echo "\">
        <div class=\"row\">
            <div class=\"col-md-6\">
                <div class=\"panel panel-inverse\">
                    <div class=\"panel-heading\">
                        <h4 class=\"panel-title\">Page Details</h4>
                    </div>
                    <div class=\"panel-body\">
                        <div class=\"row\">
                            <div class=\"col-xs-9\">
                                <label>Slug
                                    <span class=\"text-danger\">*</span>
                                </label>
                                <input type=\"text\" placeholder=\"Slug\" name=\"slug\" class=\"form-control\" required
                                       value=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "GetDescription", array(0 => $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "GetLanguage", array(), "method")), "method"), "Slug", array()), "html", null, true);
        echo "\"/>

                            </div>
                            <div class=\"col-xs-3 text-center\">
                                ";
        // line 65
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "Checkbox", array(0 => "active", 1 => $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "Active", array()), 2 => "Active"), "method");
        echo "
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6\">
                <div class=\"panel panel-inverse\">
                    <div class=\"panel-heading\">
                        <h4 class=\"panel-title\">Appearance</h4>
                    </div>
                    <div class=\"panel-body\">
                        <div class=\"row\">
                            <div class=\"col-xs-6\">
                                <label>Template</label>
                                ";
        // line 80
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => $this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "Templates", array(), "method"), 1 => "template", 2 => $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "Template", array()), 3 => "Select Template"), "method");
        echo "
                            </div>
                            <div class=\"col-xs-3 text-center\">
                                ";
        // line 83
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "Checkbox", array(0 => "useSidebar", 1 => $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "UseSidebar", array()), 2 => "Use sidebar"), "method");
        echo "
                            </div>
                            <div class=\"col-xs-3\">
                                <label>Header Padding</label>
                                <input type=\"text\" name=\"padding\" class=\"form-control\" placeholder=\"Padding (px)\" value=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Page", array()), "Padding", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div class=\"row\" style=\"margin-top: 10px;\">
                            <div class=\"col-xs-3\">
                                ";
        // line 92
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "Checkbox", array(0 => "showTitle", 1 => (((null === $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "ShowTitle", array()))) ? (true) : ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "ShowTitle", array()))), 2 => "Show Title"), "method");
        echo "
                            </div>
                            <div class=\"col-xs-3\">
                                ";
        // line 95
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "Checkbox", array(0 => "showBreadcrumbs", 1 => (((null === $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "ShowBreadcrumbs", array()))) ? (true) : ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "ShowBreadcrumbs", array()))), 2 => "Show Breadcrumbs"), "method");
        echo "
                            </div>
                            <div class=\"col-xs-6\">
                                <label>Header Style</label>
                                ";
        // line 99
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => $this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "HeaderStyles", array(), "method"), 1 => "headerStyle", 2 => (((null === $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "HeaderStyle", array()))) ? ("default") : ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "HeaderStyle", array()))), 3 => "Select Header Style"), "method");
        echo "
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class=\"row\">
            <div class=\"col-md-12\">
                <div class=\"panel panel-inverse\">
                    <div class=\"panel-heading\">
                        <h4 class=\"panel-title\">Header</h4>
                    </div>
                    <label class=\"picture-mask text-center\">
                        <div class=\"image-input image-placeholder\"
                             style=\"height: 350px; background-image: url(";
        // line 115
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "PictureSource", array(), "method"), "html", null, true);
        echo ")\"></div>
                        <input style=\"display: none;\" class=\"upload-picture-field\" type=\"file\" name=\"picture\"/>
                    </label>

                    <div class=\"builder-title-area\">
                        <input type=\"text\" placeholder=\"Title\" name=\"title\" class=\"form-control\" required
                               value=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "GetDescription", array(0 => $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "GetLanguage", array(), "method")), "method"), "Title", array()), "html", null, true);
        echo "\"/>
                        <input type=\"text\" placeholder=\"Subtitle\" name=\"subtitle\" class=\"form-control subtitle\"
                               value=\"";
        // line 123
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "GetDescription", array(0 => $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "GetLanguage", array(), "method")), "method"), "Subtitle", array()), "html", null, true);
        echo "\"/>
                        <input type=\"text\" placeholder=\"Copyrights\" name=\"copyrights\"
                               class=\"form-control builder-copyrights\"
                               value=\"";
        // line 126
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "GetDescription", array(0 => $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "GetLanguage", array(), "method")), "method"), "Copyrights", array()), "html", null, true);
        echo "\"/>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class=\"row\">
        <div class=\"col-md-3 col-lg-2 sticky\">
            <aside class=\"editor-menu\">
                <div class=\"panel-group panel-product\" id=\"accordion2\">
                    <div class=\"panel panel-inverse\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">
                                <a data-toggle=\"collapse\" data-parent=\"#accordion2\" href=\"#layout-picker\">
                                    Layout
                                </a>
                            </h4>
                        </div>
                        <div id=\"layout-picker\" class=\"panel-collapse collapse in\">
                            <div class=\"panel-body\">
                                <ul class=\"media-list user-list\">
                                    ";
        // line 148
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "BuilderLayouts", array(), "method"));
        foreach ($context['_seq'] as $context["id"] => $context["layout"]) {
            // line 149
            echo "                                        <li class=\"media\" data-new-layout=\"";
            echo twig_escape_filter($this->env, $context["id"], "html", null, true);
            echo "\">
                                            <div class=\"media-left\">
                                                <img class=\"media-object img-circle\" src=\"http://placehold.it/100x100\"
                                                     alt=\"\">
                                            </div>
                                            <div class=\"media-body\">
                                                <h4 class=\"media-heading\">
                                                    <a>";
            // line 156
            echo twig_escape_filter($this->env, $context["layout"], "html", null, true);
            echo "</a>
                                                </h4>
                                                ";
            // line 158
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "LayoutSubtitle", array(0 => $context["id"]), "method"), "html", null, true);
            echo "
                                            </div>
                                        </li>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['layout'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 162
        echo "                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class=\"panel panel-inverse\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">
                                <a data-toggle=\"collapse\" class=\"collapsed\" data-parent=\"#accordion2\"
                                   href=\"#typography-picker\">
                                    Typography
                                </a>
                            </h4>
                        </div>
                        <div id=\"typography-picker\" class=\"panel-collapse collapse\">
                            <div class=\"panel-body\">
                                <ul class=\"media-list user-list\">
                                    ";
        // line 178
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "Typography", array(), "method"));
        foreach ($context['_seq'] as $context["id"] => $context["widget"]) {
            // line 179
            echo "                                        <li class=\"media\" data-new-widget=\"";
            echo twig_escape_filter($this->env, $context["id"], "html", null, true);
            echo "\">
                                            <div class=\"media-left\">
                                                <img class=\"media-object img-circle\" src=\"http://placehold.it/100x100\"
                                                     alt=\"\">
                                            </div>
                                            <div class=\"media-body\">
                                                <h4 class=\"media-heading\">
                                                    <a>";
            // line 186
            echo twig_escape_filter($this->env, $context["widget"], "html", null, true);
            echo "</a>
                                                </h4>
                                            </div>
                                        </li>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['widget'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 191
        echo "                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class=\"panel panel-inverse\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">
                                <a data-toggle=\"collapse\" class=\"collapsed\" data-parent=\"#accordion2\"
                                   href=\"#widget-picker\">
                                    Widgets
                                </a>
                            </h4>
                        </div>
                        <div id=\"widget-picker\" class=\"panel-collapse collapse\">
                            <div class=\"panel-body\">
                                <ul class=\"media-list user-list\">
                                    ";
        // line 207
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "Widgets", array(), "method"));
        foreach ($context['_seq'] as $context["id"] => $context["widget"]) {
            // line 208
            echo "                                        <li class=\"media\" data-new-widget=\"";
            echo twig_escape_filter($this->env, $context["id"], "html", null, true);
            echo "\">
                                            <div class=\"media-left\">
                                                <img class=\"media-object img-circle\" src=\"http://placehold.it/100x100\"
                                                     alt=\"\">
                                            </div>
                                            <div class=\"media-body\">
                                                <h4 class=\"media-heading\">
                                                    <a>";
            // line 215
            echo twig_escape_filter($this->env, $context["widget"], "html", null, true);
            echo "</a>
                                                </h4>
                                            </div>
                                        </li>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['widget'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 220
        echo "                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
        </div>
        <div class=\"col-lg-10 col-md-9\">
            <section class=\"editor-content\">
                <div class=\"panel panel-inverse\">
                    <div class=\"panel-heading\">
                        <h4 class=\"panel-title\">Content</h4>
                    </div>
                    <div class=\"panel-body\" data-panel>
                        ";
        // line 234
        echo (isset($context["content"]) ? $context["content"] : null);
        echo "
                    </div>
                </div>
            </section>

            <button id=\"save-page\" class=\"btn btn-block btn-success\">Save</button>
        </div>
    </div>

    <div class=\"modal bounceIn animated\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"
         aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                    <h4 class=\"modal-title\" id=\"myModalLabel\">Edit Widget</h4>
                </div>
                <div class=\"modal-body\">
                    <form id=\"widget-form\" action=\"\">
                        <div id=\"widget-options-holder\"></div>
                    </form>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>
                    <button type=\"button\" id=\"save-widget-button\" class=\"btn btn-primary\">Save Widget</button>
                </div>
            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <div class=\"modal bounceIn animated\" id=\"layoutOptions\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"layoutOptionsLabel\"
         aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                    <h4 class=\"modal-title\" id=\"layoutOptionsLabel\">Layout Options</h4>
                </div>
                <div class=\"modal-body\">
                    <form id=\"layout-form\" action=\"\">
                        <div id=\"layout-options-holder\"></div>
                    </form>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>
                    <button type=\"button\" id=\"save-layout-button\" class=\"btn btn-primary\">Save</button>
                </div>
            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->

";
    }

    // line 287
    public function block_post_scripts($context, array $blocks = array())
    {
        // line 288
        echo "    <script src=\"Theme/Admin/lib/jquery-bbq/jquery.ba-bbq.min.js\"></script>
    <script src=\"Theme/Admin/lib/bootstrapcolorpicker/js/bootstrap-colorpicker.js\"></script>
    <script src=\"Theme/Admin/js/builder.js\"></script>
";
    }

    // line 293
    public function block_additional_styles($context, array $blocks = array())
    {
        // line 294
        echo "    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/jquery-ui/jquery-ui.css\">
    <link rel=\"stylesheet\" href=\"Theme/Admin/css/page-builder.css\">
    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/bootstrapcolorpicker/css/bootstrap-colorpicker.css\">
";
    }

    public function getTemplateName()
    {
        return "Admin/Builder/Builder.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  459 => 294,  456 => 293,  449 => 288,  446 => 287,  390 => 234,  374 => 220,  363 => 215,  352 => 208,  348 => 207,  330 => 191,  319 => 186,  308 => 179,  304 => 178,  286 => 162,  276 => 158,  271 => 156,  260 => 149,  256 => 148,  231 => 126,  225 => 123,  220 => 121,  211 => 115,  192 => 99,  185 => 95,  179 => 92,  171 => 87,  164 => 83,  158 => 80,  140 => 65,  133 => 61,  116 => 47,  112 => 46,  106 => 43,  93 => 32,  87 => 31,  79 => 29,  76 => 28,  72 => 27,  57 => 15,  54 => 14,  52 => 13,  45 => 9,  40 => 6,  37 => 5,  31 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Master.twig" %}*/
/* */
/* {% block title %}Page Builder{% endblock %}*/
/* */
/* {% block content %}*/
/* */
/*     <script type="text/javascript">*/
/*         var TWIG_OPTIONS = {*/
/*             layoutsUrl: "{{ Router.Create("layout-options") }}"*/
/*         }*/
/*     </script>*/
/* */
/*     {% set page = model.Page %}*/
/* */
/*     <form action="{{ Router.Create("page-builder-copy") }}" method="POST">*/
/*         <div class="row">*/
/*             <div class="col-md-6">*/
/*                 <div class="panel panel-danger">*/
/*                     <div class="panel-heading">*/
/*                         <h4 class="panel-title">Copy content</h4>*/
/*                     </div>*/
/*                     <div class="panel-body">*/
/*                         <label>Copy content from other version of this page</label>*/
/*                         <div class="row">*/
/*                             <div class="col-xs-9">*/
/*                                 <select class="form-control select2" name="language">*/
/*                                     {% for p in page.Details %}*/
/*                                         {% if(p.LanguageId != Helper.GetLanguage()) %}*/
/*                                             <option value="{{ p.LanguageId }}">{{ Helper.Enum("LanguagesEnum", p.LanguageId) }}</option>*/
/*                                         {% endif %}*/
/*                                     {% endfor %}*/
/*                                 </select>*/
/*                             </div>*/
/*                             <div class="col-xs-3 text-right">*/
/*                                 <button class="btn btn-success">Copy</button>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <input type="hidden" name="pageId" value="{{ page.PageId }}"/>*/
/*     </form>*/
/* */
/*     <div id="page-id" class="hidden">{{ page.PageId }}</div>*/
/*     <form id="page-basic-data" enctype="multipart/form-data" action="{{ Router.Create("page-builder-save") }}">*/
/*         <div class="row">*/
/*             <div class="col-md-6">*/
/*                 <div class="panel panel-inverse">*/
/*                     <div class="panel-heading">*/
/*                         <h4 class="panel-title">Page Details</h4>*/
/*                     </div>*/
/*                     <div class="panel-body">*/
/*                         <div class="row">*/
/*                             <div class="col-xs-9">*/
/*                                 <label>Slug*/
/*                                     <span class="text-danger">*</span>*/
/*                                 </label>*/
/*                                 <input type="text" placeholder="Slug" name="slug" class="form-control" required*/
/*                                        value="{{ page.GetDescription(Helper.GetLanguage()).Slug }}"/>*/
/* */
/*                             </div>*/
/*                             <div class="col-xs-3 text-center">*/
/*                                 {{ HtmlHelper.Checkbox("active", page.Active, "Active")|raw }}*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="col-md-6">*/
/*                 <div class="panel panel-inverse">*/
/*                     <div class="panel-heading">*/
/*                         <h4 class="panel-title">Appearance</h4>*/
/*                     </div>*/
/*                     <div class="panel-body">*/
/*                         <div class="row">*/
/*                             <div class="col-xs-6">*/
/*                                 <label>Template</label>*/
/*                                 {{ HtmlHelper.SearchableSelect(SelectableHelper.Templates(), 'template', page.Template, "Select Template")|raw }}*/
/*                             </div>*/
/*                             <div class="col-xs-3 text-center">*/
/*                                 {{ HtmlHelper.Checkbox("useSidebar", page.UseSidebar, "Use sidebar")|raw }}*/
/*                             </div>*/
/*                             <div class="col-xs-3">*/
/*                                 <label>Header Padding</label>*/
/*                                 <input type="text" name="padding" class="form-control" placeholder="Padding (px)" value="{{ model.Page.Padding }}">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="row" style="margin-top: 10px;">*/
/*                             <div class="col-xs-3">*/
/*                                 {{ HtmlHelper.Checkbox("showTitle", page.ShowTitle is null ? true : page.ShowTitle, "Show Title")|raw }}*/
/*                             </div>*/
/*                             <div class="col-xs-3">*/
/*                                 {{ HtmlHelper.Checkbox("showBreadcrumbs", page.ShowBreadcrumbs is null ? true : page.ShowBreadcrumbs, "Show Breadcrumbs")|raw }}*/
/*                             </div>*/
/*                             <div class="col-xs-6">*/
/*                                 <label>Header Style</label>*/
/*                                 {{ HtmlHelper.SearchableSelect(SelectableHelper.HeaderStyles(), 'headerStyle', page.HeaderStyle is null ? "default" : page.HeaderStyle, "Select Header Style")|raw }}*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="row">*/
/*             <div class="col-md-12">*/
/*                 <div class="panel panel-inverse">*/
/*                     <div class="panel-heading">*/
/*                         <h4 class="panel-title">Header</h4>*/
/*                     </div>*/
/*                     <label class="picture-mask text-center">*/
/*                         <div class="image-input image-placeholder"*/
/*                              style="height: 350px; background-image: url({{ page.PictureSource() }})"></div>*/
/*                         <input style="display: none;" class="upload-picture-field" type="file" name="picture"/>*/
/*                     </label>*/
/* */
/*                     <div class="builder-title-area">*/
/*                         <input type="text" placeholder="Title" name="title" class="form-control" required*/
/*                                value="{{ page.GetDescription(Helper.GetLanguage()).Title }}"/>*/
/*                         <input type="text" placeholder="Subtitle" name="subtitle" class="form-control subtitle"*/
/*                                value="{{ page.GetDescription(Helper.GetLanguage()).Subtitle }}"/>*/
/*                         <input type="text" placeholder="Copyrights" name="copyrights"*/
/*                                class="form-control builder-copyrights"*/
/*                                value="{{ page.GetDescription(Helper.GetLanguage()).Copyrights }}"/>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </form>*/
/* */
/*     <div class="row">*/
/*         <div class="col-md-3 col-lg-2 sticky">*/
/*             <aside class="editor-menu">*/
/*                 <div class="panel-group panel-product" id="accordion2">*/
/*                     <div class="panel panel-inverse">*/
/*                         <div class="panel-heading">*/
/*                             <h4 class="panel-title">*/
/*                                 <a data-toggle="collapse" data-parent="#accordion2" href="#layout-picker">*/
/*                                     Layout*/
/*                                 </a>*/
/*                             </h4>*/
/*                         </div>*/
/*                         <div id="layout-picker" class="panel-collapse collapse in">*/
/*                             <div class="panel-body">*/
/*                                 <ul class="media-list user-list">*/
/*                                     {% for id, layout in SelectableHelper.BuilderLayouts() %}*/
/*                                         <li class="media" data-new-layout="{{ id }}">*/
/*                                             <div class="media-left">*/
/*                                                 <img class="media-object img-circle" src="http://placehold.it/100x100"*/
/*                                                      alt="">*/
/*                                             </div>*/
/*                                             <div class="media-body">*/
/*                                                 <h4 class="media-heading">*/
/*                                                     <a>{{ layout }}</a>*/
/*                                                 </h4>*/
/*                                                 {{ Helper.LayoutSubtitle(id) }}*/
/*                                             </div>*/
/*                                         </li>*/
/*                                     {% endfor %}*/
/*                                 </ul>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="panel panel-inverse">*/
/*                         <div class="panel-heading">*/
/*                             <h4 class="panel-title">*/
/*                                 <a data-toggle="collapse" class="collapsed" data-parent="#accordion2"*/
/*                                    href="#typography-picker">*/
/*                                     Typography*/
/*                                 </a>*/
/*                             </h4>*/
/*                         </div>*/
/*                         <div id="typography-picker" class="panel-collapse collapse">*/
/*                             <div class="panel-body">*/
/*                                 <ul class="media-list user-list">*/
/*                                     {% for id, widget in SelectableHelper.Typography() %}*/
/*                                         <li class="media" data-new-widget="{{ id }}">*/
/*                                             <div class="media-left">*/
/*                                                 <img class="media-object img-circle" src="http://placehold.it/100x100"*/
/*                                                      alt="">*/
/*                                             </div>*/
/*                                             <div class="media-body">*/
/*                                                 <h4 class="media-heading">*/
/*                                                     <a>{{ widget }}</a>*/
/*                                                 </h4>*/
/*                                             </div>*/
/*                                         </li>*/
/*                                     {% endfor %}*/
/*                                 </ul>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="panel panel-inverse">*/
/*                         <div class="panel-heading">*/
/*                             <h4 class="panel-title">*/
/*                                 <a data-toggle="collapse" class="collapsed" data-parent="#accordion2"*/
/*                                    href="#widget-picker">*/
/*                                     Widgets*/
/*                                 </a>*/
/*                             </h4>*/
/*                         </div>*/
/*                         <div id="widget-picker" class="panel-collapse collapse">*/
/*                             <div class="panel-body">*/
/*                                 <ul class="media-list user-list">*/
/*                                     {% for id, widget in SelectableHelper.Widgets() %}*/
/*                                         <li class="media" data-new-widget="{{ id }}">*/
/*                                             <div class="media-left">*/
/*                                                 <img class="media-object img-circle" src="http://placehold.it/100x100"*/
/*                                                      alt="">*/
/*                                             </div>*/
/*                                             <div class="media-body">*/
/*                                                 <h4 class="media-heading">*/
/*                                                     <a>{{ widget }}</a>*/
/*                                                 </h4>*/
/*                                             </div>*/
/*                                         </li>*/
/*                                     {% endfor %}*/
/*                                 </ul>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </aside>*/
/*         </div>*/
/*         <div class="col-lg-10 col-md-9">*/
/*             <section class="editor-content">*/
/*                 <div class="panel panel-inverse">*/
/*                     <div class="panel-heading">*/
/*                         <h4 class="panel-title">Content</h4>*/
/*                     </div>*/
/*                     <div class="panel-body" data-panel>*/
/*                         {{ content|raw }}*/
/*                     </div>*/
/*                 </div>*/
/*             </section>*/
/* */
/*             <button id="save-page" class="btn btn-block btn-success">Save</button>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <div class="modal bounceIn animated" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"*/
/*          aria-hidden="true">*/
/*         <div class="modal-dialog">*/
/*             <div class="modal-content">*/
/*                 <div class="modal-header">*/
/*                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>*/
/*                     <h4 class="modal-title" id="myModalLabel">Edit Widget</h4>*/
/*                 </div>*/
/*                 <div class="modal-body">*/
/*                     <form id="widget-form" action="">*/
/*                         <div id="widget-options-holder"></div>*/
/*                     </form>*/
/*                 </div>*/
/*                 <div class="modal-footer">*/
/*                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>*/
/*                     <button type="button" id="save-widget-button" class="btn btn-primary">Save Widget</button>*/
/*                 </div>*/
/*             </div><!-- modal-content -->*/
/*         </div><!-- modal-dialog -->*/
/*     </div><!-- modal -->*/
/* */
/*     <div class="modal bounceIn animated" id="layoutOptions" tabindex="-1" role="dialog" aria-labelledby="layoutOptionsLabel"*/
/*          aria-hidden="true">*/
/*         <div class="modal-dialog">*/
/*             <div class="modal-content">*/
/*                 <div class="modal-header">*/
/*                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>*/
/*                     <h4 class="modal-title" id="layoutOptionsLabel">Layout Options</h4>*/
/*                 </div>*/
/*                 <div class="modal-body">*/
/*                     <form id="layout-form" action="">*/
/*                         <div id="layout-options-holder"></div>*/
/*                     </form>*/
/*                 </div>*/
/*                 <div class="modal-footer">*/
/*                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>*/
/*                     <button type="button" id="save-layout-button" class="btn btn-primary">Save</button>*/
/*                 </div>*/
/*             </div><!-- modal-content -->*/
/*         </div><!-- modal-dialog -->*/
/*     </div><!-- modal -->*/
/* */
/* {% endblock %}*/
/* */
/* {% block post_scripts %}*/
/*     <script src="Theme/Admin/lib/jquery-bbq/jquery.ba-bbq.min.js"></script>*/
/*     <script src="Theme/Admin/lib/bootstrapcolorpicker/js/bootstrap-colorpicker.js"></script>*/
/*     <script src="Theme/Admin/js/builder.js"></script>*/
/* {% endblock %}*/
/* */
/* {% block additional_styles %}*/
/*     <link rel="stylesheet" href="Theme/Admin/lib/jquery-ui/jquery-ui.css">*/
/*     <link rel="stylesheet" href="Theme/Admin/css/page-builder.css">*/
/*     <link rel="stylesheet" href="Theme/Admin/lib/bootstrapcolorpicker/css/bootstrap-colorpicker.css">*/
/* {% endblock %}*/
