<?php

/* Admin/Pages/WidgetOptions/PageList.twig */
class __TwigTemplate_5379d45a91b6099e5bf86f130c82c6a5bcc8d0315e4a09852bd8a85420008ce0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-group\">
    <label>Number of items in row</label>
    ";
        // line 3
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "Select", array(0 => array(4 => 4, 5 => 5, 6 => 6), 1 => "items", 2 => $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "items", array()), 3 => "Items in row"), "method");
        echo "
</div>

<h5>Pages</h5>

<div id=\"blog-pages-list-holder\">
    ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "pages", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
            // line 10
            echo "        <div class=\"container-fluid widget-sortable-unit\">
            <div class=\"row\">
                <div class=\"col-xs-10\">
                    ";
            // line 13
            echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => $this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "Pages", array(), "method"), 1 => (("pages[" . $this->getAttribute($context["loop"], "index0", array())) . "]"), 2 => $context["page"]), "method");
            echo "
                </div>
                <div class=\"col-xs-2 text-right\">
                    <a class=\"btn btn-danger widget-blog-page-delete\">
                        <i class=\"fa fa-times\"></i>
                    </a>
                </div>
            </div>
        </div>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "</div>

<hr>

<a class=\"add-blog-page-widget-button btn btn-block btn-success\">Add Page</a>

<div class=\"hidden\">
    <div id=\"pages-select\">
        ";
        // line 31
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => $this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "Pages", array(), "method"), 1 => ""), "method");
        echo "
    </div>
</div>

<script type=\"text/javascript\">
    function blogPageWidgetReorganizeIndexes() {
        // arrange all indexes
        var artistElements = \$(\"#blog-pages-list-holder > .container-fluid\");
        var i = 0;
        artistElements.each(function () {
            var inputs = \$(this).find(\"select\");
            \$(inputs[0]).attr(\"name\", \"pages[\" + i + \"]\");
            i++;
        });

        return i;
    }


    \$(\".add-blog-page-widget-button\").on(\"click\", function () {
        var i = blogPageWidgetReorganizeIndexes();

        var newEl = \$(\"<div>\").addClass(\"container-fluid widget-sortable-unit\");
        var select = \$(\"#pages-select\").find(\"select\").clone();
        newEl.append(
                \$(\"<div>\").addClass(\"row\")
                        .append(\$(\"<div>\").addClass(\"col-xs-10\").append(select.attr(\"name\", \"pages[\" + i + \"]\")))
                        .append(\$(\"<div>\").addClass(\"col-xs-2 text-right\").append(\$(\"<a>\").addClass(\"btn btn-danger widget-artists-delete\").append(\$(\"<i>\").addClass(\"fa fa-times\"))))
        );

        \$(\"#blog-pages-list-holder\").append(newEl);
    });

    \$(\"#blog-pages-list-holder\").on(\"click\", \".widget-blog-page-delete\", function () {
        \$(this).closest(\".container-fluid\").remove();
        blogPageWidgetReorganizeIndexes()
    });

    \$(\"#blog-pages-list-holder\").sortable({
        update: function () {
            blogPageWidgetReorganizeIndexes();
        }
    });

</script>";
    }

    public function getTemplateName()
    {
        return "Admin/Pages/WidgetOptions/PageList.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 31,  78 => 23,  54 => 13,  49 => 10,  32 => 9,  23 => 3,  19 => 1,);
    }
}
/* <div class="form-group">*/
/*     <label>Number of items in row</label>*/
/*     {{ HtmlHelper.Select({4:4, 5:5, 6:6}, 'items', widget.Options.items, "Items in row")|raw }}*/
/* </div>*/
/* */
/* <h5>Pages</h5>*/
/* */
/* <div id="blog-pages-list-holder">*/
/*     {% for page in widget.Options.pages %}*/
/*         <div class="container-fluid widget-sortable-unit">*/
/*             <div class="row">*/
/*                 <div class="col-xs-10">*/
/*                     {{ HtmlHelper.SearchableSelect(SelectableHelper.Pages(), "pages[" ~ loop.index0 ~ "]", page)|raw }}*/
/*                 </div>*/
/*                 <div class="col-xs-2 text-right">*/
/*                     <a class="btn btn-danger widget-blog-page-delete">*/
/*                         <i class="fa fa-times"></i>*/
/*                     </a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     {% endfor %}*/
/* </div>*/
/* */
/* <hr>*/
/* */
/* <a class="add-blog-page-widget-button btn btn-block btn-success">Add Page</a>*/
/* */
/* <div class="hidden">*/
/*     <div id="pages-select">*/
/*         {{ HtmlHelper.SearchableSelect(SelectableHelper.Pages(), "")|raw }}*/
/*     </div>*/
/* </div>*/
/* */
/* <script type="text/javascript">*/
/*     function blogPageWidgetReorganizeIndexes() {*/
/*         // arrange all indexes*/
/*         var artistElements = $("#blog-pages-list-holder > .container-fluid");*/
/*         var i = 0;*/
/*         artistElements.each(function () {*/
/*             var inputs = $(this).find("select");*/
/*             $(inputs[0]).attr("name", "pages[" + i + "]");*/
/*             i++;*/
/*         });*/
/* */
/*         return i;*/
/*     }*/
/* */
/* */
/*     $(".add-blog-page-widget-button").on("click", function () {*/
/*         var i = blogPageWidgetReorganizeIndexes();*/
/* */
/*         var newEl = $("<div>").addClass("container-fluid widget-sortable-unit");*/
/*         var select = $("#pages-select").find("select").clone();*/
/*         newEl.append(*/
/*                 $("<div>").addClass("row")*/
/*                         .append($("<div>").addClass("col-xs-10").append(select.attr("name", "pages[" + i + "]")))*/
/*                         .append($("<div>").addClass("col-xs-2 text-right").append($("<a>").addClass("btn btn-danger widget-artists-delete").append($("<i>").addClass("fa fa-times"))))*/
/*         );*/
/* */
/*         $("#blog-pages-list-holder").append(newEl);*/
/*     });*/
/* */
/*     $("#blog-pages-list-holder").on("click", ".widget-blog-page-delete", function () {*/
/*         $(this).closest(".container-fluid").remove();*/
/*         blogPageWidgetReorganizeIndexes()*/
/*     });*/
/* */
/*     $("#blog-pages-list-holder").sortable({*/
/*         update: function () {*/
/*             blogPageWidgetReorganizeIndexes();*/
/*         }*/
/*     });*/
/* */
/* </script>*/
