<?php

/* Admin/Pages/WidgetOptions/PromoBox.twig */
class __TwigTemplate_995cb06be8594d20bf837817223f2f8f837f8a0aaee5844db79662f49e5b0e1d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h5>Promo Box</h5>

<div class=\"form-group\">
    <label>Title</label>
    <input type=\"text\" placeholder=\"Title\" name=\"title\" class=\"form-control\"
           value=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "title", array()), "html", null, true);
        echo "\"/>
</div>

<div class=\"form-group\">
    <label>Subtitle</label>
    <input type=\"text\" placeholder=\"Subtitle\" name=\"subtitle\" class=\"form-control\"
           value=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "subtitle", array()), "html", null, true);
        echo "\"/>
</div>

<div class=\"form-group\">
    <label>Button text</label>
    <input type=\"text\" placeholder=\"Button text\" name=\"buttonText\" class=\"form-control\"
           value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "buttonText", array()), "html", null, true);
        echo "\"/>
</div>

<div class=\"form-group\">
    <label>Button url</label>
    <input type=\"text\" placeholder=\"Button url\" name=\"buttonUrl\" class=\"form-control\"
           value=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "buttonUrl", array()), "html", null, true);
        echo "\"/>
</div>

<div class=\"form-group\">
    <label>Button Positon</label>
    ";
        // line 29
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => array("left" => "Left", "center" => "Center", "right" => "Right"), 1 => "buttonPosition", 2 => $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "buttonPosition", array())), "method");
        echo "
</div>

<div class=\"form-group\">
    <label>Style</label>
    ";
        // line 34
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => array("white" => "White", "light" => "Light", "dark" => "Dark", "flat" => "Flat"), 1 => "style", 2 => $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "style", array())), "method");
        echo "
</div>

<div class=\"form-group\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <label class=\"ckbox\">
                <input type=\"checkbox\" name=\"border\"
                       ";
        // line 42
        if (($this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "border", array()) == "on")) {
            echo "checked=\"checked\"";
        }
        echo ">
                <span>Border</span>
            </label>
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "Admin/Pages/WidgetOptions/PromoBox.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 42,  69 => 34,  61 => 29,  53 => 24,  44 => 18,  35 => 12,  26 => 6,  19 => 1,);
    }
}
/* <h5>Promo Box</h5>*/
/* */
/* <div class="form-group">*/
/*     <label>Title</label>*/
/*     <input type="text" placeholder="Title" name="title" class="form-control"*/
/*            value="{{ widget.Options.title }}"/>*/
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label>Subtitle</label>*/
/*     <input type="text" placeholder="Subtitle" name="subtitle" class="form-control"*/
/*            value="{{ widget.Options.subtitle }}"/>*/
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label>Button text</label>*/
/*     <input type="text" placeholder="Button text" name="buttonText" class="form-control"*/
/*            value="{{ widget.Options.buttonText }}"/>*/
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label>Button url</label>*/
/*     <input type="text" placeholder="Button url" name="buttonUrl" class="form-control"*/
/*            value="{{ widget.Options.buttonUrl }}"/>*/
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label>Button Positon</label>*/
/*     {{ HtmlHelper.SearchableSelect({left: "Left", center: "Center", right: "Right"}, "buttonPosition", widget.Options.buttonPosition)|raw }}*/
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label>Style</label>*/
/*     {{ HtmlHelper.SearchableSelect({white: "White", light: "Light", dark: "Dark", flat: "Flat"}, "style", widget.Options.style)|raw }}*/
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <div class="row">*/
/*         <div class="col-md-12">*/
/*             <label class="ckbox">*/
/*                 <input type="checkbox" name="border"*/
/*                        {% if widget.Options.border == "on" %}checked="checked"{% endif %}>*/
/*                 <span>Border</span>*/
/*             </label>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
