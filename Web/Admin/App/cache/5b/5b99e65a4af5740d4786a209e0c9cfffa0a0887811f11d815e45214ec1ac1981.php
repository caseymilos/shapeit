<?php

/* Admin/Master/Login.twig */
class __TwigTemplate_373acaf54456876589c9f8831a8f1a95cfc6706347446ad17fe27f72c294d98c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Base.twig", "Admin/Master/Login.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
            'body_class' => array($this, 'block_body_class'),
            'head' => array($this, 'block_head'),
            'additional_styles' => array($this, 'block_additional_styles'),
            'scripts' => array($this, 'block_scripts'),
            'post_scripts' => array($this, 'block_post_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
    }

    // line 7
    public function block_body_class($context, array $blocks = array())
    {
        echo "signwrapper";
    }

    // line 9
    public function block_head($context, array $blocks = array())
    {
        // line 10
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    <link type=\"text/css\" rel=\"stylesheet\" href=\"Theme/Admin/css/style.css\"/>
    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/dropzone/dropzone.css\">
    ";
        // line 13
        $this->displayBlock('additional_styles', $context, $blocks);
        // line 14
        echo "
    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/jquery-ui/jquery-ui.css\">
    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/select2/select2.css\">

    <link rel=\"stylesheet\" href=\"Theme/Admin/css/quirk.css\">
";
    }

    // line 13
    public function block_additional_styles($context, array $blocks = array())
    {
    }

    // line 22
    public function block_scripts($context, array $blocks = array())
    {
        // line 23
        echo "    <script src=\"Theme/Admin/lib/jquery/jquery.js\"></script>
    <script src=\"Theme/Admin/lib/jquery-ui/jquery-ui.js\"></script>
    <script src=\"Theme/Admin/lib/bootstrap/js/bootstrap.js\"></script>
    <script src=\"Theme/Admin/lib/select2/select2.js\"></script>
    <script type=\"text/javascript\">
        // Date Picker
        \$('.datepicker').datepicker();
        \$('.select2').select2();
    </script>

    <script src=\"Theme/Admin/lib/jquery-toggles/toggles.js\"></script>
    <script type=\"text/javascript\">
        // Toggles
        \$('.toggle').toggles({
            height: 26
        });
    </script>
    ";
        // line 40
        $this->displayBlock('post_scripts', $context, $blocks);
        // line 41
        echo "    <script src=\"Theme/Admin/js/quirk.js\"></script>
";
    }

    // line 40
    public function block_post_scripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "Admin/Master/Login.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 40,  102 => 41,  100 => 40,  81 => 23,  78 => 22,  73 => 13,  64 => 14,  62 => 13,  55 => 10,  52 => 9,  46 => 7,  37 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Base.twig" %}*/
/* */
/* {% block body %}*/
/*     {% block content %}{% endblock %}*/
/* {% endblock %}*/
/* */
/* {% block body_class %}signwrapper{% endblock %}*/
/* */
/* {% block head %}*/
/*     {{ parent() }}*/
/*     <link type="text/css" rel="stylesheet" href="Theme/Admin/css/style.css"/>*/
/*     <link rel="stylesheet" href="Theme/Admin/lib/dropzone/dropzone.css">*/
/*     {% block additional_styles %}{% endblock %}*/
/* */
/*     <link rel="stylesheet" href="Theme/Admin/lib/jquery-ui/jquery-ui.css">*/
/*     <link rel="stylesheet" href="Theme/Admin/lib/select2/select2.css">*/
/* */
/*     <link rel="stylesheet" href="Theme/Admin/css/quirk.css">*/
/* {% endblock %}*/
/* */
/* */
/* {% block scripts %}*/
/*     <script src="Theme/Admin/lib/jquery/jquery.js"></script>*/
/*     <script src="Theme/Admin/lib/jquery-ui/jquery-ui.js"></script>*/
/*     <script src="Theme/Admin/lib/bootstrap/js/bootstrap.js"></script>*/
/*     <script src="Theme/Admin/lib/select2/select2.js"></script>*/
/*     <script type="text/javascript">*/
/*         // Date Picker*/
/*         $('.datepicker').datepicker();*/
/*         $('.select2').select2();*/
/*     </script>*/
/* */
/*     <script src="Theme/Admin/lib/jquery-toggles/toggles.js"></script>*/
/*     <script type="text/javascript">*/
/*         // Toggles*/
/*         $('.toggle').toggles({*/
/*             height: 26*/
/*         });*/
/*     </script>*/
/*     {% block post_scripts %}{% endblock %}*/
/*     <script src="Theme/Admin/js/quirk.js"></script>*/
/* {% endblock %}*/
