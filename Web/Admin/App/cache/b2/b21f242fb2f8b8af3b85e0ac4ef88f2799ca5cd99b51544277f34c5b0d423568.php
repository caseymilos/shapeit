<?php

/* Admin/Builder/Widget/Master/Panel.twig */
class __TwigTemplate_28efaf265495b1e36dec589ca216bfa6d1b021c06182701f8b45788942ccc686 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Builder/Widget/Master/Base.twig", "Admin/Builder/Widget/Master/Panel.twig", 1);
        $this->blocks = array(
            'element' => array($this, 'block_element'),
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Builder/Widget/Master/Base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_element($context, array $blocks = array())
    {
        // line 3
        echo "    <div class=\"panel panel-success\">
        <ul class=\"panel-options\">
            <li><a class=\"panel-minimize\"><i class=\"fa fa-chevron-down\"></i></a></li>
            <li><a class=\"widget-remove\"><i class=\"fa fa-remove\"></i></a></li>
        </ul>
        <div class=\"panel-heading\">
            <h3 class=\"panel-title\">";
        // line 9
        $this->displayBlock('title', $context, $blocks);
        echo "</h3>
        </div>
        <div class=\"panel-body\">
            ";
        // line 12
        $this->displayBlock('content', $context, $blocks);
        // line 17
        echo "        </div>
    </div>
";
    }

    // line 9
    public function block_title($context, array $blocks = array())
    {
    }

    // line 12
    public function block_content($context, array $blocks = array())
    {
        // line 13
        echo "                <a class=\"edit-widget-button btn btn-default\">
                    <i class=\"fa fa-edit\"></i>
                </a>
            ";
    }

    public function getTemplateName()
    {
        return "Admin/Builder/Widget/Master/Panel.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 13,  60 => 12,  55 => 9,  49 => 17,  47 => 12,  41 => 9,  33 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends "Admin/Builder/Widget/Master/Base.twig" %}*/
/* {% block element %}*/
/*     <div class="panel panel-success">*/
/*         <ul class="panel-options">*/
/*             <li><a class="panel-minimize"><i class="fa fa-chevron-down"></i></a></li>*/
/*             <li><a class="widget-remove"><i class="fa fa-remove"></i></a></li>*/
/*         </ul>*/
/*         <div class="panel-heading">*/
/*             <h3 class="panel-title">{% block title %}{% endblock %}</h3>*/
/*         </div>*/
/*         <div class="panel-body">*/
/*             {% block content %}*/
/*                 <a class="edit-widget-button btn btn-default">*/
/*                     <i class="fa fa-edit"></i>*/
/*                 </a>*/
/*             {% endblock %}*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
