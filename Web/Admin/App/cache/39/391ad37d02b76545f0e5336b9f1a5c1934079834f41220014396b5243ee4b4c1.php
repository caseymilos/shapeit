<?php

/* Admin/Offers/Partial/OfferForm.twig */
class __TwigTemplate_f572b1c616ac7fa43e21b967db1991797e11e1ba3906afe620e466072b7dfc7c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form method=\"post\" enctype=\"multipart/form-data\">
\t<div class=\"row\">
\t\t<div class=\"col-sm-6\">
\t\t\t<div class=\"panel\">
\t\t\t\t<div class=\"panel-heading nopaddingbottom\">
\t\t\t\t\t<h4 class=\"panel-title\">Offer</h4>
\t\t\t\t</div>
\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t<hr>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>Title</label>
\t\t\t\t\t\t<input type=\"text\" placeholder=\"Title\" name=\"title\" class=\"slug-field form-control\"
\t\t\t\t\t\t\t   value=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Title", array()), "html", null, true);
        echo "\"/>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>Short Description</label>
\t\t\t\t\t\t<input type=\"text\" placeholder=\"Short Description\" name=\"shortDescription\" class=\"form-control\"
\t\t\t\t\t\t\t   value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "ShortDescription", array()), "html", null, true);
        echo "\"/>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>Country</label>
\t\t\t\t\t\t<input type=\"text\" placeholder=\"Country\" name=\"country\" class=\"form-control\"
\t\t\t\t\t\t\t   value=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Country", array()), "html", null, true);
        echo "\"/>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>City</label>
\t\t\t\t\t\t<input type=\"text\" placeholder=\"City\" name=\"city\" class=\"form-control\"
\t\t\t\t\t\t\t   value=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "City", array()), "html", null, true);
        echo "\"/>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>Description</label>
\t\t\t\t\t\t<textarea placeholder=\"Description\" name=\"description\"
\t\t\t\t\t\t\t\t  class=\"form-control wysiwyg\">";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Description", array()), "html", null, true);
        echo "</textarea>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>Slug</label>
\t\t\t\t\t\t<input type=\"text\" placeholder=\"Slug\" readonly name=\"slug\" class=\"form-control\"
\t\t\t\t\t\t\t   value=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Slug", array()), "html", null, true);
        echo "\"/>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>Price</label>
\t\t\t\t\t\t<input type=\"number\" placeholder=\"Price\" name=\"price\" class=\"form-control\"
\t\t\t\t\t\t\t   value=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Price", array()), "html", null, true);
        echo "\"/>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>Picture</label>
\t\t\t\t\t\t<label class=\"picture-mask text-center\" style=\"width: 250px;\">
\t\t\t\t\t\t\t<div class=\"image-input image-placeholder\"
\t\t\t\t\t\t\t\t style=\"height: 350px; width:250px; background-image: url(";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "PictureSource", array(), "method"), "html", null, true);
        echo ")\"></div>
\t\t\t\t\t\t\t<input style=\"display: none;\" class=\"upload-picture-field\" type=\"file\" name=\"picture\"/>
\t\t\t\t\t\t</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>Offer Type
\t\t\t\t\t\t\t<span class=\"text-danger\">*</span>
\t\t\t\t\t\t</label>
\t\t\t\t\t\t";
        // line 57
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => $this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "OfferTypes", array(), "method"), 1 => "offerTypeId", 2 => $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "OfferTypeId", array()), 3 => "Select Offer Type", 4 => array("id" => "offerTypes"), 5 => null), "method");
        echo "
\t\t\t\t\t</div>


\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>Date</label>
\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control datepicker\" name=\"date\"
\t\t\t\t\t\t\t\t   placeholder=\"yyyy-mm-dd\"
\t\t\t\t\t\t\t\t   value=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Date", array()), "format", array(0 => "Y-m-d"), "method"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-calendar\"></i></span>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>

\t\t</div>

\t\t<div class=\"col-sm-6\">
\t\t\t<div class=\"panel\">
\t\t\t\t<div class=\"panel-heading nopaddingbottom\">
\t\t\t\t\t<h4 class=\"panel-title\">Related Offers</h4>
\t\t\t\t</div>
\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t<hr>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>Select Related Offers</label>
\t\t\t\t\t\t";
        // line 84
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => $this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "Offers", array(), "method"), 1 => "relatedOffers", 2 => $this->env->getExtension('gdev_twig_filters')->GetIndexes($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Children", array()), "OfferId"), 3 => "Select Related Offers", 4 => array("id" => "related-offers"), 5 => null, 6 => true), "method");
        echo "
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>

\t\t</div>

\t</div>
\t<!-- row -->
\t<div class=\"row\">
\t\t<div class=\"col-sm-12\">
\t\t\t<div class=\"panel\">
\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t<div class=\"col-sm-4\">
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-sm-4\" style=\"text-align: center\">
\t\t\t\t\t\t<button class=\"btn btn-success btn-quirk btn-wide mr5\" type=\"submit\">Submit</button>
\t\t\t\t\t\t<button type=\"reset\" class=\"btn btn-quirk btn-wide btn-default\">Reset</button>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-sm-4\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</form>";
    }

    public function getTemplateName()
    {
        return "Admin/Offers/Partial/OfferForm.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 84,  113 => 66,  101 => 57,  90 => 49,  81 => 43,  73 => 38,  65 => 33,  57 => 28,  49 => 23,  41 => 18,  33 => 13,  19 => 1,);
    }
}
/* <form method="post" enctype="multipart/form-data">*/
/* 	<div class="row">*/
/* 		<div class="col-sm-6">*/
/* 			<div class="panel">*/
/* 				<div class="panel-heading nopaddingbottom">*/
/* 					<h4 class="panel-title">Offer</h4>*/
/* 				</div>*/
/* 				<div class="panel-body">*/
/* 					<hr>*/
/* 					<div class="form-group">*/
/* 						<label>Title</label>*/
/* 						<input type="text" placeholder="Title" name="title" class="slug-field form-control"*/
/* 							   value="{{ model.Title }}"/>*/
/* 					</div>*/
/* 					<div class="form-group">*/
/* 						<label>Short Description</label>*/
/* 						<input type="text" placeholder="Short Description" name="shortDescription" class="form-control"*/
/* 							   value="{{ model.ShortDescription }}"/>*/
/* 					</div>*/
/* 					<div class="form-group">*/
/* 						<label>Country</label>*/
/* 						<input type="text" placeholder="Country" name="country" class="form-control"*/
/* 							   value="{{ model.Country }}"/>*/
/* 					</div>*/
/* 					<div class="form-group">*/
/* 						<label>City</label>*/
/* 						<input type="text" placeholder="City" name="city" class="form-control"*/
/* 							   value="{{ model.City }}"/>*/
/* 					</div>*/
/* 					<div class="form-group">*/
/* 						<label>Description</label>*/
/* 						<textarea placeholder="Description" name="description"*/
/* 								  class="form-control wysiwyg">{{ model.Description }}</textarea>*/
/* 					</div>*/
/* 					<div class="form-group">*/
/* 						<label>Slug</label>*/
/* 						<input type="text" placeholder="Slug" readonly name="slug" class="form-control"*/
/* 							   value="{{ model.Slug }}"/>*/
/* 					</div>*/
/* 					<div class="form-group">*/
/* 						<label>Price</label>*/
/* 						<input type="number" placeholder="Price" name="price" class="form-control"*/
/* 							   value="{{ model.Price }}"/>*/
/* 					</div>*/
/* 					<div class="form-group">*/
/* 						<label>Picture</label>*/
/* 						<label class="picture-mask text-center" style="width: 250px;">*/
/* 							<div class="image-input image-placeholder"*/
/* 								 style="height: 350px; width:250px; background-image: url({{ model.PictureSource() }})"></div>*/
/* 							<input style="display: none;" class="upload-picture-field" type="file" name="picture"/>*/
/* 						</label>*/
/* 					</div>*/
/* 					<div class="form-group">*/
/* 						<label>Offer Type*/
/* 							<span class="text-danger">*</span>*/
/* 						</label>*/
/* 						{{ HtmlHelper.SearchableSelect(SelectableHelper.OfferTypes(), 'offerTypeId', model.OfferTypeId, "Select Offer Type", {'id' : 'offerTypes'}, null)|raw }}*/
/* 					</div>*/
/* */
/* */
/* 					<div class="form-group">*/
/* 						<label>Date</label>*/
/* 						<div class="input-group">*/
/* 							<input type="text" class="form-control datepicker" name="date"*/
/* 								   placeholder="yyyy-mm-dd"*/
/* 								   value="{{ model.Date.format("Y-m-d") }}">*/
/* 							<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* */
/* 		</div>*/
/* */
/* 		<div class="col-sm-6">*/
/* 			<div class="panel">*/
/* 				<div class="panel-heading nopaddingbottom">*/
/* 					<h4 class="panel-title">Related Offers</h4>*/
/* 				</div>*/
/* 				<div class="panel-body">*/
/* 					<hr>*/
/* 					<div class="form-group">*/
/* 						<label>Select Related Offers</label>*/
/* 						{{ HtmlHelper.SearchableSelect(SelectableHelper.Offers(), 'relatedOffers', model.Children|indexes("OfferId"), "Select Related Offers", {'id' : 'related-offers'}, null, true)|raw }}*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* */
/* 		</div>*/
/* */
/* 	</div>*/
/* 	<!-- row -->*/
/* 	<div class="row">*/
/* 		<div class="col-sm-12">*/
/* 			<div class="panel">*/
/* 				<div class="panel-body">*/
/* 					<div class="col-sm-4">*/
/* 					</div>*/
/* */
/* 					<div class="col-sm-4" style="text-align: center">*/
/* 						<button class="btn btn-success btn-quirk btn-wide mr5" type="submit">Submit</button>*/
/* 						<button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button>*/
/* 					</div>*/
/* */
/* 					<div class="col-sm-4">*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* </form>*/
