<?php

/* Admin/Pages/WidgetOptions/Title2.twig */
class __TwigTemplate_1e83fe9e815d88554145929612056fadf0f299d7699b35cb8c5b7c3ad7d72fbf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h5>Title</h5>

<div class=\"form-group\">
    <label>Black</label>
    <input type=\"text\" placeholder=\"Black\" name=\"black\" class=\"form-control\"
           value=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "black", array()), "html", null, true);
        echo "\"/>
</div>
<div class=\"form-group\">
    <label>Colored</label>
    <input type=\"text\" placeholder=\"Colored\" name=\"colored\" class=\"form-control\"
           value=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "colored", array()), "html", null, true);
        echo "\"/>
</div>

";
    }

    public function getTemplateName()
    {
        return "Admin/Pages/WidgetOptions/Title2.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 11,  26 => 6,  19 => 1,);
    }
}
/* <h5>Title</h5>*/
/* */
/* <div class="form-group">*/
/*     <label>Black</label>*/
/*     <input type="text" placeholder="Black" name="black" class="form-control"*/
/*            value="{{ widget.Options.black }}"/>*/
/* </div>*/
/* <div class="form-group">*/
/*     <label>Colored</label>*/
/*     <input type="text" placeholder="Colored" name="colored" class="form-control"*/
/*            value="{{ widget.Options.colored }}"/>*/
/* </div>*/
/* */
/* */
