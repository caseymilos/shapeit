<?php

/* Admin/Shared/Sidebar/Panels/Settings.twig */
class __TwigTemplate_ab1b82159fe453b1842ff32efabfa6e0467bfc2fb22af5758b084449dfe0f067 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h5 class=\"sidebar-title\">Change Password</h5>
<ul class=\"list-group list-group-settings\">
    <li class=\"list-group-item\">
        <form action=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "change-password"), "method"), "html", null, true);
        echo "\" method=\"post\">

            <div class=\"form-group\">
                <input type=\"password\" name=\"oldPassword\" placeholder=\"Old Password\" class=\"form-control input-sm\"/>
            </div>

            <div class=\"form-group\">
                <input type=\"password\" name=\"newPassword\" placeholder=\"New Password\" class=\"form-control input-sm\"/>
            </div>

            <div class=\"form-group\">
                <input type=\"password\" name=\"confirmNewPassword\" placeholder=\"Confirm New Password\"
                       class=\"form-control input-sm\"/>
            </div>

            <button class=\"btn btn-primary btn-block btn-sm btn-quirk\">
                <i class=\"fa fa-lock\"></i>
                Change password
            </button>
        </form>
    </li>
</ul>

<h5 class=\"sidebar-title\">User Profile</h5>
<ul class=\"list-group list-group-settings\">
    <li class=\"list-group-item\">
        <form action=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "current-user-update-details"), "method"), "html", null, true);
        echo "\" method=\"post\">
            <div class=\"form-group\">
                <input type=\"text\" name=\"firstName\" value=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "GetCurrentUser", array(), "method"), "FirstName", array()), "html", null, true);
        echo "\"
                       placeholder=\"First Name\" class=\"form-control input-sm\"/>
            </div>

            <div class=\"form-group\">
                <input type=\"text\" name=\"lastName\" value=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "GetCurrentUser", array(), "method"), "LastName", array()), "html", null, true);
        echo "\"
                       placeholder=\"Last Name\" class=\"form-control input-sm\"/>
            </div>

            <div class=\"form-group\">
                <input type=\"text\" name=\"email\" value=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "GetCurrentUser", array(), "method"), "Email", array()), "html", null, true);
        echo "\" placeholder=\"Email\"
                       class=\"form-control input-sm\"/>
            </div>

            <button class=\"btn btn-primary btn-block btn-sm btn-quirk\">
                <i class=\"fa fa-user\"></i>
                Update Profile
            </button>
        </form>
    </li>
</ul>

<h5 class=\"sidebar-title\">Change Picture</h5>
<ul class=\"list-group list-group-settings\">
    <li class=\"list-group-item\">
        <form action=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "user-update-picture"), "method"), "html", null, true);
        echo "\" method=\"post\"
              enctype=\"multipart/form-data\">
            <label for=\"picture-field\" class=\"picture-mask text-center\">
                <div class=\"image-holder image-placeholder\" id=\"image-holder\"></div>
                <input style=\"display: none;\" class=\"upload-picture-field\" id=\"picture-field\" type=\"file\" name=\"picture\"/>
            </label>

            <button class=\"btn btn-primary btn-block btn-sm btn-quirk\">
                <i class=\"fa fa-user\"></i>
                Change Picture
            </button>
        </form>
    </li>
</ul>";
    }

    public function getTemplateName()
    {
        return "Admin/Shared/Sidebar/Panels/Settings.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 57,  74 => 42,  66 => 37,  58 => 32,  53 => 30,  24 => 4,  19 => 1,);
    }
}
/* <h5 class="sidebar-title">Change Password</h5>*/
/* <ul class="list-group list-group-settings">*/
/*     <li class="list-group-item">*/
/*         <form action="{{ Router.Create("change-password") }}" method="post">*/
/* */
/*             <div class="form-group">*/
/*                 <input type="password" name="oldPassword" placeholder="Old Password" class="form-control input-sm"/>*/
/*             </div>*/
/* */
/*             <div class="form-group">*/
/*                 <input type="password" name="newPassword" placeholder="New Password" class="form-control input-sm"/>*/
/*             </div>*/
/* */
/*             <div class="form-group">*/
/*                 <input type="password" name="confirmNewPassword" placeholder="Confirm New Password"*/
/*                        class="form-control input-sm"/>*/
/*             </div>*/
/* */
/*             <button class="btn btn-primary btn-block btn-sm btn-quirk">*/
/*                 <i class="fa fa-lock"></i>*/
/*                 Change password*/
/*             </button>*/
/*         </form>*/
/*     </li>*/
/* </ul>*/
/* */
/* <h5 class="sidebar-title">User Profile</h5>*/
/* <ul class="list-group list-group-settings">*/
/*     <li class="list-group-item">*/
/*         <form action="{{ Router.Create("current-user-update-details") }}" method="post">*/
/*             <div class="form-group">*/
/*                 <input type="text" name="firstName" value="{{ Helper.GetCurrentUser().FirstName }}"*/
/*                        placeholder="First Name" class="form-control input-sm"/>*/
/*             </div>*/
/* */
/*             <div class="form-group">*/
/*                 <input type="text" name="lastName" value="{{ Helper.GetCurrentUser().LastName }}"*/
/*                        placeholder="Last Name" class="form-control input-sm"/>*/
/*             </div>*/
/* */
/*             <div class="form-group">*/
/*                 <input type="text" name="email" value="{{ Helper.GetCurrentUser().Email }}" placeholder="Email"*/
/*                        class="form-control input-sm"/>*/
/*             </div>*/
/* */
/*             <button class="btn btn-primary btn-block btn-sm btn-quirk">*/
/*                 <i class="fa fa-user"></i>*/
/*                 Update Profile*/
/*             </button>*/
/*         </form>*/
/*     </li>*/
/* </ul>*/
/* */
/* <h5 class="sidebar-title">Change Picture</h5>*/
/* <ul class="list-group list-group-settings">*/
/*     <li class="list-group-item">*/
/*         <form action="{{ Router.Create("user-update-picture") }}" method="post"*/
/*               enctype="multipart/form-data">*/
/*             <label for="picture-field" class="picture-mask text-center">*/
/*                 <div class="image-holder image-placeholder" id="image-holder"></div>*/
/*                 <input style="display: none;" class="upload-picture-field" id="picture-field" type="file" name="picture"/>*/
/*             </label>*/
/* */
/*             <button class="btn btn-primary btn-block btn-sm btn-quirk">*/
/*                 <i class="fa fa-user"></i>*/
/*                 Change Picture*/
/*             </button>*/
/*         </form>*/
/*     </li>*/
/* </ul>*/
