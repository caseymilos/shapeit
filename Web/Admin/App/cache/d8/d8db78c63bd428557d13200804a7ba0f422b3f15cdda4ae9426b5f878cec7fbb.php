<?php

/* Admin/Pages/WidgetOptions/Button.twig */
class __TwigTemplate_961f4a0b166881417812d8c9681642b33b9c4912b4fe6dedcee8370ec4e3143f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"button-widget-form\">
    <div class=\"form-group\">
        <label>Button Text</label>
        <div class=\"row\">
            <div class=\"col-md-12\">
                <input type=\"text\" name=\"content\" class=\"widget-title form-control\"
                       value=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "content", array()), "html", null, true);
        echo "\" placeholder=\"Caption\"/>
            </div>
        </div>
    </div>
    <div class=\"form-group\">
        <label>Button Url</label>
        <div class=\"row\">
            <div class=\"col-md-12\">
                <input type=\"text\" name=\"url\" class=\"widget-url form-control\"
                       value=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "url", array()), "html", null, true);
        echo "\" placeholder=\"Url\"/>
            </div>
        </div>
    </div>
    <div class=\"form-group\">
        <label>Open link in</label>
        ";
        // line 22
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => array("_self" => "Same tab", "_blank" => "New tab"), 1 => "target", 2 => $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "target", array())), "method");
        echo "
    </div>
    <div class=\"form-group\">
        <label>Size</label>
        ";
        // line 26
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => array("default" => "Default", "mini" => "Mini", "small" => "Small", "large" => "Large", "xlarge" => "Extra Large"), 1 => "size", 2 => $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "size", array())), "method");
        echo "
    </div>
    <div class=\"form-group\">
        <label>Color</label>
        ";
        // line 30
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => array("red" => "Red", "teal" => "Teal", "yellow" => "Yellow", "green" => "Green", "brown" => "Brown", "aqua" => "Aqua", "purple" => "Purple", "leaf" => "Leaf", "pink" => "Pink", "blue" => "Blue", "dirtygreen" => "Dirty Green", "amber" => "Amber", "black" => "Black", "white" => "White"), 1 => "color", 2 => $this->getAttribute($this->getAttribute(        // line 45
(isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "color", array())), "method");
        echo "
    </div>
    <div class=\"form-group\">
        <div class=\"row\">
            <div class=\"col-md-12\">
                <label class=\"ckbox\">
                    <input type=\"checkbox\" name=\"d\"
                           ";
        // line 52
        if (($this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "d", array()) == "on")) {
            echo "checked=\"checked\"";
        }
        echo ">
                    <span>3D</span>
                </label>
            </div>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"row\">
            <div class=\"col-md-12\">
                <label class=\"ckbox\">
                    <input type=\"checkbox\" name=\"rounded\"
                           ";
        // line 63
        if (($this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "rounded", array()) == "on")) {
            echo "checked=\"checked\"";
        }
        echo ">
                    <span>Rounded</span>
                </label>
            </div>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"row\">
            <div class=\"col-md-12\">
                <label class=\"ckbox\">
                    <input type=\"checkbox\" name=\"reveal\"
                           ";
        // line 74
        if (($this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "reveal", array()) == "on")) {
            echo "checked=\"checked\"";
        }
        echo ">
                    <span>Reveal</span>
                </label>
            </div>
        </div>
    </div>
    <div class=\"form-group\">
        <label>Icon Type</label>
        <div class=\"row\">
            <div class=\"col-md-12\">
                <input type=\"text\" name=\"icon\" class=\"widget-title form-control\"
                       value=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "icon", array()), "html", null, true);
        echo "\" placeholder=\"icon\"/>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "Admin/Pages/WidgetOptions/Button.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 85,  105 => 74,  89 => 63,  73 => 52,  63 => 45,  62 => 30,  55 => 26,  48 => 22,  39 => 16,  27 => 7,  19 => 1,);
    }
}
/* <div class="button-widget-form">*/
/*     <div class="form-group">*/
/*         <label>Button Text</label>*/
/*         <div class="row">*/
/*             <div class="col-md-12">*/
/*                 <input type="text" name="content" class="widget-title form-control"*/
/*                        value="{{ widget.Options.content }}" placeholder="Caption"/>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label>Button Url</label>*/
/*         <div class="row">*/
/*             <div class="col-md-12">*/
/*                 <input type="text" name="url" class="widget-url form-control"*/
/*                        value="{{ widget.Options.url }}" placeholder="Url"/>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label>Open link in</label>*/
/*         {{ HtmlHelper.SearchableSelect({_self: "Same tab", _blank: "New tab"}, "target", widget.Options.target)|raw }}*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label>Size</label>*/
/*         {{ HtmlHelper.SearchableSelect({default: "Default", mini: "Mini", small: "Small", large: "Large", xlarge: "Extra Large"}, "size", widget.Options.size)|raw }}*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label>Color</label>*/
/*         {{ HtmlHelper.SearchableSelect({*/
/*             red: "Red",*/
/*             teal: "Teal",*/
/*             yellow: "Yellow",*/
/*             green: "Green",*/
/*             brown: "Brown",*/
/*             aqua: "Aqua",*/
/*             purple: "Purple",*/
/*             leaf: "Leaf",*/
/*             pink: "Pink",*/
/*             blue: "Blue",*/
/*             dirtygreen: "Dirty Green",*/
/*             amber: "Amber",*/
/*             black: "Black",*/
/*             white: "White",*/
/*         }, "color", widget.Options.color)|raw }}*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <div class="row">*/
/*             <div class="col-md-12">*/
/*                 <label class="ckbox">*/
/*                     <input type="checkbox" name="d"*/
/*                            {% if widget.Options.d == "on" %}checked="checked"{% endif %}>*/
/*                     <span>3D</span>*/
/*                 </label>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <div class="row">*/
/*             <div class="col-md-12">*/
/*                 <label class="ckbox">*/
/*                     <input type="checkbox" name="rounded"*/
/*                            {% if widget.Options.rounded == "on" %}checked="checked"{% endif %}>*/
/*                     <span>Rounded</span>*/
/*                 </label>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <div class="row">*/
/*             <div class="col-md-12">*/
/*                 <label class="ckbox">*/
/*                     <input type="checkbox" name="reveal"*/
/*                            {% if widget.Options.reveal == "on" %}checked="checked"{% endif %}>*/
/*                     <span>Reveal</span>*/
/*                 </label>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label>Icon Type</label>*/
/*         <div class="row">*/
/*             <div class="col-md-12">*/
/*                 <input type="text" name="icon" class="widget-title form-control"*/
/*                        value="{{ widget.Options.icon }}" placeholder="icon"/>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
