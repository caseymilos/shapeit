<?php

/* Admin/Blog/Blogs.twig */
class __TwigTemplate_2e3a07733d11a234613433754a39ce15836e1ebca8307e325b77eb33a96c4e85 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Master.twig", "Admin/Blog/Blogs.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'additional_styles' => array($this, 'block_additional_styles'),
            'post_scripts' => array($this, 'block_post_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Master.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "List of Blogs";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "
\t";
        // line 8
        echo "
\t<ol class=\"breadcrumb breadcrumb-quirk\">
\t\t<li>
\t\t\t<a href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "home"), "method"), "html", null, true);
        echo "\">
\t\t\t\t<i class=\"fa fa-home mr5\"></i>
\t\t\t\tHome
\t\t\t</a>
\t\t</li>
\t\t<li>
\t\t\t<a href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "blog-list"), "method"), "html", null, true);
        echo "\">Blogs</a>
\t\t</li>
\t\t<li class=\"active\">Blogs Directory</li>
\t</ol>

\t<div class=\"panel\">
\t\t<div class=\"panel-heading\">
\t\t\t<h4 class=\"panel-title\">Blogs List</h4>
\t\t</div>
\t\t<div class=\"panel-body\">
\t\t\t<div class=\"table-responsive\">
\t\t\t\t<table class=\"datatable table table-bordered table-striped-col\">
\t\t\t\t\t<thead>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>Image</th>
\t\t\t\t\t\t\t<th>Title</th>
\t\t\t\t\t\t\t<th>Theme</th>
\t\t\t\t\t\t\t<th class=\"text-center\" style=\"width: 80px;\">Actions</th>

\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>

\t\t\t\t\t<tfoot>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>Image</th>
\t\t\t\t\t\t\t<th>Title</th>
\t\t\t\t\t\t\t<th>Theme</th>
\t\t\t\t\t\t\t<th class=\"text-center\" style=\"width: 100px;\">Actions</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</tfoot>

\t\t\t\t\t<tbody>
\t\t\t\t\t\t";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Blogs", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["blog"]) {
            // line 50
            echo "\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td><img src=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["blog"], "PictureSource", array(0 => "Small"), "method"), "html", null, true);
            echo "\"/></td>
\t\t\t\t\t\t\t\t<td>";
            // line 52
            echo $this->getAttribute($context["blog"], "Title", array());
            echo "</td>
\t\t\t\t\t\t\t\t<td>";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["blog"], "Theme", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<ul class=\"table-options\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "blogs-thumbnail-crop", 1 => array("id" => $this->getAttribute($context["blog"], "BlogId", array()), "size" => "medium")), "method"), "html", null, true);
            echo "\"
\t\t\t\t\t\t\t\t\t\t\t   title=\"Crop thumbnail\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-crop\"></i>
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "update-blog", 1 => array("id" => $this->getAttribute($context["blog"], "BlogId", array()))), "method"), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a class=\"confirm\" style=\"cursor: pointer\"
\t\t\t\t\t\t\t\t\t\t\t   data-href=\"";
            // line 69
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "delete-blog", 1 => array("id" => $this->getAttribute($context["blog"], "BlogId", array()))), "method"), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-trash\"></i>
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['blog'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>
\t\t</div>
\t</div><!-- panel -->

";
    }

    // line 85
    public function block_additional_styles($context, array $blocks = array())
    {
        // line 86
        echo "\t<link rel=\"stylesheet\" href=\"Theme/PBS/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css\">
\t<link rel=\"stylesheet\" href=\"Theme/PBS/lib/select2/select2.css\">
";
    }

    // line 90
    public function block_post_scripts($context, array $blocks = array())
    {
        // line 91
        echo "\t<script src=\"Theme/PBS/lib/datatables/jquery.dataTables.js\"></script>
\t<script src=\"Theme/PBS/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js\"></script>
\t<script src=\"Theme/PBS/lib/select2/select2.js\"></script>

\t<script type=\"text/javascript\">
\t\t\$(document).ready(function () {
\t\t\t'use strict';
\t\t\t\$('.datatable').DataTable();

\t\t\t\$('select').select2();
\t\t});
\t</script>
";
    }

    public function getTemplateName()
    {
        return "Admin/Blog/Blogs.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 91,  165 => 90,  159 => 86,  156 => 85,  146 => 77,  132 => 69,  123 => 63,  114 => 57,  107 => 53,  103 => 52,  99 => 51,  96 => 50,  92 => 49,  57 => 17,  48 => 11,  43 => 8,  40 => 6,  37 => 5,  31 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Master.twig" %}*/
/* */
/* {% block title %}List of Blogs{% endblock %}*/
/* */
/* {% block content %}*/
/* */
/* 	{# @var model \BlogsViewModel #}*/
/* */
/* 	<ol class="breadcrumb breadcrumb-quirk">*/
/* 		<li>*/
/* 			<a href="{{ Router.Create("home") }}">*/
/* 				<i class="fa fa-home mr5"></i>*/
/* 				Home*/
/* 			</a>*/
/* 		</li>*/
/* 		<li>*/
/* 			<a href="{{ Router.Create("blog-list") }}">Blogs</a>*/
/* 		</li>*/
/* 		<li class="active">Blogs Directory</li>*/
/* 	</ol>*/
/* */
/* 	<div class="panel">*/
/* 		<div class="panel-heading">*/
/* 			<h4 class="panel-title">Blogs List</h4>*/
/* 		</div>*/
/* 		<div class="panel-body">*/
/* 			<div class="table-responsive">*/
/* 				<table class="datatable table table-bordered table-striped-col">*/
/* 					<thead>*/
/* 						<tr>*/
/* 							<th>Image</th>*/
/* 							<th>Title</th>*/
/* 							<th>Theme</th>*/
/* 							<th class="text-center" style="width: 80px;">Actions</th>*/
/* */
/* 						</tr>*/
/* 					</thead>*/
/* */
/* 					<tfoot>*/
/* 						<tr>*/
/* 							<th>Image</th>*/
/* 							<th>Title</th>*/
/* 							<th>Theme</th>*/
/* 							<th class="text-center" style="width: 100px;">Actions</th>*/
/* 						</tr>*/
/* 					</tfoot>*/
/* */
/* 					<tbody>*/
/* 						{% for blog in model.Blogs %}*/
/* 							<tr>*/
/* 								<td><img src="{{ blog.PictureSource("Small") }}"/></td>*/
/* 								<td>{{ blog.Title | raw }}</td>*/
/* 								<td>{{ blog.Theme }}</td>*/
/* 								<td>*/
/* 									<ul class="table-options">*/
/* 										<li>*/
/* 											<a href="{{ Router.Create("blogs-thumbnail-crop", {id: blog.BlogId, size: 'medium'}) }}"*/
/* 											   title="Crop thumbnail">*/
/* 												<i class="fa fa-crop"></i>*/
/* 											</a>*/
/* 										</li>*/
/* 										<li>*/
/* 											<a href="{{ Router.Create("update-blog", {id: blog.BlogId}) }}">*/
/* 												<i class="fa fa-pencil"></i>*/
/* 											</a>*/
/* 										</li>*/
/* 										<li>*/
/* 											<a class="confirm" style="cursor: pointer"*/
/* 											   data-href="{{ Router.Create("delete-blog", {id: blog.BlogId}) }}">*/
/* 												<i class="fa fa-trash"></i>*/
/* 											</a>*/
/* 										</li>*/
/* 									</ul>*/
/* 								</td>*/
/* 							</tr>*/
/* 						{% endfor %}*/
/* 					</tbody>*/
/* 				</table>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div><!-- panel -->*/
/* */
/* {% endblock %}*/
/* */
/* {% block additional_styles %}*/
/* 	<link rel="stylesheet" href="Theme/PBS/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css">*/
/* 	<link rel="stylesheet" href="Theme/PBS/lib/select2/select2.css">*/
/* {% endblock %}*/
/* */
/* {% block post_scripts %}*/
/* 	<script src="Theme/PBS/lib/datatables/jquery.dataTables.js"></script>*/
/* 	<script src="Theme/PBS/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>*/
/* 	<script src="Theme/PBS/lib/select2/select2.js"></script>*/
/* */
/* 	<script type="text/javascript">*/
/* 		$(document).ready(function () {*/
/* 			'use strict';*/
/* 			$('.datatable').DataTable();*/
/* */
/* 			$('select').select2();*/
/* 		});*/
/* 	</script>*/
/* {% endblock %}*/
