<?php

/* Admin/Master/Errors.twig */
class __TwigTemplate_2bcbd077379c7699a395bf36c665b804d855e3b1281efd8b367c591916075bde extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Base.twig", "Admin/Master/Errors.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
            'head' => array($this, 'block_head'),
            'additional_styles' => array($this, 'block_additional_styles'),
            'scripts' => array($this, 'block_scripts'),
            'post_scripts' => array($this, 'block_post_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
    }

    // line 7
    public function block_head($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    ";
        // line 9
        $this->displayBlock('additional_styles', $context, $blocks);
        // line 10
        echo "    <link rel=\"stylesheet\" href=\"Theme/Admin/css/quirk.css\">
";
    }

    // line 9
    public function block_additional_styles($context, array $blocks = array())
    {
    }

    // line 14
    public function block_scripts($context, array $blocks = array())
    {
        // line 15
        echo "    <script src=\"Theme/Admin/lib/jquery/jquery.js\"></script>
    <script src=\"Theme/Admin/lib/jquery-ui/jquery-ui.js\"></script>
    <script src=\"Theme/Admin/lib/bootstrap/js/bootstrap.js\"></script>
    ";
        // line 18
        $this->displayBlock('post_scripts', $context, $blocks);
        // line 19
        echo "    <script src=\"Theme/Admin/js/quirk.js\"></script>
";
    }

    // line 18
    public function block_post_scripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "Admin/Master/Errors.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 18,  75 => 19,  73 => 18,  68 => 15,  65 => 14,  60 => 9,  55 => 10,  53 => 9,  48 => 8,  45 => 7,  36 => 4,  33 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Base.twig" %}*/
/* */
/* {% block body %}*/
/*     {% block content %}{% endblock %}*/
/* {% endblock %}*/
/* */
/* {% block head %}*/
/*     {{ parent() }}*/
/*     {% block additional_styles %}{% endblock %}*/
/*     <link rel="stylesheet" href="Theme/Admin/css/quirk.css">*/
/* {% endblock %}*/
/* */
/* */
/* {% block scripts %}*/
/*     <script src="Theme/Admin/lib/jquery/jquery.js"></script>*/
/*     <script src="Theme/Admin/lib/jquery-ui/jquery-ui.js"></script>*/
/*     <script src="Theme/Admin/lib/bootstrap/js/bootstrap.js"></script>*/
/*     {% block post_scripts %}{% endblock %}*/
/*     <script src="Theme/Admin/js/quirk.js"></script>*/
/* {% endblock %}*/
