<?php

/* Admin/Builder/Widget/Title.twig */
class __TwigTemplate_ae22158085f5a1743f4b3fd8cc41223574746f3861dcbc240777438d015652cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Builder/Widget/Master/Panel.twig", "Admin/Builder/Widget/Title.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Builder/Widget/Master/Panel.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Title";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "\t";
        $context["inputId"] = twig_random($this->env);
        // line 7
        echo "\t<div class=\"row\">
\t\t<div class=\"col-md-6\">
\t\t\t<input type=\"text\" id=\"title-";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["inputId"]) ? $context["inputId"] : null), "html", null, true);
        echo "\" class=\"widget-title form-control\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Options", array()), "content", array()), "html", null, true);
        echo "\"/>
\t\t</div>
\t\t<div class=\"col-md-3\">
\t\t\t";
        // line 12
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => array("h1" => "h1", "h2" => "h2", "h3" => "h3", "h4" => "h4", "h5" => "h5", "h6" => "h6"), 1 => "element", 2 => $this->getAttribute($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Options", array()), "element", array()), 3 => null, 4 => array("id" => ("title-element-" . (isset($context["inputId"]) ? $context["inputId"] : null)))), "method");
        echo "
\t\t</div>
\t\t<div class=\"col-md-3\">
\t\t\t";
        // line 15
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => array("left" => "left", "center" => "center", "right" => "right"), 1 => "align", 2 => $this->getAttribute($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Options", array()), "align", array()), 3 => null, 4 => array("id" => ("title-align-" . (isset($context["inputId"]) ? $context["inputId"] : null)))), "method");
        echo "
\t\t</div>
\t</div>


\t<script type=\"text/javascript\">
\t\t\$(\"#title-";
        // line 21
        echo twig_escape_filter($this->env, (isset($context["inputId"]) ? $context["inputId"] : null), "html", null, true);
        echo "\").on(\"blur\", titleSave";
        echo twig_escape_filter($this->env, (isset($context["inputId"]) ? $context["inputId"] : null), "html", null, true);
        echo ");
\t\t\$(\"#title-element-";
        // line 22
        echo twig_escape_filter($this->env, (isset($context["inputId"]) ? $context["inputId"] : null), "html", null, true);
        echo "\").on(\"change\", titleSave";
        echo twig_escape_filter($this->env, (isset($context["inputId"]) ? $context["inputId"] : null), "html", null, true);
        echo ");
\t\t\$(\"#title-align-";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["inputId"]) ? $context["inputId"] : null), "html", null, true);
        echo "\").on(\"change\", titleSave";
        echo twig_escape_filter($this->env, (isset($context["inputId"]) ? $context["inputId"] : null), "html", null, true);
        echo ");

\t\tfunction titleSave";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["inputId"]) ? $context["inputId"] : null), "html", null, true);
        echo "() {
\t\t\tvar options = {
\t\t\t\tcontent: \$(\"#title-";
        // line 27
        echo twig_escape_filter($this->env, (isset($context["inputId"]) ? $context["inputId"] : null), "html", null, true);
        echo "\").val(),
\t\t\t\telement: \$(\"#title-element-";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["inputId"]) ? $context["inputId"] : null), "html", null, true);
        echo "\").val(),
\t\t\t\talign: \$(\"#title-align-";
        // line 29
        echo twig_escape_filter($this->env, (isset($context["inputId"]) ? $context["inputId"] : null), "html", null, true);
        echo "\").val(),
\t\t\t};

\t\t\t\$(this).closest(\"[data-widget]\").data(\"options\", options);
\t\t}
\t</script>
";
    }

    public function getTemplateName()
    {
        return "Admin/Builder/Widget/Title.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 29,  96 => 28,  92 => 27,  87 => 25,  80 => 23,  74 => 22,  68 => 21,  59 => 15,  53 => 12,  45 => 9,  41 => 7,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Builder/Widget/Master/Panel.twig" %}*/
/* */
/* {% block title %}Title{% endblock %}*/
/* */
/* {% block content %}*/
/* 	{% set inputId = random() %}*/
/* 	<div class="row">*/
/* 		<div class="col-md-6">*/
/* 			<input type="text" id="title-{{ inputId }}" class="widget-title form-control" value="{{ model.Options.content }}"/>*/
/* 		</div>*/
/* 		<div class="col-md-3">*/
/* 			{{ HtmlHelper.SearchableSelect({h1: "h1", h2: "h2", h3: "h3", h4: "h4", h5: "h5", h6: "h6"}, "element", model.Options.element, null, {id: "title-element-" ~ inputId})|raw }}*/
/* 		</div>*/
/* 		<div class="col-md-3">*/
/* 			{{ HtmlHelper.SearchableSelect({left: "left", center: "center", right: "right"}, "align", model.Options.align, null, {id: "title-align-" ~ inputId})|raw }}*/
/* 		</div>*/
/* 	</div>*/
/* */
/* */
/* 	<script type="text/javascript">*/
/* 		$("#title-{{ inputId }}").on("blur", titleSave{{ inputId }});*/
/* 		$("#title-element-{{ inputId }}").on("change", titleSave{{ inputId }});*/
/* 		$("#title-align-{{ inputId }}").on("change", titleSave{{ inputId }});*/
/* */
/* 		function titleSave{{ inputId }}() {*/
/* 			var options = {*/
/* 				content: $("#title-{{ inputId }}").val(),*/
/* 				element: $("#title-element-{{ inputId }}").val(),*/
/* 				align: $("#title-align-{{ inputId }}").val(),*/
/* 			};*/
/* */
/* 			$(this).closest("[data-widget]").data("options", options);*/
/* 		}*/
/* 	</script>*/
/* {% endblock %}*/
