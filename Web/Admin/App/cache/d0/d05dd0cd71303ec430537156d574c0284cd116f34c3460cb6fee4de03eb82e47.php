<?php

/* Admin/Blog/Partial/BlogForm.twig */
class __TwigTemplate_39541997b19bfc547ab3c32ccd9968772aa4187bacebe6ea4958134a1c3b5f0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form method=\"post\" enctype=\"multipart/form-data\">
\t<div class=\"row\">
\t\t<div class=\"col-sm-6\">
\t\t\t<div class=\"panel\">
\t\t\t\t<div class=\"panel-heading nopaddingbottom\">
\t\t\t\t\t<h4 class=\"panel-title\">Blog</h4>
\t\t\t\t</div>
\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t<hr>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>Title</label>

\t\t\t\t\t\t<input type=\"text\" placeholder=\"Title\" name=\"title\" class=\"slug-field form-control\"
\t\t\t\t\t\t\t   value=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Title", array()), "html", null, true);
        echo "\"/>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>Theme</label>
\t\t\t\t\t\t<input type=\"text\" placeholder=\"Theme\" name=\"theme\" class=\"form-control\"
\t\t\t\t\t\t\t   value=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Theme", array()), "html", null, true);
        echo "\"/>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>Slug</label>
\t\t\t\t\t\t<input type=\"text\" placeholder=\"Slug\" readonly name=\"slug\" class=\"form-control\"
\t\t\t\t\t\t\t   value=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Slug", array()), "html", null, true);
        echo "\"/>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>Description</label>
\t\t\t\t\t\t<textarea placeholder=\"Description\" name=\"description\"
\t\t\t\t\t\t\t\t  class=\"form-control wysiwyg\">";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Description", array()), "html", null, true);
        echo "</textarea>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>Short Description</label>
\t\t\t\t\t\t<textarea placeholder=\"Short Description\" name=\"excerpt\"
\t\t\t\t\t\t\t\t  class=\"form-control wysiwyg\">";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Excerpt", array()), "html", null, true);
        echo "</textarea>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>Picture</label>
\t\t\t\t\t\t<label class=\"picture-mask text-center\" style=\"width: 250px;\">
\t\t\t\t\t\t\t<div class=\"image-input image-placeholder\"
\t\t\t\t\t\t\t\t style=\"height: 350px; width:250px; background-image: url(";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Blog", array()), "PictureSource", array(), "method"), "html", null, true);
        echo ")\"></div>
\t\t\t\t\t\t\t<input style=\"display: none;\" class=\"upload-picture-field\" type=\"file\" name=\"picture\"/>
\t\t\t\t\t\t</label>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-sm-6\">
\t\t\t<div class=\"panel\">
\t\t\t\t<div class=\"panel-heading nopaddingbottom\">
\t\t\t\t\t<h4 class=\"panel-title\">Related Blogs</h4>
\t\t\t\t</div>
\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t<hr>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label>Select Related Blogs</label>
\t\t\t\t\t\t";
        // line 56
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => $this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "Blogs", array(0 => array(0 => $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "BlogId", array()))), "method"), 1 => "relatedBlogs", 2 => $this->env->getExtension('gdev_twig_filters')->GetIndexes($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Children", array()), "BlogId"), 3 => "Select Related Blogs", 4 => array("id" => "related-blogs"), 5 => null, 6 => true), "method");
        echo "
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>

\t\t</div>
\t</div>
\t<!-- row -->
\t<div class=\"row\">
\t\t<div class=\"col-sm-12\">
\t\t\t<div class=\"panel\">
\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t<div class=\"col-sm-4\">
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-sm-4\" style=\"text-align: center\">
\t\t\t\t\t\t<button class=\"btn btn-success btn-quirk btn-wide mr5\" type=\"submit\">Submit</button>
\t\t\t\t\t\t<button type=\"reset\" class=\"btn btn-quirk btn-wide btn-default\">Reset</button>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-sm-4\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</form>";
    }

    public function getTemplateName()
    {
        return "Admin/Blog/Partial/BlogForm.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 56,  75 => 40,  66 => 34,  58 => 29,  50 => 24,  42 => 19,  34 => 14,  19 => 1,);
    }
}
/* <form method="post" enctype="multipart/form-data">*/
/* 	<div class="row">*/
/* 		<div class="col-sm-6">*/
/* 			<div class="panel">*/
/* 				<div class="panel-heading nopaddingbottom">*/
/* 					<h4 class="panel-title">Blog</h4>*/
/* 				</div>*/
/* 				<div class="panel-body">*/
/* 					<hr>*/
/* 					<div class="form-group">*/
/* 						<label>Title</label>*/
/* */
/* 						<input type="text" placeholder="Title" name="title" class="slug-field form-control"*/
/* 							   value="{{ model.Title }}"/>*/
/* 					</div>*/
/* 					<div class="form-group">*/
/* 						<label>Theme</label>*/
/* 						<input type="text" placeholder="Theme" name="theme" class="form-control"*/
/* 							   value="{{ model.Theme }}"/>*/
/* 					</div>*/
/* 					<div class="form-group">*/
/* 						<label>Slug</label>*/
/* 						<input type="text" placeholder="Slug" readonly name="slug" class="form-control"*/
/* 							   value="{{ model.Slug }}"/>*/
/* 					</div>*/
/* 					<div class="form-group">*/
/* 						<label>Description</label>*/
/* 						<textarea placeholder="Description" name="description"*/
/* 								  class="form-control wysiwyg">{{ model.Description }}</textarea>*/
/* 					</div>*/
/* 					<div class="form-group">*/
/* 						<label>Short Description</label>*/
/* 						<textarea placeholder="Short Description" name="excerpt"*/
/* 								  class="form-control wysiwyg">{{ model.Excerpt }}</textarea>*/
/* 					</div>*/
/* 					<div class="form-group">*/
/* 						<label>Picture</label>*/
/* 						<label class="picture-mask text-center" style="width: 250px;">*/
/* 							<div class="image-input image-placeholder"*/
/* 								 style="height: 350px; width:250px; background-image: url({{ model.Blog.PictureSource() }})"></div>*/
/* 							<input style="display: none;" class="upload-picture-field" type="file" name="picture"/>*/
/* 						</label>*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 		<div class="col-sm-6">*/
/* 			<div class="panel">*/
/* 				<div class="panel-heading nopaddingbottom">*/
/* 					<h4 class="panel-title">Related Blogs</h4>*/
/* 				</div>*/
/* 				<div class="panel-body">*/
/* 					<hr>*/
/* 					<div class="form-group">*/
/* 						<label>Select Related Blogs</label>*/
/* 						{{ HtmlHelper.SearchableSelect(SelectableHelper.Blogs({0:model.BlogId}), 'relatedBlogs', model.Children|indexes("BlogId"), "Select Related Blogs", {'id' : 'related-blogs'}, null, true)|raw }}*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* */
/* 		</div>*/
/* 	</div>*/
/* 	<!-- row -->*/
/* 	<div class="row">*/
/* 		<div class="col-sm-12">*/
/* 			<div class="panel">*/
/* 				<div class="panel-body">*/
/* 					<div class="col-sm-4">*/
/* 					</div>*/
/* */
/* 					<div class="col-sm-4" style="text-align: center">*/
/* 						<button class="btn btn-success btn-quirk btn-wide mr5" type="submit">Submit</button>*/
/* 						<button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button>*/
/* 					</div>*/
/* */
/* 					<div class="col-sm-4">*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* </form>*/
