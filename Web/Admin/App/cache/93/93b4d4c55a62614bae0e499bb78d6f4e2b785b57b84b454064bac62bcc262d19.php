<?php

/* Admin/Pages/WidgetOptions/Offers.twig */
class __TwigTemplate_22017671eb92c3c499095ed796248deacaf13c4f69f49b6e8e9a6b3ffc037ba1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-group\">
    <label>Title</label>
    <input type=\"text\" placeholder=\"Title\" name=\"title\" class=\"form-control\"
           value=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "title", array()), "html", null, true);
        echo "\"/>
</div>

<div class=\"form-group\">
    <label>Mode</label>
\t";
        // line 9
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => array("grid" => "Grid", "slider" => "Slider"), 1 => "mode", 2 => $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "mode", array())), "method");
        echo "
</div>

<h5>Offers</h5>

<div id=\"blog-pages-list-holder\">
\t";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "offers", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["offer"]) {
            // line 16
            echo "        <div class=\"container-fluid widget-sortable-unit\">
            <div class=\"row\">
                <div class=\"col-xs-10\">
\t\t\t\t\t";
            // line 19
            echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => $this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "Offers", array(), "method"), 1 => (("offers[" . $this->getAttribute($context["loop"], "index0", array())) . "]"), 2 => $context["offer"]), "method");
            echo "
                </div>
                <div class=\"col-xs-2 text-right\">
                    <a class=\"btn btn-danger widget-offer-delete\">
                        <i class=\"fa fa-times\"></i>
                    </a>
                </div>
            </div>
        </div>
\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['offer'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "</div>

<hr>

<a class=\"add-blog-page-widget-button btn btn-block btn-success\">Add Offer</a>

<div class=\"hidden\">
    <div id=\"pages-select\">
\t\t";
        // line 37
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => $this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "Offers", array(), "method"), 1 => ""), "method");
        echo "
    </div>
</div>

<script type=\"text/javascript\">
\tfunction offersWidgetReorganizeIndexes() {
\t\t// arrange all indexes
\t\tvar artistElements = \$(\"#blog-pages-list-holder > .container-fluid\");
\t\tvar i = 0;
\t\tartistElements.each(function () {
\t\t\tvar inputs = \$(this).find(\"select\");
\t\t\t\$(inputs[0]).attr(\"name\", \"offers[\" + i + \"]\");
\t\t\ti++;
\t\t});

\t\treturn i;
\t}

\t\$(\".add-blog-page-widget-button\").on(\"click\", function () {
\t\tvar i = offersWidgetReorganizeIndexes();

\t\tvar newEl = \$(\"<div>\").addClass(\"container-fluid widget-sortable-unit\");
\t\tvar select = \$(\"#pages-select\").find(\"select\").clone();
\t\tnewEl.append(
\t\t\t\$(\"<div>\").addClass(\"row\")
\t\t\t\t.append(\$(\"<div>\").addClass(\"col-xs-10\").append(select.attr(\"name\", \"offers[\" + i + \"]\")))
\t\t\t\t.append(\$(\"<div>\").addClass(\"col-xs-2 text-right\").append(\$(\"<a>\").addClass(\"btn btn-danger widget-offer-delete\").append(\$(\"<i>\").addClass(\"fa fa-times\"))))
\t\t);

\t\t\$(\"#blog-pages-list-holder\").append(newEl);
\t});

\t\$(\"#blog-pages-list-holder\").on(\"click\", \".widget-offer-delete\", function () {
\t\t\$(this).closest(\".container-fluid\").remove();
\t\toffersWidgetReorganizeIndexes()
\t});

\t\$(\"#blog-pages-list-holder\").sortable({
\t\tupdate: function () {
\t\t\toffersWidgetReorganizeIndexes();
\t\t}
\t});

</script>";
    }

    public function getTemplateName()
    {
        return "Admin/Pages/WidgetOptions/Offers.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 37,  87 => 29,  63 => 19,  58 => 16,  41 => 15,  32 => 9,  24 => 4,  19 => 1,);
    }
}
/* <div class="form-group">*/
/*     <label>Title</label>*/
/*     <input type="text" placeholder="Title" name="title" class="form-control"*/
/*            value="{{ widget.Options.title }}"/>*/
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label>Mode</label>*/
/* 	{{ HtmlHelper.SearchableSelect({grid: "Grid", slider: "Slider"}, "mode", widget.Options.mode)|raw }}*/
/* </div>*/
/* */
/* <h5>Offers</h5>*/
/* */
/* <div id="blog-pages-list-holder">*/
/* 	{% for offer in widget.Options.offers %}*/
/*         <div class="container-fluid widget-sortable-unit">*/
/*             <div class="row">*/
/*                 <div class="col-xs-10">*/
/* 					{{ HtmlHelper.SearchableSelect(SelectableHelper.Offers(), "offers[" ~ loop.index0 ~ "]", offer)|raw }}*/
/*                 </div>*/
/*                 <div class="col-xs-2 text-right">*/
/*                     <a class="btn btn-danger widget-offer-delete">*/
/*                         <i class="fa fa-times"></i>*/
/*                     </a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/* 	{% endfor %}*/
/* </div>*/
/* */
/* <hr>*/
/* */
/* <a class="add-blog-page-widget-button btn btn-block btn-success">Add Offer</a>*/
/* */
/* <div class="hidden">*/
/*     <div id="pages-select">*/
/* 		{{ HtmlHelper.SearchableSelect(SelectableHelper.Offers(), "")|raw }}*/
/*     </div>*/
/* </div>*/
/* */
/* <script type="text/javascript">*/
/* 	function offersWidgetReorganizeIndexes() {*/
/* 		// arrange all indexes*/
/* 		var artistElements = $("#blog-pages-list-holder > .container-fluid");*/
/* 		var i = 0;*/
/* 		artistElements.each(function () {*/
/* 			var inputs = $(this).find("select");*/
/* 			$(inputs[0]).attr("name", "offers[" + i + "]");*/
/* 			i++;*/
/* 		});*/
/* */
/* 		return i;*/
/* 	}*/
/* */
/* 	$(".add-blog-page-widget-button").on("click", function () {*/
/* 		var i = offersWidgetReorganizeIndexes();*/
/* */
/* 		var newEl = $("<div>").addClass("container-fluid widget-sortable-unit");*/
/* 		var select = $("#pages-select").find("select").clone();*/
/* 		newEl.append(*/
/* 			$("<div>").addClass("row")*/
/* 				.append($("<div>").addClass("col-xs-10").append(select.attr("name", "offers[" + i + "]")))*/
/* 				.append($("<div>").addClass("col-xs-2 text-right").append($("<a>").addClass("btn btn-danger widget-offer-delete").append($("<i>").addClass("fa fa-times"))))*/
/* 		);*/
/* */
/* 		$("#blog-pages-list-holder").append(newEl);*/
/* 	});*/
/* */
/* 	$("#blog-pages-list-holder").on("click", ".widget-offer-delete", function () {*/
/* 		$(this).closest(".container-fluid").remove();*/
/* 		offersWidgetReorganizeIndexes()*/
/* 	});*/
/* */
/* 	$("#blog-pages-list-holder").sortable({*/
/* 		update: function () {*/
/* 			offersWidgetReorganizeIndexes();*/
/* 		}*/
/* 	});*/
/* */
/* </script>*/
