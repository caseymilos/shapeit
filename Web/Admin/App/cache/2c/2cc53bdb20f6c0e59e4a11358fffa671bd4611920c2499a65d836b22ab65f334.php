<?php

/* Admin/Media/PeopleWidgetBrowse.twig */
class __TwigTemplate_a4b8a7986b4061187f64d6668838dd28a8d497abed2ae562fc21ee7c6c21358f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
";
        // line 3
        $context["uid"] = twig_random($this->env);
        // line 4
        echo "
<div class=\"modal-header\">
    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
    <h4 class=\"modal-title\">Add Media</h4>
</div>
<div class=\"modal-body\">

    <div id=\"media-holder-";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["uid"]) ? $context["uid"] : null), "html", null, true);
        echo "\" class=\"media-people-widget-media-holder\" style=\"height: 280px; overflow: auto;\">
        <div class=\"container-fluid\">
            ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_array_batch($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Media", array()), 4));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 14
            echo "                <div class=\"row\">
                    ";
            // line 15
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["row"]);
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["media"]) {
                // line 16
                echo "                        <div class=\"col-md-3\">
                            ";
                // line 17
                $this->loadTemplate((("Admin/Media/Browse/Radio/" . $this->getAttribute($this->getAttribute($context["media"], "MediaType", array()), "Caption", array())) . ".twig"), "Admin/Media/PeopleWidgetBrowse.twig", 17)->display(array_merge($context, array("media" => $context["media"])));
                // line 18
                echo "                        </div>
                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['media'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 20
            echo "                </div>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "        </div>
    </div>

</div>
<div class=\"modal-footer\">
    <button type=\"button\" class=\"btn btn-default close-modal-button\">Close</button>
    <button type=\"button\" id=\"add-media-to-people-widget-button-";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["uid"]) ? $context["uid"] : null), "html", null, true);
        echo "\" class=\"btn btn-primary\">Add Media</button>
</div>

<script type=\"text/javascript\">
    \$(\".close-modal-button\").on(\"click\", function () {
        \$(this).closest(\".modal\").modal(\"hide\");
    });

    \$(\"#add-media-to-people-widget-button-";
        // line 36
        echo twig_escape_filter($this->env, (isset($context["uid"]) ? $context["uid"] : null), "html", null, true);
        echo "\").on(\"click\", function () {
        var selectedImage = \$(\"#media-holder-";
        // line 37
        echo twig_escape_filter($this->env, (isset($context["uid"]) ? $context["uid"] : null), "html", null, true);
        echo "\").find(\".image-checkbox:checked\").first();
        var target = \$(this).closest(\".modal\").data(\"targetInput\");
        target.closest(\"[data-background]\").css(\"background-image\", \"url(\" + selectedImage.next(\"img\").attr(\"src\") + \")\");
        target.val(selectedImage.val());
        \$(this).closest(\".modal\").modal(\"hide\");
    });
</script>";
    }

    public function getTemplateName()
    {
        return "Admin/Media/PeopleWidgetBrowse.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 37,  129 => 36,  118 => 28,  110 => 22,  95 => 20,  80 => 18,  78 => 17,  75 => 16,  58 => 15,  55 => 14,  38 => 13,  33 => 11,  24 => 4,  22 => 3,  19 => 2,);
    }
}
/* {# @var model \MediaViewModel #}*/
/* */
/* {% set uid = random() %}*/
/* */
/* <div class="modal-header">*/
/*     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>*/
/*     <h4 class="modal-title">Add Media</h4>*/
/* </div>*/
/* <div class="modal-body">*/
/* */
/*     <div id="media-holder-{{ uid }}" class="media-people-widget-media-holder" style="height: 280px; overflow: auto;">*/
/*         <div class="container-fluid">*/
/*             {% for row in model.Media|batch(4) %}*/
/*                 <div class="row">*/
/*                     {% for media in row %}*/
/*                         <div class="col-md-3">*/
/*                             {% include "Admin/Media/Browse/Radio/" ~ media.MediaType.Caption ~ ".twig" with {media: media} %}*/
/*                         </div>*/
/*                     {% endfor %}*/
/*                 </div>*/
/*             {% endfor %}*/
/*         </div>*/
/*     </div>*/
/* */
/* </div>*/
/* <div class="modal-footer">*/
/*     <button type="button" class="btn btn-default close-modal-button">Close</button>*/
/*     <button type="button" id="add-media-to-people-widget-button-{{ uid }}" class="btn btn-primary">Add Media</button>*/
/* </div>*/
/* */
/* <script type="text/javascript">*/
/*     $(".close-modal-button").on("click", function () {*/
/*         $(this).closest(".modal").modal("hide");*/
/*     });*/
/* */
/*     $("#add-media-to-people-widget-button-{{ uid }}").on("click", function () {*/
/*         var selectedImage = $("#media-holder-{{ uid }}").find(".image-checkbox:checked").first();*/
/*         var target = $(this).closest(".modal").data("targetInput");*/
/*         target.closest("[data-background]").css("background-image", "url(" + selectedImage.next("img").attr("src") + ")");*/
/*         target.val(selectedImage.val());*/
/*         $(this).closest(".modal").modal("hide");*/
/*     });*/
/* </script>*/
