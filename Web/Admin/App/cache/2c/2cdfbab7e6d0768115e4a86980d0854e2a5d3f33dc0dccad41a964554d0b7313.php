<?php

/* Admin/Users/Create.twig */
class __TwigTemplate_8c4c77160d26444e9f53978d7a6d8f42c1d44c215999a74c44179a61754a212b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Master.twig", "Admin/Users/Create.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Master.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Create User";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <ol class=\"breadcrumb breadcrumb-quirk\">
        <li>
            <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "home"), "method"), "html", null, true);
        echo "\">
                <i class=\"fa fa-home mr5\"></i>
                Home
            </a>
        </li>
        <li>
            <a href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "user-list"), "method"), "html", null, true);
        echo "\">Users</a>
        </li>
        <li class=\"active\">New User</li>
    </ol>

    ";
        // line 19
        $this->loadTemplate("Admin/Users/Partial/UserForm.twig", "Admin/Users/Create.twig", 19)->display($context);
        // line 20
        echo "
";
    }

    public function getTemplateName()
    {
        return "Admin/Users/Create.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 20,  59 => 19,  51 => 14,  42 => 8,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Master.twig" %}*/
/* */
/* {% block title %}Create User{% endblock %}*/
/* */
/* {% block content %}*/
/*     <ol class="breadcrumb breadcrumb-quirk">*/
/*         <li>*/
/*             <a href="{{ Router.Create("home") }}">*/
/*                 <i class="fa fa-home mr5"></i>*/
/*                 Home*/
/*             </a>*/
/*         </li>*/
/*         <li>*/
/*             <a href="{{ Router.Create("user-list") }}">Users</a>*/
/*         </li>*/
/*         <li class="active">New User</li>*/
/*     </ol>*/
/* */
/*     {% include "Admin/Users/Partial/UserForm.twig" %}*/
/* */
/* {% endblock %}*/
