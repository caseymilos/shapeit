<?php

/* Admin/Pages/WidgetOptions/TitleCenter.twig */
class __TwigTemplate_045255b3599267fc1fcd5557673b86d35ea1bb31f049dcac78d9eba5d67b819f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-group\">
    <label>Title</label>
    <input type=\"text\" placeholder=\"Title\" name=\"title\" class=\"form-control\"
           value=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "title", array()), "html", null, true);
        echo "\"/>
</div>";
    }

    public function getTemplateName()
    {
        return "Admin/Pages/WidgetOptions/TitleCenter.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 4,  19 => 1,);
    }
}
/* <div class="form-group">*/
/*     <label>Title</label>*/
/*     <input type="text" placeholder="Title" name="title" class="form-control"*/
/*            value="{{ widget.Options.title }}"/>*/
/* </div>*/
