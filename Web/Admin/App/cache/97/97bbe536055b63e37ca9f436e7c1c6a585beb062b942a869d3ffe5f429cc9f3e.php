<?php

/* Admin/Users/Partial/UserForm.twig */
class __TwigTemplate_a708ef4e48ae49f74f63c358cacb6d6e79c7f9493ce6dca8456f19e7debe4147 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form method=\"post\" enctype=\"multipart/form-data\">
    <div class=\"row\">
        <div class=\"col-sm-6\">
            <div class=\"panel\">
                <div class=\"panel-heading nopaddingbottom\">
                    <h4 class=\"panel-title\">Account Details</h4>
                </div>
                <div class=\"panel-body\">
                    <hr>
                    <div class=\"form-group\">
                        <label>Username <span class=\"text-danger\">*</span></label>
                        <input type=\"text\" placeholder=\"Username\" name=\"username\" class=\"form-control\" required
                               value=\"";
        // line 13
        echo twig_escape_filter($this->env, ((($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "UserName", array()) != null)) ? ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "UserName", array())) : ("")), "html", null, true);
        echo "\" ";
        echo ((($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "UserName", array()) != null)) ? ("readonly") : (""));
        echo "/>
                    </div>
                    <div class=\"form-group\">
                        <label>Password <span class=\"text-danger\">*</span></label>
                        <input type=\"password\" placeholder=\"Password\" name=\"password\" class=\"form-control\"/>
                    </div>
                    <div class=\"form-group\">
                        <label>Email <span class=\"text-danger\">*</span></label>
                        <input type=\"email\" placeholder=\"Email\" name=\"email\" class=\"form-control\"
                               value=\"";
        // line 22
        echo twig_escape_filter($this->env, ((($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Email", array()) != null)) ? ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Email", array())) : ("")), "html", null, true);
        echo "\"/>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-6\">
                            <div class=\"form-group\">
                                ";
        // line 27
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "Checkbox", array(0 => "active", 1 => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Active", array()), 2 => "Active Account"), "method");
        echo "
                            </div>
                        </div>
                        <div class=\"col-md-6\">
                            <div class=\"form-group\">
                                ";
        // line 32
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "Checkbox", array(0 => "approved", 1 => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Approved", array()), 2 => "Approved"), "method");
        echo "
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"panel\">
                <div class=\"panel-heading nopaddingbottom\">
                    <h4 class=\"panel-title\">Roles <span class=\"text-danger\">*</span></h4>
                </div>
                <div class=\"panel-body\">
                    <hr>
                    <div class=\"form-group\">
                        ";
        // line 46
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => $this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "UserRoles", array(), "method"), 1 => "userRoles", 2 => $this->env->getExtension('gdev_twig_filters')->GetIndexes($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Roles", array()), "RoleId"), 3 => "Select User Role", 4 => array(), 5 => "Select User Role", 6 => true), "method");
        echo "
                    </div>
                </div>
            </div>

        </div>
        <!-- col-sm-6 -->

        <!-- ####################################################### -->

        <div class=\"col-sm-6\">
            <div class=\"panel\">
                <div class=\"panel-heading nopaddingbottom\">
                    <h4 class=\"panel-title\">Personal Details</h4>
                </div>
                <div class=\"panel-body\">
                    <hr>
                    <div class=\"form-group\">
                        <label>First Name <span class=\"text-danger\">*</span></label>
                        <input type=\"text\" placeholder=\"First Name\" name=\"firstName\" class=\"form-control\" required
                               value=\"";
        // line 66
        echo twig_escape_filter($this->env, ((($this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Details", array()), "FirstName", array()) != null)) ? ($this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Details", array()), "FirstName", array())) : ("")), "html", null, true);
        echo "\"/>
                    </div>
                    <div class=\"form-group\">
                        <label>Last Name <span class=\"text-danger\">*</span></label>
                        <input type=\"text\" placeholder=\"Last Name\" name=\"lastName\" class=\"form-control\" required
                               value=\"";
        // line 71
        echo twig_escape_filter($this->env, ((($this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Details", array()), "LastName", array()) != null)) ? ($this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Details", array()), "LastName", array())) : ("")), "html", null, true);
        echo "\"/>
                    </div>

                    <div class=\"form-group\">
                        <label>Date of Birth</label>

                        <div class=\"input-group\">
                            <input type=\"text\" class=\"form-control datepicker\" name=\"dateOfBirth\"
                                   placeholder=\"mm/dd/yyyy\"
                                   value=\"";
        // line 80
        echo twig_escape_filter($this->env, ((($this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Details", array()), "DateOfBirth", array()) != null)) ? (twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Details", array()), "DateOfBirth", array()), "m/d/Y")) : ("")), "html", null, true);
        echo "\">
                            <span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-calendar\"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"panel\">
                <div class=\"panel-heading nopaddingbottom\">
                    <h4 class=\"panel-title\">User Status <span class=\"text-danger\">*</span></h4>
                </div>
                <div class=\"panel-body\">
                    <hr>
                    ";
        // line 93
        if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Statuses", array()) != null)) {
            // line 94
            echo "                        <table class=\"table user-status-table\">
                            <thead>
                            <tr>
                                <th style=\"\">Status</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Message</th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
            // line 104
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Statuses", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["status"]) {
                // line 105
                echo "                                <tr>
                                    <td>";
                // line 106
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "Enum", array(0 => "UserStatusTypesEnum", 1 => $this->getAttribute($context["status"], "UserStatusTypeId", array())), "method"), "html", null, true);
                echo "</td>
                                    <td>";
                // line 107
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["status"], "DateFrom", array()), "m/d/Y"), "html", null, true);
                echo "</td>
                                    <td>";
                // line 108
                echo twig_escape_filter($this->env, ((($this->getAttribute($context["status"], "DateTo", array()) != null)) ? (twig_date_format_filter($this->env, $this->getAttribute($context["status"], "DateTo", array()), "m/d/Y")) : (" Now ")), "html", null, true);
                echo "</td>
                                    <td>";
                // line 109
                echo twig_escape_filter($this->env, $this->getAttribute($context["status"], "Message", array()), "html", null, true);
                echo "</td>
                                </tr>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['status'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 112
            echo "                            </tbody>
                        </table>
                    <hr>
                    ";
        }
        // line 116
        echo "                    <div class=\"form-group\">
                        ";
        // line 117
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => $this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "UserStatusTypes", array(), "method"), 1 => "userStatusId", 2 => $this->env->getExtension('gdev_twig_filters')->GetIndexes($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Status", array()), "UserId"), 3 => "Select User Status", 4 => array(), 5 => "Select User Status", 6 => false), "method");
        echo "
                    </div>
                    <div class=\"form-group\">
                        <label>Status Message <span class=\"text-danger\">*</span></label>
                        <input type=\"text\" placeholder=\"Status Message\" name=\"statusMessage\" class=\"form-control\"/>
                    </div>
                </div>
            </div>

        </div>
        <!-- col-sm-6 -->
    </div>
    <!-- row -->
    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel\">
                <div class=\"panel-body\">
                    <div class=\"col-sm-4\">
                    </div>

                    <div class=\"col-sm-4\" style=\"text-align: center\">
                        <button class=\"btn btn-success btn-quirk btn-wide mr5\" type=\"submit\">Submit</button>
                        <button type=\"reset\" class=\"btn btn-quirk btn-wide btn-default\">Reset</button>
                    </div>

                    <div class=\"col-sm-4\">
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>";
    }

    public function getTemplateName()
    {
        return "Admin/Users/Partial/UserForm.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 117,  187 => 116,  181 => 112,  172 => 109,  168 => 108,  164 => 107,  160 => 106,  157 => 105,  153 => 104,  141 => 94,  139 => 93,  123 => 80,  111 => 71,  103 => 66,  80 => 46,  63 => 32,  55 => 27,  47 => 22,  33 => 13,  19 => 1,);
    }
}
/* <form method="post" enctype="multipart/form-data">*/
/*     <div class="row">*/
/*         <div class="col-sm-6">*/
/*             <div class="panel">*/
/*                 <div class="panel-heading nopaddingbottom">*/
/*                     <h4 class="panel-title">Account Details</h4>*/
/*                 </div>*/
/*                 <div class="panel-body">*/
/*                     <hr>*/
/*                     <div class="form-group">*/
/*                         <label>Username <span class="text-danger">*</span></label>*/
/*                         <input type="text" placeholder="Username" name="username" class="form-control" required*/
/*                                value="{{ (user.UserName != null) ? user.UserName : '' }}" {{ (user.UserName != null) ? 'readonly' : '' }}/>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label>Password <span class="text-danger">*</span></label>*/
/*                         <input type="password" placeholder="Password" name="password" class="form-control"/>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label>Email <span class="text-danger">*</span></label>*/
/*                         <input type="email" placeholder="Email" name="email" class="form-control"*/
/*                                value="{{ (user.Email != null) ? user.Email : '' }}"/>*/
/*                     </div>*/
/*                     <div class="row">*/
/*                         <div class="col-md-6">*/
/*                             <div class="form-group">*/
/*                                 {{ HtmlHelper.Checkbox("active", user.Active, "Active Account")|raw }}*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-6">*/
/*                             <div class="form-group">*/
/*                                 {{ HtmlHelper.Checkbox("approved", user.Approved, "Approved")|raw }}*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/*             <div class="panel">*/
/*                 <div class="panel-heading nopaddingbottom">*/
/*                     <h4 class="panel-title">Roles <span class="text-danger">*</span></h4>*/
/*                 </div>*/
/*                 <div class="panel-body">*/
/*                     <hr>*/
/*                     <div class="form-group">*/
/*                         {{ HtmlHelper.SearchableSelect(SelectableHelper.UserRoles(), 'userRoles', user.Roles|indexes("RoleId"), "Select User Role", {}, "Select User Role", true)|raw }}*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/*         </div>*/
/*         <!-- col-sm-6 -->*/
/* */
/*         <!-- ####################################################### -->*/
/* */
/*         <div class="col-sm-6">*/
/*             <div class="panel">*/
/*                 <div class="panel-heading nopaddingbottom">*/
/*                     <h4 class="panel-title">Personal Details</h4>*/
/*                 </div>*/
/*                 <div class="panel-body">*/
/*                     <hr>*/
/*                     <div class="form-group">*/
/*                         <label>First Name <span class="text-danger">*</span></label>*/
/*                         <input type="text" placeholder="First Name" name="firstName" class="form-control" required*/
/*                                value="{{ (user.Details.FirstName != null) ? user.Details.FirstName : '' }}"/>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label>Last Name <span class="text-danger">*</span></label>*/
/*                         <input type="text" placeholder="Last Name" name="lastName" class="form-control" required*/
/*                                value="{{ (user.Details.LastName != null) ? user.Details.LastName : '' }}"/>*/
/*                     </div>*/
/* */
/*                     <div class="form-group">*/
/*                         <label>Date of Birth</label>*/
/* */
/*                         <div class="input-group">*/
/*                             <input type="text" class="form-control datepicker" name="dateOfBirth"*/
/*                                    placeholder="mm/dd/yyyy"*/
/*                                    value="{{ (user.Details.DateOfBirth != null) ? user.Details.DateOfBirth|date('m/d/Y') : '' }}">*/
/*                             <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/*             <div class="panel">*/
/*                 <div class="panel-heading nopaddingbottom">*/
/*                     <h4 class="panel-title">User Status <span class="text-danger">*</span></h4>*/
/*                 </div>*/
/*                 <div class="panel-body">*/
/*                     <hr>*/
/*                     {% if user.Statuses != null %}*/
/*                         <table class="table user-status-table">*/
/*                             <thead>*/
/*                             <tr>*/
/*                                 <th style="">Status</th>*/
/*                                 <th>From</th>*/
/*                                 <th>To</th>*/
/*                                 <th>Message</th>*/
/*                             </tr>*/
/*                             </thead>*/
/*                             <tbody>*/
/*                             {% for status in user.Statuses %}*/
/*                                 <tr>*/
/*                                     <td>{{ Helper.Enum("UserStatusTypesEnum", status.UserStatusTypeId) }}</td>*/
/*                                     <td>{{ status.DateFrom|date('m/d/Y') }}</td>*/
/*                                     <td>{{ (status.DateTo != null) ? status.DateTo|date('m/d/Y') : ' Now ' }}</td>*/
/*                                     <td>{{ status.Message }}</td>*/
/*                                 </tr>*/
/*                             {% endfor %}*/
/*                             </tbody>*/
/*                         </table>*/
/*                     <hr>*/
/*                     {% endif %}*/
/*                     <div class="form-group">*/
/*                         {{ HtmlHelper.SearchableSelect(SelectableHelper.UserStatusTypes(), 'userStatusId', user.Status|indexes("UserId"), "Select User Status", {}, "Select User Status", false)|raw }}*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label>Status Message <span class="text-danger">*</span></label>*/
/*                         <input type="text" placeholder="Status Message" name="statusMessage" class="form-control"/>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/*         </div>*/
/*         <!-- col-sm-6 -->*/
/*     </div>*/
/*     <!-- row -->*/
/*     <div class="row">*/
/*         <div class="col-sm-12">*/
/*             <div class="panel">*/
/*                 <div class="panel-body">*/
/*                     <div class="col-sm-4">*/
/*                     </div>*/
/* */
/*                     <div class="col-sm-4" style="text-align: center">*/
/*                         <button class="btn btn-success btn-quirk btn-wide mr5" type="submit">Submit</button>*/
/*                         <button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button>*/
/*                     </div>*/
/* */
/*                     <div class="col-sm-4">*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </form>*/
