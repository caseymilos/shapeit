<?php

/* Admin/Shared/Header/Search.twig */
class __TwigTemplate_8906a15def3f706e35103bd35d3eb863372fd1eb55243915c33d0877b8056e3d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"searchpanel\">
    <div class=\"input-group\">
        <input type=\"text\" class=\"form-control\" placeholder=\"Search for...\">
        <span class=\"input-group-btn\">
            <button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-search\"></i></button>
        </span>
    </div><!-- input-group -->
</div>
";
    }

    public function getTemplateName()
    {
        return "Admin/Shared/Header/Search.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <div class="searchpanel">*/
/*     <div class="input-group">*/
/*         <input type="text" class="form-control" placeholder="Search for...">*/
/*         <span class="input-group-btn">*/
/*             <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>*/
/*         </span>*/
/*     </div><!-- input-group -->*/
/* </div>*/
/* */
