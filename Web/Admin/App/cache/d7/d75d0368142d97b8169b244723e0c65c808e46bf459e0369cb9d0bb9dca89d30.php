<?php

/* Admin/Users/Partial/LoggedUserForm.twig */
class __TwigTemplate_d8f4d2be6446899d6907edef9a173a7b3900873f1ef3b32c22de844d5f51ec31 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form method=\"post\" enctype=\"multipart/form-data\">
    <div class=\"row\">
        <div class=\"col-sm-6\">
            <div class=\"panel\">
                <div class=\"panel-heading nopaddingbottom\">
                    <h4 class=\"panel-title\">Account Details</h4>
                </div>
                <div class=\"panel-body\">
                    <hr>
                    <div class=\"form-group\">
                        <label>Username <span class=\"text-danger\">*</span></label>
                        <input type=\"text\" placeholder=\"Username\" name=\"username\" class=\"form-control\" required
                               value=\"";
        // line 13
        echo twig_escape_filter($this->env, ((($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "UserName", array()) != null)) ? ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "UserName", array())) : ("")), "html", null, true);
        echo "\" ";
        echo ((($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "UserName", array()) != null)) ? ("readonly") : (""));
        echo "/>
                    </div>
                    <div class=\"form-group\">
                        <label>Password <span class=\"text-danger\">*</span></label>
                        <input type=\"password\" placeholder=\"Password\" name=\"password\" class=\"form-control\"/>
                    </div>
                    <div class=\"form-group\">
                        <label>Email <span class=\"text-danger\">*</span></label>
                        <input type=\"email\" placeholder=\"Email\" name=\"email\" class=\"form-control\"
                               value=\"";
        // line 22
        echo twig_escape_filter($this->env, ((($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Email", array()) != null)) ? ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Email", array())) : ("")), "html", null, true);
        echo "\"/>
                    </div>
                </div>
            </div>

        </div>
        <!-- col-sm-6 -->

        <!-- ####################################################### -->

        <div class=\"col-sm-6\">
            <div class=\"panel\">
                <div class=\"panel-heading nopaddingbottom\">
                    <h4 class=\"panel-title\">Personal Details</h4>
                </div>
                <div class=\"panel-body\">
                    <hr>
                    <div class=\"form-group\">
                        <label>First Name <span class=\"text-danger\">*</span></label>
                        <input type=\"text\" placeholder=\"First Name\" name=\"firstName\" class=\"form-control\" required
                               value=\"";
        // line 42
        echo twig_escape_filter($this->env, ((($this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Details", array()), "FirstName", array()) != null)) ? ($this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Details", array()), "FirstName", array())) : ("")), "html", null, true);
        echo "\"/>
                    </div>
                    <div class=\"form-group\">
                        <label>Last Name <span class=\"text-danger\">*</span></label>
                        <input type=\"text\" placeholder=\"Last Name\" name=\"lastName\" class=\"form-control\" required
                               value=\"";
        // line 47
        echo twig_escape_filter($this->env, ((($this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Details", array()), "LastName", array()) != null)) ? ($this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Details", array()), "LastName", array())) : ("")), "html", null, true);
        echo "\"/>
                    </div>

                    <div class=\"form-group\">
                        <label>Date of Birth</label>

                        <div class=\"input-group\">
                            <input type=\"text\" class=\"form-control datepicker\" name=\"dateOfBirth\"
                                   placeholder=\"mm/dd/yyyy\"
                                   value=\"";
        // line 56
        echo twig_escape_filter($this->env, ((($this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Details", array()), "DateOfBirth", array()) != null)) ? (twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "Details", array()), "DateOfBirth", array()), "m/d/Y")) : ("")), "html", null, true);
        echo "\">
                            <span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-calendar\"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- col-sm-6 -->
    </div>
    <!-- row -->
    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel\">
                <div class=\"panel-body\">
                    <div class=\"col-sm-4\">
                    </div>

                    <div class=\"col-sm-4\" style=\"text-align: center\">
                        <button class=\"btn btn-success btn-quirk btn-wide mr5\" type=\"submit\">Submit</button>
                        <button type=\"reset\" class=\"btn btn-quirk btn-wide btn-default\">Reset</button>
                    </div>

                    <div class=\"col-sm-4\">
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>";
    }

    public function getTemplateName()
    {
        return "Admin/Users/Partial/LoggedUserForm.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 56,  78 => 47,  70 => 42,  47 => 22,  33 => 13,  19 => 1,);
    }
}
/* <form method="post" enctype="multipart/form-data">*/
/*     <div class="row">*/
/*         <div class="col-sm-6">*/
/*             <div class="panel">*/
/*                 <div class="panel-heading nopaddingbottom">*/
/*                     <h4 class="panel-title">Account Details</h4>*/
/*                 </div>*/
/*                 <div class="panel-body">*/
/*                     <hr>*/
/*                     <div class="form-group">*/
/*                         <label>Username <span class="text-danger">*</span></label>*/
/*                         <input type="text" placeholder="Username" name="username" class="form-control" required*/
/*                                value="{{ (user.UserName != null) ? user.UserName : '' }}" {{ (user.UserName != null) ? 'readonly' : '' }}/>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label>Password <span class="text-danger">*</span></label>*/
/*                         <input type="password" placeholder="Password" name="password" class="form-control"/>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label>Email <span class="text-danger">*</span></label>*/
/*                         <input type="email" placeholder="Email" name="email" class="form-control"*/
/*                                value="{{ (user.Email != null) ? user.Email : '' }}"/>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/*         </div>*/
/*         <!-- col-sm-6 -->*/
/* */
/*         <!-- ####################################################### -->*/
/* */
/*         <div class="col-sm-6">*/
/*             <div class="panel">*/
/*                 <div class="panel-heading nopaddingbottom">*/
/*                     <h4 class="panel-title">Personal Details</h4>*/
/*                 </div>*/
/*                 <div class="panel-body">*/
/*                     <hr>*/
/*                     <div class="form-group">*/
/*                         <label>First Name <span class="text-danger">*</span></label>*/
/*                         <input type="text" placeholder="First Name" name="firstName" class="form-control" required*/
/*                                value="{{ (user.Details.FirstName != null) ? user.Details.FirstName : '' }}"/>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label>Last Name <span class="text-danger">*</span></label>*/
/*                         <input type="text" placeholder="Last Name" name="lastName" class="form-control" required*/
/*                                value="{{ (user.Details.LastName != null) ? user.Details.LastName : '' }}"/>*/
/*                     </div>*/
/* */
/*                     <div class="form-group">*/
/*                         <label>Date of Birth</label>*/
/* */
/*                         <div class="input-group">*/
/*                             <input type="text" class="form-control datepicker" name="dateOfBirth"*/
/*                                    placeholder="mm/dd/yyyy"*/
/*                                    value="{{ (user.Details.DateOfBirth != null) ? user.Details.DateOfBirth|date('m/d/Y') : '' }}">*/
/*                             <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <!-- col-sm-6 -->*/
/*     </div>*/
/*     <!-- row -->*/
/*     <div class="row">*/
/*         <div class="col-sm-12">*/
/*             <div class="panel">*/
/*                 <div class="panel-body">*/
/*                     <div class="col-sm-4">*/
/*                     </div>*/
/* */
/*                     <div class="col-sm-4" style="text-align: center">*/
/*                         <button class="btn btn-success btn-quirk btn-wide mr5" type="submit">Submit</button>*/
/*                         <button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button>*/
/*                     </div>*/
/* */
/*                     <div class="col-sm-4">*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </form>*/
