<?php

/* Admin/Builder/Widget/Master/Base.twig */
class __TwigTemplate_b479941536be5367048f6edca3dc641832e7886c426f96b6eca582155ba17206 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'element' => array($this, 'block_element'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["uid"] = (twig_date_format_filter($this->env, "now", "U") . twig_random($this->env));
        // line 2
        echo "
<div class=\"widget-element\" data-widget=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "WidgetId", array()), "html", null, true);
        echo "\" data-type=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Type", array()), "html", null, true);
        echo "\"
     data-uid=\"";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["uid"]) ? $context["uid"] : null), "html", null, true);
        echo "\">
    ";
        // line 5
        $this->displayBlock('element', $context, $blocks);
        // line 6
        echo "</div>

<script type=\"text/javascript\">
    \$(document).ready(function () {
        \$(\"[data-uid=";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["uid"]) ? $context["uid"] : null), "html", null, true);
        echo "]\").data(\"options\", ";
        echo twig_jsonencode_filter($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Options", array()));
        echo ");
    });
</script>";
    }

    // line 5
    public function block_element($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "Admin/Builder/Widget/Master/Base.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 5,  43 => 10,  37 => 6,  35 => 5,  31 => 4,  25 => 3,  22 => 2,  20 => 1,);
    }
}
/* {% set uid = "now"|date('U') ~ random() %}*/
/* */
/* <div class="widget-element" data-widget="{{ model.WidgetId }}" data-type="{{ model.Type }}"*/
/*      data-uid="{{ uid }}">*/
/*     {% block element %}{% endblock %}*/
/* </div>*/
/* */
/* <script type="text/javascript">*/
/*     $(document).ready(function () {*/
/*         $("[data-uid={{ uid }}]").data("options", {{ model.Options|json_encode|raw }});*/
/*     });*/
/* </script>*/
