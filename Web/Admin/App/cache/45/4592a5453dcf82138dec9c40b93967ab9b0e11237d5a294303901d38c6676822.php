<?php

/* Admin/Errors/AccessDenied.twig */
class __TwigTemplate_73fc1992da1f088eb5a88954f25e118bd8f4eca19077f4e0191ccb8fff16b5aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Errors.twig", "Admin/Errors/AccessDenied.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Errors.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Access Denied";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <section>

        <div class=\"notfoundpanel\">
            <h3>Access Denied</h3>
            <h4>You don't have permissions to access this page. Try logging in with different account.</h4>

            <hr class=\"darken\">

            <a href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "login"), "method"), "html", null, true);
        echo "\" class=\"btn btn-default\">Go to Login Page</a>
        </div>

    </section>
";
    }

    public function getTemplateName()
    {
        return "Admin/Errors/AccessDenied.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 14,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Errors.twig" %}*/
/* */
/* {% block title %}Access Denied{% endblock %}*/
/* */
/* {% block content %}*/
/*     <section>*/
/* */
/*         <div class="notfoundpanel">*/
/*             <h3>Access Denied</h3>*/
/*             <h4>You don't have permissions to access this page. Try logging in with different account.</h4>*/
/* */
/*             <hr class="darken">*/
/* */
/*             <a href="{{ Router.Create("login") }}" class="btn btn-default">Go to Login Page</a>*/
/*         </div>*/
/* */
/*     </section>*/
/* {% endblock %}*/
