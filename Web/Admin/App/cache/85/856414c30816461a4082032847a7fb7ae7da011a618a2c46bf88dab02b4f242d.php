<?php

/* Admin/Menus/Partial/MenuItemsForm.twig */
class __TwigTemplate_97e20f438fd86ffbc9f737d0af56d105cb03ea960de0d74ff2fa5fcece95c293 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'post_scripts' => array($this, 'block_post_scripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<div class=\"col-sm-4\">
    <form action=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "menus-create-item", 1 => array("menuId" => $this->getAttribute($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Menu", array()), "MenuId", array()))), "method"), "html", null, true);
        echo "\" method=\"post\"
          enctype=\"multipart/form-data\">
        <div class=\"panel\">
            <div class=\"panel-heading nopaddingbottom\">
                <h4 class=\"panel-title\">Add Menu Item</h4>
            </div>
            <div class=\"panel-body\">
                <hr>
                <div class=\"form-group\">
                    <label>Caption</label>
                    <input type=\"text\" placeholder=\"Caption\" name=\"caption\" class=\"form-control\"
                           value=\"\"/>
                </div>
                <div class=\"form-group\">
                    <label>Target</label>
                    ";
        // line 19
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "Select", array(0 => $this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "MenuItemTargets", array(), "method"), 1 => "target", 2 => "_self"), "method");
        echo "
                </div>
                <div class=\"form-group\">
                    <label>Parent</label>
                    ";
        // line 23
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "Select", array(0 => $this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "MenuItems", array(0 => $this->getAttribute($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Menu", array()), "MenuId", array())), "method"), 1 => "parent"), "method");
        echo "
                </div>
                <div class=\"form-group\">
                    <label>Item Type</label>
                    ";
        // line 27
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "Select", array(0 => $this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "MenuItemTypes", array(), "method"), 1 => "itemType"), "method");
        echo "
                </div>
                <div id=\"menuItemTypeSubmenu\">
                </div>
            </div>
            <div class=\"panel-footer text-center\">
                <button class=\"btn btn-success btn-quirk btn-wide mr5\" type=\"submit\">Submit</button>
                <button type=\"reset\" class=\"btn btn-quirk btn-wide btn-default\">Reset</button>
            </div>
        </div>
    </form>
</div>
<div class=\"col-sm-4\">
    <div class=\"panel\">
        <div class=\"panel-heading nopaddingbottom\">
            <h4 class=\"panel-title\">Preview</h4>
        </div>
        <div class=\"panel-body\">
            <hr>
            <div id=\"loading\" style=\"width: 100%; text-align: center; display: none;\">
                <img style=\"margin-bottom: 30px\" src=\"../../../../Theme/Admin/images/AjaxLoader/ajax_loader.gif\"/>
            </div>
            <ul class=\"fa-ul menu-preview menu-item-list tree-preview\">
                ";
        // line 50
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Tree", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["branch"]) {
            // line 51
            echo "                    ";
            $this->loadTemplate("Admin/Menus/Partial/Tree.twig", "Admin/Menus/Partial/MenuItemsForm.twig", 51)->display(array_merge($context, array("model" => $context["branch"])));
            // line 52
            echo "                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['branch'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "            </ul>
        </div>
    </div>
</div>

";
        // line 58
        $this->displayBlock('post_scripts', $context, $blocks);
    }

    public function block_post_scripts($context, array $blocks = array())
    {
        // line 59
        echo "
";
    }

    public function getTemplateName()
    {
        return "Admin/Menus/Partial/MenuItemsForm.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 59,  123 => 58,  116 => 53,  102 => 52,  99 => 51,  82 => 50,  56 => 27,  49 => 23,  42 => 19,  24 => 4,  20 => 2,);
    }
}
/* {# @var model \Gdev\MenuManagement\Models\MenuItem #}*/
/* */
/* <div class="col-sm-4">*/
/*     <form action="{{ Router.Create("menus-create-item", {menuId: model.Menu.MenuId}) }}" method="post"*/
/*           enctype="multipart/form-data">*/
/*         <div class="panel">*/
/*             <div class="panel-heading nopaddingbottom">*/
/*                 <h4 class="panel-title">Add Menu Item</h4>*/
/*             </div>*/
/*             <div class="panel-body">*/
/*                 <hr>*/
/*                 <div class="form-group">*/
/*                     <label>Caption</label>*/
/*                     <input type="text" placeholder="Caption" name="caption" class="form-control"*/
/*                            value=""/>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                     <label>Target</label>*/
/*                     {{ HtmlHelper.Select(SelectableHelper.MenuItemTargets(), "target", "_self")|raw }}*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                     <label>Parent</label>*/
/*                     {{ HtmlHelper.Select(SelectableHelper.MenuItems(model.Menu.MenuId), "parent")|raw }}*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                     <label>Item Type</label>*/
/*                     {{ HtmlHelper.Select(SelectableHelper.MenuItemTypes(), "itemType")|raw }}*/
/*                 </div>*/
/*                 <div id="menuItemTypeSubmenu">*/
/*                 </div>*/
/*             </div>*/
/*             <div class="panel-footer text-center">*/
/*                 <button class="btn btn-success btn-quirk btn-wide mr5" type="submit">Submit</button>*/
/*                 <button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button>*/
/*             </div>*/
/*         </div>*/
/*     </form>*/
/* </div>*/
/* <div class="col-sm-4">*/
/*     <div class="panel">*/
/*         <div class="panel-heading nopaddingbottom">*/
/*             <h4 class="panel-title">Preview</h4>*/
/*         </div>*/
/*         <div class="panel-body">*/
/*             <hr>*/
/*             <div id="loading" style="width: 100%; text-align: center; display: none;">*/
/*                 <img style="margin-bottom: 30px" src="../../../../Theme/Admin/images/AjaxLoader/ajax_loader.gif"/>*/
/*             </div>*/
/*             <ul class="fa-ul menu-preview menu-item-list tree-preview">*/
/*                 {% for branch in model.Tree %}*/
/*                     {% include "Admin/Menus/Partial/Tree.twig" with {model: branch} %}*/
/*                 {% endfor %}*/
/*             </ul>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* {% block post_scripts %}*/
/* */
/* {% endblock %}*/
