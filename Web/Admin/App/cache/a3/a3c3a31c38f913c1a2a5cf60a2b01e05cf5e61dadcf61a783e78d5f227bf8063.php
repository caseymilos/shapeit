<?php

/* Admin/Pages/WidgetOptions/SkillsDropdown.twig */
class __TwigTemplate_da24243cebb3a424e5f68bb51170ffe604b0bb2fd0476e34e7966edb6769e5cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-group\">
    <label>Title</label>
    <input type=\"text\" placeholder=\"Title\" name=\"title\" class=\"form-control\"
           value=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "title", array()), "html", null, true);
        echo "\"/>
</div>
<div class=\"form-group\">
    <label>Skill</label>
    <input type=\"text\" placeholder=\"Skill\" name=\"skill\" class=\"form-control\"
           value=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "skill", array()), "html", null, true);
        echo "\"/>
</div>
<div class=\"form-group\">
    <label>Icon Type</label>
    <input type=\"text\" name=\"icon\" class=\"widget-title form-control\"
           value=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "icon", array()), "html", null, true);
        echo "\" placeholder=\"icon\"/>
</div>
";
    }

    public function getTemplateName()
    {
        return "Admin/Pages/WidgetOptions/SkillsDropdown.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 14,  32 => 9,  24 => 4,  19 => 1,);
    }
}
/* <div class="form-group">*/
/*     <label>Title</label>*/
/*     <input type="text" placeholder="Title" name="title" class="form-control"*/
/*            value="{{ widget.Options.title }}"/>*/
/* </div>*/
/* <div class="form-group">*/
/*     <label>Skill</label>*/
/*     <input type="text" placeholder="Skill" name="skill" class="form-control"*/
/*            value="{{ widget.Options.skill }}"/>*/
/* </div>*/
/* <div class="form-group">*/
/*     <label>Icon Type</label>*/
/*     <input type="text" name="icon" class="widget-title form-control"*/
/*            value="{{ widget.Options.icon }}" placeholder="icon"/>*/
/* </div>*/
/* */
