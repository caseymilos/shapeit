<?php

/* Admin/Menus/Partial/MenuForm.twig */
class __TwigTemplate_a902734e2de94182298c88558dbd47497a32a7dd7e4bfb75919d340fa2ebc34b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<div class=\"col-sm-4\">
    <form method=\"post\" enctype=\"multipart/form-data\">
        <div class=\"panel\">
            <div class=\"panel-heading nopaddingbottom\">
                <h4 class=\"panel-title\">Menu</h4>
            </div>
            <div class=\"panel-body\">
                <hr>
                <div class=\"form-group\">
                    <label>Name</label>
                    <input type=\"text\" placeholder=\"Alias\" name=\"alias\" class=\"form-control\"
                           value=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Menu", array()), "Alias", array()), "html", null, true);
        echo "\"/>
                </div>
                <div class=\"form-group\">
                    <label>Language</label>
                    ";
        // line 18
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "Radio", array(0 => $this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "Languages", array(), "method"), 1 => "language", 2 => $this->getAttribute($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Menu", array()), "LanguageId", array())), "method");
        echo "
                </div>
                <div class=\"form-group\">
                    <label>Position</label>
                    ";
        // line 22
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "Radio", array(0 => $this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "MenuPositions", array(), "method"), 1 => "position", 2 => $this->getAttribute($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Menu", array()), "MenuPositionId", array())), "method");
        echo "
                </div>
            </div>
            <div class=\"panel-footer text-center\">
                <button class=\"btn btn-success btn-quirk btn-wide mr5\" type=\"submit\">Submit</button>
                <button type=\"reset\" class=\"btn btn-quirk btn-wide btn-default\">Reset</button>
            </div>
        </div>
    </form>
</div>
<div id=\"sort-menu-script\">
</div>";
    }

    public function getTemplateName()
    {
        return "Admin/Menus/Partial/MenuForm.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 22,  40 => 18,  33 => 14,  19 => 2,);
    }
}
/* {# @var model \MenuViewModel #}*/
/* */
/* <div class="col-sm-4">*/
/*     <form method="post" enctype="multipart/form-data">*/
/*         <div class="panel">*/
/*             <div class="panel-heading nopaddingbottom">*/
/*                 <h4 class="panel-title">Menu</h4>*/
/*             </div>*/
/*             <div class="panel-body">*/
/*                 <hr>*/
/*                 <div class="form-group">*/
/*                     <label>Name</label>*/
/*                     <input type="text" placeholder="Alias" name="alias" class="form-control"*/
/*                            value="{{ model.Menu.Alias }}"/>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                     <label>Language</label>*/
/*                     {{ HtmlHelper.Radio(SelectableHelper.Languages(), "language", model.Menu.LanguageId)|raw }}*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                     <label>Position</label>*/
/*                     {{ HtmlHelper.Radio(SelectableHelper.MenuPositions(), "position", model.Menu.MenuPositionId)|raw }}*/
/*                 </div>*/
/*             </div>*/
/*             <div class="panel-footer text-center">*/
/*                 <button class="btn btn-success btn-quirk btn-wide mr5" type="submit">Submit</button>*/
/*                 <button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button>*/
/*             </div>*/
/*         </div>*/
/*     </form>*/
/* </div>*/
/* <div id="sort-menu-script">*/
/* </div>*/
