<?php

/* Admin/Builder/Layout/RowFull.twig */
class __TwigTemplate_f1d61060739ba15349d7c0bd50fa65b5f088adf2fa4904a1ce9e00974401741d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Builder/Layout/Master/Row.twig", "Admin/Builder/Layout/RowFull.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Builder/Layout/Master/Row.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"col-md-12\" data-column=\"col1\">
        <div class=\"layout-element\" data-widget-area>
            ";
        // line 6
        echo (isset($context["col1"]) ? $context["col1"] : null);
        echo "
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "Admin/Builder/Layout/RowFull.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Builder/Layout/Master/Row.twig" %}*/
/* */
/* {% block content %}*/
/*     <div class="col-md-12" data-column="col1">*/
/*         <div class="layout-element" data-widget-area>*/
/*             {{ col1|raw }}*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
