<?php

/* Admin/Menus/Update.twig */
class __TwigTemplate_0dac6975ee3f2ca8b905205cb07f13223b5a4d47f4a6a898f13a9f123dfe6f59 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Master.twig", "Admin/Menus/Update.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'post_scripts' => array($this, 'block_post_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Master.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Update Menu ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Menu", array()), "Alias", array()), "html", null, true);
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "    <ol class=\"breadcrumb breadcrumb-quirk\">
        <li>
            <a href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "home"), "method"), "html", null, true);
        echo "\">
                <i class=\"fa fa-home mr5\"></i>
                Home
            </a>
        </li>
        <li>
            <a href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "menus-list"), "method"), "html", null, true);
        echo "\">Menus</a>
        </li>
        <li class=\"active\">";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Menu", array()), "Alias", array()), "html", null, true);
        echo "</li>
    </ol>

    <div class=\"row\">
        <input type=\"hidden\" id=\"menu-id\" data-menu-id=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Menu", array()), "MenuId", array()), "html", null, true);
        echo "\">
        ";
        // line 23
        $this->loadTemplate("Admin/Menus/Partial/MenuForm.twig", "Admin/Menus/Update.twig", 23)->display(array_merge($context, array("model" => (isset($context["model"]) ? $context["model"] : null))));
        // line 24
        echo "        ";
        $this->loadTemplate("Admin/Menus/Partial/MenuItemsForm.twig", "Admin/Menus/Update.twig", 24)->display(array_merge($context, array("model" => (isset($context["model"]) ? $context["model"] : null))));
        // line 25
        echo "    </div>

    <div id=\"external-link-form\">

    </div>
";
    }

    // line 32
    public function block_post_scripts($context, array $blocks = array())
    {
        // line 33
        echo "    <script type=\"text/javascript\">
        function updateMenuSubcategory() {
            var itemTypeId = \$(this).val();
            if (itemTypeId == 5 || itemTypeId == 6 || itemTypeId == 7) {
                \$.ajax({
                    url: \"";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "menu-item-type-submenu"), "method"), "html", null, true);
        echo "\",
                    type: \"POST\",
                    data: {
                        itemTypeId: itemTypeId
                    },
                    success: function (data) {
                        \$(\"#menuItemTypeSubmenu\").html(data);
                    }
                });
            }
            else {
                \$(\"#menuItemTypeSubmenu\").html(\"\");
            }
        }

        \$(\"[name=itemType]\").on(\"change\", updateMenuSubcategory);
    </script>
";
    }

    public function getTemplateName()
    {
        return "Admin/Menus/Update.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 38,  86 => 33,  83 => 32,  74 => 25,  71 => 24,  69 => 23,  65 => 22,  58 => 18,  53 => 16,  44 => 10,  40 => 8,  37 => 7,  30 => 5,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Master.twig" %}*/
/* */
/* {# @var model \MenuViewModel #}*/
/* */
/* {% block title %}Update Menu {{ model.Menu.Alias }}{% endblock %}*/
/* */
/* {% block content %}*/
/*     <ol class="breadcrumb breadcrumb-quirk">*/
/*         <li>*/
/*             <a href="{{ Router.Create("home") }}">*/
/*                 <i class="fa fa-home mr5"></i>*/
/*                 Home*/
/*             </a>*/
/*         </li>*/
/*         <li>*/
/*             <a href="{{ Router.Create("menus-list") }}">Menus</a>*/
/*         </li>*/
/*         <li class="active">{{ model.Menu.Alias }}</li>*/
/*     </ol>*/
/* */
/*     <div class="row">*/
/*         <input type="hidden" id="menu-id" data-menu-id="{{ model.Menu.MenuId }}">*/
/*         {% include "Admin/Menus/Partial/MenuForm.twig" with {'model': model} %}*/
/*         {% include "Admin/Menus/Partial/MenuItemsForm.twig" with {'model': model} %}*/
/*     </div>*/
/* */
/*     <div id="external-link-form">*/
/* */
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block post_scripts %}*/
/*     <script type="text/javascript">*/
/*         function updateMenuSubcategory() {*/
/*             var itemTypeId = $(this).val();*/
/*             if (itemTypeId == 5 || itemTypeId == 6 || itemTypeId == 7) {*/
/*                 $.ajax({*/
/*                     url: "{{ Router.Create("menu-item-type-submenu") }}",*/
/*                     type: "POST",*/
/*                     data: {*/
/*                         itemTypeId: itemTypeId*/
/*                     },*/
/*                     success: function (data) {*/
/*                         $("#menuItemTypeSubmenu").html(data);*/
/*                     }*/
/*                 });*/
/*             }*/
/*             else {*/
/*                 $("#menuItemTypeSubmenu").html("");*/
/*             }*/
/*         }*/
/* */
/*         $("[name=itemType]").on("change", updateMenuSubcategory);*/
/*     </script>*/
/* {% endblock %}*/
