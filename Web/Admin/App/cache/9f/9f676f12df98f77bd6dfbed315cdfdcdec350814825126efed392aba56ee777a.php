<?php

/* Admin/Blog/Create.twig */
class __TwigTemplate_029d6c5fc814103e91ca2b606c99aac788901dec2fa638cac7642080e7e15222 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Master.twig", "Admin/Blog/Create.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'post_scripts' => array($this, 'block_post_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Master.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Create Blog";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <ol class=\"breadcrumb breadcrumb-quirk\">
        <li>
            <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "home"), "method"), "html", null, true);
        echo "\">
                <i class=\"fa fa-home mr5\"></i>
                Home
            </a>
        </li>
        <li>
            <a href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "blogs"), "method"), "html", null, true);
        echo "\">Blogs</a>
        </li>
        <li class=\"active\">Create log</li>
    </ol>

    ";
        // line 19
        $this->loadTemplate("Admin/Blog/Partial/BlogForm.twig", "Admin/Blog/Create.twig", 19)->display($context);
    }

    // line 22
    public function block_post_scripts($context, array $blocks = array())
    {
        // line 23
        echo "    <script type=\"text/javascript\">
        \$(\".slug-field\").on(\"keyup\", function () {
            var form = \$(this).closest(\"form\");
            var key = form.find(\"[name=title]\").val() ;
            \$(\"[name=slug]\").val(\$.slugify(key));
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "Admin/Blog/Create.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 23,  64 => 22,  60 => 19,  52 => 14,  43 => 8,  39 => 6,  36 => 5,  30 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Master.twig" %}*/
/* */
/* {% block title %}Create Blog{% endblock %}*/
/* */
/* {% block content %}*/
/*     <ol class="breadcrumb breadcrumb-quirk">*/
/*         <li>*/
/*             <a href="{{ Router.Create("home") }}">*/
/*                 <i class="fa fa-home mr5"></i>*/
/*                 Home*/
/*             </a>*/
/*         </li>*/
/*         <li>*/
/*             <a href="{{ Router.Create("blogs") }}">Blogs</a>*/
/*         </li>*/
/*         <li class="active">Create log</li>*/
/*     </ol>*/
/* */
/*     {% include "Admin/Blog/Partial/BlogForm.twig" %}*/
/* {% endblock %}*/
/* */
/* {% block post_scripts %}*/
/*     <script type="text/javascript">*/
/*         $(".slug-field").on("keyup", function () {*/
/*             var form = $(this).closest("form");*/
/*             var key = form.find("[name=title]").val() ;*/
/*             $("[name=slug]").val($.slugify(key));*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
/* */
