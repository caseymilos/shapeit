<?php

/* Admin/Menus/List.twig */
class __TwigTemplate_89bcc760fe67282d8fb589b337f698ea081e94ba003d7c7445e690ae9a93724c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Master.twig", "Admin/Menus/List.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'additional_styles' => array($this, 'block_additional_styles'),
            'post_scripts' => array($this, 'block_post_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Master.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "List of Menus";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "
    <ol class=\"breadcrumb breadcrumb-quirk\">
        <li>
            <a href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "home"), "method"), "html", null, true);
        echo "\">
                <i class=\"fa fa-home mr5\"></i>
                Home
            </a>
        </li>
        <li>
            <a href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "menus-list"), "method"), "html", null, true);
        echo "\">Menus</a>
        </li>
        <li class=\"active\">Menus Directory</li>
    </ol>

    <div class=\"panel\">
        <div class=\"panel-heading\">
            <h4 class=\"panel-title\">Menus List</h4>
        </div>
        <div class=\"panel-body\">
            <div class=\"table-responsive\">
                <table class=\"datatable table table-bordered table-striped-col\">
                    <thead>
                        <tr>
                            <th>Language</th>
                            <th>Alias</th>
                            <th>Position</th>
                            <th class=\"text-center\" style=\"width: 80px;\">Actions</th>
                        </tr>
                    </thead>

                    <tfoot>
                        <tr>
                            <th>Language</th>
                            <th>Alias</th>
                            <th>Position</th>
                            <th class=\"text-center\" style=\"width: 100px;\">Actions</th>
                        </tr>
                    </tfoot>

                    <tbody>
                        ";
        // line 48
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Menus", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["menu"]) {
            // line 49
            echo "                            <tr>
                                <td>";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["menu"], "Language", array()), "Caption", array()), "html", null, true);
            echo "</td>
                                <td>";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["menu"], "Alias", array()), "html", null, true);
            echo "</td>
                                <td>";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["menu"], "MenuPosition", array()), "Caption", array()), "html", null, true);
            echo "</td>
                                <td>
                                    <ul class=\"table-options\">
                                        <li>
                                            <a href=\"";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "menus-update", 1 => array("id" => $this->getAttribute($context["menu"], "MenuId", array()))), "method"), "html", null, true);
            echo "\">
                                                <i class=\"fa fa-pencil\"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a class=\"confirm\" style=\"cursor: pointer\" data-toggle=\"tooltip\"
                                               title=\"Delete\"
                                               data-href=\"";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "menus-delete", 1 => array("id" => $this->getAttribute($context["menu"], "MenuId", array()))), "method"), "html", null, true);
            echo "\">
                                                <i class=\"fa fa-trash\"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo "                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- panel -->

";
    }

    // line 79
    public function block_additional_styles($context, array $blocks = array())
    {
        // line 80
        echo "    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css\">
    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/select2/select2.css\">
";
    }

    // line 84
    public function block_post_scripts($context, array $blocks = array())
    {
        // line 85
        echo "    <script src=\"Theme/Admin/lib/datatables/jquery.dataTables.js\"></script>
    <script src=\"Theme/Admin/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js\"></script>
    <script src=\"Theme/Admin/lib/select2/select2.js\"></script>

    <script type=\"text/javascript\">
        \$(document).ready(function () {
            'use strict';
            \$('.datatable').DataTable();

            \$('select').select2();
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "Admin/Menus/List.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 85,  153 => 84,  147 => 80,  144 => 79,  134 => 71,  120 => 63,  110 => 56,  103 => 52,  99 => 51,  95 => 50,  92 => 49,  88 => 48,  54 => 17,  45 => 11,  40 => 8,  37 => 7,  31 => 5,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Master.twig" %}*/
/* */
/* {# @var model MenusViewModel #}*/
/* */
/* {% block title %}List of Menus{% endblock %}*/
/* */
/* {% block content %}*/
/* */
/*     <ol class="breadcrumb breadcrumb-quirk">*/
/*         <li>*/
/*             <a href="{{ Router.Create("home") }}">*/
/*                 <i class="fa fa-home mr5"></i>*/
/*                 Home*/
/*             </a>*/
/*         </li>*/
/*         <li>*/
/*             <a href="{{ Router.Create("menus-list") }}">Menus</a>*/
/*         </li>*/
/*         <li class="active">Menus Directory</li>*/
/*     </ol>*/
/* */
/*     <div class="panel">*/
/*         <div class="panel-heading">*/
/*             <h4 class="panel-title">Menus List</h4>*/
/*         </div>*/
/*         <div class="panel-body">*/
/*             <div class="table-responsive">*/
/*                 <table class="datatable table table-bordered table-striped-col">*/
/*                     <thead>*/
/*                         <tr>*/
/*                             <th>Language</th>*/
/*                             <th>Alias</th>*/
/*                             <th>Position</th>*/
/*                             <th class="text-center" style="width: 80px;">Actions</th>*/
/*                         </tr>*/
/*                     </thead>*/
/* */
/*                     <tfoot>*/
/*                         <tr>*/
/*                             <th>Language</th>*/
/*                             <th>Alias</th>*/
/*                             <th>Position</th>*/
/*                             <th class="text-center" style="width: 100px;">Actions</th>*/
/*                         </tr>*/
/*                     </tfoot>*/
/* */
/*                     <tbody>*/
/*                         {% for menu in model.Menus %}*/
/*                             <tr>*/
/*                                 <td>{{ menu.Language.Caption }}</td>*/
/*                                 <td>{{ menu.Alias }}</td>*/
/*                                 <td>{{ menu.MenuPosition.Caption }}</td>*/
/*                                 <td>*/
/*                                     <ul class="table-options">*/
/*                                         <li>*/
/*                                             <a href="{{ Router.Create("menus-update", {id: menu.MenuId}) }}">*/
/*                                                 <i class="fa fa-pencil"></i>*/
/*                                             </a>*/
/*                                         </li>*/
/*                                         <li>*/
/*                                             <a class="confirm" style="cursor: pointer" data-toggle="tooltip"*/
/*                                                title="Delete"*/
/*                                                data-href="{{ Router.Create("menus-delete", {id: menu.MenuId}) }}">*/
/*                                                 <i class="fa fa-trash"></i>*/
/*                                             </a>*/
/*                                         </li>*/
/*                                     </ul>*/
/*                                 </td>*/
/*                             </tr>*/
/*                         {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/*             </div>*/
/*         </div>*/
/*     </div><!-- panel -->*/
/* */
/* {% endblock %}*/
/* */
/* {% block additional_styles %}*/
/*     <link rel="stylesheet" href="Theme/Admin/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css">*/
/*     <link rel="stylesheet" href="Theme/Admin/lib/select2/select2.css">*/
/* {% endblock %}*/
/* */
/* {% block post_scripts %}*/
/*     <script src="Theme/Admin/lib/datatables/jquery.dataTables.js"></script>*/
/*     <script src="Theme/Admin/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>*/
/*     <script src="Theme/Admin/lib/select2/select2.js"></script>*/
/* */
/*     <script type="text/javascript">*/
/*         $(document).ready(function () {*/
/*             'use strict';*/
/*             $('.datatable').DataTable();*/
/* */
/*             $('select').select2();*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
