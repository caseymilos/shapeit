<?php

/* Admin/Shared/Sidebar/Left.twig */
class __TwigTemplate_0619c9d81e195a6703dbd0c44657b1e58ab24262edfc9437b2c8ba570b0049a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"leftpanelinner\">
    ";
        // line 2
        $this->loadTemplate("Admin/Shared/Sidebar/Panels/UserInfo.twig", "Admin/Shared/Sidebar/Left.twig", 2)->display($context);
        // line 3
        echo "

    <ul class=\"nav nav-tabs nav-justified nav-sidebar\">
        <li class=\"tooltips active\" data-toggle=\"tooltip\" title=\"Main Menu\">
            <a data-toggle=\"tab\"
               data-target=\"#mainmenu\">
                <i class=\"tooltips fa fa-ellipsis-h\"></i>
            </a>
        </li>
        <li class=\"tooltips\" data-toggle=\"tooltip\" title=\"Profile Settings\">
            <a data-toggle=\"tab\"
               data-target=\"#settings\">
                <i class=\"fa fa-cog\"></i>
            </a>
        </li>
        <li class=\"tooltips\" data-toggle=\"tooltip\" title=\"Log Out\">
            <a href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "logout"), "method"), "html", null, true);
        echo "\">
                <i class=\"fa fa-sign-out\"></i>
            </a>
        </li>
    </ul>

    <div class=\"tab-content\">
        <div class=\"tab-pane active\" id=\"mainmenu\">
            ";
        // line 27
        $this->loadTemplate("Admin/Shared/Sidebar/Panels/MainMenu.twig", "Admin/Shared/Sidebar/Left.twig", 27)->display($context);
        // line 28
        echo "        </div>
        
        <div class=\"tab-pane\" id=\"settings\">
            ";
        // line 31
        $this->loadTemplate("Admin/Shared/Sidebar/Panels/Settings.twig", "Admin/Shared/Sidebar/Left.twig", 31)->display($context);
        // line 32
        echo "        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "Admin/Shared/Sidebar/Left.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 32,  60 => 31,  55 => 28,  53 => 27,  42 => 19,  24 => 3,  22 => 2,  19 => 1,);
    }
}
/* <div class="leftpanelinner">*/
/*     {% include "Admin/Shared/Sidebar/Panels/UserInfo.twig" %}*/
/* */
/* */
/*     <ul class="nav nav-tabs nav-justified nav-sidebar">*/
/*         <li class="tooltips active" data-toggle="tooltip" title="Main Menu">*/
/*             <a data-toggle="tab"*/
/*                data-target="#mainmenu">*/
/*                 <i class="tooltips fa fa-ellipsis-h"></i>*/
/*             </a>*/
/*         </li>*/
/*         <li class="tooltips" data-toggle="tooltip" title="Profile Settings">*/
/*             <a data-toggle="tab"*/
/*                data-target="#settings">*/
/*                 <i class="fa fa-cog"></i>*/
/*             </a>*/
/*         </li>*/
/*         <li class="tooltips" data-toggle="tooltip" title="Log Out">*/
/*             <a href="{{ Router.Create("logout") }}">*/
/*                 <i class="fa fa-sign-out"></i>*/
/*             </a>*/
/*         </li>*/
/*     </ul>*/
/* */
/*     <div class="tab-content">*/
/*         <div class="tab-pane active" id="mainmenu">*/
/*             {% include "Admin/Shared/Sidebar/Panels/MainMenu.twig" %}*/
/*         </div>*/
/*         */
/*         <div class="tab-pane" id="settings">*/
/*             {% include "Admin/Shared/Sidebar/Panels/Settings.twig" %}*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
