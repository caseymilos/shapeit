<?php

/* Admin/Menus/Subselect/Page.twig */
class __TwigTemplate_ae0e011cbbe08685b0bcba0b75ea61a94e137b2f2cb69edf3ae0c23eb5e665f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-group\">
    <label>Choose Page</label>
    ";
        // line 3
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => $this->getAttribute((isset($context["SelectableHelper"]) ? $context["SelectableHelper"] : null), "Pages", array(), "method"), 1 => "page", 2 => (isset($context["value"]) ? $context["value"] : null)), "method");
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "Admin/Menus/Subselect/Page.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 3,  19 => 1,);
    }
}
/* <div class="form-group">*/
/*     <label>Choose Page</label>*/
/*     {{ HtmlHelper.SearchableSelect(SelectableHelper.Pages(), "page", value)|raw }}*/
/* </div>*/
