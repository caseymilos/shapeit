<?php

/* Admin/Shared/Sidebar/Panels/UserInfo.twig */
class __TwigTemplate_a162ab4bba3a7fda86e5bc8b4df660681f90b55a81197975135a7eaa88b286d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["currentUser"] = $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "GetCurrentUser", array(), "method");
        // line 2
        echo "<div class=\"media leftpanel-profile\">
    <div class=\"media-left\">
        <a href=\"#\">
            <img src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["currentUser"]) ? $context["currentUser"] : null), "Picture", array()), "html", null, true);
        echo "\" alt=\"\" class=\"media-object img-circle\">
        </a>
    </div>
    <div class=\"media-body\">
        <h4 class=\"media-heading\"> ";
        // line 10
        echo "            <p>Travel Insider</p>
            <a data-toggle=\"collapse\" data-target=\"#loguserinfo\"
               class=\"pull-right\">
                <i class=\"fa fa-angle-down\"></i>
            </a>
        </h4>
       ";
        // line 17
        echo "    </div>
</div><!-- leftpanel-profile -->

<div class=\"leftpanel-userinfo collapse\" id=\"loguserinfo\">
    <h5 class=\"sidebar-title\"><a class=\"btn-primary btn-quirk btn btn-block\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "user-update-details", 1 => array("username" => $this->getAttribute((isset($context["currentUser"]) ? $context["currentUser"] : null), "Username", array()))), "method"), "html", null, true);
        echo "\">View Profile</a></h5>
    <ul class=\"list-group\">
        <li class=\"list-group-item\">
            <label class=\"pull-left\">Email</label>
            <span class=\"pull-right\">";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["currentUser"]) ? $context["currentUser"] : null), "Email", array()), "html", null, true);
        echo "</span>
        </li>
        <li class=\"list-group-item\">
            <label class=\"pull-left\">Home</label>
            <span class=\"pull-right\">(032) 1234 567</span>
        </li>
        <li class=\"list-group-item\">
            <label class=\"pull-left\">Mobile</label>
            <span class=\"pull-right\">+63012 3456 789</span>
        </li>
        <li class=\"list-group-item\">
            <label class=\"pull-left\">Social</label>
            <div class=\"social-icons pull-right\">
                <a href=\"#\">
                    <i class=\"fa fa-facebook-official\"></i>
                </a>
                <a href=\"#\">
                    <i class=\"fa fa-twitter\"></i>
                </a>
                <a href=\"#\">
                    <i class=\"fa fa-pinterest\"></i>
                </a>
            </div>
        </li>
    </ul>
</div><!-- leftpanel-userinfo -->";
    }

    public function getTemplateName()
    {
        return "Admin/Shared/Sidebar/Panels/UserInfo.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 25,  47 => 21,  41 => 17,  33 => 10,  26 => 5,  21 => 2,  19 => 1,);
    }
}
/* {% set currentUser = Helper.GetCurrentUser() %}*/
/* <div class="media leftpanel-profile">*/
/*     <div class="media-left">*/
/*         <a href="#">*/
/*             <img src="{{ currentUser.Picture }}" alt="" class="media-object img-circle">*/
/*         </a>*/
/*     </div>*/
/*     <div class="media-body">*/
/*         <h4 class="media-heading"> {#{{ currentUser.FirstName }} {{ currentUser.LastName }}#}*/
/*             <p>Travel Insider</p>*/
/*             <a data-toggle="collapse" data-target="#loguserinfo"*/
/*                class="pull-right">*/
/*                 <i class="fa fa-angle-down"></i>*/
/*             </a>*/
/*         </h4>*/
/*        {# <span>Administrator</span>#}*/
/*     </div>*/
/* </div><!-- leftpanel-profile -->*/
/* */
/* <div class="leftpanel-userinfo collapse" id="loguserinfo">*/
/*     <h5 class="sidebar-title"><a class="btn-primary btn-quirk btn btn-block" href="{{ Router.Create("user-update-details", {username: currentUser.Username}) }}">View Profile</a></h5>*/
/*     <ul class="list-group">*/
/*         <li class="list-group-item">*/
/*             <label class="pull-left">Email</label>*/
/*             <span class="pull-right">{{ currentUser.Email }}</span>*/
/*         </li>*/
/*         <li class="list-group-item">*/
/*             <label class="pull-left">Home</label>*/
/*             <span class="pull-right">(032) 1234 567</span>*/
/*         </li>*/
/*         <li class="list-group-item">*/
/*             <label class="pull-left">Mobile</label>*/
/*             <span class="pull-right">+63012 3456 789</span>*/
/*         </li>*/
/*         <li class="list-group-item">*/
/*             <label class="pull-left">Social</label>*/
/*             <div class="social-icons pull-right">*/
/*                 <a href="#">*/
/*                     <i class="fa fa-facebook-official"></i>*/
/*                 </a>*/
/*                 <a href="#">*/
/*                     <i class="fa fa-twitter"></i>*/
/*                 </a>*/
/*                 <a href="#">*/
/*                     <i class="fa fa-pinterest"></i>*/
/*                 </a>*/
/*             </div>*/
/*         </li>*/
/*     </ul>*/
/* </div><!-- leftpanel-userinfo -->*/
