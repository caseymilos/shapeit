<?php

/* Admin/Builder/Widget/Gallery.twig */
class __TwigTemplate_897da9fa3abc5b09c7335a7b6ce572d37487ad85afdffcf0e1ac2114bb35f432 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Builder/Widget/Master/Panel.twig", "Admin/Builder/Widget/Gallery.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Builder/Widget/Master/Panel.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Gallery";
    }

    public function getTemplateName()
    {
        return "Admin/Builder/Widget/Gallery.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Builder/Widget/Master/Panel.twig" %}*/
/* */
/* {% block title %}Gallery{% endblock %}*/
