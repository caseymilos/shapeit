<?php

/* Admin/Builder/Widget/SocialIcon.twig */
class __TwigTemplate_d49d0956a8f391aca1b38073d0b3ad805e589d5681af8777c41409d019e75512 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Builder/Widget/Master/Panel.twig", "Admin/Builder/Widget/SocialIcon.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Builder/Widget/Master/Panel.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Social Icon";
    }

    public function getTemplateName()
    {
        return "Admin/Builder/Widget/SocialIcon.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Builder/Widget/Master/Panel.twig" %}*/
/* */
/* {% block title %}Social Icon{% endblock %}*/
