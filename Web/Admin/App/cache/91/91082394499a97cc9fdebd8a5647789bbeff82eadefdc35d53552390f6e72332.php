<?php

/* Admin/Pages/WidgetOptions/CentralBlock.twig */
class __TwigTemplate_7c5d664f45dce38783d08dd678919d8ad197d652481b5e96b3b2fe8eec8565e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["uid"] = twig_random($this->env);
        // line 2
        echo "
<div class=\"form-group\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <label>Picture</label>
        </div>
    </div>
    <a data-toggle=\"modal\" data-background href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "media-people-widget-browse"), "method"), "html", null, true);
        echo "\"
       data-target=\"#peopleGalleryWidgetModal-";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["uid"]) ? $context["uid"] : null), "html", null, true);
        echo "\"
       class=\"widget-people-picture-holder\">
        <input type=\"hidden\" id=\"picture-";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["uid"]) ? $context["uid"] : null), "html", null, true);
        echo "\" name=\"picture\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "picture", array()), "html", null, true);
        echo "\" class=\"hidden\">
    </a>
</div>

<div class=\"form-group\">
    <label>Title</label>
    <input type=\"text\" placeholder=\"Title\" name=\"title\" class=\"form-control\"
           value=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "title", array()), "html", null, true);
        echo "\"/>
</div>

<div class=\"row\">
    <div class=\"col-xs-12\">
        <label>Description</label>
        <textarea name=\"description\" class=\"form-control\">";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "description", array()), "html", null, true);
        echo "</textarea>
    </div>
</div>

<div class=\"form-group\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <label class=\"ckbox\">
                <input type=\"checkbox\" name=\"reverse\"
                       ";
        // line 34
        if (($this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "reverse", array()) == "on")) {
            echo "checked=\"checked\"";
        }
        echo ">
                <span>Reverse</span>
            </label>
        </div>
    </div>
</div>

<div class=\"modal bounceIn animated\" id=\"peopleGalleryWidgetModal-";
        // line 41
        echo twig_escape_filter($this->env, (isset($context["uid"]) ? $context["uid"] : null), "html", null, true);
        echo "\" tabindex=\"-1\" role=\"dialog\"
     aria-hidden=\"true\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                <h4 class=\"modal-title\">Add Media</h4>
            </div>
            <div class=\"modal-body\">
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>
                <button type=\"button\" id=\"add-media-to-widget-button\" class=\"btn btn-primary\">Add Media</button>
            </div>
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

<script type=\"text/javascript\">
    \$(document).ready(function () {
        \$(\".widget-people-picture-holder\").on(\"click\", function () {
            \$(\"#peopleGalleryWidgetModal-";
        // line 62
        echo twig_escape_filter($this->env, (isset($context["uid"]) ? $context["uid"] : null), "html", null, true);
        echo "\").data(\"targetInput\", \$(this).find(\"input\"));
        });
    });
</script>";
    }

    public function getTemplateName()
    {
        return "Admin/Pages/WidgetOptions/CentralBlock.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 62,  84 => 41,  72 => 34,  60 => 25,  51 => 19,  39 => 12,  34 => 10,  30 => 9,  21 => 2,  19 => 1,);
    }
}
/* {% set uid = random() %}*/
/* */
/* <div class="form-group">*/
/*     <div class="row">*/
/*         <div class="col-md-12">*/
/*             <label>Picture</label>*/
/*         </div>*/
/*     </div>*/
/*     <a data-toggle="modal" data-background href="{{ Router.Create("media-people-widget-browse") }}"*/
/*        data-target="#peopleGalleryWidgetModal-{{ uid }}"*/
/*        class="widget-people-picture-holder">*/
/*         <input type="hidden" id="picture-{{ uid }}" name="picture" value="{{ widget.Options.picture }}" class="hidden">*/
/*     </a>*/
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label>Title</label>*/
/*     <input type="text" placeholder="Title" name="title" class="form-control"*/
/*            value="{{ widget.Options.title }}"/>*/
/* </div>*/
/* */
/* <div class="row">*/
/*     <div class="col-xs-12">*/
/*         <label>Description</label>*/
/*         <textarea name="description" class="form-control">{{ widget.Options.description }}</textarea>*/
/*     </div>*/
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <div class="row">*/
/*         <div class="col-md-12">*/
/*             <label class="ckbox">*/
/*                 <input type="checkbox" name="reverse"*/
/*                        {% if widget.Options.reverse == "on" %}checked="checked"{% endif %}>*/
/*                 <span>Reverse</span>*/
/*             </label>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* <div class="modal bounceIn animated" id="peopleGalleryWidgetModal-{{ uid }}" tabindex="-1" role="dialog"*/
/*      aria-hidden="true">*/
/*     <div class="modal-dialog">*/
/*         <div class="modal-content">*/
/*             <div class="modal-header">*/
/*                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>*/
/*                 <h4 class="modal-title">Add Media</h4>*/
/*             </div>*/
/*             <div class="modal-body">*/
/*             </div>*/
/*             <div class="modal-footer">*/
/*                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>*/
/*                 <button type="button" id="add-media-to-widget-button" class="btn btn-primary">Add Media</button>*/
/*             </div>*/
/*         </div><!-- modal-content -->*/
/*     </div><!-- modal-dialog -->*/
/* </div><!-- modal -->*/
/* */
/* <script type="text/javascript">*/
/*     $(document).ready(function () {*/
/*         $(".widget-people-picture-holder").on("click", function () {*/
/*             $("#peopleGalleryWidgetModal-{{ uid }}").data("targetInput", $(this).find("input"));*/
/*         });*/
/*     });*/
/* </script>*/
