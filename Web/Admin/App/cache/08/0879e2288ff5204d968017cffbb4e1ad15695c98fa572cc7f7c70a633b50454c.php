<?php

/* Admin/Users/List.twig */
class __TwigTemplate_b2143f38b9110afdbbcd394f23678968403a56089656e8ba64e107e3edf2b418 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Master.twig", "Admin/Users/List.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'post_scripts' => array($this, 'block_post_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Master.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Users";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "
    <ol class=\"breadcrumb breadcrumb-quirk\">
        <li>
            <a href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "home"), "method"), "html", null, true);
        echo "\">
                <i class=\"fa fa-home mr5\"></i>
                Home
            </a>
        </li>
        <li>
            <a href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "user-list"), "method"), "html", null, true);
        echo "\">Users</a>
        </li>
        <li class=\"active\">Users Directory</li>
    </ol>

    <div class=\"row\">
        <div class=\"col-md-12 people-list\">

            <input type=\"hidden\" id=\"currentPage\" value=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "CurrentPage", array()), "html", null, true);
        echo "\">
            <input type=\"hidden\" id=\"totalPages\" value=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "TotalPages", array()), "html", null, true);
        echo "\">

            <div class=\"people-options clearfix\">
                <div class=\"btn-toolbar pull-left\">
                    <a href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "user-create"), "method"), "html", null, true);
        echo "\" class=\"btn btn-success btn-quirk\">Add New User</a>
                    <a href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "invitation-create"), "method"), "html", null, true);
        echo "\" class=\"btn btn-success btn-quirk\">Invite User</a>
                </div>
                <div class=\"btn-group pull-right people-pager\">
                    ";
        // line 32
        if (($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "CurrentPage", array()) != 1)) {
            // line 33
            echo "                        <button type=\"button\" class=\"btn btn-default\" id=\"previousPage\">
                            <i class=\"fa fa-chevron-left\"></i>
                        </button>
                    ";
        }
        // line 37
        echo "                    ";
        if (($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "TotalPages", array()) != $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "CurrentPage", array()))) {
            // line 38
            echo "                        <button type=\"button\" class=\"btn btn-default\" id=\"nextPage\">
                            <i class=\"fa fa-chevron-right\"></i>
                        </button>
                    ";
        }
        // line 42
        echo "                </div>
                <span class=\"people-count pull-right\">Showing <strong>";
        // line 43
        echo twig_escape_filter($this->env, ((($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "CurrentPage", array()) == 1)) ? ("1") : (((($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "UsersPerPage", array()) * $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "CurrentPage", array())) - $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "UsersPerPage", array())) + 1))), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, (((($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "UsersPerPage", array()) * $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "CurrentPage", array())) > $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "TotalUsers", array()))) ? ($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "TotalUsers", array())) : (($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "UsersPerPage", array()) * $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "CurrentPage", array())))), "html", null, true);
        echo "</strong> of <strong>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "TotalUsers", array()), "html", null, true);
        echo "</strong> users</span>
            </div>
            <!-- people-options -->

            <div class=\"row\">
                ";
        // line 48
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Users", array()));
        $context['_iterated'] = false;
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 49
            echo "                    ";
            $this->loadTemplate("Admin/Users/Shared/UserBox.twig", "Admin/Users/List.twig", 49)->display(array_merge($context, array("user" => $context["user"])));
            // line 50
            echo "                ";
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 51
            echo "                    <div class=\"col-md-12\">
                        <p>No users</p>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "            </div>
            <!-- row -->

        </div>
        <!-- col-sm-8 -->

    </div><!-- row -->

";
    }

    // line 65
    public function block_post_scripts($context, array $blocks = array())
    {
        // line 66
        echo "
    <script type=\"text/javascript\">

        \$(\".confirm\").confirmation({
            animation: true,
            placement: 'bottom',
            trigger: 'click',
            btnOkClass: 'btn-danger btn-small',
            btnCancelClass: 'btn-primary btn-small',
            container: 'body',
            popout: true,
            singleton: true,
            onCancel: function () {
                \$(\".confirm\").confirmation('hide');
            }

        });

        \$(\"body\").on(\"click\", \"#previousPage\", function (e) {
            e.preventDefault();

            var currentPage = parseInt(\$(\"#currentPage\").val());
            var previousPage = 1;

            if (currentPage != 1) {
                previousPage = currentPage - 1;
            }

            \$.ajax({
                url: \"users/list/\",
                method: \"GET\",
                data: {
                    pageNumber: previousPage
                },
                success: function (data) {
                    var newContent = \$(data);
                    \$(\".people-list\").html(newContent.find(\".people-list\").first().html());

                    \$(\".confirm\").confirmation({
                        animation: true,
                        placement: 'bottom',
                        trigger: 'click',
                        btnOkClass: 'btn-danger btn-small',
                        btnCancelClass: 'btn-primary btn-small',
                        container: 'body',
                        popout: true,
                        singleton: true,
                        onCancel: function () {
                            \$(\".confirm\").confirmation('hide');
                        }

                    });
                }
            })
        });

        \$(\"body\").on(\"click\", \"#nextPage\", function (e) {
            e.preventDefault();

            var currentPage = parseInt(\$(\"#currentPage\").val());
            var totalPages = parseInt(\$(\"#totalPages\").val());

            if (totalPages != currentPage) {
                var nextPage = currentPage + 1;
            }

            \$.ajax({
                url: \"users/list/\",
                method: \"GET\",
                data: {
                    pageNumber: nextPage
                },
                success: function (data) {
                    var newContent = \$(data);
                    \$(\".people-list\").html(newContent.find(\".people-list\").first().html());

                    \$(\".confirm\").confirmation({
                        animation: true,
                        placement: 'bottom',
                        trigger: 'click',
                        btnOkClass: 'btn-danger btn-small',
                        btnCancelClass: 'btn-primary btn-small',
                        container: 'body',
                        popout: true,
                        singleton: true,
                        onCancel: function () {
                            \$(\".confirm\").confirmation('hide');
                        }

                    });
                }
            })
        });

    </script>

";
    }

    public function getTemplateName()
    {
        return "Admin/Users/List.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 66,  172 => 65,  160 => 55,  151 => 51,  138 => 50,  135 => 49,  117 => 48,  105 => 43,  102 => 42,  96 => 38,  93 => 37,  87 => 33,  85 => 32,  79 => 29,  75 => 28,  68 => 24,  64 => 23,  53 => 15,  44 => 9,  39 => 6,  36 => 5,  30 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Master.twig" %}*/
/* */
/* {% block title %}Users{% endblock %}*/
/* */
/* {% block content %}*/
/* */
/*     <ol class="breadcrumb breadcrumb-quirk">*/
/*         <li>*/
/*             <a href="{{ Router.Create("home") }}">*/
/*                 <i class="fa fa-home mr5"></i>*/
/*                 Home*/
/*             </a>*/
/*         </li>*/
/*         <li>*/
/*             <a href="{{ Router.Create("user-list") }}">Users</a>*/
/*         </li>*/
/*         <li class="active">Users Directory</li>*/
/*     </ol>*/
/* */
/*     <div class="row">*/
/*         <div class="col-md-12 people-list">*/
/* */
/*             <input type="hidden" id="currentPage" value="{{ model.CurrentPage }}">*/
/*             <input type="hidden" id="totalPages" value="{{ model.TotalPages }}">*/
/* */
/*             <div class="people-options clearfix">*/
/*                 <div class="btn-toolbar pull-left">*/
/*                     <a href="{{ Router.Create("user-create") }}" class="btn btn-success btn-quirk">Add New User</a>*/
/*                     <a href="{{ Router.Create("invitation-create") }}" class="btn btn-success btn-quirk">Invite User</a>*/
/*                 </div>*/
/*                 <div class="btn-group pull-right people-pager">*/
/*                     {% if (model.CurrentPage != 1) %}*/
/*                         <button type="button" class="btn btn-default" id="previousPage">*/
/*                             <i class="fa fa-chevron-left"></i>*/
/*                         </button>*/
/*                     {% endif %}*/
/*                     {% if (model.TotalPages != model.CurrentPage) %}*/
/*                         <button type="button" class="btn btn-default" id="nextPage">*/
/*                             <i class="fa fa-chevron-right"></i>*/
/*                         </button>*/
/*                     {% endif %}*/
/*                 </div>*/
/*                 <span class="people-count pull-right">Showing <strong>{{ (model.CurrentPage == 1) ? "1" : model.UsersPerPage * model.CurrentPage - model.UsersPerPage + 1 }}-{{ (model.UsersPerPage * model.CurrentPage > model.TotalUsers) ? model.TotalUsers : model.UsersPerPage * model.CurrentPage }}</strong> of <strong>{{ model.TotalUsers }}</strong> users</span>*/
/*             </div>*/
/*             <!-- people-options -->*/
/* */
/*             <div class="row">*/
/*                 {% for user in model.Users %}*/
/*                     {% include "Admin/Users/Shared/UserBox.twig" with {user: user} %}*/
/*                 {% else %}*/
/*                     <div class="col-md-12">*/
/*                         <p>No users</p>*/
/*                     </div>*/
/*                 {% endfor %}*/
/*             </div>*/
/*             <!-- row -->*/
/* */
/*         </div>*/
/*         <!-- col-sm-8 -->*/
/* */
/*     </div><!-- row -->*/
/* */
/* {% endblock %}*/
/* */
/* {% block post_scripts %}*/
/* */
/*     <script type="text/javascript">*/
/* */
/*         $(".confirm").confirmation({*/
/*             animation: true,*/
/*             placement: 'bottom',*/
/*             trigger: 'click',*/
/*             btnOkClass: 'btn-danger btn-small',*/
/*             btnCancelClass: 'btn-primary btn-small',*/
/*             container: 'body',*/
/*             popout: true,*/
/*             singleton: true,*/
/*             onCancel: function () {*/
/*                 $(".confirm").confirmation('hide');*/
/*             }*/
/* */
/*         });*/
/* */
/*         $("body").on("click", "#previousPage", function (e) {*/
/*             e.preventDefault();*/
/* */
/*             var currentPage = parseInt($("#currentPage").val());*/
/*             var previousPage = 1;*/
/* */
/*             if (currentPage != 1) {*/
/*                 previousPage = currentPage - 1;*/
/*             }*/
/* */
/*             $.ajax({*/
/*                 url: "users/list/",*/
/*                 method: "GET",*/
/*                 data: {*/
/*                     pageNumber: previousPage*/
/*                 },*/
/*                 success: function (data) {*/
/*                     var newContent = $(data);*/
/*                     $(".people-list").html(newContent.find(".people-list").first().html());*/
/* */
/*                     $(".confirm").confirmation({*/
/*                         animation: true,*/
/*                         placement: 'bottom',*/
/*                         trigger: 'click',*/
/*                         btnOkClass: 'btn-danger btn-small',*/
/*                         btnCancelClass: 'btn-primary btn-small',*/
/*                         container: 'body',*/
/*                         popout: true,*/
/*                         singleton: true,*/
/*                         onCancel: function () {*/
/*                             $(".confirm").confirmation('hide');*/
/*                         }*/
/* */
/*                     });*/
/*                 }*/
/*             })*/
/*         });*/
/* */
/*         $("body").on("click", "#nextPage", function (e) {*/
/*             e.preventDefault();*/
/* */
/*             var currentPage = parseInt($("#currentPage").val());*/
/*             var totalPages = parseInt($("#totalPages").val());*/
/* */
/*             if (totalPages != currentPage) {*/
/*                 var nextPage = currentPage + 1;*/
/*             }*/
/* */
/*             $.ajax({*/
/*                 url: "users/list/",*/
/*                 method: "GET",*/
/*                 data: {*/
/*                     pageNumber: nextPage*/
/*                 },*/
/*                 success: function (data) {*/
/*                     var newContent = $(data);*/
/*                     $(".people-list").html(newContent.find(".people-list").first().html());*/
/* */
/*                     $(".confirm").confirmation({*/
/*                         animation: true,*/
/*                         placement: 'bottom',*/
/*                         trigger: 'click',*/
/*                         btnOkClass: 'btn-danger btn-small',*/
/*                         btnCancelClass: 'btn-primary btn-small',*/
/*                         container: 'body',*/
/*                         popout: true,*/
/*                         singleton: true,*/
/*                         onCancel: function () {*/
/*                             $(".confirm").confirmation('hide');*/
/*                         }*/
/* */
/*                     });*/
/*                 }*/
/*             })*/
/*         });*/
/* */
/*     </script>*/
/* */
/* {% endblock %}*/
