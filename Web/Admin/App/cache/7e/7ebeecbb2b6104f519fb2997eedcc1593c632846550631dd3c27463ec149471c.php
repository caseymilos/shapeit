<?php

/* Admin/Pages/List.twig */
class __TwigTemplate_aec7215d1257d43a8cd0585ac5ebfea7f7cd258e24b4c37bdd873993a94d968e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Master.twig", "Admin/Pages/List.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'additional_styles' => array($this, 'block_additional_styles'),
            'post_scripts' => array($this, 'block_post_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Master.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "List of pages";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "
    <ol class=\"breadcrumb breadcrumb-quirk\">
        <li>
            <a href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "home"), "method"), "html", null, true);
        echo "\">
                <i class=\"fa fa-home mr5\"></i>
                Home
            </a>
        </li>
        <li>
            <a href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "pages-list"), "method"), "html", null, true);
        echo "\">Pages</a>
        </li>
        <li class=\"active\">Pages Directory</li>
    </ol>


    <div class=\"panel\">
        <div class=\"panel-heading\">
            <h4 class=\"panel-title\">Pages List</h4>
        </div>
        <div class=\"panel-body\">
            <div class=\"table-responsive\">
                <table class=\"datatable table table-bordered table-striped-col\">
                    <thead>
                        <tr>
                            <th style=\"width: 100px;\">Image</th>
                            <th>Title</th>
                            <th class=\"text-center\" style=\"width: 80px;\">Actions</th>
                        </tr>
                    </thead>

                    <tfoot>
                        <tr>
                            <th>Image</th>
                            <th>Title</th>
                            <th class=\"text-center\" style=\"width: 80px;\">Actions</th>
                        </tr>
                    </tfoot>

                    <tbody>
                        ";
        // line 47
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Pages", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
            // line 48
            echo "                            <tr>
                                <td>
                                    <img class=\"media-object\" style=\"width: 100px; height: 100px; margin: auto\" alt=\"\"
                                         src=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["page"], "PictureSource", array(0 => "Small"), "method"), "html", null, true);
            echo "\">
                                </td>
                                <td>";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["page"], "GetDescription", array(0 => $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "GetLanguage", array(), "method")), "method"), "Title", array()), "html", null, true);
            echo "</td>
                                <td>
                                    <ul class=\"table-options\">
                                        <li>
                                            <a href=\"";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "page-builder-duplicate", 1 => array("id" => $this->getAttribute($context["page"], "PageId", array()))), "method"), "html", null, true);
            echo "\"
                                               title=\"Duplicate\">
                                                <i class=\"fa fa-copy\"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href=\"";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "pages-thumbnail-crop", 1 => array("id" => $this->getAttribute($context["page"], "PageId", array()), "size" => "header")), "method"), "html", null, true);
            echo "\"
                                               title=\"Crop header image\">
                                                <i class=\"fa fa-crop\"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href=\"";
            // line 69
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "pages-thumbnail-crop", 1 => array("id" => $this->getAttribute($context["page"], "PageId", array()), "size" => "thumb")), "method"), "html", null, true);
            echo "\"
                                               title=\"Crop thumbnail\">
                                                <i class=\"fa fa-crop\"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href=\"";
            // line 75
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "page-builder", 1 => array("id" => $this->getAttribute($context["page"], "PageId", array()))), "method"), "html", null, true);
            echo "\">
                                                <i class=\"fa fa-pencil\"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a class=\"confirm\" style=\"cursor: pointer\"
                                               data-href=\"";
            // line 81
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "pages-delete", 1 => array("id" => $this->getAttribute($context["page"], "PageId", array()))), "method"), "html", null, true);
            echo "\">
                                                <i class=\"fa fa-trash\"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 89
        echo "                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- panel -->

";
    }

    // line 97
    public function block_additional_styles($context, array $blocks = array())
    {
        // line 98
        echo "    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css\">
    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/select2/select2.css\">
";
    }

    // line 102
    public function block_post_scripts($context, array $blocks = array())
    {
        // line 103
        echo "    <script src=\"Theme/Admin/lib/datatables/jquery.dataTables.js\"></script>
    <script src=\"Theme/Admin/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js\"></script>
    <script src=\"Theme/Admin/lib/select2/select2.js\"></script>

    <script type=\"text/javascript\">
        \$(document).ready(function () {
            'use strict';
            \$('.datatable').DataTable();

            \$('select').select2();
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "Admin/Pages/List.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  180 => 103,  177 => 102,  171 => 98,  168 => 97,  158 => 89,  144 => 81,  135 => 75,  126 => 69,  117 => 63,  108 => 57,  101 => 53,  96 => 51,  91 => 48,  87 => 47,  54 => 17,  45 => 11,  40 => 8,  37 => 7,  31 => 5,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Master.twig" %}*/
/* */
/* {# @var model \PagesViewModel #}*/
/* */
/* {% block title %}List of pages{% endblock %}*/
/* */
/* {% block content %}*/
/* */
/*     <ol class="breadcrumb breadcrumb-quirk">*/
/*         <li>*/
/*             <a href="{{ Router.Create("home") }}">*/
/*                 <i class="fa fa-home mr5"></i>*/
/*                 Home*/
/*             </a>*/
/*         </li>*/
/*         <li>*/
/*             <a href="{{ Router.Create("pages-list") }}">Pages</a>*/
/*         </li>*/
/*         <li class="active">Pages Directory</li>*/
/*     </ol>*/
/* */
/* */
/*     <div class="panel">*/
/*         <div class="panel-heading">*/
/*             <h4 class="panel-title">Pages List</h4>*/
/*         </div>*/
/*         <div class="panel-body">*/
/*             <div class="table-responsive">*/
/*                 <table class="datatable table table-bordered table-striped-col">*/
/*                     <thead>*/
/*                         <tr>*/
/*                             <th style="width: 100px;">Image</th>*/
/*                             <th>Title</th>*/
/*                             <th class="text-center" style="width: 80px;">Actions</th>*/
/*                         </tr>*/
/*                     </thead>*/
/* */
/*                     <tfoot>*/
/*                         <tr>*/
/*                             <th>Image</th>*/
/*                             <th>Title</th>*/
/*                             <th class="text-center" style="width: 80px;">Actions</th>*/
/*                         </tr>*/
/*                     </tfoot>*/
/* */
/*                     <tbody>*/
/*                         {% for page in model.Pages %}*/
/*                             <tr>*/
/*                                 <td>*/
/*                                     <img class="media-object" style="width: 100px; height: 100px; margin: auto" alt=""*/
/*                                          src="{{ page.PictureSource("Small") }}">*/
/*                                 </td>*/
/*                                 <td>{{ page.GetDescription(Helper.GetLanguage()).Title }}</td>*/
/*                                 <td>*/
/*                                     <ul class="table-options">*/
/*                                         <li>*/
/*                                             <a href="{{ Router.Create("page-builder-duplicate", {id: page.PageId}) }}"*/
/*                                                title="Duplicate">*/
/*                                                 <i class="fa fa-copy"></i>*/
/*                                             </a>*/
/*                                         </li>*/
/*                                         <li>*/
/*                                             <a href="{{ Router.Create("pages-thumbnail-crop", {id: page.PageId, size: 'header'}) }}"*/
/*                                                title="Crop header image">*/
/*                                                 <i class="fa fa-crop"></i>*/
/*                                             </a>*/
/*                                         </li>*/
/*                                         <li>*/
/*                                             <a href="{{ Router.Create("pages-thumbnail-crop", {id: page.PageId, size: 'thumb'}) }}"*/
/*                                                title="Crop thumbnail">*/
/*                                                 <i class="fa fa-crop"></i>*/
/*                                             </a>*/
/*                                         </li>*/
/*                                         <li>*/
/*                                             <a href="{{ Router.Create("page-builder", {id: page.PageId}) }}">*/
/*                                                 <i class="fa fa-pencil"></i>*/
/*                                             </a>*/
/*                                         </li>*/
/*                                         <li>*/
/*                                             <a class="confirm" style="cursor: pointer"*/
/*                                                data-href="{{ Router.Create("pages-delete", {id: page.PageId}) }}">*/
/*                                                 <i class="fa fa-trash"></i>*/
/*                                             </a>*/
/*                                         </li>*/
/*                                     </ul>*/
/*                                 </td>*/
/*                             </tr>*/
/*                         {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/*             </div>*/
/*         </div>*/
/*     </div><!-- panel -->*/
/* */
/* {% endblock %}*/
/* */
/* {% block additional_styles %}*/
/*     <link rel="stylesheet" href="Theme/Admin/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css">*/
/*     <link rel="stylesheet" href="Theme/Admin/lib/select2/select2.css">*/
/* {% endblock %}*/
/* */
/* {% block post_scripts %}*/
/*     <script src="Theme/Admin/lib/datatables/jquery.dataTables.js"></script>*/
/*     <script src="Theme/Admin/lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>*/
/*     <script src="Theme/Admin/lib/select2/select2.js"></script>*/
/* */
/*     <script type="text/javascript">*/
/*         $(document).ready(function () {*/
/*             'use strict';*/
/*             $('.datatable').DataTable();*/
/* */
/*             $('select').select2();*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
