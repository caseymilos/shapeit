<?php

/* Admin/Pages/WidgetOptions/ContactBox.twig */
class __TwigTemplate_241421742bccb44e6704a162d428932cd954e4baca0e38d312ab00cb5064e1f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-group\">
    <label>Box Icon</label>
    ";
        // line 3
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => array("calendar" => "Calendar", "phone" => "Mobile", "phone2" => "Mobile II", "phone3" => "Telephone", "thumbsUp" => "Like us", "facebook" => "Facebook", "paypal" => "Paypal", "googlePlus" => "Google Plus", "wikipedia" => "Wikipedia", "spotify" => "Spotify", "reddit" => "Reddit", "linkedIn" => "Linked In", "skype" => "Skype", "twitter" => "Twitter", "youtube" => "Youtube", "vimeo" => "Vimeo", "yahoo" => "Yahoo", "dropbox" => "Dropbox", "ebay" => "Ebay", "trumblr" => "Trumblr", "wordpress" => "Wordpress", "yelp" => "Yelp", "acrobat" => "Acrobat", "amazon" => "Amazon", "appstore" => "Appstore", "pinterst" => "Pinterst"), 1 => "boxIcon", 2 => $this->getAttribute($this->getAttribute(        // line 30
(isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "boxIcon", array())), "method");
        echo "
</div>
<div class=\"form-group\">
    <label>Icon Url</label>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <input type=\"text\" name=\"url\" class=\"widget-url form-control\"
                   value=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "url", array()), "html", null, true);
        echo "\" placeholder=\"Url\"/>
        </div>
    </div>
</div>
<div class=\"form-group\">
    <label>Title</label>
    <input type=\"text\" placeholder=\"Title\" name=\"title\" class=\"form-control\"
           value=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "title", array()), "html", null, true);
        echo "\"/>
</div>

<div class=\"form-group\">
    <label>Subtitle</label>
    <input type=\"text\" placeholder=\"Subtitle\" name=\"subtitle\" class=\"form-control\"
           value=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "subtitle", array()), "html", null, true);
        echo "\"/>
</div>
";
    }

    public function getTemplateName()
    {
        return "Admin/Pages/WidgetOptions/ContactBox.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 50,  44 => 44,  34 => 37,  24 => 30,  23 => 3,  19 => 1,);
    }
}
/* <div class="form-group">*/
/*     <label>Box Icon</label>*/
/*     {{ HtmlHelper.SearchableSelect({*/
/*         calendar: "Calendar",*/
/*         phone: "Mobile",*/
/*         phone2: "Mobile II",*/
/*         phone3: "Telephone",*/
/*         thumbsUp: "Like us",*/
/*         facebook: "Facebook",*/
/*         paypal: "Paypal",*/
/*         googlePlus: "Google Plus",*/
/*         wikipedia: "Wikipedia",*/
/*         spotify: "Spotify",*/
/*         reddit: "Reddit",*/
/*         linkedIn: "Linked In",*/
/*         skype: "Skype",*/
/*         twitter: "Twitter",*/
/*         youtube: "Youtube",*/
/*         vimeo: "Vimeo",*/
/*         yahoo: "Yahoo",*/
/*         dropbox: "Dropbox",*/
/*         ebay: "Ebay",*/
/*         trumblr: "Trumblr",*/
/*         wordpress: "Wordpress",*/
/*         yelp: "Yelp",*/
/*         acrobat: "Acrobat",*/
/*         amazon: "Amazon",*/
/*         appstore: "Appstore",*/
/*         pinterst: "Pinterst",*/
/*     }, "boxIcon", widget.Options.boxIcon)|raw }}*/
/* </div>*/
/* <div class="form-group">*/
/*     <label>Icon Url</label>*/
/*     <div class="row">*/
/*         <div class="col-md-12">*/
/*             <input type="text" name="url" class="widget-url form-control"*/
/*                    value="{{ widget.Options.url }}" placeholder="Url"/>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <div class="form-group">*/
/*     <label>Title</label>*/
/*     <input type="text" placeholder="Title" name="title" class="form-control"*/
/*            value="{{ widget.Options.title }}"/>*/
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label>Subtitle</label>*/
/*     <input type="text" placeholder="Subtitle" name="subtitle" class="form-control"*/
/*            value="{{ widget.Options.subtitle }}"/>*/
/* </div>*/
/* */
