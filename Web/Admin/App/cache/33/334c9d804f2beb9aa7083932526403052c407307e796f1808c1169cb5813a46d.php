<?php

/* Admin/Builder/Layout/Row33_33_33.twig */
class __TwigTemplate_81cd55acc0f2a3efcf4eb93d75aac15939b28193bebf1456dbef32ecc6532f31 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Builder/Layout/Master/Row.twig", "Admin/Builder/Layout/Row33_33_33.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Builder/Layout/Master/Row.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"col-md-4\" data-column=\"col1\">
        <div class=\"layout-element\" data-widget-area>
            ";
        // line 6
        echo (isset($context["col1"]) ? $context["col1"] : null);
        echo "
        </div>
    </div>
    <div class=\"col-md-4\" data-column=\"col2\">
        <div class=\"layout-element\" data-widget-area>
            ";
        // line 11
        echo (isset($context["col2"]) ? $context["col2"] : null);
        echo "
        </div>
    </div>
    <div class=\"col-md-4\" data-column=\"col3\">
        <div class=\"layout-element\" data-widget-area>
            ";
        // line 16
        echo (isset($context["col3"]) ? $context["col3"] : null);
        echo "
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "Admin/Builder/Layout/Row33_33_33.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 16,  43 => 11,  35 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Builder/Layout/Master/Row.twig" %}*/
/* */
/* {% block content %}*/
/*     <div class="col-md-4" data-column="col1">*/
/*         <div class="layout-element" data-widget-area>*/
/*             {{ col1|raw }}*/
/*         </div>*/
/*     </div>*/
/*     <div class="col-md-4" data-column="col2">*/
/*         <div class="layout-element" data-widget-area>*/
/*             {{ col2|raw }}*/
/*         </div>*/
/*     </div>*/
/*     <div class="col-md-4" data-column="col3">*/
/*         <div class="layout-element" data-widget-area>*/
/*             {{ col3|raw }}*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
