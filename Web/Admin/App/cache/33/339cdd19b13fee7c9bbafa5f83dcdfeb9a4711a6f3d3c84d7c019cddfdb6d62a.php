<?php

/* Admin/Media/MediaManager.twig */
class __TwigTemplate_9953d89374e1f94049170a93458e85475658fffd909b809923e42bfc3b26418c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Master.twig", "Admin/Media/MediaManager.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'additional_styles' => array($this, 'block_additional_styles'),
            'post_scripts' => array($this, 'block_post_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Master.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Media Manager";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "
    <ol class=\"breadcrumb breadcrumb-quirk\">
        <li>
            <a href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "home"), "method"), "html", null, true);
        echo "\">
                <i class=\"fa fa-home mr5\"></i>
                Home
            </a>
        </li>
        <li>
            <a href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "media-manager"), "method"), "html", null, true);
        echo "\">Media</a>
        </li>
        <li class=\"active\">Media Manager</li>
    </ol>

    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"well well-asset-options clearfix\">
                <div class=\"btn-toolbar btn-toolbar-media-manager pull-left\" role=\"toolbar\">

                    <div class=\"btn-group\" role=\"group\">
                        <button type=\"button\" class=\"btn btn-default\" id=\"batch-delete-media\">
                            <i class=\"fa fa-trash\"></i>
                            Delete
                        </button>
                    </div>
                </div><!-- btn-toolbar -->

                <div class=\"btn-group pull-right\" data-toggle=\"buttons\">
                    <button data-toggle=\"modal\" data-target=\"#mediaModal\"
                            class=\"btn btn-danger btn-quirk btn-block\">
                        Upload Files
                    </button>
                </div>
            </div>

            <div class=\"row filemanager\">

                ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Media", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["media"]) {
            // line 46
            echo "                    <div class=\"col-xs-6 col-sm-4 col-md-3 col-lg-2 ";
            echo twig_escape_filter($this->env, twig_lower_filter($this->env, $this->getAttribute($this->getAttribute($context["media"], "MediaType", array()), "Caption", array())), "html", null, true);
            echo " media-manager-item\"
                         data-id=\"";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["media"], "MediaId", array()), "html", null, true);
            echo "\">
                        ";
            // line 48
            $this->loadTemplate((("Admin/Media/Manager/Items/" . $this->getAttribute($this->getAttribute($context["media"], "MediaType", array()), "Caption", array())) . ".twig"), "Admin/Media/MediaManager.twig", 48)->display(array_merge($context, array("media" => $context["media"])));
            // line 49
            echo "                    </div><!-- col-xs-6 -->
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['media'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "            </div><!-- row -->
        </div><!-- col-sm-9 -->
    </div>

    <!-- Modal -->
    <div class=\"modal bounceIn animated\" id=\"mediaModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mediaModalLabel\"
         aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                    <h4 class=\"modal-title\" id=\"mediaModalLabel\">Upload Media</h4>
                </div>
                <div class=\"modal-body\">
                    <form action=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "media-upload"), "method"), "html", null, true);
        echo "\" class=\"dropzone\" id=\"upload-files\">
                    </form>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default close-media-manager-modal-button\">Close</button>
                    <button type=\"button\" class=\"btn btn-primary\" id=\"upload-files-button\">Upload</button>
                </div>
            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <!-- Modal -->
    <div class=\"modal bounceIn animated\" id=\"media-edit-modal\" tabindex=\"-1\" role=\"dialog\"
         aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <div id=\"uploaded-file-template\" class=\"hidden\">
        <div class=\"media-uploaded-file\">
            <div class=\"row\">
                <div class=\"col-md-3\">
                    <div class=\"dz-preview dz-image-preview\">
                        <div class=\"dz-image\">
                            <img data-dz-thumbnail=\"\"/>
                        </div>
                        <div class=\"dz-progress\">
                            <span class=\"dz-upload\" data-dz-uploadprogress=\"\"></span>
                        </div>
                        <div class=\"dz-success-mark\">
                            <span>✔</span>
                        </div>
                        <div class=\"dz-error-mark\">
                            <span>✘</span>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-9\">
                    <input type=\"text\" class=\"form-control\" data-title placeholder=\"Enter title here\">
                    <input type=\"text\" class=\"form-control\" data-copyrights placeholder=\"Enter copyrights here\">
                    <div class=\"dz-details row\">
                        <div class=\"dz-filename col-md-8\">
                            <span data-dz-name=\"\"></span>
                        </div>
                        <div class=\"dz-size col-md-4 text-right\" data-dz-size=\"\"></div>
                    </div>

                    <div class=\"dz-error-message\">
                        <span data-dz-errormessage=\"\"></span>
                    </div>

                    <div class=\"text-right\">
                        <a class=\"media-remove-file-button btn btn-xs btn-danger\" data-dz-remove>
                            <i class=\"fa fa-times\"></i>
                            &nbsp; Remove file
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id=\"media-manager-item-template\" class=\"hidden\">
        <div class=\"col-xs-6 col-sm-4 col-md-3 col-lg-2 media-manager-item\"
             data-id=\"\">
            <div class=\"thmb\">
                <label class=\"ckbox\">
                    <input type=\"checkbox\" data-selected-media-checkbox name=\"selectedMedia[]\" value=\"\">
                    <span></span>
                </label>
                <div class=\"btn-group fm-group\">
                    <button type=\"button\" class=\"btn btn-default dropdown-toggle fm-toggle\"
                            data-toggle=\"dropdown\">
                        <span class=\"caret\"></span>
                    </button>
                    <ul class=\"dropdown-menu pull-right fm-menu\" role=\"menu\">
                        <li>
                            <a href=\"#\">
                                <i class=\"fa fa-share\"></i>
                                Share
                            </a>
                        </li>
                        <li>
                            <a class=\"media-manager-edit-item\">
                                <i class=\"fa fa-pencil\"></i>
                                Edit
                            </a>
                        </li>
                        <li>
                            <a target=\"_blank\" data-download-button href=\"\">
                                <i class=\"fa fa-download\"></i>
                                Download
                            </a>
                        </li>
                        <li>
                            <a class=\"media-manager-delete-item\">
                                <i class=\"fa fa-trash-o\"></i>
                                Delete
                            </a>
                        </li>
                    </ul>
                </div>
                <div class=\"thmb-prev\">
                    <img src=\"\" class=\"img-responsive\"
                         alt=\"\"/>
                </div>
                <h5 class=\"fm-title\">
                    <a href=\"\"></a>
                </h5>
                <small class=\"text-muted\">Added:
                    <span class=\"item-added-time\"></span>
                </small>
            </div>
        </div>
    </div>
";
    }

    // line 184
    public function block_additional_styles($context, array $blocks = array())
    {
        // line 185
        echo "    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/jquery.gritter/jquery.gritter.css\">
";
    }

    // line 188
    public function block_post_scripts($context, array $blocks = array())
    {
        // line 189
        echo "    <script type=\"text/javascript\" src=\"Theme/Admin/lib/jquery.gritter/jquery.gritter.js\"></script>
    <script type=\"text/javascript\">
        jQuery(document).ready(function () {

            \$(\".filemanager\").on(\"click\", \".media-manager-delete-item\", function () {
                var item = \$(this).closest(\".media-manager-item\");
                var id = item.data(\"id\");
                \$.ajax({
                    url: \"";
        // line 197
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "media-delete"), "method"), "html", null, true);
        echo "\",
                    type: \"POST\",
                    dataType: \"json\",
                    data: {
                        id: id
                    },
                    success: function (response) {
                        if (response.Success == true) {
                            \$.gritter.add({
                                title: 'Success',
                                text: 'File is deleted',
                                class_name: 'with-icon check-circle success'
                            });
                            item.fadeOut();
                        }
                    }
                });
            });

            \$(\".filemanager\").on(\"click\", \".media-manager-edit-item\", function () {
                var item = \$(this).closest(\".media-manager-item\");
                var id = item.data(\"id\");
                \$.ajax({
                    url: \"";
        // line 220
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "media-edit"), "method"), "html", null, true);
        echo "\",
                    type: \"GET\",
                    dataType: \"html\",
                    data: {
                        id: id
                    },
                    success: function (response) {
                        \$(\"#media-edit-modal\").find(\".modal-content\").html(response);
                        \$(\"#media-edit-modal\").modal(\"show\");
                    }
                });
            });

            \$(\".close-media-manager-modal-button\").on(\"click\", function () {
                Dropzone.forElement(\"#upload-files\").removeAllFiles(true);
                \$(this).closest(\".modal\").modal(\"hide\");
            });

            \$(\"#batch-delete-media\").on(\"click\", function () {
                var selectedMedia = \$(\".ckbox :checked\");
                if (selectedMedia.length == 0) {
                    \$.gritter.add({
                        title: 'Failed',
                        text: 'You didn\\'t select any item',
                        class_name: 'with-icon times-circle danger'
                    });
                }
                else {
                    var indexes = [];
                    selectedMedia.each(function () {
                        indexes.push(\$(this).val());
                    });

                    \$.ajax({
                        method: \"POST\",
                        url: \"";
        // line 255
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "media-batch-delete"), "method"), "html", null, true);
        echo "\",
                        data: {
                            ids: indexes
                        },
                        success: function () {

                            for (var i = 0; i < indexes.length; i++) {
                                var mediaId = indexes[i];
                                \$(\".media-manager-item[data-id=\" + mediaId + \"]\").fadeOut();
                            }

                            \$.gritter.add({
                                title: 'Success',
                                text: 'Items are deleted',
                                class_name: 'with-icon check-circle success'
                            });


                        }
                    });
                }
            });

            // dropzone

            Dropzone.options.uploadFiles = {
                // Prevents Dropzone from uploading dropped files immediately
                autoProcessQueue: false,
                maxFilesize: 2,
                parallelUploads: 1000,
                previewTemplate: document.getElementById('uploaded-file-template').innerHTML,

                accept: function (file, done) {
                    done();
                },

                init: function () {
                    var submitButton = document.querySelector(\"#upload-files-button\"),
                            uploadFiles = this;

                    submitButton.addEventListener(\"click\", function () {
                        uploadFiles.processQueue();
                    });

                    this.on(\"addedfile\", function () {

                    });

                    this.on(\"sending\", function (file, xhr, formData) {
                        var title = \$(file.previewElement).closest(\".media-uploaded-file\").find(\"[data-title]\").val();
                        formData.append(\"title\", title);
                        var copyrights = \$(file.previewElement).closest(\".media-uploaded-file\").find(\"[data-copyrights]\").val();
                        formData.append(\"copyrights\", copyrights);
                    });

                    this.on(\"success\", function (file, response) {
                        var \$this = this;
                        \$(file.previewElement).delay(500).slideUp(100, function () {
                            \$this.removeFile(file);
                        });

                        // create a thumbnail in media manager
                        var template = \$(\"#media-manager-item-template\").find(\".media-manager-item\").first().clone();
                        template.attr(\"data-id\", response.Media.MediaId);
                        template.addClass(response.Media.Type);
                        template.find(\"[data-download-button]\").attr(\"href\", response.Media.SourceUrl);
                        template.find(\"[data-selected-media-checkbox]\").val(response.Media.MediaId);
                        template.find(\".thmb-prev\").find(\"img\").attr(\"src\", response.Media.PictureSource).attr(\"alt\", response.Media.Title);
                        template.find(\".fm-title\").find(\"a\").attr(\"href\", response.Media.SourceUrl).attr(\"alt\", response.Media.Title).text(response.Media.Title);
                        template.find(\".item-added-time\").text(response.Media.AddedTime);

                        \$(\".filemanager\").prepend(template.hide().fadeIn(1000));
                        activateCheckboxes();
                    });

                }
            };

            'use strict';

            function activateCheckboxes() {
                jQuery('.thmb').hover(function () {
                    var t = jQuery(this);
                    t.find('.ckbox').show();
                    t.find('.fm-group').show();
                }, function () {
                    var t = jQuery(this);
                    if (!t.closest('.thmb').hasClass('checked')) {
                        t.find('.ckbox').hide();
                        t.find('.fm-group').hide();
                    }
                });

                jQuery('.ckbox').each(function () {
                    var t = jQuery(this);
                    var parent = t.parent();
                    if (t.find('input').is(':checked')) {
                        t.show();
                        parent.find('.fm-group').show();
                        parent.addClass('checked');
                    }
                });


                jQuery('.ckbox').click(function () {
                    var t = jQuery(this);
                    if (!t.find('input').is(':checked')) {
                        t.closest('.thmb').removeClass('checked');
                        enable_itemopt(false);
                    } else {
                        t.closest('.thmb').addClass('checked');
                        enable_itemopt(true);
                    }
                });

                jQuery('#selectall').click(function () {
                    if (jQuery(this).is(':checked')) {
                        jQuery('.thmb').each(function () {
                            jQuery(this).find('input').attr('checked', true);
                            jQuery(this).addClass('checked');
                            jQuery(this).find('.ckbox, .fm-group').show();
                        });
                        enable_itemopt(true);
                    } else {
                        jQuery('.thmb').each(function () {
                            jQuery(this).find('input').attr('checked', false);
                            jQuery(this).removeClass('checked');
                            jQuery(this).find('.ckbox, .fm-group').hide();
                        });
                        enable_itemopt(false);
                    }
                });

                function enable_itemopt(enable) {
                    if (enable) {
                        jQuery('.itemopt').removeClass('disabled');
                    } else {

                        // check all thumbs if no remaining checks
                        // before we can disabled the options
                        var ch = false;
                        jQuery('.thmb').each(function () {
                            if (jQuery(this).hasClass('checked'))
                                ch = true;
                        });

                        if (!ch)
                            jQuery('.itemopt').addClass('disabled');
                    }
                }
            }

            activateCheckboxes();

            // jQuery(\"a[data-rel^='prettyPhoto']\").prettyPhoto();

        });

    </script>

";
    }

    public function getTemplateName()
    {
        return "Admin/Media/MediaManager.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  351 => 255,  313 => 220,  287 => 197,  277 => 189,  274 => 188,  269 => 185,  266 => 184,  144 => 65,  128 => 51,  113 => 49,  111 => 48,  107 => 47,  102 => 46,  85 => 45,  54 => 17,  45 => 11,  40 => 8,  37 => 7,  31 => 5,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Master.twig" %}*/
/* */
/* {# @var model \MediaViewModel #}*/
/* */
/* {% block title %}Media Manager{% endblock %}*/
/* */
/* {% block content %}*/
/* */
/*     <ol class="breadcrumb breadcrumb-quirk">*/
/*         <li>*/
/*             <a href="{{ Router.Create("home") }}">*/
/*                 <i class="fa fa-home mr5"></i>*/
/*                 Home*/
/*             </a>*/
/*         </li>*/
/*         <li>*/
/*             <a href="{{ Router.Create("media-manager") }}">Media</a>*/
/*         </li>*/
/*         <li class="active">Media Manager</li>*/
/*     </ol>*/
/* */
/*     <div class="row">*/
/*         <div class="col-sm-12">*/
/*             <div class="well well-asset-options clearfix">*/
/*                 <div class="btn-toolbar btn-toolbar-media-manager pull-left" role="toolbar">*/
/* */
/*                     <div class="btn-group" role="group">*/
/*                         <button type="button" class="btn btn-default" id="batch-delete-media">*/
/*                             <i class="fa fa-trash"></i>*/
/*                             Delete*/
/*                         </button>*/
/*                     </div>*/
/*                 </div><!-- btn-toolbar -->*/
/* */
/*                 <div class="btn-group pull-right" data-toggle="buttons">*/
/*                     <button data-toggle="modal" data-target="#mediaModal"*/
/*                             class="btn btn-danger btn-quirk btn-block">*/
/*                         Upload Files*/
/*                     </button>*/
/*                 </div>*/
/*             </div>*/
/* */
/*             <div class="row filemanager">*/
/* */
/*                 {% for media in model.Media %}*/
/*                     <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 {{ media.MediaType.Caption|lower }} media-manager-item"*/
/*                          data-id="{{ media.MediaId }}">*/
/*                         {% include "Admin/Media/Manager/Items/" ~ media.MediaType.Caption ~ ".twig" with {media: media} %}*/
/*                     </div><!-- col-xs-6 -->*/
/*                 {% endfor %}*/
/*             </div><!-- row -->*/
/*         </div><!-- col-sm-9 -->*/
/*     </div>*/
/* */
/*     <!-- Modal -->*/
/*     <div class="modal bounceIn animated" id="mediaModal" tabindex="-1" role="dialog" aria-labelledby="mediaModalLabel"*/
/*          aria-hidden="true">*/
/*         <div class="modal-dialog">*/
/*             <div class="modal-content">*/
/*                 <div class="modal-header">*/
/*                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>*/
/*                     <h4 class="modal-title" id="mediaModalLabel">Upload Media</h4>*/
/*                 </div>*/
/*                 <div class="modal-body">*/
/*                     <form action="{{ Router.Create("media-upload") }}" class="dropzone" id="upload-files">*/
/*                     </form>*/
/*                 </div>*/
/*                 <div class="modal-footer">*/
/*                     <button type="button" class="btn btn-default close-media-manager-modal-button">Close</button>*/
/*                     <button type="button" class="btn btn-primary" id="upload-files-button">Upload</button>*/
/*                 </div>*/
/*             </div><!-- modal-content -->*/
/*         </div><!-- modal-dialog -->*/
/*     </div><!-- modal -->*/
/* */
/*     <!-- Modal -->*/
/*     <div class="modal bounceIn animated" id="media-edit-modal" tabindex="-1" role="dialog"*/
/*          aria-hidden="true">*/
/*         <div class="modal-dialog">*/
/*             <div class="modal-content">*/
/*             </div><!-- modal-content -->*/
/*         </div><!-- modal-dialog -->*/
/*     </div><!-- modal -->*/
/* */
/*     <div id="uploaded-file-template" class="hidden">*/
/*         <div class="media-uploaded-file">*/
/*             <div class="row">*/
/*                 <div class="col-md-3">*/
/*                     <div class="dz-preview dz-image-preview">*/
/*                         <div class="dz-image">*/
/*                             <img data-dz-thumbnail=""/>*/
/*                         </div>*/
/*                         <div class="dz-progress">*/
/*                             <span class="dz-upload" data-dz-uploadprogress=""></span>*/
/*                         </div>*/
/*                         <div class="dz-success-mark">*/
/*                             <span>✔</span>*/
/*                         </div>*/
/*                         <div class="dz-error-mark">*/
/*                             <span>✘</span>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="col-md-9">*/
/*                     <input type="text" class="form-control" data-title placeholder="Enter title here">*/
/*                     <input type="text" class="form-control" data-copyrights placeholder="Enter copyrights here">*/
/*                     <div class="dz-details row">*/
/*                         <div class="dz-filename col-md-8">*/
/*                             <span data-dz-name=""></span>*/
/*                         </div>*/
/*                         <div class="dz-size col-md-4 text-right" data-dz-size=""></div>*/
/*                     </div>*/
/* */
/*                     <div class="dz-error-message">*/
/*                         <span data-dz-errormessage=""></span>*/
/*                     </div>*/
/* */
/*                     <div class="text-right">*/
/*                         <a class="media-remove-file-button btn btn-xs btn-danger" data-dz-remove>*/
/*                             <i class="fa fa-times"></i>*/
/*                             &nbsp; Remove file*/
/*                         </a>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <div id="media-manager-item-template" class="hidden">*/
/*         <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 media-manager-item"*/
/*              data-id="">*/
/*             <div class="thmb">*/
/*                 <label class="ckbox">*/
/*                     <input type="checkbox" data-selected-media-checkbox name="selectedMedia[]" value="">*/
/*                     <span></span>*/
/*                 </label>*/
/*                 <div class="btn-group fm-group">*/
/*                     <button type="button" class="btn btn-default dropdown-toggle fm-toggle"*/
/*                             data-toggle="dropdown">*/
/*                         <span class="caret"></span>*/
/*                     </button>*/
/*                     <ul class="dropdown-menu pull-right fm-menu" role="menu">*/
/*                         <li>*/
/*                             <a href="#">*/
/*                                 <i class="fa fa-share"></i>*/
/*                                 Share*/
/*                             </a>*/
/*                         </li>*/
/*                         <li>*/
/*                             <a class="media-manager-edit-item">*/
/*                                 <i class="fa fa-pencil"></i>*/
/*                                 Edit*/
/*                             </a>*/
/*                         </li>*/
/*                         <li>*/
/*                             <a target="_blank" data-download-button href="">*/
/*                                 <i class="fa fa-download"></i>*/
/*                                 Download*/
/*                             </a>*/
/*                         </li>*/
/*                         <li>*/
/*                             <a class="media-manager-delete-item">*/
/*                                 <i class="fa fa-trash-o"></i>*/
/*                                 Delete*/
/*                             </a>*/
/*                         </li>*/
/*                     </ul>*/
/*                 </div>*/
/*                 <div class="thmb-prev">*/
/*                     <img src="" class="img-responsive"*/
/*                          alt=""/>*/
/*                 </div>*/
/*                 <h5 class="fm-title">*/
/*                     <a href=""></a>*/
/*                 </h5>*/
/*                 <small class="text-muted">Added:*/
/*                     <span class="item-added-time"></span>*/
/*                 </small>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block additional_styles %}*/
/*     <link rel="stylesheet" href="Theme/Admin/lib/jquery.gritter/jquery.gritter.css">*/
/* {% endblock %}*/
/* */
/* {% block post_scripts %}*/
/*     <script type="text/javascript" src="Theme/Admin/lib/jquery.gritter/jquery.gritter.js"></script>*/
/*     <script type="text/javascript">*/
/*         jQuery(document).ready(function () {*/
/* */
/*             $(".filemanager").on("click", ".media-manager-delete-item", function () {*/
/*                 var item = $(this).closest(".media-manager-item");*/
/*                 var id = item.data("id");*/
/*                 $.ajax({*/
/*                     url: "{{ Router.Create("media-delete") }}",*/
/*                     type: "POST",*/
/*                     dataType: "json",*/
/*                     data: {*/
/*                         id: id*/
/*                     },*/
/*                     success: function (response) {*/
/*                         if (response.Success == true) {*/
/*                             $.gritter.add({*/
/*                                 title: 'Success',*/
/*                                 text: 'File is deleted',*/
/*                                 class_name: 'with-icon check-circle success'*/
/*                             });*/
/*                             item.fadeOut();*/
/*                         }*/
/*                     }*/
/*                 });*/
/*             });*/
/* */
/*             $(".filemanager").on("click", ".media-manager-edit-item", function () {*/
/*                 var item = $(this).closest(".media-manager-item");*/
/*                 var id = item.data("id");*/
/*                 $.ajax({*/
/*                     url: "{{ Router.Create("media-edit") }}",*/
/*                     type: "GET",*/
/*                     dataType: "html",*/
/*                     data: {*/
/*                         id: id*/
/*                     },*/
/*                     success: function (response) {*/
/*                         $("#media-edit-modal").find(".modal-content").html(response);*/
/*                         $("#media-edit-modal").modal("show");*/
/*                     }*/
/*                 });*/
/*             });*/
/* */
/*             $(".close-media-manager-modal-button").on("click", function () {*/
/*                 Dropzone.forElement("#upload-files").removeAllFiles(true);*/
/*                 $(this).closest(".modal").modal("hide");*/
/*             });*/
/* */
/*             $("#batch-delete-media").on("click", function () {*/
/*                 var selectedMedia = $(".ckbox :checked");*/
/*                 if (selectedMedia.length == 0) {*/
/*                     $.gritter.add({*/
/*                         title: 'Failed',*/
/*                         text: 'You didn\'t select any item',*/
/*                         class_name: 'with-icon times-circle danger'*/
/*                     });*/
/*                 }*/
/*                 else {*/
/*                     var indexes = [];*/
/*                     selectedMedia.each(function () {*/
/*                         indexes.push($(this).val());*/
/*                     });*/
/* */
/*                     $.ajax({*/
/*                         method: "POST",*/
/*                         url: "{{ Router.Create("media-batch-delete") }}",*/
/*                         data: {*/
/*                             ids: indexes*/
/*                         },*/
/*                         success: function () {*/
/* */
/*                             for (var i = 0; i < indexes.length; i++) {*/
/*                                 var mediaId = indexes[i];*/
/*                                 $(".media-manager-item[data-id=" + mediaId + "]").fadeOut();*/
/*                             }*/
/* */
/*                             $.gritter.add({*/
/*                                 title: 'Success',*/
/*                                 text: 'Items are deleted',*/
/*                                 class_name: 'with-icon check-circle success'*/
/*                             });*/
/* */
/* */
/*                         }*/
/*                     });*/
/*                 }*/
/*             });*/
/* */
/*             // dropzone*/
/* */
/*             Dropzone.options.uploadFiles = {*/
/*                 // Prevents Dropzone from uploading dropped files immediately*/
/*                 autoProcessQueue: false,*/
/*                 maxFilesize: 2,*/
/*                 parallelUploads: 1000,*/
/*                 previewTemplate: document.getElementById('uploaded-file-template').innerHTML,*/
/* */
/*                 accept: function (file, done) {*/
/*                     done();*/
/*                 },*/
/* */
/*                 init: function () {*/
/*                     var submitButton = document.querySelector("#upload-files-button"),*/
/*                             uploadFiles = this;*/
/* */
/*                     submitButton.addEventListener("click", function () {*/
/*                         uploadFiles.processQueue();*/
/*                     });*/
/* */
/*                     this.on("addedfile", function () {*/
/* */
/*                     });*/
/* */
/*                     this.on("sending", function (file, xhr, formData) {*/
/*                         var title = $(file.previewElement).closest(".media-uploaded-file").find("[data-title]").val();*/
/*                         formData.append("title", title);*/
/*                         var copyrights = $(file.previewElement).closest(".media-uploaded-file").find("[data-copyrights]").val();*/
/*                         formData.append("copyrights", copyrights);*/
/*                     });*/
/* */
/*                     this.on("success", function (file, response) {*/
/*                         var $this = this;*/
/*                         $(file.previewElement).delay(500).slideUp(100, function () {*/
/*                             $this.removeFile(file);*/
/*                         });*/
/* */
/*                         // create a thumbnail in media manager*/
/*                         var template = $("#media-manager-item-template").find(".media-manager-item").first().clone();*/
/*                         template.attr("data-id", response.Media.MediaId);*/
/*                         template.addClass(response.Media.Type);*/
/*                         template.find("[data-download-button]").attr("href", response.Media.SourceUrl);*/
/*                         template.find("[data-selected-media-checkbox]").val(response.Media.MediaId);*/
/*                         template.find(".thmb-prev").find("img").attr("src", response.Media.PictureSource).attr("alt", response.Media.Title);*/
/*                         template.find(".fm-title").find("a").attr("href", response.Media.SourceUrl).attr("alt", response.Media.Title).text(response.Media.Title);*/
/*                         template.find(".item-added-time").text(response.Media.AddedTime);*/
/* */
/*                         $(".filemanager").prepend(template.hide().fadeIn(1000));*/
/*                         activateCheckboxes();*/
/*                     });*/
/* */
/*                 }*/
/*             };*/
/* */
/*             'use strict';*/
/* */
/*             function activateCheckboxes() {*/
/*                 jQuery('.thmb').hover(function () {*/
/*                     var t = jQuery(this);*/
/*                     t.find('.ckbox').show();*/
/*                     t.find('.fm-group').show();*/
/*                 }, function () {*/
/*                     var t = jQuery(this);*/
/*                     if (!t.closest('.thmb').hasClass('checked')) {*/
/*                         t.find('.ckbox').hide();*/
/*                         t.find('.fm-group').hide();*/
/*                     }*/
/*                 });*/
/* */
/*                 jQuery('.ckbox').each(function () {*/
/*                     var t = jQuery(this);*/
/*                     var parent = t.parent();*/
/*                     if (t.find('input').is(':checked')) {*/
/*                         t.show();*/
/*                         parent.find('.fm-group').show();*/
/*                         parent.addClass('checked');*/
/*                     }*/
/*                 });*/
/* */
/* */
/*                 jQuery('.ckbox').click(function () {*/
/*                     var t = jQuery(this);*/
/*                     if (!t.find('input').is(':checked')) {*/
/*                         t.closest('.thmb').removeClass('checked');*/
/*                         enable_itemopt(false);*/
/*                     } else {*/
/*                         t.closest('.thmb').addClass('checked');*/
/*                         enable_itemopt(true);*/
/*                     }*/
/*                 });*/
/* */
/*                 jQuery('#selectall').click(function () {*/
/*                     if (jQuery(this).is(':checked')) {*/
/*                         jQuery('.thmb').each(function () {*/
/*                             jQuery(this).find('input').attr('checked', true);*/
/*                             jQuery(this).addClass('checked');*/
/*                             jQuery(this).find('.ckbox, .fm-group').show();*/
/*                         });*/
/*                         enable_itemopt(true);*/
/*                     } else {*/
/*                         jQuery('.thmb').each(function () {*/
/*                             jQuery(this).find('input').attr('checked', false);*/
/*                             jQuery(this).removeClass('checked');*/
/*                             jQuery(this).find('.ckbox, .fm-group').hide();*/
/*                         });*/
/*                         enable_itemopt(false);*/
/*                     }*/
/*                 });*/
/* */
/*                 function enable_itemopt(enable) {*/
/*                     if (enable) {*/
/*                         jQuery('.itemopt').removeClass('disabled');*/
/*                     } else {*/
/* */
/*                         // check all thumbs if no remaining checks*/
/*                         // before we can disabled the options*/
/*                         var ch = false;*/
/*                         jQuery('.thmb').each(function () {*/
/*                             if (jQuery(this).hasClass('checked'))*/
/*                                 ch = true;*/
/*                         });*/
/* */
/*                         if (!ch)*/
/*                             jQuery('.itemopt').addClass('disabled');*/
/*                     }*/
/*                 }*/
/*             }*/
/* */
/*             activateCheckboxes();*/
/* */
/*             // jQuery("a[data-rel^='prettyPhoto']").prettyPhoto();*/
/* */
/*         });*/
/* */
/*     </script>*/
/* */
/* {% endblock %}*/
