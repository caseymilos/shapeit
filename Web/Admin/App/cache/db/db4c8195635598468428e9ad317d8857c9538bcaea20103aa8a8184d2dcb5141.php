<?php

/* Admin/Shared/Sidebar/Panels/MainMenu.twig */
class __TwigTemplate_2fdbc70badc96cd794a6a43eac231784cf2b69ae5ac793099412743097ba114c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((array_key_exists("model", $context) && ($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Changed", array()) == true))) {
            // line 2
            echo "    <div class=\"alert alert-success\" style=\"text-align: center\">
        <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
        Changes are successfully made!
    </div>
";
        }
        // line 7
        echo "<h5 class=\"sidebar-title\">Travel</h5>
<ul class=\"nav nav-pills nav-stacked nav-quirk\">
    <li class=\"nav-parent ";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Coaches"), "method"), "html", null, true);
        echo "\">
        <a href=\"\">
            <i class=\"fa fa-globe\"></i>
            <span>Offers</span>
        </a>
        <ul class=\"children\">
            <li class=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Offers", 1 => "Offers"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "offers"), "method"), "html", null, true);
        echo "\">List of Offers</a>
            </li>
            <li class=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Offers", 1 => "Create"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "create-offer"), "method"), "html", null, true);
        echo "\">Create Offer</a>
            </li>
        </ul>
    </li>
    <li class=\"nav-parent ";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Bookings"), "method"), "html", null, true);
        echo "\">
        <a href=\"\">
            <i class=\"fa fa-calendar\"></i>
            <span>Bookings</span>
        </a>
        <ul class=\"children\">
            <li class=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Bookings", 1 => "Bookings"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "bookings"), "method"), "html", null, true);
        echo "\">List of Bookings</a>
            </li>
            <li class=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Bookings", 1 => "Create"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "create-booking"), "method"), "html", null, true);
        echo "\">Create Booking</a>
            </li>
        </ul>
    </li>
    <li class=\"nav-parent ";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Blogs"), "method"), "html", null, true);
        echo "\">
        <a href=\"\">
            <i class=\"fa fa-align-left\"></i>
            <span>Blog</span>
        </a>
        <ul class=\"children\">
            <li class=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Blogs", 1 => "Blogs"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "blogs"), "method"), "html", null, true);
        echo "\">List of Blogs</a>
            </li>
            <li class=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Blog", 1 => "Blogs"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "create-blog"), "method"), "html", null, true);
        echo "\">Create Blog</a>
            </li>
        </ul>
    </li>


</ul>


<h5 class=\"sidebar-title\">Content</h5>
<ul class=\"nav nav-pills nav-stacked nav-quirk\">
    <li class=\"nav-parent ";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Menus"), "method"), "html", null, true);
        echo "\">
        <a href=\"\">
            <i class=\"fa fa-bars\"></i>
            <span>Menus</span>
        </a>
        <ul class=\"children\">
            <li class=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Menus", 1 => "List"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "menus-list"), "method"), "html", null, true);
        echo "\">List of Menus</a>
            </li>
            <li class=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Menus", 1 => "Create"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "menus-create"), "method"), "html", null, true);
        echo "\">Create New Menu</a>
            </li>
        </ul>
    </li>

    <li class=\"nav-parent ";
        // line 73
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Builder"), "method"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Pages"), "method"), "html", null, true);
        echo "\">
        <a href=\"\">
            <i class=\"fa fa-file-text\"></i>
            <span>Pages</span>
        </a>
        <ul class=\"children\">
            <li class=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Pages", 1 => "List"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "pages-list"), "method"), "html", null, true);
        echo "\">List of Pages</a>
            </li>
            <li class=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Builder", 1 => "Builder"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "page-builder-create"), "method"), "html", null, true);
        echo "\">Create New Page</a>
            </li>
        </ul>
    </li>

    <li class=\"nav-parent ";
        // line 88
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Media"), "method"), "html", null, true);
        echo "\">
        <a href=\"\">
            <i class=\"fa fa-picture-o\"></i>
            <span>Media</span>
        </a>
        <ul class=\"children\">
            <li class=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Media", 1 => "MediaManager"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "media-manager"), "method"), "html", null, true);
        echo "\">
                    <span>Media Manager</span>
                </a>
            </li>
            <li class=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Media", 1 => "Vimeo"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "media-vimeo-videos"), "method"), "html", null, true);
        echo "\">
                    <span>Vimeo Import</span>
                </a>
            </li>
        </ul>
    </li>




    <li class=\"nav-parent ";
        // line 110
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "SocialIcons"), "method"), "html", null, true);
        echo "\">
        <a href=\"\">
            <i class=\"fa fa-picture-o\"></i>
            <span>Social Icons</span>
        </a>
        <ul class=\"children\">
            <li class=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "SocialIcons", 1 => "List"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 117
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "social-icon-list"), "method"), "html", null, true);
        echo "\">List of Social Icons</a>
            </li>
            <li class=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "SocialIcons", 1 => "Create"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 120
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "social-icon-create"), "method"), "html", null, true);
        echo "\">Create New Social Icon</a>
            </li>
        </ul>
    </li>
</ul>

<h5 class=\"sidebar-title\">User Management</h5>
<ul class=\"nav nav-pills nav-stacked nav-quirk\">
    <li class=\"nav-parent ";
        // line 128
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Users"), "method"), "html", null, true);
        echo "\">
        <a href=\"\">
            <i class=\"fa fa-users\"></i>
            <span>Users</span>
        </a>
        <ul class=\"children\">
            <li class=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Users", 1 => "List"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "user-list"), "method"), "html", null, true);
        echo "\">List of Users</a>
            </li>
            <li class=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Users", 1 => "NotApprovedUsersList"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "not-approved-user-list"), "method"), "html", null, true);
        echo "\">List of Not Approved Users</a>
            </li>
            <li class=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Users", 1 => "Create"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "user-create"), "method"), "html", null, true);
        echo "\">Create New User</a>
            </li>
            <li class=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Users", 1 => "BlockedUsers"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "user-blocked-list"), "method"), "html", null, true);
        echo "\">Blocked Users</a>
            </li>
        </ul>
    </li>
    <li class=\"nav-parent ";
        // line 148
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Roles"), "method"), "html", null, true);
        echo "\">
        <a href=\"\">
            <i class=\"fa fa-key\"></i>
            <span>Roles</span>
        </a>
        <ul class=\"children\">
            <li class=\"";
        // line 154
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Roles", 1 => "List"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 155
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "role-list"), "method"), "html", null, true);
        echo "\">List of Roles</a>
            </li>
            <li class=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Roles", 1 => "Create"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 158
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "role-create"), "method"), "html", null, true);
        echo "\">Create New Role</a>
            </li>
        </ul>
    </li>
    <li class=\"nav-parent ";
        // line 162
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Invitations"), "method"), "html", null, true);
        echo "\">
        <a href=\"\">
            <i class=\"fa fa-paper-plane\"></i>
            <span>Invitations</span>
        </a>
        <ul class=\"children\">
            <li class=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Invitations", 1 => "List"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 169
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "invitation-list"), "method"), "html", null, true);
        echo "\">List of Invitations</a>
            </li>
            <li class=\"";
        // line 171
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "ActiveButton", array(0 => "Invitations", 1 => "Create"), "method"), "html", null, true);
        echo "\">
                <a href=\"";
        // line 172
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "invitation-create"), "method"), "html", null, true);
        echo "\">Create Invitation</a>
            </li>
        </ul>
    </li>
</ul>
";
    }

    public function getTemplateName()
    {
        return "Admin/Shared/Sidebar/Panels/MainMenu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  356 => 172,  352 => 171,  347 => 169,  343 => 168,  334 => 162,  327 => 158,  323 => 157,  318 => 155,  314 => 154,  305 => 148,  298 => 144,  294 => 143,  289 => 141,  285 => 140,  280 => 138,  276 => 137,  271 => 135,  267 => 134,  258 => 128,  247 => 120,  243 => 119,  238 => 117,  234 => 116,  225 => 110,  212 => 100,  208 => 99,  201 => 95,  197 => 94,  188 => 88,  180 => 83,  176 => 82,  171 => 80,  167 => 79,  156 => 73,  148 => 68,  144 => 67,  139 => 65,  135 => 64,  126 => 58,  112 => 47,  108 => 46,  103 => 44,  99 => 43,  90 => 37,  83 => 33,  79 => 32,  74 => 30,  70 => 29,  61 => 23,  54 => 19,  50 => 18,  45 => 16,  41 => 15,  32 => 9,  28 => 7,  21 => 2,  19 => 1,);
    }
}
/* {% if model is defined and model.Changed == true %}*/
/*     <div class="alert alert-success" style="text-align: center">*/
/*         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>*/
/*         Changes are successfully made!*/
/*     </div>*/
/* {% endif %}*/
/* <h5 class="sidebar-title">Travel</h5>*/
/* <ul class="nav nav-pills nav-stacked nav-quirk">*/
/*     <li class="nav-parent {{ Helper.ActiveButton("Coaches") }}">*/
/*         <a href="">*/
/*             <i class="fa fa-globe"></i>*/
/*             <span>Offers</span>*/
/*         </a>*/
/*         <ul class="children">*/
/*             <li class="{{ Helper.ActiveButton("Offers", "Offers") }}">*/
/*                 <a href="{{ Router.Create("offers") }}">List of Offers</a>*/
/*             </li>*/
/*             <li class="{{ Helper.ActiveButton("Offers", "Create") }}">*/
/*                 <a href="{{ Router.Create("create-offer") }}">Create Offer</a>*/
/*             </li>*/
/*         </ul>*/
/*     </li>*/
/*     <li class="nav-parent {{ Helper.ActiveButton("Bookings") }}">*/
/*         <a href="">*/
/*             <i class="fa fa-calendar"></i>*/
/*             <span>Bookings</span>*/
/*         </a>*/
/*         <ul class="children">*/
/*             <li class="{{ Helper.ActiveButton("Bookings", "Bookings") }}">*/
/*                 <a href="{{ Router.Create("bookings") }}">List of Bookings</a>*/
/*             </li>*/
/*             <li class="{{ Helper.ActiveButton("Bookings", "Create") }}">*/
/*                 <a href="{{ Router.Create("create-booking") }}">Create Booking</a>*/
/*             </li>*/
/*         </ul>*/
/*     </li>*/
/*     <li class="nav-parent {{ Helper.ActiveButton("Blogs") }}">*/
/*         <a href="">*/
/*             <i class="fa fa-align-left"></i>*/
/*             <span>Blog</span>*/
/*         </a>*/
/*         <ul class="children">*/
/*             <li class="{{ Helper.ActiveButton("Blogs", "Blogs") }}">*/
/*                 <a href="{{ Router.Create("blogs") }}">List of Blogs</a>*/
/*             </li>*/
/*             <li class="{{ Helper.ActiveButton("Blog", "Blogs") }}">*/
/*                 <a href="{{ Router.Create("create-blog") }}">Create Blog</a>*/
/*             </li>*/
/*         </ul>*/
/*     </li>*/
/* */
/* */
/* </ul>*/
/* */
/* */
/* <h5 class="sidebar-title">Content</h5>*/
/* <ul class="nav nav-pills nav-stacked nav-quirk">*/
/*     <li class="nav-parent {{ Helper.ActiveButton("Menus") }}">*/
/*         <a href="">*/
/*             <i class="fa fa-bars"></i>*/
/*             <span>Menus</span>*/
/*         </a>*/
/*         <ul class="children">*/
/*             <li class="{{ Helper.ActiveButton("Menus", "List") }}">*/
/*                 <a href="{{ Router.Create("menus-list") }}">List of Menus</a>*/
/*             </li>*/
/*             <li class="{{ Helper.ActiveButton("Menus", "Create") }}">*/
/*                 <a href="{{ Router.Create("menus-create") }}">Create New Menu</a>*/
/*             </li>*/
/*         </ul>*/
/*     </li>*/
/* */
/*     <li class="nav-parent {{ Helper.ActiveButton("Builder") }} {{ Helper.ActiveButton("Pages") }}">*/
/*         <a href="">*/
/*             <i class="fa fa-file-text"></i>*/
/*             <span>Pages</span>*/
/*         </a>*/
/*         <ul class="children">*/
/*             <li class="{{ Helper.ActiveButton("Pages", "List") }}">*/
/*                 <a href="{{ Router.Create("pages-list") }}">List of Pages</a>*/
/*             </li>*/
/*             <li class="{{ Helper.ActiveButton("Builder", "Builder") }}">*/
/*                 <a href="{{ Router.Create("page-builder-create") }}">Create New Page</a>*/
/*             </li>*/
/*         </ul>*/
/*     </li>*/
/* */
/*     <li class="nav-parent {{ Helper.ActiveButton("Media") }}">*/
/*         <a href="">*/
/*             <i class="fa fa-picture-o"></i>*/
/*             <span>Media</span>*/
/*         </a>*/
/*         <ul class="children">*/
/*             <li class="{{ Helper.ActiveButton("Media", "MediaManager") }}">*/
/*                 <a href="{{ Router.Create("media-manager") }}">*/
/*                     <span>Media Manager</span>*/
/*                 </a>*/
/*             </li>*/
/*             <li class="{{ Helper.ActiveButton("Media", "Vimeo") }}">*/
/*                 <a href="{{ Router.Create("media-vimeo-videos") }}">*/
/*                     <span>Vimeo Import</span>*/
/*                 </a>*/
/*             </li>*/
/*         </ul>*/
/*     </li>*/
/* */
/* */
/* */
/* */
/*     <li class="nav-parent {{ Helper.ActiveButton("SocialIcons") }}">*/
/*         <a href="">*/
/*             <i class="fa fa-picture-o"></i>*/
/*             <span>Social Icons</span>*/
/*         </a>*/
/*         <ul class="children">*/
/*             <li class="{{ Helper.ActiveButton("SocialIcons", "List") }}">*/
/*                 <a href="{{ Router.Create("social-icon-list") }}">List of Social Icons</a>*/
/*             </li>*/
/*             <li class="{{ Helper.ActiveButton("SocialIcons", "Create") }}">*/
/*                 <a href="{{ Router.Create("social-icon-create") }}">Create New Social Icon</a>*/
/*             </li>*/
/*         </ul>*/
/*     </li>*/
/* </ul>*/
/* */
/* <h5 class="sidebar-title">User Management</h5>*/
/* <ul class="nav nav-pills nav-stacked nav-quirk">*/
/*     <li class="nav-parent {{ Helper.ActiveButton("Users") }}">*/
/*         <a href="">*/
/*             <i class="fa fa-users"></i>*/
/*             <span>Users</span>*/
/*         </a>*/
/*         <ul class="children">*/
/*             <li class="{{ Helper.ActiveButton("Users", "List") }}">*/
/*                 <a href="{{ Router.Create("user-list") }}">List of Users</a>*/
/*             </li>*/
/*             <li class="{{ Helper.ActiveButton("Users", "NotApprovedUsersList") }}">*/
/*                 <a href="{{ Router.Create("not-approved-user-list") }}">List of Not Approved Users</a>*/
/*             </li>*/
/*             <li class="{{ Helper.ActiveButton("Users", "Create") }}">*/
/*                 <a href="{{ Router.Create("user-create") }}">Create New User</a>*/
/*             </li>*/
/*             <li class="{{ Helper.ActiveButton("Users", "BlockedUsers") }}">*/
/*                 <a href="{{ Router.Create("user-blocked-list") }}">Blocked Users</a>*/
/*             </li>*/
/*         </ul>*/
/*     </li>*/
/*     <li class="nav-parent {{ Helper.ActiveButton("Roles") }}">*/
/*         <a href="">*/
/*             <i class="fa fa-key"></i>*/
/*             <span>Roles</span>*/
/*         </a>*/
/*         <ul class="children">*/
/*             <li class="{{ Helper.ActiveButton("Roles", "List") }}">*/
/*                 <a href="{{ Router.Create("role-list") }}">List of Roles</a>*/
/*             </li>*/
/*             <li class="{{ Helper.ActiveButton("Roles", "Create") }}">*/
/*                 <a href="{{ Router.Create("role-create") }}">Create New Role</a>*/
/*             </li>*/
/*         </ul>*/
/*     </li>*/
/*     <li class="nav-parent {{ Helper.ActiveButton("Invitations") }}">*/
/*         <a href="">*/
/*             <i class="fa fa-paper-plane"></i>*/
/*             <span>Invitations</span>*/
/*         </a>*/
/*         <ul class="children">*/
/*             <li class="{{ Helper.ActiveButton("Invitations", "List") }}">*/
/*                 <a href="{{ Router.Create("invitation-list") }}">List of Invitations</a>*/
/*             </li>*/
/*             <li class="{{ Helper.ActiveButton("Invitations", "Create") }}">*/
/*                 <a href="{{ Router.Create("invitation-create") }}">Create Invitation</a>*/
/*             </li>*/
/*         </ul>*/
/*     </li>*/
/* </ul>*/
/* */
