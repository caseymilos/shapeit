<?php

/* Admin/Master/Master.twig */
class __TwigTemplate_b67f6110a728669ff311c7093911ab20f4752327fc3bbf3b9ccaa07e9d08986a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Base.twig", "Admin/Master/Master.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'additional_styles' => array($this, 'block_additional_styles'),
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'post_scripts' => array($this, 'block_post_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    <link type=\"text/css\" rel=\"stylesheet\" href=\"Theme/Admin/css/style.css\"/>
    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/dropzone/dropzone.css\">
    ";
        // line 7
        $this->displayBlock('additional_styles', $context, $blocks);
        // line 8
        echo "
    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/jquery-toggles/toggles-full.css\">
    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.css\">
    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/summernote/summernote.css\">
    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/jquery-ui/jquery-ui.css\">
    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/select2/select2.css\">
    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/timepicker/jquery.timepicker.css\">
    <link rel=\"stylesheet\" href=\"Theme/Admin/fonts/stylesheet.css\"/>
    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/summernote/plugin/widget/summernote-ext-widget.css\"/>
    <script src=\"Theme/Admin/lib/jquery/jquery.js\"></script>

    <link rel=\"stylesheet\" href=\"Theme/Admin/css/quirk.css\">
";
    }

    // line 7
    public function block_additional_styles($context, array $blocks = array())
    {
    }

    // line 22
    public function block_body($context, array $blocks = array())
    {
        // line 23
        echo "    <header>
        <div class=\"headerpanel\">
            <div class=\"logopanel admin-logo\">
                <h2>
                   ";
        // line 30
        echo "                </h2>
            </div>

            <div class=\"headerbar\">
                <a id=\"menuToggle\" class=\"menutoggle\">
                    <i class=\"fa fa-bars\"></i>
                </a>

                ";
        // line 38
        $this->loadTemplate("Admin/Shared/Header/Search.twig", "Admin/Master/Master.twig", 38)->display($context);
        // line 39
        echo "                <div class=\"header-right\">
                    <ul class=\"headermenu\">
                        <li>";
        // line 41
        $this->loadTemplate("Admin/Shared/Header/Right/Languages.twig", "Admin/Master/Master.twig", 41)->display($context);
        echo "</li>
                        <li>";
        // line 42
        $this->loadTemplate("Admin/Shared/Header/Right/User.twig", "Admin/Master/Master.twig", 42)->display($context);
        echo "</li>
                    </ul>
                </div><!-- header-right -->
            </div><!-- headerbar -->
        </div><!-- header-->
    </header>

    <section>

        <div class=\"leftpanel\">
            ";
        // line 52
        $this->loadTemplate("Admin/Shared/Sidebar/Left.twig", "Admin/Master/Master.twig", 52)->display($context);
        // line 53
        echo "        </div>

        <div class=\"mainpanel\">
            <div class=\"contentpanel\">
                ";
        // line 57
        $this->displayBlock('content', $context, $blocks);
        // line 58
        echo "            </div>
        </div>
    </section>
";
    }

    // line 57
    public function block_content($context, array $blocks = array())
    {
    }

    // line 63
    public function block_scripts($context, array $blocks = array())
    {
        // line 64
        echo "    <script src=\"Theme/Admin/lib/jquery-ui/jquery-ui.js\"></script>
    <script src=\"Theme/Admin/lib/bootstrap/js/bootstrap.js\"></script>
    <script src=\"Theme/Admin/lib/timepicker/jquery.timepicker.js\"></script>
    <script src=\"Theme/Admin/lib/jquery-toggles/toggles.js\"></script>
    <script src=\"Theme/Admin/lib/dropzone/dropzone.js\"></script>
    <script src=\"Theme/Admin/lib/speakingurl/speakingurl.min.js\"></script>
    <script src=\"Theme/Admin/lib/slugify/slugify.min.js\"></script>
    <script src=\"Theme/Admin/js/plugins.js\"></script>

    <script src=\"Theme/Admin/lib/wysihtml5x/wysihtml5x.js\"></script>
    <script src=\"Theme/Admin/lib/wysihtml5x/wysihtml5x-toolbar.js\"></script>
    <script src=\"Theme/Admin/lib/handlebars/handlebars.js\"></script>
    <script src=\"Theme/Admin/lib/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.all.js\"></script>

    <script src=\"Theme/Admin/lib/summernote/summernote.min.js\"></script>
    <script src=\"Theme/Admin/lib/summernote/plugin/widget/summernote-ext-widget.js\"></script>
    <script src=\"Theme/Admin/lib/summernote/plugin/template/summernote-ext-template.js\"></script>
    <script src=\"Theme/Admin/js/sort-menu.js\"></script>

    <script>
        \$(document).ready(function () {
            'use strict';

            // HTML5 WYSIWYG Editor
            \$('.wysiwyg').wysihtml5({
                toolbar: {
                    image: false,
                    html: true,
                    fa: true
                }
            });

            initializeSummernote();

            \$(\".timepicker\").find(\"input\").timepicker({'timeFormat': 'H:i', 'step': 15})

        });



        function initializeSummernote() {
            \$('.summernote').summernote({
                height: 200,
                tabsize: 2,
                toolbar: [
                    ['style', ['style', 'template']],
                    ['font', ['bold', 'italic', 'underline', 'clear', 'strikethrough', 'superscript', 'subscript']],
                    ['fontname', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['table', ['table']],
                    ['widget', ['widget']],
                    ['insert', ['link', 'picture', 'video']],
                    ['view', ['fullscreen', 'codeview', 'help']]
                ],
                template: {
                    path: 'Theme/Admin/lib/summernote/tpls', // path to your template folder
                    list: [ // list of your template (without the .html extension)
                        'red-button',
                        'white-button',
                        'two-columns',
                        'column-and-sidebar',
                        'people-left',
                        'people-right',
                        'accordion',
                        'sidebar-secondary'
                    ]
                }
            });
        }
    </script>

    <script src=\"Theme/Admin/lib/select2/select2.js\"></script>
    <script type=\"text/javascript\">
        // Date Picker
        \$('.datepicker').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        \$('.select2').select2();
    </script>

    <script src=\"Theme/Admin/lib/jquery-toggles/toggles.js\"></script>
    <script type=\"text/javascript\">
        // Toggles
        \$('.toggle').toggles({
            height: 26
        });
    </script>

    <script type=\"text/javascript\">
        \$(document).ready(function () {

            \$(\".confirm\").confirmation({
                animation: true,
                placement: 'bottom',
                trigger: 'click',
                btnOkClass: 'btn-danger btn-small',
                btnCancelClass: 'btn-primary btn-small',
                container: 'body',
                popout: true,
                singleton: true,
                onCancel: function () {
                    \$(\".confirm\").confirmation('hide');
                }

            });

            \$(\".upload-picture-field\").on('change', function () {
                //Get count of selected files
                var countFiles = \$(this)[0].files.length;
                var imgPath = \$(this)[0].value;
                var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
                var image_holder = \$(this).prev(\".image-placeholder\");
                image_holder.empty();
                if (extn == \"gif\" || extn == \"png\" || extn == \"jpg\" || extn == \"jpeg\") {
                    if (typeof(FileReader) != \"undefined\") {
                        //loop for each file selected for uploaded.
                        for (var i = 0; i < countFiles; i++) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                image_holder.css(\"background-image\", \"url(\" + e.target.result + \")\")
                            };
                            image_holder.show().addClass(\"no-holder\");
                            reader.readAsDataURL(\$(this)[0].files[i]);
                        }
                    } else {
                        alert(\"This browser does not support FileReader.\");
                    }
                } else {
                    alert(\"Please select only images\");
                }
            });
        });
    </script>

    ";
        // line 200
        $this->displayBlock('post_scripts', $context, $blocks);
        // line 201
        echo "    <script src=\"Theme/Admin/js/quirk.js\"></script>
";
    }

    // line 200
    public function block_post_scripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "Admin/Master/Master.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  278 => 200,  273 => 201,  271 => 200,  133 => 64,  130 => 63,  125 => 57,  118 => 58,  116 => 57,  110 => 53,  108 => 52,  95 => 42,  91 => 41,  87 => 39,  85 => 38,  75 => 30,  69 => 23,  66 => 22,  61 => 7,  45 => 8,  43 => 7,  36 => 4,  33 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Base.twig" %}*/
/* */
/* {% block head %}*/
/*     {{ parent() }}*/
/*     <link type="text/css" rel="stylesheet" href="Theme/Admin/css/style.css"/>*/
/*     <link rel="stylesheet" href="Theme/Admin/lib/dropzone/dropzone.css">*/
/*     {% block additional_styles %}{% endblock %}*/
/* */
/*     <link rel="stylesheet" href="Theme/Admin/lib/jquery-toggles/toggles-full.css">*/
/*     <link rel="stylesheet" href="Theme/Admin/lib/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.css">*/
/*     <link rel="stylesheet" href="Theme/Admin/lib/summernote/summernote.css">*/
/*     <link rel="stylesheet" href="Theme/Admin/lib/jquery-ui/jquery-ui.css">*/
/*     <link rel="stylesheet" href="Theme/Admin/lib/select2/select2.css">*/
/*     <link rel="stylesheet" href="Theme/Admin/lib/timepicker/jquery.timepicker.css">*/
/*     <link rel="stylesheet" href="Theme/Admin/fonts/stylesheet.css"/>*/
/*     <link rel="stylesheet" href="Theme/Admin/lib/summernote/plugin/widget/summernote-ext-widget.css"/>*/
/*     <script src="Theme/Admin/lib/jquery/jquery.js"></script>*/
/* */
/*     <link rel="stylesheet" href="Theme/Admin/css/quirk.css">*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     <header>*/
/*         <div class="headerpanel">*/
/*             <div class="logopanel admin-logo">*/
/*                 <h2>*/
/*                    {# <a href="{{ Router.Create("home") }}">*/
/*                         <img src="Theme/Admin/images/logo_admin.png" alt="KK Niš" style="padding-left: 60px;"/>*/
/*                     </a>#}*/
/*                 </h2>*/
/*             </div>*/
/* */
/*             <div class="headerbar">*/
/*                 <a id="menuToggle" class="menutoggle">*/
/*                     <i class="fa fa-bars"></i>*/
/*                 </a>*/
/* */
/*                 {% include "Admin/Shared/Header/Search.twig" %}*/
/*                 <div class="header-right">*/
/*                     <ul class="headermenu">*/
/*                         <li>{% include "Admin/Shared/Header/Right/Languages.twig" %}</li>*/
/*                         <li>{% include "Admin/Shared/Header/Right/User.twig" %}</li>*/
/*                     </ul>*/
/*                 </div><!-- header-right -->*/
/*             </div><!-- headerbar -->*/
/*         </div><!-- header-->*/
/*     </header>*/
/* */
/*     <section>*/
/* */
/*         <div class="leftpanel">*/
/*             {% include "Admin/Shared/Sidebar/Left.twig" %}*/
/*         </div>*/
/* */
/*         <div class="mainpanel">*/
/*             <div class="contentpanel">*/
/*                 {% block content %}{% endblock %}*/
/*             </div>*/
/*         </div>*/
/*     </section>*/
/* {% endblock %}*/
/* */
/* {% block scripts %}*/
/*     <script src="Theme/Admin/lib/jquery-ui/jquery-ui.js"></script>*/
/*     <script src="Theme/Admin/lib/bootstrap/js/bootstrap.js"></script>*/
/*     <script src="Theme/Admin/lib/timepicker/jquery.timepicker.js"></script>*/
/*     <script src="Theme/Admin/lib/jquery-toggles/toggles.js"></script>*/
/*     <script src="Theme/Admin/lib/dropzone/dropzone.js"></script>*/
/*     <script src="Theme/Admin/lib/speakingurl/speakingurl.min.js"></script>*/
/*     <script src="Theme/Admin/lib/slugify/slugify.min.js"></script>*/
/*     <script src="Theme/Admin/js/plugins.js"></script>*/
/* */
/*     <script src="Theme/Admin/lib/wysihtml5x/wysihtml5x.js"></script>*/
/*     <script src="Theme/Admin/lib/wysihtml5x/wysihtml5x-toolbar.js"></script>*/
/*     <script src="Theme/Admin/lib/handlebars/handlebars.js"></script>*/
/*     <script src="Theme/Admin/lib/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.all.js"></script>*/
/* */
/*     <script src="Theme/Admin/lib/summernote/summernote.min.js"></script>*/
/*     <script src="Theme/Admin/lib/summernote/plugin/widget/summernote-ext-widget.js"></script>*/
/*     <script src="Theme/Admin/lib/summernote/plugin/template/summernote-ext-template.js"></script>*/
/*     <script src="Theme/Admin/js/sort-menu.js"></script>*/
/* */
/*     <script>*/
/*         $(document).ready(function () {*/
/*             'use strict';*/
/* */
/*             // HTML5 WYSIWYG Editor*/
/*             $('.wysiwyg').wysihtml5({*/
/*                 toolbar: {*/
/*                     image: false,*/
/*                     html: true,*/
/*                     fa: true*/
/*                 }*/
/*             });*/
/* */
/*             initializeSummernote();*/
/* */
/*             $(".timepicker").find("input").timepicker({'timeFormat': 'H:i', 'step': 15})*/
/* */
/*         });*/
/* */
/* */
/* */
/*         function initializeSummernote() {*/
/*             $('.summernote').summernote({*/
/*                 height: 200,*/
/*                 tabsize: 2,*/
/*                 toolbar: [*/
/*                     ['style', ['style', 'template']],*/
/*                     ['font', ['bold', 'italic', 'underline', 'clear', 'strikethrough', 'superscript', 'subscript']],*/
/*                     ['fontname', ['fontsize']],*/
/*                     ['color', ['color']],*/
/*                     ['para', ['ul', 'ol', 'paragraph']],*/
/*                     ['height', ['height']],*/
/*                     ['table', ['table']],*/
/*                     ['widget', ['widget']],*/
/*                     ['insert', ['link', 'picture', 'video']],*/
/*                     ['view', ['fullscreen', 'codeview', 'help']]*/
/*                 ],*/
/*                 template: {*/
/*                     path: 'Theme/Admin/lib/summernote/tpls', // path to your template folder*/
/*                     list: [ // list of your template (without the .html extension)*/
/*                         'red-button',*/
/*                         'white-button',*/
/*                         'two-columns',*/
/*                         'column-and-sidebar',*/
/*                         'people-left',*/
/*                         'people-right',*/
/*                         'accordion',*/
/*                         'sidebar-secondary'*/
/*                     ]*/
/*                 }*/
/*             });*/
/*         }*/
/*     </script>*/
/* */
/*     <script src="Theme/Admin/lib/select2/select2.js"></script>*/
/*     <script type="text/javascript">*/
/*         // Date Picker*/
/*         $('.datepicker').datepicker({*/
/*             dateFormat: 'yy-mm-dd'*/
/*         });*/
/*         $('.select2').select2();*/
/*     </script>*/
/* */
/*     <script src="Theme/Admin/lib/jquery-toggles/toggles.js"></script>*/
/*     <script type="text/javascript">*/
/*         // Toggles*/
/*         $('.toggle').toggles({*/
/*             height: 26*/
/*         });*/
/*     </script>*/
/* */
/*     <script type="text/javascript">*/
/*         $(document).ready(function () {*/
/* */
/*             $(".confirm").confirmation({*/
/*                 animation: true,*/
/*                 placement: 'bottom',*/
/*                 trigger: 'click',*/
/*                 btnOkClass: 'btn-danger btn-small',*/
/*                 btnCancelClass: 'btn-primary btn-small',*/
/*                 container: 'body',*/
/*                 popout: true,*/
/*                 singleton: true,*/
/*                 onCancel: function () {*/
/*                     $(".confirm").confirmation('hide');*/
/*                 }*/
/* */
/*             });*/
/* */
/*             $(".upload-picture-field").on('change', function () {*/
/*                 //Get count of selected files*/
/*                 var countFiles = $(this)[0].files.length;*/
/*                 var imgPath = $(this)[0].value;*/
/*                 var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();*/
/*                 var image_holder = $(this).prev(".image-placeholder");*/
/*                 image_holder.empty();*/
/*                 if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {*/
/*                     if (typeof(FileReader) != "undefined") {*/
/*                         //loop for each file selected for uploaded.*/
/*                         for (var i = 0; i < countFiles; i++) {*/
/*                             var reader = new FileReader();*/
/*                             reader.onload = function (e) {*/
/*                                 image_holder.css("background-image", "url(" + e.target.result + ")")*/
/*                             };*/
/*                             image_holder.show().addClass("no-holder");*/
/*                             reader.readAsDataURL($(this)[0].files[i]);*/
/*                         }*/
/*                     } else {*/
/*                         alert("This browser does not support FileReader.");*/
/*                     }*/
/*                 } else {*/
/*                     alert("Please select only images");*/
/*                 }*/
/*             });*/
/*         });*/
/*     </script>*/
/* */
/*     {% block post_scripts %}{% endblock %}*/
/*     <script src="Theme/Admin/js/quirk.js"></script>*/
/* {% endblock %}*/
