<?php

/* Admin/Pages/WidgetOptions/Gallery.twig */
class __TwigTemplate_514bcf9de1177de830f7bfd596df4e18a3e5a6e3a51d74933cf4751f3a991231 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"row\">
    <div class=\"col-md-6\">
        <div class=\"form-group\">
            <label>Columns</label>
            ";
        // line 5
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => array(2 => "2", 3 => "3", 4 => "4", 5 => "5", 6 => "6"), 1 => "numColumns", 2 => $this->getAttribute($this->getAttribute(        // line 11
(isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "numColumns", array())), "method");
        echo "
        </div>
    </div>
    <div class=\"col-md-6\">
        <div class=\"form-group\">
            <label>Big image (0 for all small)</label>
            <input type=\"number\" min=\"0\" step=\"1\" placeholder=\"0\" name=\"big\" class=\"form-control\"
                   value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "big", array()), "html", null, true);
        echo "\"/>
        </div>

    </div>
</div>

<h5>Media</h5>

<div id=\"media-list-holder\" style=\"height: 250px; overflow: auto;\">
    ";
        // line 27
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "media", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["media"]) {
            // line 28
            echo "        <div class=\"container-fluid widget-sortable-unit\">
            <div class=\"hidden\">
                <input type=\"text\" value=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["media"], "mediaId", array()), "html", null, true);
            echo "\"
                       name=\"media[";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index0", array()), "html", null, true);
            echo "][mediaId]\">
                <input type=\"text\" value=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["media"], "pictureUrl", array()), "html", null, true);
            echo "\"
                       name=\"media[";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index0", array()), "html", null, true);
            echo "][pictureUrl]\">
                <input type=\"text\" value=\"";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["media"], "title", array()), "html", null, true);
            echo "\"
                       name=\"media[";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index0", array()), "html", null, true);
            echo "][title]\">
            </div>
            <div class=\"row\">
                <div class=\"col-xs-2\">
                    <img class=\"img-responsive\" src=\"";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["media"], "pictureUrl", array()), "html", null, true);
            echo "\" alt=\"\">
                </div>
                <div class=\"col-xs-8\">
                    ";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($context["media"], "title", array()), "html", null, true);
            echo "
                </div>
                <div class=\"col-xs-2 text-right\">
                    <a class=\"btn btn-danger widget-media-delete\">
                        <i class=\"fa fa-times\"></i>
                    </a>
                </div>
            </div>
        </div>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['media'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "</div>

<hr>

<a class=\"add-media-widget-button btn btn-block btn-success\" href=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "media-gallery-widget-browse"), "method"), "html", null, true);
        echo "\"
   data-toggle=\"modal\" data-target=\"#galleryWidgetModal\">
    Add Media
</a>


<div class=\"modal bounceIn animated\" id=\"galleryWidgetModal\" tabindex=\"-1\" role=\"dialog\"
     aria-labelledby=\"galleryWidgetModalLabel\"
     aria-hidden=\"true\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                <h4 class=\"modal-title\">Add Media</h4>
            </div>
            <div class=\"modal-body\">
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>
                <button type=\"button\" id=\"add-media-to-widget-button\" class=\"btn btn-primary\">Add Media</button>
            </div>
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

<script type=\"text/javascript\">
    function mediaWidgetReorganizeIndexes() {
        // arrange all indexes
        var elements = \$(\"#media-list-holder > .container-fluid\");
        var i = 0;
        elements.each(function () {
            var inputs = \$(this).find(\"input\");
            \$(inputs[0]).attr(\"name\", \"media[\" + i + \"][mediaId]\");
            \$(inputs[1]).attr(\"name\", \"media[\" + i + \"][pictureUrl]\");
            \$(inputs[2]).attr(\"name\", \"media[\" + i + \"][title]\");
            i++;
        });

        return i;
    }

    \$(\"#media-list-holder\").on(\"click\", \".widget-media-delete\", function () {
        \$(this).closest(\".container-fluid\").remove();
        mediaWidgetReorganizeIndexes();
    });

    \$(\"#media-list-holder\").sortable({
        update: function () {
            mediaWidgetReorganizeIndexes();
        }
    });

</script>";
    }

    public function getTemplateName()
    {
        return "Admin/Pages/WidgetOptions/Gallery.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 56,  126 => 52,  102 => 42,  96 => 39,  89 => 35,  85 => 34,  81 => 33,  77 => 32,  73 => 31,  69 => 30,  65 => 28,  48 => 27,  36 => 18,  26 => 11,  25 => 5,  19 => 1,);
    }
}
/* <div class="row">*/
/*     <div class="col-md-6">*/
/*         <div class="form-group">*/
/*             <label>Columns</label>*/
/*             {{ HtmlHelper.SearchableSelect({*/
/*                 2: "2",*/
/*                 3: "3",*/
/*                 4: "4",*/
/*                 5: "5",*/
/*                 6: "6",*/
/*             }, "numColumns", widget.Options.numColumns)|raw }}*/
/*         </div>*/
/*     </div>*/
/*     <div class="col-md-6">*/
/*         <div class="form-group">*/
/*             <label>Big image (0 for all small)</label>*/
/*             <input type="number" min="0" step="1" placeholder="0" name="big" class="form-control"*/
/*                    value="{{ widget.Options.big }}"/>*/
/*         </div>*/
/* */
/*     </div>*/
/* </div>*/
/* */
/* <h5>Media</h5>*/
/* */
/* <div id="media-list-holder" style="height: 250px; overflow: auto;">*/
/*     {% for media in widget.Options.media %}*/
/*         <div class="container-fluid widget-sortable-unit">*/
/*             <div class="hidden">*/
/*                 <input type="text" value="{{ media.mediaId }}"*/
/*                        name="media[{{ loop.index0 }}][mediaId]">*/
/*                 <input type="text" value="{{ media.pictureUrl }}"*/
/*                        name="media[{{ loop.index0 }}][pictureUrl]">*/
/*                 <input type="text" value="{{ media.title }}"*/
/*                        name="media[{{ loop.index0 }}][title]">*/
/*             </div>*/
/*             <div class="row">*/
/*                 <div class="col-xs-2">*/
/*                     <img class="img-responsive" src="{{ media.pictureUrl }}" alt="">*/
/*                 </div>*/
/*                 <div class="col-xs-8">*/
/*                     {{ media.title }}*/
/*                 </div>*/
/*                 <div class="col-xs-2 text-right">*/
/*                     <a class="btn btn-danger widget-media-delete">*/
/*                         <i class="fa fa-times"></i>*/
/*                     </a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     {% endfor %}*/
/* </div>*/
/* */
/* <hr>*/
/* */
/* <a class="add-media-widget-button btn btn-block btn-success" href="{{ Router.Create("media-gallery-widget-browse") }}"*/
/*    data-toggle="modal" data-target="#galleryWidgetModal">*/
/*     Add Media*/
/* </a>*/
/* */
/* */
/* <div class="modal bounceIn animated" id="galleryWidgetModal" tabindex="-1" role="dialog"*/
/*      aria-labelledby="galleryWidgetModalLabel"*/
/*      aria-hidden="true">*/
/*     <div class="modal-dialog">*/
/*         <div class="modal-content">*/
/*             <div class="modal-header">*/
/*                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>*/
/*                 <h4 class="modal-title">Add Media</h4>*/
/*             </div>*/
/*             <div class="modal-body">*/
/*             </div>*/
/*             <div class="modal-footer">*/
/*                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>*/
/*                 <button type="button" id="add-media-to-widget-button" class="btn btn-primary">Add Media</button>*/
/*             </div>*/
/*         </div><!-- modal-content -->*/
/*     </div><!-- modal-dialog -->*/
/* </div><!-- modal -->*/
/* */
/* <script type="text/javascript">*/
/*     function mediaWidgetReorganizeIndexes() {*/
/*         // arrange all indexes*/
/*         var elements = $("#media-list-holder > .container-fluid");*/
/*         var i = 0;*/
/*         elements.each(function () {*/
/*             var inputs = $(this).find("input");*/
/*             $(inputs[0]).attr("name", "media[" + i + "][mediaId]");*/
/*             $(inputs[1]).attr("name", "media[" + i + "][pictureUrl]");*/
/*             $(inputs[2]).attr("name", "media[" + i + "][title]");*/
/*             i++;*/
/*         });*/
/* */
/*         return i;*/
/*     }*/
/* */
/*     $("#media-list-holder").on("click", ".widget-media-delete", function () {*/
/*         $(this).closest(".container-fluid").remove();*/
/*         mediaWidgetReorganizeIndexes();*/
/*     });*/
/* */
/*     $("#media-list-holder").sortable({*/
/*         update: function () {*/
/*             mediaWidgetReorganizeIndexes();*/
/*         }*/
/*     });*/
/* */
/* </script>*/
