<?php

/* Admin/Builder/Widget/Divider.twig */
class __TwigTemplate_bf196e4bc138d672e05270c92e070cc47bf74f8a07a360b75b807aa7c4b61dc3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Builder/Widget/Master/Panel.twig", "Admin/Builder/Widget/Divider.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Builder/Widget/Master/Panel.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Divider";
    }

    public function getTemplateName()
    {
        return "Admin/Builder/Widget/Divider.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Builder/Widget/Master/Panel.twig" %}*/
/* */
/* {% block title %}Divider{% endblock %}*/
