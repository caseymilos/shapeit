<?php

/* Admin/Offers/Update.twig */
class __TwigTemplate_a3a517f95746111389d1bb754aed652bbc073dfe99dd3ddcaa479950e468538d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("Admin/Master/Master.twig", "Admin/Offers/Update.twig", 3);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Master.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Edit Offer ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Title", array()), "html", null, true);
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "    <ol class=\"breadcrumb breadcrumb-quirk\">
        <li>
            <a href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "home"), "method"), "html", null, true);
        echo "\">
                <i class=\"fa fa-home mr5\"></i>
                Home
            </a>
        </li>
        <li>
            <a href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "offeres-list"), "method"), "html", null, true);
        echo "\">Offers</a>
        </li>
        <li class=\"active\">Edit offer ";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Title", array()), "html", null, true);
        echo "</li>
    </ol>

    ";
        // line 21
        $this->loadTemplate("Admin/Offers/Partial/OfferForm.twig", "Admin/Offers/Update.twig", 21)->display(array_merge($context, array("model" => (isset($context["model"]) ? $context["model"] : null))));
    }

    public function getTemplateName()
    {
        return "Admin/Offers/Update.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 21,  57 => 18,  52 => 16,  43 => 10,  39 => 8,  36 => 7,  29 => 5,  11 => 3,);
    }
}
/* {# @var model \OfferViewModel #}*/
/* */
/* {% extends "Admin/Master/Master.twig" %}*/
/* */
/* {% block title %}Edit Offer {{ model.Title }}{% endblock %}*/
/* */
/* {% block content %}*/
/*     <ol class="breadcrumb breadcrumb-quirk">*/
/*         <li>*/
/*             <a href="{{ Router.Create("home") }}">*/
/*                 <i class="fa fa-home mr5"></i>*/
/*                 Home*/
/*             </a>*/
/*         </li>*/
/*         <li>*/
/*             <a href="{{ Router.Create("offeres-list") }}">Offers</a>*/
/*         </li>*/
/*         <li class="active">Edit offer {{ model.Title }}</li>*/
/*     </ol>*/
/* */
/*     {% include "Admin/Offers/Partial/OfferForm.twig" with {'model': model} %}*/
/* {% endblock %}*/
