<?php

/* Admin/Shared/Header/Right/User.twig */
class __TwigTemplate_969365aadc0c1c178b4f43b41017db409a1337d8d427463e8688df7a25c888a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["currentUser"] = $this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "GetCurrentUser", array(), "method");
        // line 2
        echo "<div class=\"btn-group\">
    <button type=\"button\" class=\"btn btn-logged\" data-toggle=\"dropdown\">
        <img src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["currentUser"]) ? $context["currentUser"] : null), "Picture", array()), "html", null, true);
        echo "\" alt=\"\"/>
        ";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["currentUser"]) ? $context["currentUser"] : null), "FirstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["currentUser"]) ? $context["currentUser"] : null), "LastName", array()), "html", null, true);
        echo "
        <span class=\"caret\"></span>
    </button>
    <ul class=\"dropdown-menu pull-right\">
        <li>
            <a href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "logged-user-update-details"), "method"), "html", null, true);
        echo "\">
                <i class=\"glyphicon glyphicon-user\"></i>
                My Profile
            </a>
        </li>
        <li>
            <a href=\"#\">
                <i class=\"glyphicon glyphicon-question-sign\"></i>
                Help
            </a>
        </li>
        <li>
            <a href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "logout"), "method"), "html", null, true);
        echo "\">
                <i class=\"glyphicon glyphicon-log-out\"></i>
                Log Out
            </a>
        </li>
    </ul>
</div>";
    }

    public function getTemplateName()
    {
        return "Admin/Shared/Header/Right/User.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 22,  39 => 10,  29 => 5,  25 => 4,  21 => 2,  19 => 1,);
    }
}
/* {% set currentUser = Helper.GetCurrentUser() %}*/
/* <div class="btn-group">*/
/*     <button type="button" class="btn btn-logged" data-toggle="dropdown">*/
/*         <img src="{{ currentUser.Picture }}" alt=""/>*/
/*         {{ currentUser.FirstName }} {{ currentUser.LastName }}*/
/*         <span class="caret"></span>*/
/*     </button>*/
/*     <ul class="dropdown-menu pull-right">*/
/*         <li>*/
/*             <a href="{{ Router.Create("logged-user-update-details") }}">*/
/*                 <i class="glyphicon glyphicon-user"></i>*/
/*                 My Profile*/
/*             </a>*/
/*         </li>*/
/*         <li>*/
/*             <a href="#">*/
/*                 <i class="glyphicon glyphicon-question-sign"></i>*/
/*                 Help*/
/*             </a>*/
/*         </li>*/
/*         <li>*/
/*             <a href="{{ Router.Create("logout") }}">*/
/*                 <i class="glyphicon glyphicon-log-out"></i>*/
/*                 Log Out*/
/*             </a>*/
/*         </li>*/
/*     </ul>*/
/* </div>*/
