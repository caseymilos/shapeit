<?php

/* Admin/Pages/WidgetOptions/LatestBlog.twig */
class __TwigTemplate_f5a4dbb918acb6a2563724c45d45371af8729b1f8eaa59ec04c6fa786a569855 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-group\">
\t<label>Title</label>
\t<input type=\"text\" placeholder=\"Title\" name=\"title\" class=\"form-control\"
\t\t   value=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "title", array()), "html", null, true);
        echo "\"/>
</div>

<div class=\"form-group\">
\t<label>Subtitle</label>
\t<input type=\"text\" placeholder=\"Subtitle\" name=\"subtitle\" class=\"form-control\"
\t\t   value=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "subtitle", array()), "html", null, true);
        echo "\"/>
</div>

<div class=\"form-group\">
\t<label>Number of Items <em>(enter 0 for all posts)</em></label>
\t<input type=\"number\" step=\"1\" min=\"0\" placeholder=\"Number\" name=\"number\" class=\"form-control\"
\t\t   value=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "number", array()), "html", null, true);
        echo "\"/>
</div>";
    }

    public function getTemplateName()
    {
        return "Admin/Pages/WidgetOptions/LatestBlog.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 16,  33 => 10,  24 => 4,  19 => 1,);
    }
}
/* <div class="form-group">*/
/* 	<label>Title</label>*/
/* 	<input type="text" placeholder="Title" name="title" class="form-control"*/
/* 		   value="{{ widget.Options.title }}"/>*/
/* </div>*/
/* */
/* <div class="form-group">*/
/* 	<label>Subtitle</label>*/
/* 	<input type="text" placeholder="Subtitle" name="subtitle" class="form-control"*/
/* 		   value="{{ widget.Options.subtitle }}"/>*/
/* </div>*/
/* */
/* <div class="form-group">*/
/* 	<label>Number of Items <em>(enter 0 for all posts)</em></label>*/
/* 	<input type="number" step="1" min="0" placeholder="Number" name="number" class="form-control"*/
/* 		   value="{{ widget.Options.number }}"/>*/
/* </div>*/
