<?php

/* Admin/Offers/ThumbnailCrop.twig */
class __TwigTemplate_400debc8a48b6d2fbe0490e079a4b3ff41dc488bcf37f2921450c9c2fe748f30 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Master.twig", "Admin/Offers/ThumbnailCrop.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'post_scripts' => array($this, 'block_post_scripts'),
            'additional_styles' => array($this, 'block_additional_styles'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Master.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Crop Thumbnail";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "
\t<ol class=\"breadcrumb breadcrumb-quirk\">
\t\t<li>
\t\t\t<a href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "home"), "method"), "html", null, true);
        echo "\">
\t\t\t\t<i class=\"fa fa-home mr5\"></i>
\t\t\t\tHome
\t\t\t</a>
\t\t</li>
\t\t<li>
\t\t\t<a href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "media-manager"), "method"), "html", null, true);
        echo "\">Offers</a>
\t\t</li>
\t\t<li class=\"active\">Crop Thumbnail</li>
\t</ol>

\t<div class=\"row\">
\t\t<div class=\"col-sm-12\">
\t\t\t<div class=\"panel\">
\t\t\t\t<div class=\"panel-heading nopaddingbottom\">
\t\t\t\t\t<h4 class=\"panel-title\">Crop Thumbnail</h4>
\t\t\t\t</div>
\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t<hr>

\t\t\t\t\t<div id=\"event-thumbnail\">
\t\t\t\t\t</div>

\t\t\t\t\t<hr>

\t\t\t\t\t<a href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "offer-thumbnail-crop", 1 => array("id" => $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "OfferId", array()), "size" => (isset($context["size"]) ? $context["size"] : null))), "method"), "html", null, true);
        echo "\"
\t\t\t\t\t   class=\"btn btn-primary\" id=\"save-thumbnail\">Save
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>


";
    }

    // line 47
    public function block_post_scripts($context, array $blocks = array())
    {
        // line 48
        echo "\t<script src=\"Theme/Admin/lib/Croppie/croppie.min.js\"></script>
\t<script type=\"text/javascript\">
\t\t\$('#event-thumbnail').croppie({
\t\t\turl: '";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "PictureSource", array(), "method"), "html", null, true);
        echo "',
\t\t\tviewport: {
\t\t\t\twidth: ";
        // line 53
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dimensions"]) ? $context["dimensions"] : null), 0, array()), "html", null, true);
        echo ",
\t\t\t\theight: ";
        // line 54
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dimensions"]) ? $context["dimensions"] : null), 1, array()), "html", null, true);
        echo ",
\t\t\t\ttype: 'square'
\t\t\t},
\t\t\tboundary: {
\t\t\t\twidth: ";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dimensions"]) ? $context["dimensions"] : null), 2, array()), "html", null, true);
        echo ",
\t\t\t\theight: ";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dimensions"]) ? $context["dimensions"] : null), 3, array()), "html", null, true);
        echo "
\t\t\t}
\t\t});

\t\t\$(\"#save-thumbnail\").on(\"click\", function (e) {
\t\t\te.preventDefault();
\t\t\tvar data = \$(\"#event-thumbnail\").croppie('get');
\t\t\t\$(\"body\").addClass(\"loading\");

\t\t\t\$.ajax({
\t\t\t\turl: \$(this).attr(\"href\"),
\t\t\t\ttype: \"POST\",
\t\t\t\tdata: {
\t\t\t\t\tpoints: data.points
\t\t\t\t},
\t\t\t\tcomplete: function () {
\t\t\t\t\t\$(\"body\").removeClass(\"loading\");
\t\t\t\t}
\t\t\t})
\t\t});
\t</script>
";
    }

    // line 82
    public function block_additional_styles($context, array $blocks = array())
    {
        // line 83
        echo "\t<link rel=\"stylesheet\" href=\"Theme/Admin/lib/Croppie/croppie.css\">

";
    }

    public function getTemplateName()
    {
        return "Admin/Offers/ThumbnailCrop.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 83,  144 => 82,  118 => 59,  114 => 58,  107 => 54,  103 => 53,  98 => 51,  93 => 48,  90 => 47,  76 => 36,  54 => 17,  45 => 11,  40 => 8,  37 => 7,  31 => 5,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Master.twig" %}*/
/* */
/* {# @var model \Data\Models\Offer #}*/
/* */
/* {% block title %}Crop Thumbnail{% endblock %}*/
/* */
/* {% block content %}*/
/* */
/* 	<ol class="breadcrumb breadcrumb-quirk">*/
/* 		<li>*/
/* 			<a href="{{ Router.Create("home") }}">*/
/* 				<i class="fa fa-home mr5"></i>*/
/* 				Home*/
/* 			</a>*/
/* 		</li>*/
/* 		<li>*/
/* 			<a href="{{ Router.Create("media-manager") }}">Offers</a>*/
/* 		</li>*/
/* 		<li class="active">Crop Thumbnail</li>*/
/* 	</ol>*/
/* */
/* 	<div class="row">*/
/* 		<div class="col-sm-12">*/
/* 			<div class="panel">*/
/* 				<div class="panel-heading nopaddingbottom">*/
/* 					<h4 class="panel-title">Crop Thumbnail</h4>*/
/* 				</div>*/
/* 				<div class="panel-body">*/
/* 					<hr>*/
/* */
/* 					<div id="event-thumbnail">*/
/* 					</div>*/
/* */
/* 					<hr>*/
/* */
/* 					<a href="{{ Router.Create("offer-thumbnail-crop", {"id": model.OfferId, "size": size}) }}"*/
/* 					   class="btn btn-primary" id="save-thumbnail">Save*/
/* 					</a>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* */
/* */
/* {% endblock %}*/
/* */
/* {% block post_scripts %}*/
/* 	<script src="Theme/Admin/lib/Croppie/croppie.min.js"></script>*/
/* 	<script type="text/javascript">*/
/* 		$('#event-thumbnail').croppie({*/
/* 			url: '{{ model.PictureSource() }}',*/
/* 			viewport: {*/
/* 				width: {{ dimensions.0 }},*/
/* 				height: {{ dimensions.1 }},*/
/* 				type: 'square'*/
/* 			},*/
/* 			boundary: {*/
/* 				width: {{ dimensions.2 }},*/
/* 				height: {{ dimensions.3 }}*/
/* 			}*/
/* 		});*/
/* */
/* 		$("#save-thumbnail").on("click", function (e) {*/
/* 			e.preventDefault();*/
/* 			var data = $("#event-thumbnail").croppie('get');*/
/* 			$("body").addClass("loading");*/
/* */
/* 			$.ajax({*/
/* 				url: $(this).attr("href"),*/
/* 				type: "POST",*/
/* 				data: {*/
/* 					points: data.points*/
/* 				},*/
/* 				complete: function () {*/
/* 					$("body").removeClass("loading");*/
/* 				}*/
/* 			})*/
/* 		});*/
/* 	</script>*/
/* {% endblock %}*/
/* */
/* {% block additional_styles %}*/
/* 	<link rel="stylesheet" href="Theme/Admin/lib/Croppie/croppie.css">*/
/* */
/* {% endblock %}*/
