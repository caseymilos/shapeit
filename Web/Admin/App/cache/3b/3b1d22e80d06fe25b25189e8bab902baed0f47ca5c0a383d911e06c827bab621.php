<?php

/* Admin/Home/Index.twig */
class __TwigTemplate_2090fdaf3f56158777dacb809b4a35ed1101aec3bea64175f995c6ba2853c35e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Master.twig", "Admin/Home/Index.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Master.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Dashboard";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <div class=\"col-md-12 col-lg-8 dash-left\">
        <div class=\"panel panel-inverse\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\">Dashboard</h3>
            </div>
        </div>

        <div class=\"row panel-quick-page\">
            <div class=\"col-xs-4 col-sm-5 col-md-4 page-user\">
                <div class=\"panel\">
                    <a href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "offers"), "method"), "html", null, true);
        echo "\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">Offers</h4>
                        </div>
                        <div class=\"panel-body\">
                            <div class=\"page-icon\">
                                <i class=\"fa fa-globe\"></i>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class=\"col-xs-4 col-sm-5 col-md-4 page-user\">
                <div class=\"panel\">
                    <a href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "bookings"), "method"), "html", null, true);
        echo "\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">Bookings</h4>
                        </div>
                        <div class=\"panel-body\">
                            <div class=\"page-icon\">
                                <i class=\"fa  fa-calendar\"></i>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class=\"col-xs-4 col-sm-4 col-md-4 page-products\">
                <div class=\"panel\">
                    <a href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "pages-list"), "method"), "html", null, true);
        echo "\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">Pages</h4>
                        </div>
                        <div class=\"panel-body\">
                            <div class=\"page-icon\">
                                <i class=\"icon ion-ios-copy-outline\"></i>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class=\"col-xs-4 col-sm-5 col-md-4 page-events\">
                <div class=\"panel\">
                    <a href=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "page-builder-create"), "method"), "html", null, true);
        echo "\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">New Page</h4>
                        </div>
                        <div class=\"panel-body\">
                            <div class=\"page-icon\">
                                <i class=\"icon ion-ios-compose-outline\"></i>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class=\"col-xs-4 col-sm-5 col-md-2 page-reports\">
                <div class=\"panel\">
                    <a href=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "user-create"), "method"), "html", null, true);
        echo "\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">Create New User</h4>
                        </div>
                        <div class=\"panel-body\">
                            <div class=\"page-icon\">
                                <i class=\"icon ion-person-add\"></i>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class=\"col-xs-4 col-sm-4 col-md-2 page-statistics\">
                <div class=\"panel\">
                    <a href=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "blogs"), "method"), "html", null, true);
        echo "\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">Blog</h4>
                        </div>
                        <div class=\"panel-body\">
                            <div class=\"page-icon\">
                                <i class=\"fa fa-align-left\"></i>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class=\"col-xs-4 col-sm-4 col-md-4 page-support\">
                <div class=\"panel\">
                    <a href=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "media-manager"), "method"), "html", null, true);
        echo "\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">Media Manager</h4>
                        </div>
                        <div class=\"panel-body\">
                            <div class=\"page-icon\">
                                <i class=\"icon ion-android-image\"></i>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class=\"col-xs-4 col-sm-4 col-md-2 page-settings\">
                <div class=\"panel\">
                    <a href=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "user-update-details", 1 => array("username" => $this->getAttribute($this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "GetCurrentUser", array(), "method"), "Username", array()))), "method"), "html", null, true);
        echo "\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">Settings</h4>
                        </div>
                        <div class=\"panel-body\">
                            <div class=\"page-icon\">
                                <i class=\"icon ion-gear-a\"></i>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class=\"col-xs-4 col-sm-4 col-md-2 page-privacy\">
                <div class=\"panel\">
                    <a href=\"";
        // line 128
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "logout"), "method"), "html", null, true);
        echo "\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">Logout</h4>
                        </div>
                        <div class=\"panel-body\">
                            <div class=\"page-icon\">
                                <i class=\"icon ion-android-exit\"></i>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div><!-- row -->
    </div>
";
    }

    public function getTemplateName()
    {
        return "Admin/Home/Index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 128,  169 => 114,  152 => 100,  135 => 86,  118 => 72,  101 => 58,  84 => 44,  67 => 30,  50 => 16,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Master.twig" %}*/
/* */
/* {% block title %}Dashboard{% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="col-md-12 col-lg-8 dash-left">*/
/*         <div class="panel panel-inverse">*/
/*             <div class="panel-heading">*/
/*                 <h3 class="panel-title">Dashboard</h3>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="row panel-quick-page">*/
/*             <div class="col-xs-4 col-sm-5 col-md-4 page-user">*/
/*                 <div class="panel">*/
/*                     <a href="{{ Router.Create("offers") }}">*/
/*                         <div class="panel-heading">*/
/*                             <h4 class="panel-title">Offers</h4>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <div class="page-icon">*/
/*                                 <i class="fa fa-globe"></i>*/
/*                             </div>*/
/*                         </div>*/
/*                     </a>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="col-xs-4 col-sm-5 col-md-4 page-user">*/
/*                 <div class="panel">*/
/*                     <a href="{{ Router.Create("bookings") }}">*/
/*                         <div class="panel-heading">*/
/*                             <h4 class="panel-title">Bookings</h4>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <div class="page-icon">*/
/*                                 <i class="fa  fa-calendar"></i>*/
/*                             </div>*/
/*                         </div>*/
/*                     </a>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="col-xs-4 col-sm-4 col-md-4 page-products">*/
/*                 <div class="panel">*/
/*                     <a href="{{ Router.Create("pages-list") }}">*/
/*                         <div class="panel-heading">*/
/*                             <h4 class="panel-title">Pages</h4>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <div class="page-icon">*/
/*                                 <i class="icon ion-ios-copy-outline"></i>*/
/*                             </div>*/
/*                         </div>*/
/*                     </a>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="col-xs-4 col-sm-5 col-md-4 page-events">*/
/*                 <div class="panel">*/
/*                     <a href="{{ Router.Create("page-builder-create") }}">*/
/*                         <div class="panel-heading">*/
/*                             <h4 class="panel-title">New Page</h4>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <div class="page-icon">*/
/*                                 <i class="icon ion-ios-compose-outline"></i>*/
/*                             </div>*/
/*                         </div>*/
/*                     </a>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="col-xs-4 col-sm-5 col-md-2 page-reports">*/
/*                 <div class="panel">*/
/*                     <a href="{{ Router.Create("user-create") }}">*/
/*                         <div class="panel-heading">*/
/*                             <h4 class="panel-title">Create New User</h4>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <div class="page-icon">*/
/*                                 <i class="icon ion-person-add"></i>*/
/*                             </div>*/
/*                         </div>*/
/*                     </a>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="col-xs-4 col-sm-4 col-md-2 page-statistics">*/
/*                 <div class="panel">*/
/*                     <a href="{{ Router.Create("blogs") }}">*/
/*                         <div class="panel-heading">*/
/*                             <h4 class="panel-title">Blog</h4>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <div class="page-icon">*/
/*                                 <i class="fa fa-align-left"></i>*/
/*                             </div>*/
/*                         </div>*/
/*                     </a>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="col-xs-4 col-sm-4 col-md-4 page-support">*/
/*                 <div class="panel">*/
/*                     <a href="{{ Router.Create("media-manager") }}">*/
/*                         <div class="panel-heading">*/
/*                             <h4 class="panel-title">Media Manager</h4>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <div class="page-icon">*/
/*                                 <i class="icon ion-android-image"></i>*/
/*                             </div>*/
/*                         </div>*/
/*                     </a>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="col-xs-4 col-sm-4 col-md-2 page-settings">*/
/*                 <div class="panel">*/
/*                     <a href="{{ Router.Create("user-update-details", {username: Helper.GetCurrentUser().Username}) }}">*/
/*                         <div class="panel-heading">*/
/*                             <h4 class="panel-title">Settings</h4>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <div class="page-icon">*/
/*                                 <i class="icon ion-gear-a"></i>*/
/*                             </div>*/
/*                         </div>*/
/*                     </a>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="col-xs-4 col-sm-4 col-md-2 page-privacy">*/
/*                 <div class="panel">*/
/*                     <a href="{{ Router.Create("logout") }}">*/
/*                         <div class="panel-heading">*/
/*                             <h4 class="panel-title">Logout</h4>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <div class="page-icon">*/
/*                                 <i class="icon ion-android-exit"></i>*/
/*                             </div>*/
/*                         </div>*/
/*                     </a>*/
/*                 </div>*/
/*             </div>*/
/*         </div><!-- row -->*/
/*     </div>*/
/* {% endblock %}*/
