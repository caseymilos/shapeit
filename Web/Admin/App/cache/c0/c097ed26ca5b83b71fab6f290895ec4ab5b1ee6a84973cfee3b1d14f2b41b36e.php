<?php

/* Admin/Pages/WidgetOptions/SocialIcon.twig */
class __TwigTemplate_9b7d9074809df31d71f2458c4b1c31c143e1aabd097ad8155ec8077aa2a05247 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"button-widget-form\">
    <div class=\"form-group\">
        <label>Icon</label>
        ";
        // line 4
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => array("facebook" => "Facebook", "paypal" => "Paypal", "googlePlus" => "Google Plus", "wikipedia" => "Wikipedia", "spotify" => "Spotify", "reddit" => "Reddit", "linkedIn" => "Linked In", "skype" => "Skype", "twitter" => "Twitter", "youtube" => "Youtube", "vimeo" => "Vimeo", "yahoo" => "Yahoo", "dropbox" => "Dropbox", "ebay" => "Ebay", "trumblr" => "Trumblr", "wordpress" => "Wordpress", "yelp" => "Yelp", "acrobat" => "Acrobat", "amazon" => "Amazon", "appstore" => "Appstore", "pinterst" => "Pinterst"), 1 => "socialIcon", 2 => $this->getAttribute($this->getAttribute(        // line 26
(isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "socialIcon", array())), "method");
        echo "
    </div>
    <div class=\"form-group\">
        <label>Icon Url</label>
        <div class=\"row\">
            <div class=\"col-md-12\">
                <input type=\"text\" name=\"url\" class=\"widget-url form-control\"
                       value=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "url", array()), "html", null, true);
        echo "\" placeholder=\"Url\"/>
            </div>
        </div>
    </div>
    <div class=\"form-group\">
        <label>Open link in</label>
        ";
        // line 39
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => array("_self" => "Same tab", "_blank" => "New tab"), 1 => "target", 2 => $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "target", array())), "method");
        echo "
    </div>
    <div class=\"form-group\">
        <label>Size</label>
        ";
        // line 43
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => array("default" => "Default", "small" => "Small", "large" => "Large"), 1 => "size", 2 => $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "size", array())), "method");
        echo "
    </div>
    <div class=\"form-group\">
        <label>Style</label>
        ";
        // line 47
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => array("default" => "Default", "light" => "Light", "dark" => "Dark", "textColor" => "Text-color", "colored" => "Colored"), 1 => "style", 2 => $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "style", array())), "method");
        echo "
    </div>
    <div class=\"form-group\">
        <div class=\"row\">
            <div class=\"col-md-12\">
                <label class=\"ckbox\">
                    <input type=\"checkbox\" name=\"border\"
                           ";
        // line 54
        if (($this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "border", array()) == "on")) {
            echo "checked=\"checked\"";
        }
        echo ">
                    <span>Border</span>
                </label>
            </div>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"row\">
            <div class=\"col-md-12\">
                <label class=\"ckbox\">
                    <input type=\"checkbox\" name=\"rounded\"
                           ";
        // line 65
        if (($this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "rounded", array()) == "on")) {
            echo "checked=\"checked\"";
        }
        echo ">
                    <span>Rounded</span>
                </label>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "Admin/Pages/WidgetOptions/SocialIcon.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 65,  68 => 54,  58 => 47,  51 => 43,  44 => 39,  35 => 33,  25 => 26,  24 => 4,  19 => 1,);
    }
}
/* <div class="button-widget-form">*/
/*     <div class="form-group">*/
/*         <label>Icon</label>*/
/*         {{ HtmlHelper.SearchableSelect({*/
/*             facebook: "Facebook",*/
/*             paypal: "Paypal",*/
/*             googlePlus: "Google Plus",*/
/*             wikipedia: "Wikipedia",*/
/*             spotify: "Spotify",*/
/*             reddit: "Reddit",*/
/*             linkedIn: "Linked In",*/
/*             skype: "Skype",*/
/*             twitter: "Twitter",*/
/*             youtube: "Youtube",*/
/*             vimeo: "Vimeo",*/
/*             yahoo: "Yahoo",*/
/*             dropbox: "Dropbox",*/
/*             ebay: "Ebay",*/
/*             trumblr: "Trumblr",*/
/*             wordpress: "Wordpress",*/
/*             yelp: "Yelp",*/
/*             acrobat: "Acrobat",*/
/*             amazon: "Amazon",*/
/*             appstore: "Appstore",*/
/*             pinterst: "Pinterst",*/
/*         }, "socialIcon", widget.Options.socialIcon)|raw }}*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label>Icon Url</label>*/
/*         <div class="row">*/
/*             <div class="col-md-12">*/
/*                 <input type="text" name="url" class="widget-url form-control"*/
/*                        value="{{ widget.Options.url }}" placeholder="Url"/>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label>Open link in</label>*/
/*         {{ HtmlHelper.SearchableSelect({_self: "Same tab", _blank: "New tab"}, "target", widget.Options.target)|raw }}*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label>Size</label>*/
/*         {{ HtmlHelper.SearchableSelect({default: "Default", small: "Small", large: "Large"}, "size", widget.Options.size)|raw }}*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label>Style</label>*/
/*         {{ HtmlHelper.SearchableSelect({default: "Default", light: "Light", dark: "Dark", textColor: "Text-color", colored: "Colored"}, "style", widget.Options.style)|raw }}*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <div class="row">*/
/*             <div class="col-md-12">*/
/*                 <label class="ckbox">*/
/*                     <input type="checkbox" name="border"*/
/*                            {% if widget.Options.border == "on" %}checked="checked"{% endif %}>*/
/*                     <span>Border</span>*/
/*                 </label>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <div class="row">*/
/*             <div class="col-md-12">*/
/*                 <label class="ckbox">*/
/*                     <input type="checkbox" name="rounded"*/
/*                            {% if widget.Options.rounded == "on" %}checked="checked"{% endif %}>*/
/*                     <span>Rounded</span>*/
/*                 </label>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
