<?php

/* Admin/Pages/WidgetOptions/Divider.twig */
class __TwigTemplate_1c042c8cbe0d75beba6d3acbe551f31bbcbda2171ccee4809ad5ba44f3af4a04 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h5>Divider</h5>

<div class=\"form-group\">
    <label>Positon</label>
    ";
        // line 5
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => array("left" => "Left", "center" => "Center", "right" => "Right"), 1 => "dividerPosition", 2 => $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "dividerPosition", array())), "method");
        echo "
</div>

<div class=\"form-group\">
    <label>Style</label>
    ";
        // line 10
        echo $this->getAttribute((isset($context["HtmlHelper"]) ? $context["HtmlHelper"] : null), "SearchableSelect", array(0 => array("default" => "Default", "short" => "Short"), 1 => "style", 2 => $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "style", array())), "method");
        echo "
</div>

<div class=\"form-group\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <label class=\"ckbox\">
                <input type=\"checkbox\" name=\"border\"
                       ";
        // line 18
        if (($this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "border", array()) == "on")) {
            echo "checked=\"checked\"";
        }
        echo ">
                <span>Border</span>
            </label>
        </div>
    </div>
</div>
<div class=\"form-group\">
    <label>Icon Type</label>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <input type=\"text\" name=\"icon\" class=\"widget-title form-control\"
                   value=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "icon", array()), "html", null, true);
        echo "\" placeholder=\"icon\"/>
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "Admin/Pages/WidgetOptions/Divider.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 29,  44 => 18,  33 => 10,  25 => 5,  19 => 1,);
    }
}
/* <h5>Divider</h5>*/
/* */
/* <div class="form-group">*/
/*     <label>Positon</label>*/
/*     {{ HtmlHelper.SearchableSelect({left: "Left", center: "Center", right: "Right"}, "dividerPosition", widget.Options.dividerPosition)|raw }}*/
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label>Style</label>*/
/*     {{ HtmlHelper.SearchableSelect({default: "Default", short: "Short"}, "style", widget.Options.style)|raw }}*/
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <div class="row">*/
/*         <div class="col-md-12">*/
/*             <label class="ckbox">*/
/*                 <input type="checkbox" name="border"*/
/*                        {% if widget.Options.border == "on" %}checked="checked"{% endif %}>*/
/*                 <span>Border</span>*/
/*             </label>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <div class="form-group">*/
/*     <label>Icon Type</label>*/
/*     <div class="row">*/
/*         <div class="col-md-12">*/
/*             <input type="text" name="icon" class="widget-title form-control"*/
/*                    value="{{ widget.Options.icon }}" placeholder="icon"/>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
