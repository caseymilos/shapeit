<?php

/* Admin/Pages/WidgetOptions/FeaturedBox.twig */
class __TwigTemplate_2e3c6194c5cfaf1cf79c7130460054a8329ae0de2155a007ea9277c7b4fc58b1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["uid"] = twig_random($this->env);
        // line 2
        echo "
<div class=\"form-group\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <label>Picture</label>
        </div>
    </div>
    <a data-toggle=\"modal\" data-background href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "media-people-widget-browse"), "method"), "html", null, true);
        echo "\"
       data-target=\"#peopleGalleryWidgetModal-";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["uid"]) ? $context["uid"] : null), "html", null, true);
        echo "\"
       class=\"widget-people-picture-holder\">
        <input type=\"hidden\" id=\"picture-";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["uid"]) ? $context["uid"] : null), "html", null, true);
        echo "\" name=\"picture\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "picture", array()), "html", null, true);
        echo "\" class=\"hidden\">
    </a>
</div>

<div class=\"form-group\">
    <label>Picture Url</label>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <input type=\"text\" name=\"url\" class=\"widget-url form-control\"
                   value=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "url", array()), "html", null, true);
        echo "\" placeholder=\"Url\"/>
        </div>
    </div>
</div>

<div class=\"form-group\">
    <label>Title</label>
    <input type=\"text\" placeholder=\"Title\" name=\"title\" class=\"form-control\"
           value=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "title", array()), "html", null, true);
        echo "\"/>
</div>

<div class=\"form-group\">
    <label>Subtitle</label>
    <input type=\"text\" placeholder=\"Subtitle\" name=\"subtitle\" class=\"form-control\"
           value=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget"]) ? $context["widget"] : null), "Options", array()), "subtitle", array()), "html", null, true);
        echo "\"/>
</div>


<div class=\"modal bounceIn animated\" id=\"peopleGalleryWidgetModal-";
        // line 39
        echo twig_escape_filter($this->env, (isset($context["uid"]) ? $context["uid"] : null), "html", null, true);
        echo "\" tabindex=\"-1\" role=\"dialog\"
     aria-hidden=\"true\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                <h4 class=\"modal-title\">Add Media</h4>
            </div>
            <div class=\"modal-body\">
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>
                <button type=\"button\" id=\"add-media-to-widget-button\" class=\"btn btn-primary\">Add Media</button>
            </div>
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

<script type=\"text/javascript\">
    \$(document).ready(function () {
        \$(\".widget-people-picture-holder\").on(\"click\", function () {
            \$(\"#peopleGalleryWidgetModal-";
        // line 60
        echo twig_escape_filter($this->env, (isset($context["uid"]) ? $context["uid"] : null), "html", null, true);
        echo "\").data(\"targetInput\", \$(this).find(\"input\"));
        });
    });
</script>";
    }

    public function getTemplateName()
    {
        return "Admin/Pages/WidgetOptions/FeaturedBox.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 60,  80 => 39,  73 => 35,  64 => 29,  53 => 21,  39 => 12,  34 => 10,  30 => 9,  21 => 2,  19 => 1,);
    }
}
/* {% set uid = random() %}*/
/* */
/* <div class="form-group">*/
/*     <div class="row">*/
/*         <div class="col-md-12">*/
/*             <label>Picture</label>*/
/*         </div>*/
/*     </div>*/
/*     <a data-toggle="modal" data-background href="{{ Router.Create("media-people-widget-browse") }}"*/
/*        data-target="#peopleGalleryWidgetModal-{{ uid }}"*/
/*        class="widget-people-picture-holder">*/
/*         <input type="hidden" id="picture-{{ uid }}" name="picture" value="{{ widget.Options.picture }}" class="hidden">*/
/*     </a>*/
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label>Picture Url</label>*/
/*     <div class="row">*/
/*         <div class="col-md-12">*/
/*             <input type="text" name="url" class="widget-url form-control"*/
/*                    value="{{ widget.Options.url }}" placeholder="Url"/>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label>Title</label>*/
/*     <input type="text" placeholder="Title" name="title" class="form-control"*/
/*            value="{{ widget.Options.title }}"/>*/
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label>Subtitle</label>*/
/*     <input type="text" placeholder="Subtitle" name="subtitle" class="form-control"*/
/*            value="{{ widget.Options.subtitle }}"/>*/
/* </div>*/
/* */
/* */
/* <div class="modal bounceIn animated" id="peopleGalleryWidgetModal-{{ uid }}" tabindex="-1" role="dialog"*/
/*      aria-hidden="true">*/
/*     <div class="modal-dialog">*/
/*         <div class="modal-content">*/
/*             <div class="modal-header">*/
/*                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>*/
/*                 <h4 class="modal-title">Add Media</h4>*/
/*             </div>*/
/*             <div class="modal-body">*/
/*             </div>*/
/*             <div class="modal-footer">*/
/*                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>*/
/*                 <button type="button" id="add-media-to-widget-button" class="btn btn-primary">Add Media</button>*/
/*             </div>*/
/*         </div><!-- modal-content -->*/
/*     </div><!-- modal-dialog -->*/
/* </div><!-- modal -->*/
/* */
/* <script type="text/javascript">*/
/*     $(document).ready(function () {*/
/*         $(".widget-people-picture-holder").on("click", function () {*/
/*             $("#peopleGalleryWidgetModal-{{ uid }}").data("targetInput", $(this).find("input"));*/
/*         });*/
/*     });*/
/* </script>*/
