<?php

/* Admin/Security/Login.twig */
class __TwigTemplate_60a8e3fd7dbebbec0ee51f8174df764afd90251b4b4155051077c3577eda7676 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Login.twig", "Admin/Security/Login.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Login.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Login";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "
    <div class=\"signpanel\"></div>

    <div class=\"panel signin\">
        <div class=\"panel-heading\">
            <h1>
                <img class=\"img-responsive\" src=\"Theme/Admin/images/boulez_saal_akademie.png\" alt=\"Boulez Saal Akademie\"/>
            </h1>
        </div>
        <div class=\"panel-body\">


            ";
        // line 18
        if ( !twig_test_empty((isset($context["errors"]) ? $context["errors"] : null))) {
            // line 19
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 20
                echo "                    <div class=\"alert alert-danger\">
                        <b>Error!</b>
                        ";
                // line 22
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "
                    </div>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "            ";
        }
        // line 26
        echo "
            <div class=\"or\"><i class=\"fa fa-key\"></i></div>
            <form method=\"post\">
                <div class=\"form-group mb10\">
                    <div class=\"input-group\">
                        <span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-user\"></i></span>
                        <input type=\"text\" name=\"username\" class=\"form-control\" placeholder=\"Enter Username\">
                    </div>
                </div>
                <div class=\"form-group nomargin\">
                    <div class=\"input-group\">
                        <span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-lock\"></i></span>
                        <input type=\"password\" name=\"password\" class=\"form-control\" placeholder=\"Enter Password\">
                    </div>
                </div>
                <div>
                    <a href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "reset-password"), "method"), "html", null, true);
        echo "\" class=\"forgot\">Forgot password?</a>
                </div>
                <div class=\"form-group\">
                    <button class=\"btn btn-success btn-quirk btn-block\">Sign In</button>
                </div>
            </form>
            <hr class=\"invisible\">
            <div class=\"form-group\">
                <a href=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "registration"), "method"), "html", null, true);
        echo "\"
                   class=\"btn btn-default btn-quirk btn-stroke btn-stroke-thin btn-block btn-sign\">
                    Not a member? Request access!
                </a>
            </div>
        </div>
    </div><!-- panel -->

";
    }

    public function getTemplateName()
    {
        return "Admin/Security/Login.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 50,  93 => 42,  75 => 26,  72 => 25,  63 => 22,  59 => 20,  54 => 19,  52 => 18,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Login.twig" %}*/
/* */
/* {% block title %}Login{% endblock %}*/
/* */
/* {% block content %}*/
/* */
/*     <div class="signpanel"></div>*/
/* */
/*     <div class="panel signin">*/
/*         <div class="panel-heading">*/
/*             <h1>*/
/*                 <img class="img-responsive" src="Theme/Admin/images/boulez_saal_akademie.png" alt="Boulez Saal Akademie"/>*/
/*             </h1>*/
/*         </div>*/
/*         <div class="panel-body">*/
/* */
/* */
/*             {% if errors is not empty %}*/
/*                 {% for error in errors %}*/
/*                     <div class="alert alert-danger">*/
/*                         <b>Error!</b>*/
/*                         {{ error }}*/
/*                     </div>*/
/*                 {% endfor %}*/
/*             {% endif %}*/
/* */
/*             <div class="or"><i class="fa fa-key"></i></div>*/
/*             <form method="post">*/
/*                 <div class="form-group mb10">*/
/*                     <div class="input-group">*/
/*                         <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>*/
/*                         <input type="text" name="username" class="form-control" placeholder="Enter Username">*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="form-group nomargin">*/
/*                     <div class="input-group">*/
/*                         <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>*/
/*                         <input type="password" name="password" class="form-control" placeholder="Enter Password">*/
/*                     </div>*/
/*                 </div>*/
/*                 <div>*/
/*                     <a href="{{ Router.Create("reset-password") }}" class="forgot">Forgot password?</a>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                     <button class="btn btn-success btn-quirk btn-block">Sign In</button>*/
/*                 </div>*/
/*             </form>*/
/*             <hr class="invisible">*/
/*             <div class="form-group">*/
/*                 <a href="{{ Router.Create("registration") }}"*/
/*                    class="btn btn-default btn-quirk btn-stroke btn-stroke-thin btn-block btn-sign">*/
/*                     Not a member? Request access!*/
/*                 </a>*/
/*             </div>*/
/*         </div>*/
/*     </div><!-- panel -->*/
/* */
/* {% endblock %}*/
