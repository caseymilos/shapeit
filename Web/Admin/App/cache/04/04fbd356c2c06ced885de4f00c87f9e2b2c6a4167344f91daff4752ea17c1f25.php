<?php

/* Admin/Builder/Widget/PromoBox.twig */
class __TwigTemplate_eb267fac4b8197c43dbbe70d31351843f185d91e340ed2a27addb3a7aa3e5ff0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Builder/Widget/Master/Panel.twig", "Admin/Builder/Widget/PromoBox.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Builder/Widget/Master/Panel.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Promo Box";
    }

    public function getTemplateName()
    {
        return "Admin/Builder/Widget/PromoBox.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Builder/Widget/Master/Panel.twig" %}*/
/* */
/* {% block title %}Promo Box{% endblock %}*/
