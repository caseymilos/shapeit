<?php

/* Admin/Users/UpdateLoggedUser.twig */
class __TwigTemplate_ac676e2758dbacb57bc1d0e5894696af99938703684c599749becc7bbaead547 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Admin/Master/Master.twig", "Admin/Users/UpdateLoggedUser.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'post_scripts' => array($this, 'block_post_scripts'),
            'additional_styles' => array($this, 'block_additional_styles'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Admin/Master/Master.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Update User";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <ol class=\"breadcrumb breadcrumb-quirk\">
        <li>
            <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "home"), "method"), "html", null, true);
        echo "\">
                <i class=\"fa fa-home mr5\"></i>
                Home
            </a>
        </li>
        <li>
            <a href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "user-list"), "method"), "html", null, true);
        echo "\">Users</a>
        </li>
        <li class=\"active\">Update user</li>
    </ol>


    ";
        // line 20
        $this->loadTemplate("Admin/Users/Partial/LoggedUserForm.twig", "Admin/Users/UpdateLoggedUser.twig", 20)->display($context);
        // line 21
        echo "
";
    }

    // line 24
    public function block_post_scripts($context, array $blocks = array())
    {
        // line 25
        echo "    <script src=\"Theme/Admin/lib/select2/select2.js\"></script>
    <script type=\"text/javascript\">
        // Date Picker
        \$('.datepicker').datepicker();
        \$('.select2').select2();
    </script>
";
    }

    // line 33
    public function block_additional_styles($context, array $blocks = array())
    {
        // line 34
        echo "    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/jquery-ui/jquery-ui.css\">
    <link rel=\"stylesheet\" href=\"Theme/Admin/lib/select2/select2.css\">
";
    }

    public function getTemplateName()
    {
        return "Admin/Users/UpdateLoggedUser.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 34,  82 => 33,  72 => 25,  69 => 24,  64 => 21,  62 => 20,  53 => 14,  44 => 8,  40 => 6,  37 => 5,  31 => 3,  11 => 1,);
    }
}
/* {% extends "Admin/Master/Master.twig" %}*/
/* */
/* {% block title %}Update User{% endblock %}*/
/* */
/* {% block content %}*/
/*     <ol class="breadcrumb breadcrumb-quirk">*/
/*         <li>*/
/*             <a href="{{ Router.Create("home") }}">*/
/*                 <i class="fa fa-home mr5"></i>*/
/*                 Home*/
/*             </a>*/
/*         </li>*/
/*         <li>*/
/*             <a href="{{ Router.Create("user-list") }}">Users</a>*/
/*         </li>*/
/*         <li class="active">Update user</li>*/
/*     </ol>*/
/* */
/* */
/*     {% include "Admin/Users/Partial/LoggedUserForm.twig" %}*/
/* */
/* {% endblock %}*/
/* */
/* {% block post_scripts %}*/
/*     <script src="Theme/Admin/lib/select2/select2.js"></script>*/
/*     <script type="text/javascript">*/
/*         // Date Picker*/
/*         $('.datepicker').datepicker();*/
/*         $('.select2').select2();*/
/*     </script>*/
/* {% endblock %}*/
/* */
/* {% block additional_styles %}*/
/*     <link rel="stylesheet" href="Theme/Admin/lib/jquery-ui/jquery-ui.css">*/
/*     <link rel="stylesheet" href="Theme/Admin/lib/select2/select2.css">*/
/* {% endblock %}*/
