<?php

/**
 * Class SponsorViewModel
 *
 * @property \Data\Models\Sponsor Sponsor
 * @property \Data\Models\SponsorGroupDetails GroupDetails
 */
class SponsorViewModel
{

    public $Sponsor;
    public $GroupDetails;

}