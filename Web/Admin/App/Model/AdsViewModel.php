<?php

use Data\Models\Ad;

/**
 * Class AdsViewModel
 *
 * @property Ad[] Ads
 */
class AdsViewModel extends MVCModel {
	public $Ads = [];
}