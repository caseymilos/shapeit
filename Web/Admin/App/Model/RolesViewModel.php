<?php
use Gdev\UserManagement\Models\Role;

/**
 * Class RolesViewModel
 * @property Role[] Roles
 * @property integer CurrentPage
 * @property integer TotalPages
 * @property integer TotalRoles
 * @property integer RolesPerPage
 */
class RolesViewModel {

    public $Roles = [];
    public $TotalRoles;
    public $CurrentPage;
    public $TotalPages;
    public $RolesPerPage;

}