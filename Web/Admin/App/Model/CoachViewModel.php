<?php

/**
 * Class CoachViewModel
 *
 * @property \Data\Models\Coach Coach
 * @property \Data\Models\CoachDetails Details
 */
class CoachViewModel
{

    public $Coach;
    public $Details;

}