<?php
use Gdev\UserManagement\Models\User;

/**
 * Class UsersViewModel
 * @property User[] Users
 * @property integer CurrentPage
 * @property integer TotalPages
 * @property integer TotalUsers
 * @property integer UsersPerPage
 */
class UsersViewModel {

    public $Users = [];
    public $TotalUsers;
    public $CurrentPage;
    public $TotalPages;
    public $UsersPerPage;

}