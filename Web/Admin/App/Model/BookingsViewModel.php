<?php

use Data\Models\Booking;

/**
 * Class BookingsViewModel
 *
 * @property Booking[] Bookings
 */
class BookingsViewModel extends MVCModel {
	public $Bookings = [];
}