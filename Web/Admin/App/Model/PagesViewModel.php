<?php

use Data\Models\Page;

/**
 * Class PagesViewModel
 *
 * @property Page[] Pages
 */
class PagesViewModel extends MVCModel {
    public $Pages = [];

}