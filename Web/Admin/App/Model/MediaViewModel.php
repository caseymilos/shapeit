<?php
use Data\Models\Media;

/**
 * Class MediaViewModel
 *
 * @property Media[] Media
 */
class MediaViewModel extends MVCModel {
    public $Media;
}