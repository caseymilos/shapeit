<?php
use Data\Models\Page;
use Data\Models\PageDetails;

/**
 * Class PageViewModel
 *
 * @property Page Page
 * @property  PageDetails Details
 */
class PageViewModel {

    public $Page;
    public $Details;

}