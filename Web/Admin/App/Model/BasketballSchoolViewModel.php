<?php

/**
 * Class BasketballSchoolViewModel
 *
 * @property \Data\Models\BasketballSchool BasketballSchool
 * @property \Data\Models\BasketballSchoolDetails Details
 */
class BasketballSchoolViewModel
{

    public $BasketballSchool;
    public $Details;

}