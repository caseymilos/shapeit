<?php

use Data\Models\Post;

/**
 * Class PostsViewModel
 *
 * @property Post[] Posts
 */
class PostsViewModel extends MVCModel {
	public $Posts = [];
}