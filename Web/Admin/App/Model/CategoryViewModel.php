<?php

/**
 * Class CategoryViewModel
 *
 * @property \Data\Models\Category Category
 * @property \Data\Models\CategoryDetails Details
 */
class CategoryViewModel
{

    public $Category;
    public $Details;

}