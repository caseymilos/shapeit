<?php
use Gdev\MenuManagement\Models\Menu;

/**
 * Class MenusViewModel
 *
 * @property Menu[] Menus
 */
class MenusViewModel extends MVCModel {
    public $Menus = [];

}