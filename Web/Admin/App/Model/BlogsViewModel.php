<?php

use Data\Models\Blog;

/**
 * Class BlogsViewModel
 *
 * @property Blog[] Blogs
 */
class BlogsViewModel extends MVCModel {
	public $Blogs = [];
}