<?php

/**
 * Class BasketballSchoolsViewModel
 *
 * @property \Data\Models\BasketballSchool[] BasketballSchools
 */
class BasketballSchoolsViewModel
{
    public $BasketballSchools = [];

}