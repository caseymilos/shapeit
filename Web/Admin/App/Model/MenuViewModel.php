<?php
use Gdev\MenuManagement\Models\Menu;
use Gdev\MenuManagement\Models\MenuItem;


/**
 * Class MenuViewModel
 *
 * @property Menu Menu
 * @property MenuItem[] Tree
 */
class MenuViewModel extends MVCModel {
    public $Menu;
    public $Tree;
}