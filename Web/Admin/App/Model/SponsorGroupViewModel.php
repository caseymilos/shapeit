<?php

/**
 * Class SponsorGroupViewModel
 *
 * @property \Data\Models\SponsorGroup SponsorGroup
 * @property \Data\Models\SponsorGroupDetails GroupDetails
 */
class SponsorGroupViewModel
{

    public $SponsorGroup;
    public $SponsorGroupDetails;

}