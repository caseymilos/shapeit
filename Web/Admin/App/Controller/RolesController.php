<?php
use Business\Enums\PermissionsEnum;
use Gdev\UserManagement\ApiControllers\RolePermissionsApiController;
use Gdev\UserManagement\ApiControllers\RolesApiController;
use Gdev\UserManagement\Models\Role;
use Gdev\UserManagement\Models\RolePermission;

/**
 * Created by PhpStorm.
 * User: nikola
 * Date: 13.6.16.
 * Time: 18.24
 */
class RolesController extends MVCController {

    public function GetList($pageNumber = 1, $permissions = [PermissionsEnum::ViewRoles]) {

        $model = new RolesViewModel();

        $limit = Config::BoxesLimit;
        $offset = ($pageNumber - 1) * $limit;

        $totalRoles = RolesApiController::GetRoles();
        $roles = RolesApiController::GetRoles($offset, $limit);

        $model->TotalRoles = count($totalRoles);
        $model->Roles = $roles;
        $model->TotalPages = ceil($model->TotalRoles / $limit);
        $model->CurrentPage = $pageNumber;
        $model->RolesPerPage = $limit;

        $this->RenderView("Roles/List", ['model' => $model]);
    }

    public function GetCreate($permissions = [PermissionsEnum::CreateRole]) {
        $this->RenderView();
    }

    public function PostCreate($name, $active = null, $protected = null, $permissionIds = null, $permissions = [PermissionsEnum::CreateRole]) {

        $role = new Role();

        $role->Name = $name;

        if ($active == "1") {
            $role->Active = 1;
        }
        else {
            $role->Active = 0;
        }

        if ($protected == "1") {
            $role->Protected = 1;
        }
        else {
            $role->Protected = 0;
        }

        $roleId = RolesApiController::InsertRole($role);

        if ($permissionIds != null) {
            foreach ($permissionIds as $permissionId) {
                $rolePermissionModel = new RolePermission();
                $rolePermissionModel->PermissionId = $permissionId;
                $rolePermissionModel->RoleId = $roleId;
                $rolePermissionModel->Protected = 1;
                RolePermissionsApiController::InsertRolesPermission($rolePermissionModel);
            }
        }

        Router::Redirect("role-list");

    }

    public function GetUpdate($roleId, $permissions = [PermissionsEnum::EditRole]) {

        $role = RolesApiController::GetRoleById($roleId);

        $this->RenderView("Roles/Update", ['role' => $role]);

    }

    public function PostUpdate($roleId, $name, $protected = null, $active = null, $permissionIds = null, $permissions = [PermissionsEnum::EditRole]) {
        $role = RolesApiController::GetRoleById($roleId);

        $role->Name = $name;

        if ($active == "1") {
            $role->Active = 1;
        }
        else {
            $role->Active = 0;
        }

        if ($protected == "1") {
            $role->Protected = 1;
        }
        else {
            $role->Protected = 0;
        }

        RolesApiController::UpdatetRole($role);

        $oldRolePermissions = RolePermissionsApiController::GetRolePermissions($roleId);
        $oldRolePermissionTypes = [];

        if ($permissionIds != null) {
            if ($oldRolePermissions != null) {
                foreach ($oldRolePermissions as $oldRolePermission) {
                    $oldRolePermissionTypes[] = $oldRolePermission->PermissionId;
                    if (!in_array($oldRolePermission->PermissionId, $permissionIds, false)) {
                        RolePermissionsApiController::DeleteRolesPermission($oldRolePermission->RolePermissionId);
                    }
                }
            }

            foreach ($permissionIds as $permissionId) {
                $rolePermissionModel = new RolePermission();
                $rolePermissionModel->PermissionId = $permissionId;
                $rolePermissionModel->RoleId = $roleId;
                $rolePermissionModel->Protected = 1;
                if (count($oldRolePermissions) > 0) {
                    if (!in_array($permissionId, $oldRolePermissionTypes, false)) {
                        RolePermissionsApiController::InsertRolesPermission($rolePermissionModel);
                    }
                }
                else {
                    RolePermissionsApiController::InsertRolesPermission($rolePermissionModel);
                }
            }
        }
        else {
            foreach ($oldRolePermissions as $oldRolePermission) {
                RolePermissionsApiController::DeleteRolesPermission($oldRolePermission->RolePermissionId);
            }
        }

        Router::Redirect("role-update", ["roleId" => $roleId]);
    }

    public function GetDelete($id, $permissions = [PermissionsEnum::DeleteRole]) {
        RolesApiController::DeleteRole($id);
        Router::Redirect("role-list");
    }

}