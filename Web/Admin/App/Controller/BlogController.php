<?php

use Business\ApiControllers\BlogsApiController;
use Business\ApiControllers\CommentsApiController;
use Business\Helper\ImageResizeHelper;
use Data\Models\Blog;

class BlogController extends MVCController {

	public function GetBlogs() {
		$model = new BlogsViewModel();
		$model->Blogs = BlogsApiController::GetBlogs();
		$this->RenderView(null, ['model' => $model]);
	}

	/**
	 * @param $id
	 */
	public function GetBlog($id) {
		$model = new BlogViewModel();
		$model->Blog = BlogsApiController::GetBlog($id);
	}

	public function GetCreate() {
		$this->RenderView();
	}

	public function GetDelete($id) {
		BlogsApiController::DeleteBlog($id);
		Router::Redirect("blogs");
	}

	public function PostCreate($title, $theme, $description, $excerpt, $picture, $slug, $relatedBlogs = null) {
		$model = new Blog();

		if ($picture != null) {
			if ($picture['name'] !== "") {
				$newName = self::_generatePictureName($title);
				$imagePath = self::_generatePictureFullPath($newName);
				move_uploaded_file($picture['tmp_name'], $imagePath);
				ImageResizeHelper::ResizeFitImage(100, 100, $imagePath, self::_generatePictureFullPath($newName, "Small"), 75, 'jpeg');
				ImageResizeHelper::ResizeFitImage(400, 400, $imagePath, self::_generatePictureFullPath($newName, "Medium"), 75, 'jpeg');
				ImageResizeHelper::ResizeFitImage(1920, 300, $imagePath, self::_generatePictureFullPath($newName, "Header"), 75, 'jpeg');
				$model->Picture = $newName;
			}
		}

		$model->Title = $title;
		$model->Theme = $theme;
		$model->Slug = $slug;
		$model->Description = $description;
		$model->Excerpt = $excerpt;
		$model->Date = new DateTime();
		$model->AuthorId = Security::GetCurrentUser()->UserId;

		$blogId = BlogsApiController::SaveBlog($model);

		if (count($relatedBlogs) > 0) {
			foreach ($relatedBlogs as $childId) {
				$relation = new \Data\Models\RelatedPosts();
				$relation->ParentId = $blogId;
				$relation->ChildId = $childId;
				BlogsApiController::AddRelation($relation);
			}
		}

		Router::Redirect("blogs");
	}

	public function GetUpdate($id) {
		$model = BlogsApiController::GetBlog($id);
		$this->RenderView(null, ['model' => $model]);
	}

	public function PostUpdate($id, $title, $theme, $excerpt, $description, $picture, $relatedBlogs = []) {

		$model = BlogsApiController::GetBlog($id);

		if ($picture != null) {
			if ($picture['name'] !== "") {
				$newName = self::_generatePictureName($title);
				$imagePath = self::_generatePictureFullPath($newName);
				move_uploaded_file($picture['tmp_name'], $imagePath);
				ImageResizeHelper::ResizeFitImage(100, 100, $imagePath, self::_generatePictureFullPath($newName, "Small"), 75, 'jpeg');
				ImageResizeHelper::ResizeFitImage(400, 400, $imagePath, self::_generatePictureFullPath($newName, "Medium"), 75, 'jpeg');
				ImageResizeHelper::ResizeFitImage(1920, 300, $imagePath, self::_generatePictureFullPath($newName, "Header"), 75, 'jpeg');
				$model->Picture = $newName;
			}
		}

		$model->Title = $title;
		$model->Theme = $theme;
		$model->Description = $description;
		$model->Excerpt = $excerpt;


		BlogsApiController::SaveBlog($model);

		// handle related blogs

		// delete old
		foreach ($model->Children as $child) {
			if (!in_array($child->BlogId, $relatedBlogs)) {
				BlogsApiController::DeleteRelation($id, $child->BlogId);
			} else {
				if (($key = array_search($child->BlogId, $relatedBlogs)) !== false) {
					unset($relatedBlogs[$key]);
				}
			}
		}

		// add mew
		if (count($relatedBlogs)) {
			foreach ($relatedBlogs as $childId) {
				BlogsApiController::AddRelation(new \Data\Models\RelatedPosts([
					"ParentId" => $id,
					"ChildId" => $childId
				]));
			}
		}

		Router::Redirect("blogs");
	}

	public function GetPostComments($blogId) {

		$model = new BlogViewModel();
		$model->Comments = CommentsApiController::GetCommentsByBlogId($blogId);

		$this->RenderView(null, ["model" => $model]);
	}


	private static function _generatePictureName($name) {
		return CommonHelper::StringToFilename(sprintf("travel-insider-%s-%s.%s", $name, CommonHelper::GenerateRandomString(3), "jpg"));
	}

	private static function _generatePictureFullPath($pictureName, $thumb = null) {
		return sprintf("%sMedia/Blogs/%s/%s", CDN_PATH, is_null($thumb) ? "" : $thumb . "/", $pictureName);
	}
}