<?php

use Business\ApiControllers\PostsApiController;
use Business\Helper\ImageResizeHelper;
use Data\Models\Post;

class PostsController extends MVCController {

	public function GetPosts() {
		$model = new PostsViewModel();
		$model->Posts = PostsApiController::GetPosts();
		$this->RenderView(null, ['model' => $model]);
	}

	public function GetPostsByTypeId($postTypeId) {
		$model = new PostsViewModel();
		$model->Posts = PostsApiController::GetPostsByPostTypeId($postTypeId);
		$this->RenderView(null, ['model' => $model]);
	}

	/**
	 * @param $id
	 */
	public function GetPost($id) {
		$model = new PostViewModel();
		$model->Post = PostsApiController::GetPost($id);
	}

	public function GetCreate() {
		$this->RenderView();
	}

	public function GetDelete($id) {
		PostsApiController::DeletePost($id);

		Router::Redirect("posts");
	}

	public function PostCreate($title, $description, $shortDescription, $slug, $theme, $picture, $date, $postTypeId, $relatedPosts = []) {
		$model = new Post();

		if ($picture != null) {
			if ($picture['name'] !== "") {
				$newName = self::_generatePictureName($title);
				$imagePath = self::_generatePictureFullPath($newName);
				move_uploaded_file($picture['tmp_name'], $imagePath);
				ImageResizeHelper::ResizeFitImage(100, 100, $imagePath, self::_generatePictureFullPath($newName, "Small"), 75, 'jpeg');
				ImageResizeHelper::ResizeFitImage(800, 600, $imagePath, self::_generatePictureFullPath($newName, "Medium"), 75, 'jpeg');
				ImageResizeHelper::ResizeFitImage(1920, 300, $imagePath, self::_generatePictureFullPath($newName, "Header"), 75, 'jpeg');
				$model->Picture = $newName;
			}
		}


		$model->Title = $title;
		$model->Description = $description;
		$model->Excerpt = $shortDescription;
		$model->Theme = $theme;
		$model->Date = new DateTime($date);
		$model->Slug = $slug;
		$model->PostTypeId = $postTypeId;
		$model->AuthorId = Security::GetCurrentUser()->UserId;

		$postId = PostsApiController::SavePost($model);

		// manage related posts

		// add new relations
		if (count($relatedPosts) > 0) {
			foreach ($relatedPosts as $childId) {
				$relation = new \Data\Models\RelatedPosts();
				$relation->ParentId = $postId;
				$relation->ChildId = $childId;
				PostsApiController::AddRelation($relation);
			}
		}

		Router::Redirect("posts");
	}

	public function GetUpdate($id) {
		$model = PostsApiController::GetPost($id);
		$this->RenderView(null, ['model' => $model]);
	}

	public function PostUpdate($id, $title, $description, $shortDescription, $slug, $theme, $picture, $postTypeId, $relatedPosts = []) {

		$model = PostsApiController::GetPost($id);

		if ($picture != null) {
			if ($picture['name'] !== "") {
				$newName = self::_generatePictureName($title);
				$imagePath = self::_generatePictureFullPath($newName);
				move_uploaded_file($picture['tmp_name'], $imagePath);
				ImageResizeHelper::ResizeFitImage(100, 100, $imagePath, self::_generatePictureFullPath($newName, "Small"), 75, 'jpeg');
				ImageResizeHelper::ResizeFitImage(800, 600, $imagePath, self::_generatePictureFullPath($newName, "Medium"), 75, 'jpeg');
				ImageResizeHelper::ResizeFitImage(1920, 300, $imagePath, self::_generatePictureFullPath($newName, "Header"), 75, 'jpeg');
				$model->Picture = $newName;
			}
		}

		$model->Title = $title;
		$model->Description = $description;
		$model->Excerpt = $shortDescription;
		$model->Theme = $theme;
		$model->Slug = $slug;
		$model->PostTypeId = $postTypeId;

		PostsApiController::SavePost($model);

		// manage related posts

		// delete unneeded relations
		if (count($model->Children) > 0) {
			foreach ($model->Children as $child) {
				if (!in_array($child->PostId, $relatedPosts)) {
					PostsApiController::RemoveRelation($model->PostId, $child->PostId);
				} else {
					if (($key = array_search($child->PostId, $relatedPosts)) !== false) {
						unset($relatedPosts[$key]);
					}
				}
			}
		}

		// add new relations
		if (count($relatedPosts) > 0) {
			foreach ($relatedPosts as $childId) {
				$relation = new \Data\Models\RelatedPosts();
				$relation->ParentId = $model->PostId;
				$relation->ChildId = $childId;
				PostsApiController::AddRelation($relation);
			}
		}


		Router::Redirect("posts");


	}

	public function GetThumbnailCrop($id, $size) {
		$model = PostsApiController::GetPost($id);

		switch ($size) {
			case "medium":
				$dimensions = [800, 600, 800, 600];
				break;
			default:
				throw new Exception("Unknown image size");
		}

		$this->RenderView(null, ["model" => $model, "size" => $size, "dimensions" => $dimensions]);
	}

	public function PostThumbnailCrop($id, $size, $points) {
		$post = PostsApiController::GetPost($id);
		$picture = new \abeautifulsite\SimpleImage($post->PicturePath());
		$picture->crop($points[0], $points[1], $points[2], $points[3])->thumbnail(800, 600)->save($post->PicturePath("Medium"), 75, "jpeg");
		// clear cloudflare cache
		$this->_clearCloudFlareCache($post);
	}


	private static function _generatePictureName($name) {
		return CommonHelper::StringToFilename(sprintf("travel-insider-%s-%s.%s", $name, CommonHelper::GenerateRandomString(3), "jpg"));
	}

	private static function _generatePictureFullPath($pictureName, $thumb = null) {
		return sprintf("%sMedia/Posts/%s/%s", CDN_PATH, is_null($thumb) ? "" : $thumb . "/", $pictureName);
	}
}