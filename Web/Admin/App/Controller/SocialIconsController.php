<?php
use Business\ApiControllers\SocialIconsApiController;
use Business\DTO\OrderDTO;
use Data\Models\SocialIcon;

class SocialIconsController extends MVCController
{
    public function GetList() {

        $icons = SocialIconsApiController::GetSocialIcons();
        $this->RenderView("SocialIcons/List", ['model' => $icons]);
    }

    public function GetCreate() {
        $this->RenderView();
    }

    public function PostCreate($name, $link, $color, $icon, $active = null) {

        $iconModel = new SocialIcon();

        $iconModel->Name = $name;
        $iconModel->Link = $link;
        $iconModel->Color = $color;
        $iconModel->Icon = $icon;

        if ($active == "1") {
            $iconModel->Active = 1;
        }
        else {
            $iconModel->Active = null;
        }

        SocialIconsApiController::SaveSocialIcon($iconModel);

        Router::Redirect("social-icon-list");

    }

    public function GetDelete($socialIconId) {
        SocialIconsApiController::DeleteSocialIcon($socialIconId);
        Router::Redirect("social-icon-list");
    }

    public function GetUpdate($socialIconId) {
        $model = SocialIconsApiController::GetSocialIcon($socialIconId);
        $this->RenderView("SocialIcons/Update", ['model' => $model]);
    }

    public function PostUpdate($socialIconId, $name, $link, $color, $icon, $active = null) {

        $iconModel = SocialIconsApiController::GetSocialIcon($socialIconId);

        $iconModel->Name = $name;
        $iconModel->Link = $link;
        $iconModel->Color = $color;
        $iconModel->Icon = $icon;

        if ($active == "1") {
            $iconModel->Active = 1;
        }
        else {
            $iconModel->Active = null;
        }

        SocialIconsApiController::SaveSocialIcon($iconModel);

        Router::Redirect("social-icon-list");
    }

    /**
     * @param OrderDTO[] $weights
     */
    public function PostUpdateWeight($weights)
    {
        echo SocialIconsApiController::UpdateSocialIconWeight($weights);
    }

}