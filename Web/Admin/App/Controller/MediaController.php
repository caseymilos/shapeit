<?php

use Alb\OEmbed\Provider;
use Business\ApiControllers\MediaApiController;
use Business\Enums\MediaTypesEnum;
use Business\Helper\ImageResizeHelper;
use Data\Models\Media;
use Data\Models\MediaDescription;

/**
 * Class MediaController
 */
class MediaController extends MVCController {
	public function GetMediaManager() {
		$model = new MediaViewModel();
		$model->Media = MediaApiController::GetAllMedia();

		$this->RenderView(null, ['model' => $model]);
	}

	public function PostUpload($file, $title = null, $copyrights = null) {
		// handle file
		$success = false;
		$mediaObject = null;

		if ($file['name'] !== "") {
			$newName = self::_generateFileName($file['name'], $title);
			$filePath = self::_generateFileFullPath($newName);
			move_uploaded_file($file['tmp_name'], $filePath);
			ImageResizeHelper::ResizeCropImage(100, 100, $filePath, self::_generateFileFullPath($newName, "Small"));
			ImageResizeHelper::ResizeCropImage(500, 500, $filePath, self::_generateFileFullPath($newName, "Medium"));
			ImageResizeHelper::ResizeCropImage(750, 450, $filePath, self::_generateFileFullPath($newName, "Gallery"));

			$media = new Media();
			$media->DateCreated = new DateTime();
			$media->Source = $newName;
			$media->MediaTypeId = MediaTypesEnum::Picture;
			$media->Copyrights = $copyrights;

			$mediaId = MediaApiController::Save($media);

			$description = new MediaDescription();
			$description->MediaId = $mediaId;
			$description->Title = $title;
			$description->LanguageId = MainHelper::GetLanguage();

			$result = MediaApiController::SaveDescription($description);
			$success = $result > 0;

			$mediaObject = (object)[
				"MediaId" => $mediaId,
				"Type" => strtolower(MediaTypesEnum::Description($media->MediaTypeId)),
				"SourceUrl" => $media->SourceUrl(),
				"PictureSource" => $media->PictureSource("Medium"),
				"Title" => $title,
				"AddedTime" => $media->DateCreated->format("F jS, Y")
			];
		}


		$response = (object)[
			"Success" => $success,
			"Error" => $success == false ? "Could not add item to the database" : null,
			"Media" => $mediaObject
		];

		header('Content-Type: application/json');
		echo json_encode($response);


	}

	public function PostDelete($id) {
		$result = MediaApiController::DeleteMedia($id);

		$response = (object)[
			"Success" => $result,
			"Error" => $result == false ? "Could not delete media item from database" : null
		];

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function PostBatchDelete($ids) {
		$result = MediaApiController::BatchDeleteMedia($ids);

		$response = (object)[
			"Success" => $result,
			"Error" => $result == false ? "Could not delete media item from database" : null
		];

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function GetGalleryWidgetBrowse() {
		$model = new MediaViewModel();
		$model->Media = MediaApiController::GetAllMedia();
		$this->RenderView(null, ['model' => $model]);
	}

	public function GetPeopleWidgetBrowse() {
		$model = new MediaViewModel();
		$model->Media = MediaApiController::GetAllMedia();
		$this->RenderView(null, ['model' => $model]);
	}

	public function GetEventWidgetBrowse() {
		$model = new MediaViewModel();
		$model->Media = MediaApiController::GetAllMedia();
		$this->RenderView(null, ['model' => $model]);
	}

	public function GetEdit($id) {
		$model = MediaApiController::GetMedia($id);
		$this->RenderView(null, ["model" => $model]);
	}

	public function PostEdit($id, $title, $copyrights) {
		$media = MediaApiController::GetMedia($id);
		$media->Copyrights = $copyrights;

        MediaApiController::Save($media);

		$mediaDescription = $media->GetDescription(MainHelper::GetLanguage());
		if (null == $mediaDescription) {
			$mediaDescription = new MediaDescription();
			$mediaDescription->LanguageId = MainHelper::GetLanguage();
			$mediaDescription->MediaId = $id;
		}
		$mediaDescription->Title = $title;

		MediaApiController::SaveDescription($mediaDescription);

		$response = (object)[
			"Success" => true,
			"Media" => (object)[
				"MediaId" => $media->MediaId,
				"Title" => $title
			]
		];

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function GetVimeo() {
		$config = \Business\Utilities\Config\Config::GetInstance();
		$vimeo = new \Vimeo\Vimeo($config->vimeo->clientId, $config->vimeo->secret, $config->vimeo->token);
		$response = $vimeo->request('/me/videos', array('sort' => 'date', 'direction' => 'desc'), 'GET');

		foreach ($response['body']['data'] as $video) {

			$media = \Data\Repositories\MediaRepository::getInstance()->first(["Uri" => $video['uri']]);
			if (false == $media) {
				$media = new Media();
			}

			$media->MediaTypeId = MediaTypesEnum::Video;
			$media->Uri = $video['uri'];
			$media->Thumbnail = $video['pictures']['sizes'][2]['link_with_play_button'];
			$media->DateCreated = new DateTime();

			$provider = new Provider('http://vimeo.com/api/oembed.json', 'json');
			$oembed = $provider->request($video['link']);
			$media->Source = $oembed->getRaw()->video_id;

			$mediaId = MediaApiController::Save($media);
			$mediaId = $media->MediaId;

			foreach (\Business\Enums\LanguagesEnum::enum() as $key => $value) {
				$description = $media->GetDescription($value);
				if (false == $description) {
					$description = new MediaDescription();
				}
				$description->MediaId = $mediaId;
				$description->Title = $video['name'];
				$description->LanguageId = $value;

				$result = MediaApiController::SaveDescription($description);

			}
		}

		$this->RenderView(null, ['success' => true]);
	}


	public function GetThumbnailCrop($id, $size) {
		$model = MediaApiController::GetMedia($id);

		switch ($size) {
			case "gallery":
				$dimensions = [750, 450, 750, 450];
				break;
			default:
				throw new Exception("Unknown image size");
		}

		$this->RenderView(null, ["model" => $model, "size" => $size, "dimensions" => $dimensions]);
	}

	public function PostThumbnailCrop($id, $size, $points) {
		$event = MediaApiController::GetMedia($id);
		$picture = new \abeautifulsite\SimpleImage($event->PicturePath());
		$picture->crop($points[0], $points[1], $points[2], $points[3])->thumbnail(750, 450)->save($event->PicturePath("Gallery"), 75, "jpeg");
	}


	// Private methods

	private static function _generateFileName($file, $title) {
		return CommonHelper::StringToFilename(sprintf("file-%s-%s.%s", $title, CommonHelper::GenerateRandomString(), CommonHelper::GetExtension($file)));
	}

	private static function _generateFileFullPath($file, $thumb = null) {
		return sprintf("%sMedia/Media/%s/%s", CDN_PATH, is_null($thumb) ? "" : $thumb . "/", $file);
	}

}