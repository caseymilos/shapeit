<?php

use Business\Helper\ImageResizeHelper;

class MaintenanceController extends MVCController {

    public function GetRegenerateThumbnails() {

        $mediaRoot = sprintf("%sMedia", CDN_PATH);

        // preparing settings
        // in settings we need to define the directories
        // where we take the original images from
        // and directories and sizes where we put thumbnails
        $settings = [
            [
                "directory" => sprintf("%s/Events/", $mediaRoot),
                "targetDirectory" => sprintf("%s/Events/Header/", $mediaRoot),
                "width" => 1920,
                "height" => 600,
                "jpeg" => true,
                "quality" => 75
            ],
            [
                "directory" => sprintf("%s/Events/", $mediaRoot),
                "targetDirectory" => sprintf("%s/Events/Medium/", $mediaRoot),
                "width" => 400,
                "height" => 400,
                "jpeg" => true,
                "quality" => 75
            ],
            [
                "directory" => sprintf("%s/Events/", $mediaRoot),
                "targetDirectory" => sprintf("%s/Events/Small/", $mediaRoot),
                "width" => 100,
                "height" => 100,
                "jpeg" => true,
                "quality" => 75
            ],
            [
                "directory" => sprintf("%s/Pages/", $mediaRoot),
                "targetDirectory" => sprintf("%s/Pages/Header/", $mediaRoot),
                "width" => 1920,
                "height" => 600,
                "jpeg" => true,
                "quality" => 75
            ],
            [
                "directory" => sprintf("%s/Pages/", $mediaRoot),
                "targetDirectory" => sprintf("%s/Pages/Medium/", $mediaRoot),
                "width" => 400,
                "height" => 400,
                "jpeg" => true,
                "quality" => 75
            ],
            [
                "directory" => sprintf("%s/Pages/", $mediaRoot),
                "targetDirectory" => sprintf("%s/Pages/Small/", $mediaRoot),
                "width" => 100,
                "height" => 100,
                "jpeg" => true,
                "quality" => 75
            ],
            [
                "directory" => sprintf("%s/Pages/", $mediaRoot),
                "targetDirectory" => sprintf("%s/Pages/Related/", $mediaRoot),
                "width" => 370,
                "height" => 230,
                "jpeg" => true,
                "quality" => 75
            ],
            [
                "directory" => sprintf("%s/Partners/", $mediaRoot),
                "targetDirectory" => sprintf("%s/Partners/Medium/", $mediaRoot),
                "width" => 500,
                "height" => 500,
                "jpeg" => true,
                "quality" => null
            ],
            [
                "directory" => sprintf("%s/Partners/", $mediaRoot),
                "targetDirectory" => sprintf("%s/Partners/Small/", $mediaRoot),
                "width" => 100,
                "height" => 100,
                "jpeg" => true,
                "quality" => null
            ],
            [
                "directory" => sprintf("%s/Slides/", $mediaRoot),
                "targetDirectory" => sprintf("%s/Slides/Header/", $mediaRoot),
                "width" => 1920,
                "height" => 600,
                "jpeg" => true,
                "quality" => 75
            ],
            [
                "directory" => sprintf("%s/Slides/", $mediaRoot),
                "targetDirectory" => sprintf("%s/Slides/Medium/", $mediaRoot),
                "width" => 500,
                "height" => 500,
                "jpeg" => true,
                "quality" => null
            ],
            [
                "directory" => sprintf("%s/Slides/", $mediaRoot),
                "targetDirectory" => sprintf("%s/Slides/Small/", $mediaRoot),
                "width" => 100,
                "height" => 100,
                "jpeg" => true,
                "quality" => null
            ],
            [
                "directory" => sprintf("%s/Users/", $mediaRoot),
                "targetDirectory" => sprintf("%s/Users/Medium/", $mediaRoot),
                "width" => 500,
                "height" => 500,
                "jpeg" => true,
                "quality" => null
            ],
            [
                "directory" => sprintf("%s/Users/", $mediaRoot),
                "targetDirectory" => sprintf("%s/Users/Small/", $mediaRoot),
                "width" => 100,
                "height" => 100,
                "jpeg" => true,
                "quality" => null
            ],
        ];

        $model = [];

        foreach ($settings as $setting) {
            // get all original images from folder
            $iterator = new DirectoryIterator($setting['directory']);
            foreach ($iterator as $file) {
                // skipping the dot
                if ($file->isDot()) continue;
                if ($this->_isImage($file->getPathName())) {
                    $model[] = (object)[
                        "Image" => $file->getFilename(),
                        "Settings" => $setting
                    ];
                }
            }

            //MaintenanceApiController::RegenerateThumbnails($setting['directory'], $setting['targetDirectory'], $setting['width'], $setting['height']);
        }

        $this->RenderView(null, ['model' => $model]);
    }

    public function PostRegenerateOneThumb($originalImage, $options) {
        $imageSrc = str_replace(["//", "\\"], "/", $options["directory"]) . $originalImage;


        if ($options['jpeg']) {
            $targetDir = str_replace(["//", "\\"], "/", $options["targetDirectory"]) . $this->_replaceExtension($originalImage, "jpg");
        }
        else {
            $targetDir = str_replace(["//", "\\"], "/", $options["targetDirectory"]) . $originalImage;
        }

        ImageResizeHelper::ResizeFitImage($options["width"], $options["height"], $imageSrc, $targetDir, $options['quality'], $options["jpeg"] ? "jpeg" : null);

        $response = (object)[
            "Original" => $originalImage,
            "Thumb" => $options['targetDirectory'],
            "Successful" => true,
            "Width" => $options["width"],
            "Height" => $options["height"]
        ];
        echo json_encode($response);
    }

    private function _isImage($path) {
        if (@is_array(getimagesize($path))) {
            return true;
        }
        else {
            return false;
        }
    }


    // Helpers

    private function _replaceExtension($filename, $new_extension) {
        $info = pathinfo($filename);
        return $info['filename'] . '.' . $new_extension;
    }

}