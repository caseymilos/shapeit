<?php
use Business\ApiControllers\InvitationsApiController;
use Business\DTO\CurrentUserDTO;
use Business\DTO\RecipientDTO;
use Business\DTO\SenderDTO;
use Business\Enums\PermissionsEnum;
use Business\Enums\UserStatusTypesEnum;
use Business\Helper\ImageResizeHelper;
use Business\Helper\NotificationHelper;
use Business\Security\Crypt;
use Gdev\UserManagement\ApiControllers\ConfirmationLinksApiController;
use Gdev\UserManagement\ApiControllers\RolePermissionsApiController;
use Gdev\UserManagement\ApiControllers\RolesApiController;
use Gdev\UserManagement\ApiControllers\UserRolesApiController;
use Gdev\UserManagement\ApiControllers\UsersApiController;
use Gdev\UserManagement\ApiControllers\UserStatusesApiController;
use Gdev\UserManagement\Models\ConfirmationLink;
use Gdev\UserManagement\Models\User;
use Gdev\UserManagement\Models\UserDetails;
use Gdev\UserManagement\Models\UserRole;
use Gdev\UserManagement\Models\UserStatus;

/**
 * Created by PhpStorm.
 * User: nikola
 * Date: 13.6.16.
 * Time: 18.24
 */
class UsersController extends MVCController {

    public function GetList($pageNumber = 1, $permissions = [PermissionsEnum::ViewUsers]) {

        $model = new UsersViewModel();

        $limit = Config::BoxesLimit;
        $offset = ($pageNumber - 1) * $limit;

        $users = UsersApiController::GetActiveUsers();

        $model->TotalUsers = count($users);
        $model->Users = UsersApiController::GetActiveUsers($offset, $limit);
        $model->TotalPages = ceil($model->TotalUsers / $limit);
        $model->CurrentPage = $pageNumber;
        $model->UsersPerPage = $limit;

        $this->RenderView("Users/List", ['model' => $model]);
    }

    public function GetBlockedUsers() {

        $model = new UsersViewModel();

        $model->Users = UsersApiController::GetUsersByCurrentStatus(UserStatusTypesEnum::Blocked);

        $this->RenderView("Users/List", ['model' => $model]);
    }

    public function GetNotApprovedUsersList($permissions = [PermissionsEnum::ViewUsers]) {

        $model = new UsersViewModel();

        $model->Users = UsersApiController::GetNotApprovedUsers();

        $this->RenderView("Users/NotApprovedUsers", ['model' => $model]);
    }

    public function GetCreate($permissions = [PermissionsEnum::CreateUser]) {
        $this->RenderView();
    }

    public function PostCreate($username, $password, $email, $firstName, $lastName, $userRoles, $approved = null, $active = null, $dateOfBirth = null, $userStatusId = null, $statusMessage = null, $permissions = [PermissionsEnum::CreateUser]) {

        $user = new User();

        $user->Email = $email;
        $user->Password = Crypt::HashPassword($password);
        $user->UserName = $username;
        $user->RegistrationDate = new DateTime();

        if ($approved == "1") {
            $user->Approved = 1;
        }
        else {
            $user->Approved = null;
        }

        if ($active == "1") {
            $user->Active = 1;
        }
        else {
            $user->Active = null;
        }

        $userId = UsersApiController::InsertUser($user);

        $userDetails = new UserDetails();

        $userDetails->FirstName = $firstName;
        $userDetails->LastName = $lastName;

        if ($dateOfBirth) {
            $userDetails->DateOfBirth = new DateTime($dateOfBirth);
        }
        else {
            $userDetails->DateOfBirth = null;
        }
        $userDetails->UserId = $userId;

        UsersApiController::InsertUserDetails($userDetails);

        if ($userRoles != null) {
            foreach ($userRoles as $userRole) {
                $userRoleModel = new UserRole();
                $userRoleModel->UserId = $userId;
                $userRoleModel->RoleId = intval($userRole);
                UserRolesApiController::InsertUserRole($userRoleModel);
            }
        }

        if ($userStatusId != null) {
            $userStatusModel = new UserStatus();
            $userStatusModel->UserId = $userId;
            $userStatusModel->UserStatusTypeId = intval($userStatusId);
            $userStatusModel->DateFrom = new DateTime();

            if ($statusMessage != null) {
                $userStatusModel->Message = $statusMessage;
            }
            else {
                $userStatusModel->Message = "No Message";
            }

            UserStatusesApiController::InsertUserStatus($userStatusModel);
        }


        Router::Redirect("user-list");

    }

    public function GetUpdateDetails($username, $permissions = [PermissionsEnum::EditUser]) {

        $model = new UserViewModel();

        $model->User = UsersApiController::GetUserByUserName($username);

        $this->RenderView("Users/Update", ['user' => $model->User]);

    }

    public function GetUpdateLoggedUserDetails() {

        $currentUser = Security::GetCurrentUser();

        $model = new UserViewModel();
        $model->User = UsersApiController::GetUserByUserName($currentUser->Username);

        $this->RenderView("Users/UpdateLoggedUser", ['user' => $model->User]);

    }

    public function PostUpdateDetails($username, $password, $email, $firstName, $lastName, $approved = null, $active = null, $userRoles = null, $dateOfBirth = null, $userStatusId = null, $statusMessage = null, $permissions = [PermissionsEnum::EditUser]) {

        $user = UsersApiController::GetUserByUserName($username);

        $user->Email = $email;

        if ($password != null) {
            $user->Password = Crypt::HashPassword($password);
        }

        if ($approved == "1") {
            $user->Approved = 1;
        }
        else {
            $user->Approved = null;
        }

        if ($active == "1") {
            $user->Active = 1;
        }
        else {
            $user->Active = null;
        }

        UsersApiController::InsertUser($user);

        $userDetails = $user->Details;

        $userDetails->LastName = $lastName;
        $userDetails->FirstName = $firstName;

        if ($dateOfBirth) {
            $userDetails->DateOfBirth = new DateTime($dateOfBirth);
        }
        else {
            $userDetails->DateOfBirth = null;
        }

        UsersApiController::InsertUserDetails($userDetails);

        $oldRoles = UserRolesApiController::GetUserRoles($user->UserId);
        $oldRoleTypes = [];

        if ($userRoles != null) {
            if ($oldRoles != null) {
                foreach ($oldRoles as $oldRole) {
                    $oldRoleTypes[] = $oldRole->PermissionId;
                    if (!in_array($oldRole->RoleId, $userRoles, false)) {
                        UserRolesApiController::DeleteUserRoles($oldRole->UserRoleId);
                    }
                }
            }

            foreach ($userRoles as $userRoleId) {
                $userRoleModel = new UserRole();
                $userRoleModel->UserId = $user->UserId;
                $userRoleModel->RoleId = $userRoleId;
                if (count($oldRoles) > 0) {
                    if (!in_array($userRoleId, $oldRoleTypes, false)) {
                        UserRolesApiController::InsertUserRole($userRoleModel);
                    }
                }
                else {
                    UserRolesApiController::InsertUserRole($userRoleModel);
                }
            }
        }
        else {
            foreach ($oldRoles as $oldRole) {
                UserRolesApiController::DeleteUserRoles($oldRole->UserRoleId);
            }
        }

        if ($userStatusId != "Select User Status") {

            $currentUserStatus = UserStatusesApiController::GetCurrentUserStatus($user->UserId);
            $currentUserStatus->DateTo = new DateTime();
            UserStatusesApiController::UpdateUserStatus($currentUserStatus);

            $userStatusModel = new UserStatus();
            $userStatusModel->UserId = $user->UserId;
            $userStatusModel->UserStatusTypeId = intval($userStatusId);
            $userStatusModel->DateFrom = new DateTime();

            if ($statusMessage != null) {
                $userStatusModel->Message = $statusMessage;
            }
            else {
                $userStatusModel->Message = "No Message";
            }

            UserStatusesApiController::InsertUserStatus($userStatusModel);
        }

        //change user info if he/she is logged in
        $currentUser = Security::GetCurrentUser();

        if ($currentUser->UserId == $user->UserId) {

            $user = UsersApiController::GetUserById($currentUser->UserId);
            // Packing CurrentUserDTO from user

            $currentUser->Username = $user->UserName;
            $currentUser->Email = $user->Email;

            //$userDto->Picture = $userDetails->PictureSource();
            $currentUser->FirstName = $user->Details->FirstName;
            $currentUser->LastName = $user->Details->LastName;

            $userRoles = $user->Roles;

            $rolePermissions = [];

            foreach ($userRoles as $userRole) {
                $oneRolePermissions = $userRole->Permissions;
                foreach ($oneRolePermissions as $permission) {
                    $rolePermissions[] = $permission->PermissionId;
                }
            }

            $currentUser->Permissions = array_unique($rolePermissions);

            Security::SetCurrentUser($currentUser);
        }

        Router::Redirect("user-update-details", ["username" => $username]);

    }

    public function PostUpdateLoggedUserDetails($password, $email, $firstName, $lastName, $dateOfBirth = null) {

        $currentUser = Security::GetCurrentUser();
        $user = UsersApiController::GetUserByUserName($currentUser->Username);

        $user->Email = $email;

        if ($password != null) {
            $user->Password = Crypt::HashPassword($password);
        }

        UsersApiController::InsertUser($user);

        $userDetails = $user->Details;

        $userDetails->LastName = $lastName;
        $userDetails->FirstName = $firstName;

        if ($dateOfBirth) {
            $userDetails->DateOfBirth = new DateTime($dateOfBirth);
        }
        else {
            $userDetails->DateOfBirth = null;
        }

        UsersApiController::InsertUserDetails($userDetails);

		$user = UsersApiController::GetUserById($currentUser->UserId);
		// Packing CurrentUserDTO from user

		$currentUser->Username = $user->UserName;
		$currentUser->Email = $user->Email;

		//$userDto->Picture = $userDetails->PictureSource();
		$currentUser->FirstName = $user->Details->FirstName;
		$currentUser->LastName = $user->Details->LastName;

		$userRoles = $user->Roles;

		$rolePermissions = [];

		foreach ($userRoles as $userRole) {
			$oneRolePermissions = $userRole->Permissions;
			foreach ($oneRolePermissions as $permission) {
				$rolePermissions[] = $permission->PermissionId;
			}
		}

		$currentUser->Permissions = array_unique($rolePermissions);

		Security::SetCurrentUser($currentUser);


        Router::Redirect("logged-user-update-details");

    }

    public function PostUpdatePicture($picture, $permissions = [PermissionsEnum::AccessSystem]) {

        if ($picture['name'] !== "") {

            $currentUser = Security::GetCurrentUser();

            $user = UsersApiController::GetUserById($currentUser->UserId);
            $userDetails = $user->Details;

            if (!empty($userDetails->Picture)) {
                if (is_readable($userDetails->PicturePath("Small"))) {
                    unlink($userDetails->PicturePath("Small"));
                }
                if (is_readable($userDetails->PicturePath("Medium"))) {
                    unlink($userDetails->PicturePath("Medium"));
                }
                if (is_readable($userDetails->PicturePath())) {
                    unlink($userDetails->PicturePath());
                }
            }

            $newName = self::_generatePictureName($picture['name'], $userDetails->FirstName, $userDetails->LastName);
            $imagePath = self::_generatePictureFullPath($newName);
            move_uploaded_file($picture['tmp_name'], $imagePath);
            ImageResizeHelper::ResizeCropImage(100, 100, $imagePath, self::_generatePictureFullPath($newName, "Small"));
            ImageResizeHelper::ResizeCropImage(500, 500, $imagePath, self::_generatePictureFullPath($newName, "Medium"));
            $userDetails->Picture = $newName;

            UsersApiController::InsertUserDetails($userDetails);

            // update current user
            $currentUser->Picture = $userDetails->PictureSource("Medium");
            Security::SetCurrentUser($currentUser);
        }

        Router::Redirect("home");

    }

    public function GetDelete($username, $permissions = [PermissionsEnum::DeleteUser]) {
        $user = UsersApiController::GetUserByUserName($username);
        UsersApiController::DeleteUser($user->UserId);

        Router::Redirect("user-list");
    }

    public function GetUpdateCurrentUserDetails($permissions = [PermissionsEnum::AccessSystem]) {
        $this->RenderView("Home/Index");
    }

    public function PostUpdateCurrentUserDetails($firstName = null, $lastName = null, $email = null, $permissions = [PermissionsEnum::AccessSystem]) {

        $currentUser = Security::GetCurrentUser();

        $user = UsersApiController::GetUserById($currentUser->UserId);
        $userDetails = $user->Details;

        if ($email != null) {
            $user->Email = $email;
            $currentUser->Email = $email;
        }

        UsersApiController::InsertUser($user);

        if ($firstName != null) {
            $userDetails->FirstName = $firstName;
            $currentUser->FirstName = $firstName;
        }
        if ($lastName != null) {
            $userDetails->LastName = $lastName;
            $currentUser->LastName = $lastName;
        }

        UsersApiController::InsertUserDetails($userDetails);

        Security::SetCurrentUser($currentUser);

        $model = new PasswordOrInfoChangedViewModel();
        $model->Changed = true;
        $this->RenderView("Home/Index", ["model" => $model]);
    }

    public function GetRegister() {
        $this->RenderView("Security/Register");
    }

    public function PostRegister($username, $email, $password, $confirmPassword, $firstName, $lastName, $genderId = null, $dateOfBirth = null, $token = null) {

        if ($token != null) {
            $invitation = InvitationsApiController::GetInvitationByToken($token);
        }

        $findUserByEmail = UsersApiController::GetUserByEmail($email);

        //insert user
        $user = new User();

        if ($findUserByEmail == false) {
            $user->Email = $email;
        }
        else {
            $errors[] = "Email is already in use, please try again";
            $this->RenderView("Security/Register", ["errors" => $errors]);
            return;
        }

        $findUserByUsername = UsersApiController::GetUserByUserName($username);

        if ($findUserByUsername == false) {
            $user->UserName = $username;
        }
        else {
            $errors[] = "Username is already in use, please try again";
            $this->RenderView("Security/Register", ["errors" => $errors]);
            return;
        }

        $user->RegistrationDate = new DateTime();

        if ($token != null) {
            $user->Approved = 1;
        }

        if ($password == $confirmPassword) {
            $user->Password = Crypt::HashPassword($password);;
        }
        else {
            $errors[] = "Passwords do not match, please try again!";
            $this->RenderView("Security/Register", ["errors" => $errors]);
            return;
        }

        $userId = UsersApiController::InsertUser($user);

        //insert user details
        $userDetails = new UserDetails();

        $userDetails->LastName = $lastName;
        $userDetails->FirstName = $firstName;
        if ($dateOfBirth) {
            $userDetails->DateOfBirth = new DateTime($dateOfBirth);
        }
        else {
            $dateOfBirth = null;
        }

        if ($genderId != null) {
            $userDetails->Gender = $genderId;
        }

        $userDetails->UserId = $userId;

        UsersApiController::InsertUserDetails($userDetails);

        if ($token != null) {
            $roles = $invitation->Roles;

            //insert user roles
            if ($roles != null) {
                foreach ($roles as $role) {
                    $userRoleModel = new UserRole();
                    $userRoleModel->UserId = $userId;
                    $userRoleModel->RoleId = intval($role->RoleId);
                    UserRolesApiController::InsertUserRole($userRoleModel);
                }
            }

            // Updating invitation
            $invitation->RegisteredUserId = $userId;
            InvitationsApiController::SaveInvitation($invitation);

        }
        else {
            $userRoleModel = new UserRole();
            $userRoleModel->UserId = $userId;
            $userRoleModel->RoleId = 1;                    //Todo; hardcoded roleId for Administrator
            UserRolesApiController::InsertUserRole($userRoleModel);
        }

        //insert user "Active" status
        $userStatus = new UserStatus();

        $userStatus->DateFrom = new DateTime();
        $userStatus->UserId = $userId;

        if ($token != null) {
            $userStatus->Message = "Registration from Invitation";
            $userStatus->UserStatusTypeId = UserStatusTypesEnum::Active;
        }
        else {
            $userStatus->Message = "Requested Registration";
            $userStatus->UserStatusTypeId = UserStatusTypesEnum::Pending;
        }

        UserStatusesApiController::InsertUserStatus($userStatus);

        $confirmationLink = $this->createConfirmationLink($userId);

        $mailModel = new UserRegisterEmailViewModel();

        $mailModel->Name = $firstName;
        $mailModel->ConfirmationLink = $confirmationLink->Token;
        $mailModel->ExpirationDate = $confirmationLink->ExpirationDate;

        $config = \Business\Utilities\Config\Config::GetInstance();
        NotificationHelper::Send(
            "Welcome to Admin!",
            $this->RenderView("Mail/UserRegistration", ["model" => $mailModel], true),
            "Your account for Admin website has been created",
            [new SenderDTO("Admin", $config->smtp->from)],
            [new RecipientDTO(sprintf("%s %s", $userDetails->FirstName, $userDetails->LastName), $user->Email)]
        );

        Router::Redirect("registration-link-sent");
    }

    public function GetRegistrationLinkSent() {
        $this->RenderView("Security/RegistrationLinkSent");
    }

    public function GetRegistrationSuccessful() {
        $this->RenderView("Security/RegistrationSuccessful");
    }

    public function GetRegistrationUnsuccessful() {
        $this->RenderView("Security/RegistrationUnsuccessful");
    }

    public function GetRegistrationConfirmed($token) {
        $confirmationLink = ConfirmationLinksApiController::GetConfirmationLink($token);

        $now = new DateTime();
        if (!empty($confirmationLink)) {
            if ($now <= $confirmationLink->ExpirationDate) {
                $user = UsersApiController::GetUserById($confirmationLink->UserId);

                $user->Active = 1;

                UsersApiController::InsertUser($user);

                $userDto = new CurrentUserDTO();

                $userDto->Email = $user->Email;
                $userDto->UserId = $user->UserId;
                $userDto->Username = $user->UserName;

                $userDto->Picture = $user->Details->PictureSource("Medium");
                $userDto->FirstName = $user->Details->FirstName;
                $userDto->LastName = $user->Details->LastName;

                $userRoles = $user->Roles;

                $rolePermissions = [];

                foreach ($userRoles as $userRole) {
                    $oneRolePermissions = $userRole->Permissions; // RolePermissionsApiController::GetRolePermissions($userRole->RoleId);
                    foreach ($oneRolePermissions as $permission) {
                        $rolePermissions[] = $permission->PermissionId;
                    }
                }

                $userDto->Permissions = array_unique($rolePermissions);

                Security::SetCurrentUser($userDto);

                Router::Redirect("registration-successful");

            }
            else {
                Router::Redirect("registration-unsuccessful");
            }
        }
        else {
            Router::Redirect("login");
        }

    }

    /**
     * @param $userId
     * @return ConfirmationLink
     * @throws Exception
     */
    private function createConfirmationLink($userId) {
        $confirmationLink = new ConfirmationLink();
        $confirmationLink->UserId = $userId;
        $confirmationLink->Token = bin2hex(openssl_random_pseudo_bytes(8));

        $date = new DateTime();
        $date->modify('+30 days');
        $confirmationLink->ExpirationDate = $date;

        ConfirmationLinksApiController::InsertConfirmationLink($confirmationLink);

        return $confirmationLink;
    }

    // Private methods

    private static function _generatePictureName($logo, $name, $lastName) {
        return CommonHelper::StringToFilename(sprintf("user-%s-%s-%s.%s", $name, $lastName, CommonHelper::GenerateRandomString(), CommonHelper::GetExtension($logo)));
    }

    private static function _generatePictureFullPath($pictureName, $thumb = null) {
        return sprintf("%sMedia/Users/%s/%s", CDN_PATH, is_null($thumb) ? "" : $thumb . "/", $pictureName);
    }

}