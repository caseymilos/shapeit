<?php
use Data\Database\Database;

/**
 * Created by PhpStorm.
 * User: nikola
 * Date: 19.5.16.
 * Time: 21.24
 */
class InstallController extends MVCController {

    public function GetInstall() {
        $this->RenderView("Install/Install");
    }

    public function PostInstall($database_dbname, $database_host, $database_user, $database_pass, $database_port,
                                $portal_url, $portal_theme) {

        $config = (object)[
            "db" => (object)[
                "default" => (object)[
                    "type" => "mysql",
                    "host" => $database_host,
                    "user" => $database_user,
                    "pass" => $database_pass,
                    "port" => $database_port,
                    "dbname" => $database_dbname,
                    "driver" => "pdo_mysql"
                ]
            ],
            "portal" => (object)[
                "url" => $portal_url,
                "theme" => $portal_theme
            ]
        ];

        file_put_contents(ROOT_PATH . "/config/config.json", json_encode($config, JSON_PRETTY_PRINT));

        Router::Redirect("install-database");

    }

    public function GetInstallSuccessful() {
        $this->RenderView();
    }


    public function GetDatabase() {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        Database::install();
        // Router::Redirect("install-success");
    }

}