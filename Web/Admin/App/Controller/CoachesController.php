<?php
use Business\ApiControllers\CoachesApiController;
use Business\Enums\PermissionsEnum;
use Business\Helper\ImageResizeHelper;
use Data\Models\Coach;
use Data\Models\CoachDetails;


class CoachesController extends MVCController
{
    public function GetList()
    {
        $model = new CoachesViewModel();
        $model->Coaches = CoachesApiController::GetCoaches();

        $this->RenderView(null, ['model' => $model]);
    }

    public function GetCreate()
    {
        $this->RenderView();
    }

    public function PostCreate($firstname, $lastname, $slug, $biography, $categories = [], $picture = null, $video = null, $dateOfBirth = null)
    {

        $coach = new Coach();
        $coach->FirstName = $firstname;
        $coach->LastName = $lastname;

        if ($video != null) {
            if ($video['name'] !== "") {
                $newName = self::_generateVideoName($firstname, $lastname, $video["name"]);
                $imagePath = self::_generateVideoFullPath($newName);
                move_uploaded_file($video['tmp_name'], $imagePath);
                $coach->Video = $newName;
            }
        }

        if ($picture != null) {
            if ($picture['name'] !== "") {
                $newName = self::_generatePictureName($firstname, $lastname);
                $imagePath = self::_generatePictureFullPath($newName);
                move_uploaded_file($picture['tmp_name'], $imagePath);
                ImageResizeHelper::ResizeFitImage(100, 100, $imagePath, self::_generatePictureFullPath($newName, "Small"), null, 'jpeg');
                ImageResizeHelper::ResizeFitImage(500, 500, $imagePath, self::_generatePictureFullPath($newName, "Medium"), null, 'jpeg');
                $coach->Picture = $newName;
            }
        }

        $coach->DateOfBirth = new DateTime($dateOfBirth);

        $coachId = CoachesApiController::SaveCoach($coach);

        $coachDetails = new CoachDetails();
        $coachDetails->Biography = $biography;
        $coachDetails->CoachId = $coachId;
        $coachDetails->Slug = $slug;
        $coachDetails->LanguageId = MainHelper::GetLanguage();


        CoachesApiController::SaveCoachDetails($coachDetails);


        // Handle categories
        if (count($categories) > 0) {
            foreach ($categories as $categoryId) {
                $cg = new \Data\Models\CoachCategory();
                $cg->CategoryId = $categoryId;
                $cg->CoachId = $coachId;

                CoachesApiController::SaveCategory($cg);
            }
        }

        Router::Redirect("coaches-list");

    }

    public function GetUpdate($id)
    {

        $model = new CoachViewModel();
        $model->Coach = CoachesApiController::GetCoach($id);
        $model->Details = $model->Coach->GetDetails(MainHelper::GetLanguage());

        $this->RenderView(null, ['model' => $model]);
    }

    public function PostUpdate($id, $firstname, $lastname, $slug, $biography, $categories = [], $picture = null, $video = null, $dateOfBirth = null)
    {
        $coach = CoachesApiController::GetCoach($id);
        $coach->FirstName = $firstname;
        $coach->LastName = $lastname;

        if ($video != null) {
            if ($video['name'] !== "") {
                $newName = self::_generateVideoName($firstname, $lastname, $video["name"]);
                $imagePath = self::_generateVideoFullPath($newName);
                $result = move_uploaded_file($video['tmp_name'], $imagePath);
                $coach->Video = $newName;
            }
        }

        if ($picture != null) {
            if ($picture['name'] !== "") {
                if (!empty($coach->Picture)) {
                    if (is_readable($coach->PicturePath("Small"))) {
                        unlink($coach->PicturePath("Small"));
                    }
                    if (is_readable($coach->PicturePath("Medium"))) {
                        unlink($coach->PicturePath("Medium"));
                    }
                    if (is_readable($coach->PicturePath())) {
                        unlink($coach->PicturePath());
                    }
                }
                $newName = self::_generatePictureName($firstname, $lastname);
                $imagePath = self::_generatePictureFullPath($newName);
                move_uploaded_file($picture['tmp_name'], $imagePath);
                ImageResizeHelper::ResizeFitImage(100, 100, $imagePath, self::_generatePictureFullPath($newName, "Small"), null, 'jpeg');
                ImageResizeHelper::ResizeFitImage(500, 500, $imagePath, self::_generatePictureFullPath($newName, "Medium"), null, 'jpeg');
                $coach->Picture = $newName;


            }
        }

        $coach->DateOfBirth = new DateTime($dateOfBirth);

        CoachesApiController::SaveCoach($coach);

        $details = $coach->GetDetails(MainHelper::GetLanguage());
        if (is_null($details)) {
            $details = new CoachDetails();
            $details->LanguageId = MainHelper::GetLanguage();
        }

        $details->Biography = $biography;
        $details->Slug = $slug;


        CoachesApiController::SaveCoachDetails($details);


        // Handle categories
        $old = [];
        if (count($coach->Categories) > 0) {
            foreach ($coach->Categories as $oldCategory) {
                if (!in_array($oldCategory->CategoryId, $categories)) {
                    CoachesApiController::DeleteCoachCategory($id, $oldCategory->CategoryId);
                }
                $old[] = $oldCategory->CategoryId;
            }
        }
        if (count($categories) > 0) {
            foreach ($categories as $categoryId) {
                if (!in_array($categoryId, $old)) {
                    $cg = new \Data\Models\CoachCategory();
                    $cg->CategoryId = $categoryId;
                    $cg->CoachId = $id;

                    CoachesApiController::SaveCategory($cg);
                }
            }
        }

        Router::Redirect("coaches-list");
    }

    public function GetDelete($id)
    {
        $coach = CoachesApiController::GetCoach($id);
        if (!empty($coach->Picture)) {
            if (is_readable($coach->PicturePath("Small"))) {
                unlink($coach->PicturePath("Small"));
            }
            if (is_readable($coach->PicturePath("Medium"))) {
                unlink($coach->PicturePath("Medium"));
            }
            if (is_readable($coach->PicturePath())) {
                unlink($coach->PicturePath());
            }
        }

        CoachesApiController::DeleteCoach($id);
        Router::Redirect("coaches-list");
    }

    public function GetThumbnailCrop($id, $size)
    {
        $model = CoachesApiController::GetCoach($id);

        switch ($size) {
            case "medium":
                $dimensions = [434, 500, 434, 500];
                break;
            default:
                throw new Exception("Unknown image size");
        }

        $this->RenderView(null, ["model" => $model, "size" => $size, "dimensions" => $dimensions]);
    }

    public function PostThumbnailCrop($id, $size, $points)
    {
        $coach = CoachesApiController::GetCoach($id);
        $picture = new \abeautifulsite\SimpleImage($coach->PicturePath());
        $picture->crop($points[0], $points[1], $points[2], $points[3])->thumbnail(434, 500)->save($coach->PicturePath("Medium"), 75, "jpeg");
        // clear cloudflare cache
        $this->_clearCloudFlareCache($coach);
    }


    private static function _generatePictureName($firstName, $lastName)
    {
        return CommonHelper::StringToFilename(sprintf("coach-%s-%s-%s.%s", $firstName, $lastName, CommonHelper::GenerateRandomString(), "jpg"));
    }

    private static function _generatePictureFullPath($pictureName, $thumb = null)
    {
        return sprintf("%sMedia/Coaches/%s/%s", CDN_PATH, is_null($thumb) ? "" : $thumb . "/", $pictureName);
    }

    private static function _generateVideoName($firstName, $lastName, $fileName)
    {
        return CommonHelper::StringToFilename(sprintf("coach-%s-%s-%s.%s", $firstName, $lastName, CommonHelper::GenerateRandomString(), CommonHelper::GetExtension($fileName)));
    }

    private static function _generateVideoFullPath($pictureName)
    {
        return sprintf("%sMedia/Coaches/Videos/%s", CDN_PATH, $pictureName);
    }

    private function _clearCloudFlareCache(Coach $coach)
    {
        // clear cloudflare cache
        $cloudFlare = new \Business\Helper\CloudFlareHelper();
        $cloudFlare->PurgeFiles([
            $coach->PictureSource(),
            $coach->PictureSource("Small"),
            $coach->PictureSource("Medium")
        ]);
    }

}