<?php
use Business\ApiControllers\SponsorsApiController;
use Business\Enums\PermissionsEnum;
use Data\Models\SponsorGroup;
use Data\Models\SponsorGroupDetails;

class SponsorGroupsController extends MVCController
{

    public function GetList()
    {
        $model = new SponsorGroupsViewModel();
        $model->SponsorGroups = SponsorsApiController::GetSponsorGroups();

        $this->RenderView(null, ['model' => $model]);
    }

    public function GetCreate()
    {
        $this->RenderView();
    }

    public function PostCreate($name)
    {

        $sponsorGroup = new SponsorGroup();
        $sponsorGroupId = SponsorsApiController::SaveSponsorGroup($sponsorGroup);

        $sponsorGroupDetails = new SponsorGroupDetails();

        $sponsorGroupDetails->SponsorGroupId = $sponsorGroupId;
        $sponsorGroupDetails->Name = $name;
        $sponsorGroupDetails->LanguageId = MainHelper::GetLanguage();

        $sponsorGroupDetailsId = SponsorsApiController::SaveSponsorGroupDetails($sponsorGroupDetails);


        Router::Redirect("sponsorGroups-list");
    }

    public function GetUpdate($id)
    {

        $model = new SponsorGroupViewModel();
        $model->SponsorGroup = SponsorsApiController::GetSponsorGroup($id);
        $model->GroupDetails = $model->SponsorGroup->GetDetails(MainHelper::GetLanguage());

        $this->RenderView(null, ['model' => $model]);
    }

    public function PostUpdate($id, $name)
    {
        $sponsorGroup = SponsorsApiController::GetSponsorGroup($id);
        SponsorsApiController::SaveSponsorGroup($sponsorGroup);

        $sponsorGroupDetails = SponsorsApiController::GetSponsorGroupDetails($id, MainHelper::GetLanguage());
        if (!$sponsorGroupDetails) {
            $sponsorGroupDetails = new SponsorGroupDetails();
            $sponsorGroupDetails->LanguageId = MainHelper::GetLanguage();
            $sponsorGroupDetails->SponsorGroupId = $id;
        }

        $sponsorGroupDetails->Name = $name;

        SponsorsApiController::SaveSponsorGroupDetails($sponsorGroupDetails);
        Router::Redirect("sponsorGroups-list");
    }

    public function GetDelete($id)
    {
        SponsorsApiController::DeleteSponsorGroup($id);
        Router::Redirect("sponsorGroups-list");
    }


}