<?php

use Business\ApiControllers\BookingsApiController;
use Business\Helper\ImageResizeHelper;
use Data\Models\Booking;

class BookingsController extends MVCController {

	public function GetBookings() {

		$model = new BookingsViewModel();
		$model->Bookings = BookingsApiController::GetBookings();
		$this->RenderView(null, ['model' => $model]);
	}

}