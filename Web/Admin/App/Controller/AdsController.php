<?php

use Business\ApiControllers\AdsApiController;
use Business\Helper\ImageResizeHelper;
use Data\Models\Ad;

class AdsController extends MVCController {

	public function GetAds() {
		$model = new AdsViewModel();
		$model->Ads = AdsApiController::GetAds();
		$this->RenderView(null, ['model' => $model]);
	}

	/**
	 * @param $id
	 */
	public function GetAd($id) {
		$model = new AdViewModel();
		$model->Ad = AdsApiController::GetAd($id);
	}

	public function GetCreate() {
		$this->RenderView();
	}

	public function GetDelete($id) {
		AdsApiController::DeleteAd($id);
		Router::Redirect("ad-list");
	}

	public function PostCreate($title, $code, $adPositionId) {
		$model = new Ad();

		$model->Title = $title;
		$model->Code = $code;
		$model->Position = $adPositionId;

		$adId = AdsApiController::SaveAd($model);

		Router::Redirect("ad-list");
	}

	public function GetUpdateAd($adId) {
		$model = AdsApiController::GetAd($adId);
		$this->RenderView(null, ['model' => $model]);
	}

	public function PostUpdateAd($adId, $title, $code, $adPositionId) {

		$model = AdsApiController::GetAd($adId);
		$model->Title = $title;
		$model->Code = $code;
		$model->Position = $adPositionId;

		AdsApiController::SaveAd($model);

		Router::Redirect("ad-list");
	}
}