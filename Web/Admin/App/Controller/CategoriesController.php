<?php
use Business\ApiControllers\CategoriesApiController;
use Business\Enums\PermissionsEnum;
use Business\Helper\ImageResizeHelper;
use Data\Models\Category;
use Data\Models\CategoryDetails;

class CategoriesController extends MVCController
{

    public function GetList()
    {
        $model = new CategoriesViewModel();
        $model->Categories = CategoriesApiController::GetCategories();

        $this->RenderView(null, ['model' => $model]);
    }

    public function GetCreate()
    {
        $this->RenderView();
    }

    public function PostCreate($name, $subtitle, $picture = null, $video = null)
    {

        $category = new Category();
        $category->Video = $video;

        if ($picture != null) {
            if ($picture['name'] !== "") {
                $newName = self::_generatePictureName($name);
                $imagePath = self::_generatePictureFullPath($newName);
                move_uploaded_file($picture['tmp_name'], $imagePath);
                ImageResizeHelper::ResizeFitImage(100, 100, $imagePath, self::_generatePictureFullPath($newName, "Small"), null, 'jpeg');
                ImageResizeHelper::ResizeFitImage(500, 500, $imagePath, self::_generatePictureFullPath($newName, "Medium"), null, 'jpeg');
                $category->Picture = $newName;
            }
        }

        $categoryId = CategoriesApiController::SaveCategory($category);

        $categoryDetails = new CategoryDetails();
        $categoryDetails->CategoryId = $categoryId;
        $categoryDetails->LanguageId = MainHelper::GetLanguage();
        $categoryDetails->Name = $name;
        $categoryDetails->Subtitle = $subtitle;


        CategoriesApiController::SaveCategoryDetails($categoryDetails);

        Router::Redirect("categories-list");
    }

    public function GetUpdate($id)
    {

        $model = new CategoryViewModel();
        $model->Category = CategoriesApiController::GetCategory($id);
        $model->Details = $model->Category->GetDetails(MainHelper::GetLanguage());

        $this->RenderView(null, ['model' => $model]);
    }

    public function PostUpdate($id, $name, $subtitle, $video = null, $picture = null)
    {
        $category = CategoriesApiController::GetCategory($id);
        $category->Video = $video;

        if ($picture != null) {
            if ($picture['name'] !== "") {
                if (!empty($category->Picture)) {
                    if (is_readable($category->PicturePath("Small"))) {
                        unlink($category->PicturePath("Small"));
                    }
                    if (is_readable($category->PicturePath("Medium"))) {
                        unlink($category->PicturePath("Medium"));
                    }
                    if (is_readable($category->PicturePath())) {
                        unlink($category->PicturePath());
                    }
                }
                $newName = self::_generatePictureName($name);
                $imagePath = self::_generatePictureFullPath($newName);
                move_uploaded_file($picture['tmp_name'], $imagePath);
                ImageResizeHelper::ResizeFitImage(100, 100, $imagePath, self::_generatePictureFullPath($newName, "Small"), null, 'jpeg');
                ImageResizeHelper::ResizeFitImage(500, 500, $imagePath, self::_generatePictureFullPath($newName, "Medium"), null, 'jpeg');
                $category->Picture = $newName;


            }
        }
        CategoriesApiController::SaveCategory($category);

        $details = $category->GetDetails(MainHelper::GetLanguage());
        if (is_null($details)) {
            $details = new CategoryDetails();
            $details->LanguageId = MainHelper::GetLanguage();
        }

        $details->CategoryId = $id;
        $details->Name = $name;
        $details->Subtitle = $subtitle;

        CategoriesApiController::SaveCategoryDetails($details);
        Router::Redirect("categories-list");
    }

    public function GetDelete($id)
    {
        CategoriesApiController::DeleteCategory($id);
        Router::Redirect("categories-list");
    }

    private static function _generatePictureName($name)
    {
        return CommonHelper::StringToFilename(sprintf("category-%s-%s.%s", $name, CommonHelper::GenerateRandomString(), "jpg"));
    }

    private static function _generatePictureFullPath($pictureName, $thumb = null)
    {
        return sprintf("%sMedia/Categories/%s/%s", CDN_PATH, is_null($thumb) ? "" : $thumb . "/", $pictureName);
    }


}