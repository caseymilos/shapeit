<?php
use abeautifulsite\SimpleImage;
use Business\ApiControllers\PagesApiController;
use Business\Enums\BuilderLayoutsEnum;
use Business\Enums\WidgetsEnum;
use Business\Helper\ImageResizeHelper;
use Data\Cache\MemcachedService;
use Data\Models\Page;
use Data\Models\PageDetails;
use Data\Models\Widget;
use Data\Repositories\LayoutsRepository;
use Data\Repositories\WidgetsRepository;

/**
 * Class BuilderController
 */
class BuilderController extends MVCController
{

    public function GetBuilder($id = null)
    {
        $content = null;
        $model = new PageViewModel();
        if ($id == null) {
            $model->Page = new Page();
            $model->Details = new PageDetails();
        } else {
            $model->Page = \Business\ApiControllers\PagesApiController::GetPage($id);
            $model->Details = $model->Page->GetDescription(MainHelper::GetLanguage());
            if ($model->Details instanceof PageDetails) {
                $content = $this->_build($model->Details->PageDetailsId);
            }
        }
        $this->RenderView(null, ['model' => $model, 'content' => $content]);
    }

    public function PostLayout($layout)
    {
        $enum = new \Business\Enums\BuilderLayoutsEnum();
        $model = (object)[
            "LayoutTypeId" => $layout
        ];
        $this->RenderView("Builder/Layout/" . $enum->Caption($layout), ['model' => $model]);
    }

    public function PostWidget($widget)
    {
        $model = (object)[
            "Type" => $widget
        ];
        if ($widget < 100) {
            $enum = new \Business\Enums\WidgetsEnum();
        } else {
            $enum = new \Business\Enums\TypographyEnum();
        }
        $this->RenderView("Builder/Widget/" . $enum->Caption($widget), ['model' => $model]);
    }

    public function PostSave($id, $title, $subtitle, $slug, $copyrights, $template, $headerStyle, $padding, $showTitle = null, $showBreadcrumbs = null, $useOverlay = null, $active = null, $picture = null, $deletedLayouts = [], $deletedWidgets = [], $content = [])
    {

        $isNew = false;

        $deletedWidgets = explode(",", $deletedWidgets);
        $deletedLayouts = explode(",", $deletedLayouts);

        // saving page
        if (empty($id)) {
            $isNew = true;
            $page = new Page();
        } else {
            $page = PagesApiController::GetPage($id);
        }

        $page->Active = $active == "1";
        if ($padding == "") {
            $page->Padding = null;
        } else {
            $page->Padding = $padding;
        }
        $page->UseOverlay = $useOverlay == "1";
        $page->ShowTitle = $showTitle == "1";
        $page->ShowBreadcrumbs = $showBreadcrumbs == "1";
        $page->HeaderStyle = $headerStyle;

        if ($picture != null && $picture['name'] !== "") {
            if (!empty($page->Picture)) {
                if (is_readable($page->PicturePath("Small"))) {
                    unlink($page->PicturePath("Small"));
                }
                if (is_readable($page->PicturePath("Medium"))) {
                    unlink($page->PicturePath("Medium"));
                }
                if (is_readable($page->PicturePath())) {
                    unlink($page->PicturePath());
                }
            }

            if ($picture['name'] !== "") {
                $newName = self::_generatePictureName($picture['name'], $title);
                $imagePath = self::_generatePictureFullPath($newName);
                move_uploaded_file($picture['tmp_name'], $imagePath);
                ImageResizeHelper::ResizeFitImage(100, 100, $imagePath, self::_generatePictureFullPath($newName, "Small"), null, 'jpeg');
                ImageResizeHelper::ResizeFitImage(500, 500, $imagePath, self::_generatePictureFullPath($newName, "Medium"), null, 'jpeg');
                ImageResizeHelper::ResizeFitImage(1920, 300, $imagePath, self::_generatePictureFullPath($newName, "Header"), 75, 'jpeg');
                ImageResizeHelper::ResizeFitImage(370, 230, $imagePath, self::_generatePictureFullPath($newName, "Related"), null, 'jpeg');
                $page->Picture = $newName;
            }
        }

        $page->Template = $template;

        $pageId = PagesApiController::SavePage($page);

        $details = $page->GetDescription(MainHelper::GetLanguage());

        if (is_null($details)) {
            $details = new PageDetails();
            $details->LanguageId = MainHelper::GetLanguage();
            $details->PageId = $page->PageId;
        }

        $details->Title = $title;
        $details->Subtitle = $subtitle;
        $details->Slug = $slug;
        $details->Copyrights = $copyrights;

        $pageDetailsId = PagesApiController::SaveDetails($details);
        $pageDetailsId = $details->PageDetailsId;

        // deleted panels
        if (count($deletedLayouts) > 0) {
            LayoutsRepository::getInstance()->delete(["LayoutId" => $deletedLayouts]);
        }

        // deleted widgets
        if (count($deletedWidgets) > 0) {
            \Business\ApiControllers\WidgetsApiController::DeleteWidget($deletedWidgets);
        }

        // CREATING CONTENT
        $content = json_decode($content);
        if (count($content) > 0) {
            foreach ($content as $layout) {
                $layout = (object)$layout;
                if ($layout->layoutId) {
                    $l = LayoutsRepository::getInstance()->get($layout->layoutId);
                } else {
                    $l = new \Data\Models\Layout();
                }
                $l->LayoutTypeId = $layout->layoutTypeId;
                $l->Weight = $layout->weight;
                $l->PageDetailsId = $pageDetailsId;
                $l->LayoutId = $layout->layoutId;
                $l->Options = $layout->options;

                $layoutId = LayoutsRepository::getInstance()->save($l);

                if (count($layout->widgets) > 0) {
                    foreach ($layout->widgets as $widget) {
                        $widget = (object)$widget;
                        if ($widget->widgetId) {
                            $w = \Business\ApiControllers\WidgetsApiController::GetWidget($widget->widgetId);
                        } else {
                            $w = new \Data\Models\Widget();
                        }
                        $w->LayoutId = $l->LayoutId;
                        $w->Active = true;
                        $w->Type = $widget->type;
                        $w->Weight = $widget->weight;
                        $w->WidgetId = $widget->widgetId;
                        $w->Options = $widget->options;
                        $w->Position = $widget->position;

                        \Business\ApiControllers\WidgetsApiController::SaveWidget($w);
                    }
                }
            }
        }

        // clearing cache
        MemcachedService::getInstance()->delete("PageLayouts" . $pageDetailsId);

        header("Content-Type: application/json");
        echo json_encode((object)[
            "html" => $this->_build($pageDetailsId),
            "redirect" => $isNew ? Router::Create("page-builder", ["id" => $pageId]) : false
        ]);
    }

    public function _build($pageDetailsId)
    {
        $layouts = LayoutsRepository::getInstance()->where(["PageDetailsId" => $pageDetailsId])->order(["Weight" => "ASC"])->with(["Widgets"]);

        $content = "";

        $layoutsEnum = new BuilderLayoutsEnum();
        $widgetsEnum = new WidgetsEnum();
        $typographyEnum = new \Business\Enums\TypographyEnum();

        if (count($layouts) > 0) {
            foreach ($layouts as $layout) {
                /** @var \Data\Models\Layout $layout */
                $layoutContent = [];
                if (count($layout->Widgets) > 0) {
                    foreach ($layout->Widgets as $widget) {
                        /** @var \Data\Models\Widget $widget */
                        if ($widget->Type <= 100) {
                            $caption = $widgetsEnum->Caption($widget->Type);
                        } else {
                            $caption = $typographyEnum->Caption($widget->Type);
                        }
                        if (!array_key_exists($widget->Position, $layoutContent)) {
                            $layoutContent[$widget->Position] = "";
                        }
                        $layoutContent[$widget->Position] .= HtmlHelper::RenderView("Builder/Widget/" . $caption, ['model' => $widget], true);

                    }
                }

                $layoutContent['model'] = $layout;
                $content .= HtmlHelper::RenderView("Builder/Layout/" . $layoutsEnum->Caption($layout->LayoutTypeId), $layoutContent, true);
            }
        }

        return $content;
    }

    public function PostCopyStructure($pageId, $language)
    {
        $currentLanguage = MainHelper::GetLanguage();
        $page = PagesApiController::GetPage($pageId);

        // take the other language page details from database
        $originalPageDetails = $page->GetDescription($language);

        // take the current page details from database
        $pageDetails = $page->GetDescription($currentLanguage);

        if (false == $pageDetails) {
            $pageDetails = new PageDetails();
            $pageDetails->PageId = $pageId;
            $pageDetails->LanguageId = $currentLanguage;
            $pageDetails->Slug = $originalPageDetails->Slug;
            $pageDetails->Title = $originalPageDetails->Title;
            $pageDetails->Subtitle = $originalPageDetails->Subtitle;
            $pageDetailsId = PagesApiController::SaveDetails($pageDetails);
        } else {
            // delete its layouts
            \Data\Repositories\LayoutsRepository::getInstance()->delete(["PageDetailsId" => $pageDetails->PageDetailsId]);
        }

        if ($originalPageDetails instanceof PageDetails) {
            // copy its layouts
            $layouts = LayoutsRepository::getInstance()->where(["PageDetailsId" => $originalPageDetails->PageDetailsId])->with(["Widgets"]);
            foreach ($layouts as $layout) {
                $newLayout = new \Data\Models\Layout();
                $newLayout->LayoutTypeId = $layout->LayoutTypeId;
                $newLayout->Weight = $layout->Weight;
                $newLayout->PageDetailsId = $pageDetails->PageDetailsId;

                LayoutsRepository::getInstance()->save($newLayout);

                // copy widgets
                foreach ($layout->Widgets as $widget) {
                    $newWidget = new Widget();
                    $newWidget->Weight = $widget->Weight;
                    $newWidget->Active = $widget->Active;
                    $newWidget->LayoutId = $newLayout->LayoutId;
                    $newWidget->Options = $widget->Options;
                    $newWidget->Position = $widget->Position;
                    $newWidget->Type = $widget->Type;

                    WidgetsRepository::getInstance()->save($newWidget);
                }
            }
        }

        Router::Redirect("page-builder", ["id" => $page->PageId]);
    }

    public function GetDuplicate($id)
    {
        $page = PagesApiController::GetPage($id);

        $newPage = new Page();
        $newPage->Active = $page->Active;
        $newPage->Style = $page->Style;
        $newPage->Template = $page->Template;
        $newPage->HeaderStyle = $page->HeaderStyle;
        $newPage->UseOverlay = $page->UseOverlay;
        $newPage->ShowTitle = $page->ShowTitle;
        $newPage->ShowBreadcrumbs = $page->ShowBreadcrumbs;
        $newPage->Padding = $page->Padding;

        if ($page->Picture) {
            $img = new SimpleImage($page->PicturePath());
            $name = "copy_" . $page->Picture;
            $fullPath = $this->_generatePictureFullPath($name);
            $img->save($fullPath);

            ImageResizeHelper::ResizeFitImage(100, 100, $fullPath, self::_generatePictureFullPath($name, "Small"), null, 'jpeg');
            ImageResizeHelper::ResizeFitImage(500, 500, $fullPath, self::_generatePictureFullPath($name, "Medium"), null, 'jpeg');
            ImageResizeHelper::ResizeFitImage(1920, 300, $fullPath, self::_generatePictureFullPath($name, "Header"), 75, 'jpeg');
            ImageResizeHelper::ResizeFitImage(370, 230, $fullPath, self::_generatePictureFullPath($name, "Related"), null, 'jpeg');

            $newPage->Picture = $name;
        }

        $newPageId = PagesApiController::SavePage($newPage);

        // save the details in all languages
        foreach ($page->Details as $originalPageDetails) {

            $slug = "copy-" . $originalPageDetails->Slug;
            $i = 2;
            while (PagesApiController::GetPageBySlug($slug, $originalPageDetails->LanguageId)) {
                $slug = "copy-" . $originalPageDetails->Slug . "-" . $i;
                $i++;
            }

            // take the current page details from database
            $pageDetails = new PageDetails();
            $pageDetails->Title = $originalPageDetails->Title . " (Copy)";
            $pageDetails->LanguageId = $originalPageDetails->LanguageId;
            $pageDetails->Copyrights = $originalPageDetails->Copyrights;
            $pageDetails->Description = $originalPageDetails->Description;
            $pageDetails->Slug = $slug;
            $pageDetails->Subtitle = $originalPageDetails->Subtitle;
            $pageDetails->PageId = $newPageId;
            $pageDetails->PageDetailsId = null;
            $pageDetailsId = PagesApiController::SaveDetails($pageDetails);

            if ($originalPageDetails instanceof PageDetails) {
                // copy its layouts
                $layouts = LayoutsRepository::getInstance()->where(["PageDetailsId" => $originalPageDetails->PageDetailsId])->with(["Widgets"]);
                foreach ($layouts as $layout) {
                    $newLayout = new \Data\Models\Layout();
                    $newLayout->LayoutTypeId = $layout->LayoutTypeId;
                    $newLayout->Weight = $layout->Weight;
                    $newLayout->PageDetailsId = $pageDetails->PageDetailsId;

                    LayoutsRepository::getInstance()->save($newLayout);

                    // copy widgets
                    foreach ($layout->Widgets as $widget) {
                        $newWidget = new Widget();
                        $newWidget->Weight = $widget->Weight;
                        $newWidget->Active = $widget->Active;
                        $newWidget->LayoutId = $newLayout->LayoutId;
                        $newWidget->Options = $widget->Options;
                        $newWidget->Position = $widget->Position;
                        $newWidget->Type = $widget->Type;

                        WidgetsRepository::getInstance()->save($newWidget);
                    }
                }
            }
        }

        Router::Redirect("page-builder", ["id" => $newPageId]);

    }

    public function PostLayoutOptions($options) {
        $this->RenderView("Builder/Layout/Options/Options", ["options" => (object)$options]);
    }

    private static function _generatePictureName($logo, $name)
    {
        return CommonHelper::StringToFilename(sprintf("page-%s-%s.%s", $name, CommonHelper::GenerateRandomString(), "jpg"));
    }

    private static function _generatePictureFullPath($pictureName, $thumb = null)
    {
        return sprintf("%sMedia/Pages/%s/%s", CDN_PATH, is_null($thumb) ? "" : $thumb . "/", $pictureName);
    }


}