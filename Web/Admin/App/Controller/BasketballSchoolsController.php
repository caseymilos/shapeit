<?php
use Business\ApiControllers\BasketballSchoolsApiController;
use Business\Enums\PermissionsEnum;
use Business\Helper\ImageResizeHelper;
use Data\Models\BasketballSchool;
use Data\Models\BasketballSchoolDetails;

class BasketballSchoolsController extends MVCController
{

    public function GetList()
    {
        $model = new BasketballSchoolsViewModel();
        $model->BasketballSchools = BasketballSchoolsApiController::GetBasketballSchools();

        $this->RenderView(null, ['model' => $model]);
    }

    public function GetCreate()
    {
        $this->RenderView();
    }

    public function PostCreate($name, $picture = null, $video = null, $logo, $slug)
    {

        $basketballSchool = new BasketballSchool();
        $basketballSchool->Video = $video;

        if ($logo != null) {
            if ($logo['name'] !== "") {
                $newName = self::_generatePictureName($name);
                $imagePath = self::_generatePictureFullPath($newName, "Logo");
                move_uploaded_file($logo['tmp_name'], $imagePath);
                $basketballSchool->Logo = $newName;
            }
        }



        if ($picture != null) {
            if ($picture['name'] !== "") {
                $newName = self::_generatePictureName($name);
                $imagePath = self::_generatePictureFullPath($newName);
                move_uploaded_file($picture['tmp_name'], $imagePath);
                ImageResizeHelper::ResizeFitImage(100, 100, $imagePath, self::_generatePictureFullPath($newName, "Small"), null, 'jpeg');
                ImageResizeHelper::ResizeFitImage(500, 500, $imagePath, self::_generatePictureFullPath($newName, "Medium"), null, 'jpeg');
                $basketballSchool->Picture = $newName;
            }
        }

        $basketballSchoolId = BasketballSchoolsApiController::SaveBasketballSchool($basketballSchool);

        $basketballSchoolDetails = new BasketballSchoolDetails();
        $basketballSchoolDetails->BasketballSchoolId = $basketballSchoolId;
        $basketballSchoolDetails->LanguageId = MainHelper::GetLanguage();
        $basketballSchoolDetails->Name = $name;
        $basketballSchoolDetails->Slug = $slug;


        BasketballSchoolsApiController::SaveBasketballSchoolDetails($basketballSchoolDetails);

        Router::Redirect("basketballSchools-list");
    }

    public function GetUpdate($id)
    {

        $model = new BasketballSchoolViewModel();
        $model->BasketballSchool = BasketballSchoolsApiController::GetBasketballSchool($id);
        $model->Details = $model->BasketballSchool->GetDetails(MainHelper::GetLanguage());

        $this->RenderView(null, ['model' => $model]);
    }

    public function PostUpdate($id, $name, $video= null, $picture = null, $slug, $logo)
    {
        $basketballSchool = BasketballSchoolsApiController::GetBasketballSchool($id);
        $basketballSchool->Video = $video;

        if ($logo != null) {
            if ($logo['name'] !== "") {
                $newName = self::_generatePictureName($name);
                $imagePath = self::_generatePictureFullPath($newName, "Logo");
                move_uploaded_file($logo['tmp_name'], $imagePath);
                $basketballSchool->Logo = $newName;
            }
        }

        if ($picture != null) {
            if ($picture['name'] !== "") {
                if (!empty($basketballSchool->Picture)) {
                    if (is_readable($basketballSchool->PicturePath("Small"))) {
                        unlink($basketballSchool->PicturePath("Small"));
                    }
                    if (is_readable($basketballSchool->PicturePath("Medium"))) {
                        unlink($basketballSchool->PicturePath("Medium"));
                    }
                    if (is_readable($basketballSchool->PicturePath())) {
                        unlink($basketballSchool->PicturePath());
                    }
                }
                $newName = self::_generatePictureName($name);
                $imagePath = self::_generatePictureFullPath($newName);
                move_uploaded_file($picture['tmp_name'], $imagePath);
                ImageResizeHelper::ResizeFitImage(100, 100, $imagePath, self::_generatePictureFullPath($newName, "Small"), null, 'jpeg');
                ImageResizeHelper::ResizeFitImage(500, 500, $imagePath, self::_generatePictureFullPath($newName, "Medium"), null, 'jpeg');
                $basketballSchool->Picture = $newName;


            }
        }
        BasketballSchoolsApiController::SaveBasketballSchool($basketballSchool);

        $details = $basketballSchool->GetDetails(MainHelper::GetLanguage());
        if (is_null($details)) {
            $details = new BasketballSchoolDetails();
            $details->LanguageId = MainHelper::GetLanguage();
        }

        $details->BasketballSchoolId = $id;
        $details->Name = $name;
        $details->Slug = $slug;


        BasketballSchoolsApiController::SaveBasketballSchoolDetails($details);
        Router::Redirect("basketballSchools-list");
    }

    public function GetDelete($id)
    {
        BasketballSchoolsApiController::DeleteBasketballSchool($id);
        Router::Redirect("basketballSchools-list");
    }

    private static function _generatePictureName($name) {
        return CommonHelper::StringToFilename(sprintf("basketballSchool-%s-%s.%s", $name, CommonHelper::GenerateRandomString(), "jpg"));
    }

    private static function _generatePictureFullPath($pictureName, $thumb = null) {
        return sprintf("%sMedia/BasketballSchools/%s/%s", CDN_PATH, is_null($thumb) ? "" : $thumb . "/", $pictureName);
    }


}