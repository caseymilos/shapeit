<?php


use Business\Enums\PermissionsEnum;

class HomeController extends MVCController {

    public function GetIndex($permissions = [PermissionsEnum::AccessSystem]) {

        $this->RenderView();
    }

}