<?php

use Business\DTO\CurrentUserDTO;
use Business\DTO\RecipientDTO;
use Business\DTO\SenderDTO;
use Business\Enums\PermissionsEnum;
use Business\Enums\UserStatusTypesEnum;
use Business\Helper\NotificationHelper;
use Business\Security\Crypt;
use Business\Security\Tokens;
use Gdev\UserManagement\ApiControllers\PasswordResetLinksApiController;
use Gdev\UserManagement\ApiControllers\RolePermissionsApiController;
use Gdev\UserManagement\ApiControllers\UserAccessTokensApiController;
use Gdev\UserManagement\ApiControllers\UsersApiController;
use Gdev\UserManagement\ApiControllers\UserStatusesApiController;
use Gdev\UserManagement\DataManagers\UserAccessTokensDataManager;
use Gdev\UserManagement\DataManagers\UserRolesDataManager;
use Gdev\UserManagement\Models\PasswordResetLink;
use Gdev\UserManagement\Models\User;
use Gdev\UserManagement\Repositories\UserAccessTokensRepository;

class SecurityController extends MVCController {

    public function GetLogin() {
        if (Cookie::Exists("UserAccessAdminToken")) {
            $user = UsersApiController::Login(null, null, Cookie::Get("UserAccessAdminToken"));
            $this->LoginUser($user, Cookie::Get("UserAccessAdminToken"));
            return;
        }
        $this->_renderLoginPage();
    }


    public function PostLogin($username, $password) {
        $user = UsersApiController::Login($username, $password);
        $this->LoginUser($user);
    }

    public function GetLogout() {
        /** @var User $user */
        $user = Security::GetCurrentUser();

        if (UserAccessTokensApiController::RemoveUserAccessToken($user->UserId)) {

            Session::Remove("CurrentUser");
            Cookie::Delete("UserAccessAdminToken");

            Router::Redirect("login");
        }
        else {

            Router::Redirect("home");
        }
    }

    private function _renderLoginPage($permissions = [PermissionsEnum::AccessSystem]) {
        $this->RenderView("Security/Login");
    }

    public function PostValidateUsername($username) {
        $user = UsersApiController::GetUserByUserName($username);

        if ($user === null) {
            $userExists = false;
        }
        else {
            $userExists = true;
        }

        echo json_encode(
            (object)[
                "Success" => $userExists
            ]
        );
    }

    public function PostValidateEmail($email) {
        $user = UsersApiController::GetUserByEmail($email);

        if ($user === null) {
            $userExists = false;
        }
        else {
            $userExists = true;
        }

        echo json_encode(
            (object)[
                "Success" => $userExists
            ]
        );
    }

    /**
     * @param string|null $activeToken
     */
    private function LoginUser($user, $activeToken = null) {

        if (!is_null($user)) {
            $currentUserStatus = UserStatusesApiController::GetCurrentUserStatus($user->UserId);
            if ($user->Active == 1 && $user->Approved == 1  && !empty($currentUserStatus) && $currentUserStatus->UserStatusTypeId == UserStatusTypesEnum::Active) {
                if (is_null($activeToken)) {
                    $activeToken = UserAccessTokensDataManager::GetActiveToken($user->UserId)->Token;
                }
                if (!is_null($activeToken)) {
                    Cookie::Set("UserAccessAdminToken", $activeToken, Cookie::SevenDays);
                }

                // Packing CurrentUserDTO from user
                $userDto = new CurrentUserDTO();

                $userDto->Username = $user->UserName;
                $userDto->Email = $user->Email;
                $userDto->UserId = $user->UserId;

                $userDetails = UsersApiController::GetUserById($user->UserId);

                $userDto->Picture = $userDetails->Details->PictureSource("Medium");
                $userDto->FirstName = $userDetails->Details->FirstName;
                $userDto->LastName = $userDetails->Details->LastName;

                $userRoles = $userDetails->Roles;

                $rolePermissions = [];

                foreach ($userRoles as $userRole) {
                    $oneRolePermissions = $userRole->Permissions; // RolePermissionsApiController::GetRolePermissions($userRole->RoleId);
                    foreach ($oneRolePermissions as $permission) {
                        $rolePermissions[] = $permission->PermissionId;
                    }
                }

                $userDto->Permissions = array_unique($rolePermissions);

                Security::SetCurrentUser($userDto);

                Router::Redirect('home');
            } else {
                $errors = ["Your account is not active or approved"];
                $this->RenderView("Security/Login", ["errors" => $errors]);
            }
        }
        else {
            $errors = ["Username and/or password are incorrect. Please try again."];
            $this->RenderView("Security/Login", ["errors" => $errors]);
        }
    }

    public function GetChangePassword() {
        $this->RenderView("Home/Index");
    }

    public function PostChangePassword($newPassword, $oldPassword, $confirmNewPassword) {

        $currentUser = Security::GetCurrentUser();
        $user = UsersApiController::GetUserById($currentUser->UserId);

        if (Crypt::CheckPassword($oldPassword, $user->Password)) {

            if ($newPassword == $confirmNewPassword) {
                $user->Password = Crypt::HashPassword($confirmNewPassword);

                UsersApiController::UpdateUser($user);
            }

            $model = new PasswordOrInfoChangedViewModel();
            $model->Changed = true;
            $this->RenderView("Home/Index", ["model" => $model]);

        }
        else {
            Router::Redirect('home');
        }
    }


    public function GetRequestAccess() {
        $this->RenderView();
    }

    public function PostRequestAccess() {
        $errors = ["Request access is not implemented yet!"];
        $this->RenderView(null, ["errors" => $errors]);
    }

    public function GetResetPassword() {
        $this->RenderView();
    }

    public function PostResetPassword($email) {
        $errors = [];
        $user = UsersApiController::GetUserByEmail($email);
        if ($user) {
            $resetPassword = new PasswordResetLink();
            $resetPassword->ExpirationDate = new DateTime("+7 days");
            $resetPassword->Used = false;
            $resetPassword->UserId = $user->UserId;
            $resetPassword->Token = Tokens::CreateToken();

            PasswordResetLinksApiController::InsertPasswordResetLink($resetPassword);

            $mailModel = new EmailToResetPasswordViewModel();

            $mailModel->Name = $user->Details->FirstName;
            $mailModel->Token = $resetPassword->Token;

            $config = \Business\Utilities\Config\Config::GetInstance();
            NotificationHelper::Send(
                "Reset Your Admin Password",
                HtmlHelper::RenderView("Mail/ResetPasswordEmail", ["model" => $mailModel], true),
                "",
                [new SenderDTO("Admin Admin", $config->smtp->from)],
                [new RecipientDTO(sprintf("%s %s", $user->Details->FirstName, $user->Details->LastName), $user->Email)]
            );

            Router::Redirect("reset-password-successful");
        }
        else {
            $errors = [sprintf("There is no registered user with email address %s. Please try again.", $email)];
        }


        $this->RenderView(null, ["errors" => $errors]);
    }

    public function GetResetPasswordSuccessful() {
        $this->RenderView();
    }

    public function GetResetPasswordConfirm($token) {
        $passwordReset = PasswordResetLinksApiController::GetPasswordResetLink($token);
        $errors = [];
        if ($passwordReset == false) {
            $errors[] = "Link is invalid";
        }
        else {
            if ($passwordReset->ExpirationDate < new DateTime()) {
                $errors[] = "Link expired";
            }
            if ($passwordReset->Used) {
                $errors[] = "Link is already used";
            }
        }
        $this->RenderView(null, ["errors" => $errors]);
    }

    public function PostResetPasswordConfirm($token, $password, $passwordConfirm) {
        if ($password !== $passwordConfirm) {
            $errors[] = "Passwords do not match!";
            $this->RenderView(null, ["errors" => $errors]);
            return;
        }

        $passwordReset = PasswordResetLinksApiController::GetPasswordResetLink($token);
        $user = $passwordReset->User;
        $user->Password = Crypt::HashPassword($password);

        UsersApiController::UpdateUser($user);

        Router::Redirect("login");
    }

    /*public function GetLogin() {
        $this->RenderView();
    }

    public function PostLogin($username, $password) {
        $errors = ["Login is not implemented yet!"];
        $this->RenderView(null, ["errors" => $errors]);
    }

    public function GetLogout($permissions = [PermissionsEnum::AccessSystem]) {
        Session::Destroy();
        Router::Redirect("login");
    }

    public function PostChangePassword($password, $repeat) {

    }*/
}