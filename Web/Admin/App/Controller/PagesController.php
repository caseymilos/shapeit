<?php
use Business\ApiControllers\PagesApiController;
use Business\ApiControllers\WidgetsApiController;
use Business\Helper\ImageResizeHelper;
use Data\DataManagers\PagesDataManager;
use Data\Models\Page;
use Data\Models\PageDetails;
use Data\Models\Widget;
use Data\Repositories\LayoutsRepository;
use Data\Repositories\WidgetsRepository;


/**
 * Class PagesController
 */
class PagesController extends MVCController {
    public function GetList() {
        $model = new PagesViewModel();
        $model->Pages = PagesApiController::GetPages();
        $this->RenderView(null, ['model' => $model]);
    }

    public function GetDelete($id) {
        PagesApiController::DeletePage($id);
        Router::Redirect("pages-list");
    }

    public function GetCreate() {
        $this->RenderView();
    }

    public function PostCreate($title, $subtitle, $description, $slug, $template, $useSidebar = null, $useOverlay = null, $active = null, $picture = null) {

        $page = new Page();

        $page->Active = $active == "1";
        $page->UseSidebar = $useSidebar == "1";
        $page->UseOverlay = $useOverlay == "1";

        if ($picture != null) {
            if ($picture['name'] !== "") {
                $newName = self::_generatePictureName($picture['name'], $title);
                $imagePath = self::_generatePictureFullPath($newName);
                move_uploaded_file($picture['tmp_name'], $imagePath);
                ImageResizeHelper::ResizeFitImage(100, 100, $imagePath, self::_generatePictureFullPath($newName, "Small"), 75, 'jpeg');
                ImageResizeHelper::ResizeFitImage(400, 400, $imagePath, self::_generatePictureFullPath($newName, "Medium"), 75, 'jpeg');
                ImageResizeHelper::ResizeFitImage(1920, 300, $imagePath, self::_generatePictureFullPath($newName, "Header"), 75, 'jpeg');
                ImageResizeHelper::ResizeFitImage(370, 230, $imagePath, self::_generatePictureFullPath($newName, "Related"), 75, 'jpeg');
                $page->Picture = $newName;
            }
        }

        $page->Template = $template;

        $pageId = PagesApiController::SavePage($page);

        $details = new PageDetails();

        $details->PageId = $pageId;
        $details->LanguageId = MainHelper::GetLanguage();
        $details->Description = $description;
        $details->Title = $title;
        $details->Subtitle = $subtitle;
        $details->Slug = $slug;

        PagesApiController::SaveDetails($details);

        Router::Redirect("pages-list");

    }

    public function GetUpdate($id) {
        $model = PagesApiController::GetPage($id);
        $model->SortWidgets();
        $this->RenderView(null, ['model' => $model]);
    }

    public function PostUpdate($id, $title, $subtitle, $description, $slug, $template, $useSidebar = null, $useOverlay = null, $active = null, $picture = null) {
        $page = PagesApiController::GetPage($id);

        $page->Active = $active == "1";
        $page->UseSidebar = $useSidebar == "1";
        $page->UseOverlay = $useOverlay == "1";

        if ($picture != null && $picture['name'] !== "") {
            if (!empty($page->Picture)) {
                if (is_readable($page->PicturePath("Small"))) {
                    unlink($page->PicturePath("Small"));
                }
                if (is_readable($page->PicturePath("Medium"))) {
                    unlink($page->PicturePath("Medium"));
                }
                if (is_readable($page->PicturePath())) {
                    unlink($page->PicturePath());
                }
            }

            if ($picture['name'] !== "") {
                $newName = self::_generatePictureName($picture['name'], $title);
                $imagePath = self::_generatePictureFullPath($newName);
                move_uploaded_file($picture['tmp_name'], $imagePath);
                ImageResizeHelper::ResizeFitImage(100, 100, $imagePath, self::_generatePictureFullPath($newName, "Small"), 75, 'jpeg');
                ImageResizeHelper::ResizeFitImage(400, 400, $imagePath, self::_generatePictureFullPath($newName, "Medium"), 75, 'jpeg');
                ImageResizeHelper::ResizeFitImage(1920, 300, $imagePath, self::_generatePictureFullPath($newName, "Header"), 75, 'jpeg');
                ImageResizeHelper::ResizeFitImage(370, 230, $imagePath, self::_generatePictureFullPath($newName, "Related"), 75, 'jpeg');
                $page->Picture = $newName;

                // clear cloudflare cache
                $this->_clearCloudFlareCache($page);
            }
        }

        $page->Template = $template;

        $pageId = PagesApiController::SavePage($page);

        $details = $page->GetDescription(MainHelper::GetLanguage());

        if (is_null($details)) {
            $details = new PageDetails();
            $details->LanguageId = MainHelper::GetLanguage();
            $details->PageId = $id;
        }

        $details->Description = $description;
        $details->Title = $title;
        $details->Subtitle = $subtitle;
        $details->Slug = $slug;

        PagesApiController::SaveDetails($details);

        Router::Redirect("pages-list");
    }

    private static function _generatePictureName($logo, $name) {
        return CommonHelper::StringToFilename(sprintf("page-%s-%s.%s", $name, CommonHelper::GenerateRandomString(), "jpg"));
    }

    private static function _generatePictureFullPath($pictureName, $thumb = null) {
        return sprintf("%sMedia/Pages/%s/%s", CDN_PATH, is_null($thumb) ? "" : $thumb . "/", $pictureName);
    }


    // Widgets

    public function PostWidgetOptions($type, $id = null, $options = null) {
        if (!is_null($id) && !empty($id)) {
            $widget = WidgetsApiController::GetWidget($id);
        } else {
            $widget = new Widget();
            $widget->Options = json_decode(json_encode($options));
        }

        if ($type > 100) {
            $enum = new \Business\Enums\TypographyEnum();
        } else {
            $enum = new \Business\Enums\WidgetsEnum();
        }
        $this->RenderView("Pages/WidgetOptions/" . $enum->Caption($type), ["widget" => $widget]);
    }

    public function PostWidgetSave($widgetType, $pageId, $widgetId = null, $widgetOptions = null) {
        $widget = is_null($widgetId) || empty($widgetId) ? new Widget() : WidgetsApiController::GetWidget($widgetId);

        $widget->Active = true;
        $widget->Type = $widgetType;
        $widget->Options = (object)$widgetOptions;
        $widget->PageId = (int)$pageId;

        $save = WidgetsApiController::SaveWidget($widget);

        if ($widget->isNew()) {
            if ($save > 0) {
                $success = true;
            } else {
                $success = false;
            }
        } else {
            $success = true;
        }

        $response = (object)[
            "Success" => $success,
            "Error" => $success == false ? "Could not save widget to database" : null,
            "Widget" => $widget
        ];

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function PostWidgetDelete($id) {
        $success = WidgetsApiController::DeleteWidget($id);

        $response = (object)[
            "Success" => $success,
            "Error" => $success == false ? "Could not remove widget from database" : null
        ];

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function PostWidgetReorder($order) {
        foreach ($order as $orderObj) {
            $orderObj = (object)$orderObj;
            $widget = WidgetsApiController::GetWidget($orderObj->WidgetId);
            $widget->Weight = $orderObj->Weight;
            WidgetsApiController::SaveWidget($widget);
        }
    }

    public function GetThumbnailCrop($id, $size) {
        $model = new PageViewModel();
        $model->Page = PagesApiController::GetPage($id);

        switch ($size) {
            case "header":
                $dimensions = [640, 100, 640, 100];
                break;
            case "thumb":
                $dimensions = [370, 230, 370, 370];
                break;
            default:
                throw new Exception("Unknown image size");

        }


        $this->RenderView(null, ["model" => $model, "size" => $size, "dimensions" => $dimensions]);
    }

    public function PostThumbnailCrop($id, $size, $points) {
        $page = PagesDataManager::GetPage($id);
        $picture = new \abeautifulsite\SimpleImage($page->PicturePath());

        if ($size == "thumb") {

            $clone = clone($picture);

            // crop related
            $clone->crop($points[0], $points[1], $points[2], $points[3])->thumbnail(370, 230)->save($page->PicturePath("Related"), 75, "jpeg");

            // crop medium
            $margin = $picture->get_height() * 0.375;
            $points[1] = (int)$points[1] - $margin;
            $points[3] = (int)$points[3] + $margin;
            $clone->crop($points[0], $points[1], $points[2], $points[3])->thumbnail(400, 400)->save($page->PicturePath("Medium"), 75, "jpeg");

        } elseif ($size == "header") {
            $picture->crop($points[0], $points[1], $points[2], $points[3])->thumbnail(1920, 300)->save($page->PicturePath("Header"), 75, "jpeg");
        }

        // clear cloudflare cache
        $this->_clearCloudFlareCache($page);
    }

    private function _clearCloudFlareCache(Page $item) {
        // clear cloudflare cache
        $cloudFlare = new \Business\Helper\CloudFlareHelper();
        $cloudFlare->PurgeFiles([
            $item->PictureSource(),
            $item->PictureSource("Small"),
            $item->PictureSource("Medium"),
            $item->PictureSource("Header"),
            $item->PictureSource("Related"),
        ]);
    }

}