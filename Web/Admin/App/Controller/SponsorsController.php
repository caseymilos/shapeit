<?php
use Business\ApiControllers\SponsorsApiController;
use Business\Enums\PermissionsEnum;
use Business\Helper\ImageResizeHelper;
use Data\Models\Sponsor;
use Data\Models\SponsorGroupDetails;

class SponsorsController extends MVCController
{

    public function GetList()
    {
        $model = new SponsorsViewModel();
        $model->Sponsors = SponsorsApiController::GetSponsors();

        $this->RenderView(null, ['model' => $model]);
    }

    public function GetCreate()
    {
        $this->RenderView();
    }

    public function PostCreate($name, $logo, $link, $active, $sponsorGroupId)
    {

        $sponsor = new Sponsor();
        $sponsor->Link = $link;
        $sponsor->Name = $name;
        $sponsor->Active = $active;
        $sponsor->SponsorGroupId = $sponsorGroupId;


        if ($logo != null) {
            if ($logo['name'] !== "") {
                $newName = self::_generatePictureName($name);
                $imagePath = self::_generatePictureFullPath($newName, "Logo");
                move_uploaded_file($logo['tmp_name'], $imagePath);
                $sponsor->Logo = $newName;
            }
        }




        $sponsorId = SponsorsApiController::SaveSponsor($sponsor);


        Router::Redirect("sponsors-list");
    }

    public function GetUpdate($id)
    {

        $model = new SponsorViewModel();
        $model->Sponsor = SponsorsApiController::GetSponsor($id);

        $this->RenderView(null, ['model' => $model]);
    }

    public function PostUpdate($id, $name,$link,$active,$sponsorGroupId, $logo)
    {
        $sponsor = SponsorsApiController::GetSponsor($id);
        $sponsor->Link = $link;
        $sponsor->Name = $name;
        $sponsor->Active = $active;
        $sponsor->SponsorGroupId = $sponsorGroupId;

        if ($logo != null) {
            if ($logo['name'] !== "") {
                $newName = self::_generatePictureName($name);
                $imagePath = self::_generatePictureFullPath($newName, "Logo");
                move_uploaded_file($logo['tmp_name'], $imagePath);
                $sponsor->Logo = $newName;
            }
        }


       SponsorsApiController::SaveSponsor($sponsor);

        Router::Redirect("sponsors-list");
    }

    public function GetDelete($id)
    {
        SponsorsApiController::DeleteSponsor($id);
        Router::Redirect("sponsors-list");
    }

    private static function _generatePictureName($name) {
        return CommonHelper::StringToFilename(sprintf("sponsor-%s-%s.%s", $name, CommonHelper::GenerateRandomString(), "jpg"));
    }

    private static function _generatePictureFullPath($pictureName, $thumb = null) {
        return sprintf("%sMedia/Sponsors/%s/%s", CDN_PATH, is_null($thumb) ? "" : $thumb . "/", $pictureName);
    }


}