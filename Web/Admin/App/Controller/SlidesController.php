<?php

use Business\ApiControllers\SlidesApiController;
use Business\DTO\OrderDTO;
use Business\Helper\ImageResizeHelper;
use Data\Models\Slide;
use Data\Models\SlideDescription;

/**
 * Class SlidesController
 */
class SlidesController extends MVCController {

    public function GetList() {

        $slides = SlidesApiController::GetSlides();

        $this->RenderView("Slides/List", ['model' => $slides]);
    }

    public function GetCreate() {
        $this->RenderView();
    }

    public function PostCreate($title, $description, $link, $linkButtonText, $active = null, $picture = null) {

        $slide = new Slide();

        if ($active == "1") {
            $slide->Active = 1;
        } else {
            $slide->Active = null;
        }

        if ($picture != null) {
            if ($picture['name'] !== "") {
                $newName = self::_generatePictureName($picture['name'], $title);
                $imagePath = self::_generatePictureFullPath($newName);
                move_uploaded_file($picture['tmp_name'], $imagePath);
                ImageResizeHelper::ResizeFitImage(100, 100, $imagePath, self::_generatePictureFullPath($newName, "Small"), null, 'jpeg');
                ImageResizeHelper::ResizeFitImage(500, 500, $imagePath, self::_generatePictureFullPath($newName, "Medium"), null, 'jpeg');
                ImageResizeHelper::ResizeFitImage(1920, 300, $imagePath, self::_generatePictureFullPath($newName, "Header"), 75, 'jpeg');
                $slide->Picture = $newName;
            }
        }

        $slideId = SlidesApiController::SaveSlide($slide);

        $details = new SlideDescription();

        $details->SlideId = $slideId;
        $details->LanguageId = MainHelper::GetLanguage();
        $details->Description = $description;
        $details->Title = $title;
        $details->Link = $link;
        $details->LinkText = $linkButtonText;

        SlidesApiController::SaveSlideDescription($details);

        Router::Redirect("slide-list");

    }

    public function GetDelete($slideId) {
        SlidesApiController::DeleteSlide($slideId);
        Router::Redirect("slide-list");
    }

    public function GetUpdateSlide($slideId) {
        $model = SlidesApiController::GetSlide($slideId);
        $this->RenderView("Slides/Update", ['model' => $model]);
    }

    public function PostUpdateSlide($slideId, $title, $description, $link, $linkButtonText, $active = null, $picture = null) {

        $slide = SlidesApiController::GetSlide($slideId);

        if ($active == "1") {
            $slide->Active = 1;
        } else {
            $slide->Active = null;
        }

        if ($picture != null) {
            if ($picture['name'] !== "") {
                if (!empty($slide->Picture)) {
                    if (is_readable($slide->PicturePath("Small"))) {
                        unlink($slide->PicturePath("Small"));
                    }
                    if (is_readable($slide->PicturePath("Medium"))) {
                        unlink($slide->PicturePath("Medium"));
                    }
                    if (is_readable($slide->PicturePath("Header"))) {
                        unlink($slide->PicturePath("Header"));
                    }
                    if (is_readable($slide->PicturePath())) {
                        unlink($slide->PicturePath());
                    }
                }

                $newName = self::_generatePictureName($picture['name'], $title);
                $imagePath = self::_generatePictureFullPath($newName);
                move_uploaded_file($picture['tmp_name'], $imagePath);
                ImageResizeHelper::ResizeFitImage(100, 100, $imagePath, self::_generatePictureFullPath($newName, "Small"), null, 'jpeg');
                ImageResizeHelper::ResizeFitImage(500, 500, $imagePath, self::_generatePictureFullPath($newName, "Medium"), null, 'jpeg');
                ImageResizeHelper::ResizeFitImage(1920, 300, $imagePath, self::_generatePictureFullPath($newName, "Header"), 75, 'jpeg');
                $slide->Picture = $newName;

                // clear cloudflare cache
                $this->_clearCloudFlareCache($slide);
            }
        }

        SlidesApiController::SaveSlide($slide);

        $details = $slide->GetDescription(MainHelper::GetLanguage());
        if (is_null($details)) {
            $details = new SlideDescription();
            $details->LanguageId = MainHelper::GetLanguage();
            $details->SlideId = $slideId;
        }

        $details->Description = $description;
        $details->Title = $title;
        $details->Link = $link;
        $details->LinkText = $linkButtonText;

        SlidesApiController::SaveSlideDescription($details);
        Router::Redirect("slide-list");
    }

    /**
     * @param OrderDTO[] $weights
     */
    public function PostUpdateWeight($weights) {
        echo SlidesApiController::UpdateSlideWeight($weights);
    }

    private static function _generatePictureName($logo, $name) {
        return CommonHelper::StringToFilename(sprintf("slide-%s-%s.%s", $name, CommonHelper::GenerateRandomString(), "jpg"));
    }

    private static function _generatePictureFullPath($pictureName, $thumb = null) {
        return sprintf("%sMedia/Slides/%s/%s", CDN_PATH, is_null($thumb) ? "" : $thumb . "/", $pictureName);
    }

    private function _clearCloudFlareCache(Slide $item) {
        // clear cloudflare cache
        $cloudFlare = new \Business\Helper\CloudFlareHelper();
        $cloudFlare->PurgeFiles([
            $item->PictureSource(),
            $item->PictureSource("Small"),
            $item->PictureSource("Medium"),
            $item->PictureSource("Header"),
        ]);
    }


}