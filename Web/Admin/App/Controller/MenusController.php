<?php

use Business\DTO\OrderDTO;
use Business\Enums\MenuItemTypesEnum;
use Business\Enums\PermissionsEnum;
use Gdev\MenuManagement\ApiControllers\MenuItemsApiController;
use Gdev\MenuManagement\ApiControllers\MenusApiController;
use Gdev\MenuManagement\Models\Menu;
use Gdev\MenuManagement\Models\MenuItem;


/**
 * Class MenusController
 */
class MenusController extends MVCController {
    public function GetList($permissions = [PermissionsEnum::ViewMenus]) {
        $model = new MenusViewModel();
        $model->Menus = MenusApiController::GetMenus();
        $this->RenderView(null, ['model' => $model]);
    }

    public function GetCreate($permissions = [PermissionsEnum::CreateMenu]) {
        $this->RenderView();
    }

    public function PostCreate($alias, $language, $position, $permissions = [PermissionsEnum::CreateMenu]) {
        $menu = new Menu();
        $menu->Alias = $alias;
        $menu->MenuPositionId = $position;
        $menu->LanguageId = $language;

        MenusApiController::SaveMenu($menu);

        // clear menu from cache
        \Data\Cache\MemcachedService::getInstance()->delete("MenuOutput" . $menu->MenuPositionId . $menu->LanguageId);

        Router::Redirect("menus-list");
    }

    public function GetUpdate($id, $permissions = [PermissionsEnum::EditMenu]) {
        $model = new MenuViewModel();
        $model->Menu = MenusApiController::GetMenu($id);
        $model->Tree = MenusApiController::GetTree($id);
        $this->RenderView(null, ['model' => $model]);
    }

    public function PostUpdate($id, $alias, $language, $position, $permissions = [PermissionsEnum::CreateMenu]) {
        $menu = MenusApiController::GetMenu($id);

        // clear menu from cache
        \Data\Cache\MemcachedService::getInstance()->delete("MenuOutput" . $menu->MenuPositionId . $menu->LanguageId);

        $menu->Alias = $alias;
        $menu->MenuPositionId = $position;
        $menu->LanguageId = $language;

        MenusApiController::SaveMenu($menu);

        // clear menu from cache
        \Data\Cache\MemcachedService::getInstance()->delete("MenuOutput" . $menu->MenuPositionId . $menu->LanguageId);

        Router::Redirect("menus-list");
    }


    public function PostCreateItem($menuId, $caption, $itemType, $parent, $target, $page = null, $url = null, $permissions = [PermissionsEnum::EditMenu]) {

        if (empty($page)) {
            $page = null;
        }

        $menuItem = new MenuItem;

        $menuItem->MenuId = $menuId;
        $menuItem->Caption = $caption;
        $menuItem->PageId = $page;
        $menuItem->MenuItemTypeId = $itemType;
        $menuItem->Url = $url;
        $menuItem->Target = $target;

        if ($parent == "") {
            $menuItem->ParentId = null;
        } else {
            $menuItem->ParentId = $parent;
        }

        MenuItemsApiController::SaveMenuItem($menuItem);

        // clear menu from cache
        $menu = MenusApiController::GetMenu($menuId);
        \Data\Cache\MemcachedService::getInstance()->delete("MenuOutput" . $menu->MenuPositionId . $menu->LanguageId);

        Router::Redirect("menus-update", ["id" => $menuId]);
    }

    public function GetUpdateItem($menuItemId, $permissions = [PermissionsEnum::EditMenu]) {

        $model = MenuItemsApiController::GetMenuItem($menuItemId);
        $this->RenderView("Menus/UpdateItem", ['model' => $model]);
    }

    public function PostUpdateItem($menuItemId, $caption, $itemType, $parent, $target, $page = null, $url = null, $permissions = [PermissionsEnum::EditMenu]) {

        if (empty($page)) {
            $page = null;
        }

        $menuItem = MenuItemsApiController::GetMenuItem($menuItemId);

        $menuItem->Caption = $caption;
        $menuItem->PageId = $page;
        $menuItem->MenuItemTypeId = $itemType;
        $menuItem->Url = $url;
        $menuItem->Target = $target;

        if ($parent == "") {
            $menuItem->ParentId = null;
        } else {
            $menuItem->ParentId = $parent;
        }

        MenuItemsApiController::SaveMenuItem($menuItem);

        // clear menu from cache
        $menu = MenusApiController::GetMenu($menuItem->MenuId);
        \Data\Cache\MemcachedService::getInstance()->delete("MenuOutput" . $menu->MenuPositionId . $menu->LanguageId);

        Router::Redirect("menus-update-item", ["menuItemId" => $menuItemId]);
    }

    public function GetDelete($id, $permissions = [PermissionsEnum::DeleteMenu]) {
        // clear menu from cache
        $menu = MenusApiController::GetMenu($id);
        \Data\Cache\MemcachedService::getInstance()->delete("MenuOutput" . $menu->MenuPositionId . $menu->LanguageId);

        MenusApiController::DeleteMenu($id);
        Router::Redirect("menus-list");
    }

    public function GetDeleteItem($id, $permissions = [PermissionsEnum::EditMenu]) {
        $menuItem = MenuItemsApiController::GetMenuItem($id);
        MenuItemsApiController::DeleteMenuItem($id);

        // clear menu from cache
        $menu = MenusApiController::GetMenu($menuItem->MenuId);
        \Data\Cache\MemcachedService::getInstance()->delete("MenuOutput" . $menu->MenuPositionId . $menu->LanguageId);

        Router::Redirect("menus-update", ["id" => $menuItem->MenuId]);
    }

    public function PostItemTypeSubmenu($itemTypeId, $permissions = [PermissionsEnum::EditMenu]) {
        $view = null;
        switch ($itemTypeId) {
            case MenuItemTypesEnum::Page:
                $view = "Menus/Subselect/Page";
                break;
            case MenuItemTypesEnum::External:
                $view = "Menus/Subselect/External";
                break;
            case MenuItemTypesEnum::Program:
                $view = "Menus/Subselect/Program";
                break;
        }

        if (!is_null($view)) {
            $this->RenderView($view);
        }
    }

    /**
     * @param OrderDTO[] $weights
     */
    public function PostUpdateWeight($weights) {
        echo MenuItemsApiController::UpdateWeight($weights);

        // clear menu from cache
        $itemId = $weights[0]["ItemId"];
        $item = MenuItemsApiController::GetMenuItem($itemId);
        $menu = MenusApiController::GetMenu($item->MenuId);
        \Data\Cache\MemcachedService::getInstance()->delete("MenuOutput" . $menu->MenuPositionId . $menu->LanguageId);

    }

}