$(".menu-item-list").sortable({
	update: function (event, ui) {
		var menuItemWeight = [];
		var weight = 1;

		$(this).children( "li" ).each(function () {
			var menuItemId = $(this).data("menu-item-id");
			var dto = {
				ItemId: menuItemId,
				Weight: weight
			};

			menuItemWeight.push(dto);
			weight++;

		});
		console.log(menuItemWeight);

		$.ajax({
			url: "weight-menu-items",
			method: "POST",
			data: {
				weights: menuItemWeight
			},
			success: function (response) {
				if (response == 0) {
					alert("Sorting failed. Please try again.");
				} else {
					var i = 1;
					$(this).find( "li" ).each(function () {
						$(this).children(".weight").html(i++);
					});

					var id = $("#menu-id").data("menu-id");
					$.ajax({
						url: "menus/update/" + id,
						method: "GET",
						data: {
							id: id
						},
						beforeSend: function () {
							$(".tree-preview").hide();
							$("#loading").show();
						},
						success: function (data) {
							var newContent = $(data);
							$("#loading").hide();
							$(".tree-preview").html(newContent.find(".tree-preview").first().html());
							$(".tree-preview").show();
							$("#sort-menu-script").empty();
							$("#sort-menu-script").html('<script src="Theme/Admin/js/sort-menu.js"></script>');
						}
					});
				}
			}
		})
	}
});