// global
var deletedWidgets = [];
var deletedLayouts = [];

$(function () {

    function initialize() {

        // global
        deletedWidgets = [];
        deletedLayouts = [];


        // business
        initializeLayout();

        $("[data-panel]").droppable({
            accept: "[data-new-layout]",
            classes: {
                "ui-droppable-active": "ui-state-active",
                "ui-droppable-hover": "ui-state-hover"
            },
            drop: function (event, ui) {
                var $this = $(this);
                var layoutId = ui.draggable.data("newLayout");
                $.ajax({
                    url: "builder/layout",
                    data: {
                        layout: layoutId
                    },
                    dataType: "html",
                    method: "POST",
                    success: function (response) {
                        $this.append(response);
                        initializeLayout();
                    }
                })
            }
        });

        // Widgets

        $(".editor-content").on("click", ".edit-widget-button", function () {
            var modal = $("#myModal");
            var widgetHolder = $(this).closest("[data-widget]");
            var type = widgetHolder.data("type");
            var uid = widgetHolder.data("uid");
            var options = widgetHolder.data("options");
            $.ajax({
                url: "widgets/options",
                type: "POST",
                data: {
                    type: type,
                    options: options
                },
                success: function (content) {
                    $("#widget-options-holder").html(content);
                }
            });
            modal.data("widgetUid", uid);
            modal.modal("show");
        });

        $(".editor-content").on("click", ".widget-remove", function () {
            var widget = $(this).closest("[data-widget]");
            var widgetId = widget.data("widget");
            if (widgetId != "") {
                deletedWidgets.push(widgetId);
            }
            widget.slideUp(300, function () {
                widget.remove()
            });
        });

        $(".editor-content").on("click", "[data-delete-layout]", function () {
            var layout = $(this).closest("[data-layout]");
            var layoutId = layout.data("layout");
            if (layoutId != "") {
                deletedLayouts.push(layoutId);
            }
            layout.slideUp(300, function () {
                layout.remove()
            });
        });

        $("[data-widget-area]").sortable({
            connectWith: '[data-widget-area]',
            cancel: '.note-editor, input, select, .select2'
        });

        $("[data-panel]").sortable({
            cancel: '[data-widget]'
        });

        $("#widgets-area-holder").disableSelection();

    }


    // INITIAL

    $("#save-page").on("click", function () {
        saveBuilder();
    });

    $("[data-new-widget]").draggable({
        helper: "clone",
        start: function (e, ui) {
            $(ui.helper).addClass("ui-draggable-helper");
        }
    });

    $("[data-new-layout]").draggable({
        helper: "clone",
        start: function (e, ui) {
            $(ui.helper).addClass("ui-draggable-helper");
        }
    });

    $("#save-widget-button").on("click", function () {
        var modal = $("#myModal");
        var data = jQuery.deparam($("#widget-form").serialize());
        var widgetUid = modal.data("widgetUid");
        var widget = $("[data-uid='" + widgetUid + "']");
        widget.data("options", data);
        modal.modal("hide");
    });

    // layout options
    $("body").on("click", "[data-edit-layout]", function() {
        var modal = $("#layoutOptions");

        var layoutHolder = $(this).closest("[data-layout]");
        var uid = layoutHolder.data("layoutUid");
        var options = layoutHolder.data("options");
        $.ajax({
            url: TWIG_OPTIONS.layoutsUrl,
            type: "POST",
            data: {
                options: options
            },
            success: function(content) {
                $("#layout-options-holder").html(content);
            }
        });

        modal.data("layoutUid", uid);
        modal.modal("show");
    });

    $("#save-layout-button").on("click", function() {
        saveLayoutOptions();
        var modal = $("#layoutOptions");
        modal.modal("hide");
    });

    function saveLayoutOptions() {
        var modal = $("#layoutOptions");
        var data = jQuery.deparam($("#layout-form").serialize());
        var layoutUid = modal.data("layoutUid");
        var layout = $("[data-layout-uid='" + layoutUid + "']");
        layout.data("options", data);
    }

    initialize();

    // LIB

    function initializeLayout() {
        $("[data-widget-area]").droppable({
            accept: "[data-new-widget]",
            classes: {
                "ui-droppable-active": "ui-state-active",
                "ui-droppable-hover": "ui-state-hover"
            },
            drop: function (event, ui) {
                var $this = $(this);
                var widgetId = ui.draggable.data("newWidget");
                $.ajax({
                    url: "builder/widget",
                    data: {
                        widget: widgetId
                    },
                    dataType: "html",
                    method: "POST",
                    success: function (response) {
                        $this.append(response);
                    }
                })
            }
        });
    }

    function saveBuilder() {
        showLoading();
        // get all layouts
        var layouts = $("[data-layout]");
        var layoutWeight = 0;
        var content = [];
        layouts.each(function () {

            // get all widgets
            var widgets = $(this).find("[data-widget]");
            var widgetData = [];
            var widgetWeight = 0;
            widgets.each(function () {
                var widgetObj = {
                    widgetId: $(this).data("widget"),
                    type: $(this).data("type"),
                    options: $(this).data("options"),
                    active: true,
                    weight: widgetWeight++,
                    position: $(this).closest("[data-column]").data("column")
                };

                widgetData.push(widgetObj);
            });

            // create layout object
            var obj = {
                layoutId: $(this).data("layout"),
                layoutTypeId: $(this).data("type"),
                weight: layoutWeight++,
                options: $(this).data("options"),
                widgets: widgetData
            };

            content.push(obj);
        });

        var form = $("#page-basic-data")[0];
        var formData = new FormData(form);
        formData.append("id", $("#page-id").text());
        formData.append("content", JSON.stringify(content));
        formData.append("deletedWidgets", deletedWidgets);
        formData.append("deletedLayouts", deletedLayouts);

        // execute save
        $.ajax({
            url: $("#page-basic-data").attr("action"),
            method: "POST",
            dataType: "json",
            processData: false,
            contentType: false,
            data: formData,
            complete: hideLoading,
            success: function (response) {
                if (response.redirect) {
                    window.location = response.redirect;
                }
                else {
                    $("[data-panel]").html($(response.html));
                    initialize();
                }
            }
        })
    }

    function showLoading() {
        $("body").addClass("loading");
    }

    function hideLoading() {
        setTimeout(
            function () {
                $("body").delay(1000).removeClass("loading");
            },
            2000
        );
    }


});