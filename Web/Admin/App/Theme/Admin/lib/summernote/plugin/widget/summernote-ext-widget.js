(function (factory) {
    /* global define */
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node/CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(window.jQuery);
    }
}(function ($) {

    // pull in some summernote core functions
    var ui = $.summernote.ui;
    var dom = $.summernote.dom;

    // define the popover plugin
    var WidgetPlugin = function (context) {
        var self = this;
        var options = context.options;
        var lang = options.langInfo;

        self.icon = '<i class="fa fa-square-o"/>';

        // add context menu button for dialog
        context.memo('button.widget', function () {
            return ui.button({
                contents: self.icon,
                tooltip: lang.widget.insert,
                click: context.createInvokeHandler('widget.showDialog')
            }).render();
        });

        // add popover edit button
        context.memo('button.widgetDialog', function () {
            return ui.button({
                contents: self.icon,
                tooltip: lang.widget.edit,
                click: context.createInvokeHandler('widget.showDialog')
            }).render();
        });

        self.events = {
            'summernote.init': function (we, e) {
                // update existing containers
                $('div.ext-widget', e.editable).each(function () {
                    self.setContent($(this));
                });
                // TODO: make this an undo snapshot...
            },
            'summernote.keyup summernote.mouseup summernote.change summernote.scroll': function () {
                self.update();
            },
            'summernote.dialog.shown': function () {
                self.hidePopover();
            }
        };

        self.initialize = function () {
            // create dialog markup
            var $container = options.dialogsInBody ? $(document.body) : context.layoutInfo.editor;

            var body = '<div class="form-group row-fluid">' +
                '<label>' + lang.widget.testLabel + '</label>' +
                '<select class="ext-widget-test form-control">' +
                '</select>' +
                '</div>';
            var footer = '<button href="#" class="btn btn-primary ext-widget-save">' + lang.widget.insert + '</button>';

            self.$dialog = ui.dialog({
                title: lang.widget.name,
                fade: options.dialogsFade,
                body: body,
                footer: footer
            }).render().appendTo($container);

            // create popover
            self.$popover = ui.popover({
                className: 'ext-widget-popover'
            }).render().appendTo('body');
            var $content = self.$popover.find('.popover-content');

            context.invoke('buttons.build', $content, options.popover.widget);
        };

        self.destroy = function () {
            self.$popover.remove();
            self.$popover = null;
            self.$dialog.remove();
            self.$dialog = null;
        };

        self.update = function () {
            // Prevent focusing on editable when invoke('code') is executed
            if (!context.invoke('editor.hasFocus')) {
                self.hidePopover();
                return;
            }

            var rng = context.invoke('editor.createRange');
            var visible = false;

            if (rng.isOnData()) {
                var $data = $(rng.sc).closest('div.ext-widget');

                if ($data.length) {
                    var pos = dom.posFromPlaceholder($data[0]);

                    self.$popover.css({
                        display: 'block',
                        left: pos.left,
                        top: pos.top
                    });

                    // save editor target to let size buttons resize the container
                    context.invoke('editor.saveTarget', $data[0]);

                    visible = true;
                }

            }

            // hide if not visible
            if (!visible) {
                self.hidePopover();
            }

        };

        self.hidePopover = function () {
            self.$popover.hide();
        };

        // define plugin dialog
        self.getInfo = function () {
            var rng = context.invoke('editor.createRange');

            if (rng.isOnData()) {
                var $data = $(rng.sc).closest('data.ext-widget');

                if ($data.length) {
                    // Get the first node on range(for edit).
                    return {
                        node: $data,
                        widgetId: $data.attr('data-widget-id')
                    };
                }
            }

            return {};
        };

        self.setContent = function ($node) {
            $node.html('<p contenteditable="false">Widget #' +
                $node.attr('data-widget-id') + '</p>');
        };

        self.updateNode = function (info) {
            self.setContent(info.node
                .attr('data-widget-id', info.widgetId));
        };

        self.createNode = function (info) {
            var $node = $('<div class="ext-widget" data-partial-load="' + info.widgetId + '" data-widget-id="' + info.widgetId + '"></div>');

            if ($node) {
                // save node to info structure
                info.node = $node;
                // insert node into editor dom
                context.invoke('editor.insertNode', $node[0]);
            }

            return $node;
        };

        self.showDialog = function () {
            var widgets = $("#widgets-area-holder").find(".widget-holder");
            var selects = [];
            widgets.each(function () {
                selects.push($("<option>").attr("value", $(this).data("widgetId")).text($(this).text()));
            });

            self.$dialog.find('.ext-widget-test').html("").append(selects);

            var info = self.getInfo();
            var newNode = !info.node;
            context.invoke('editor.saveRange');

            self
                .openDialog(info)
                .then(function (dialogInfo) {
                    // [workaround] hide dialog before restore range for IE range focus
                    ui.hideDialog(self.$dialog);
                    context.invoke('editor.restoreRange');

                    // update info with dialog info
                    $.extend(info, dialogInfo);

                    // insert a new node
                    if (newNode) {
                        self.createNode(info);
                    }

                    self.updateNode(info);
                })
                .fail(function () {
                    context.invoke('editor.restoreRange');
                });

        };

        self.openDialog = function (info) {
            return $.Deferred(function (deferred) {
                var $inpTest = self.$dialog.find('.ext-widget-test');
                var $saveBtn = self.$dialog.find('.ext-widget-save');
                var onKeyup = function (event) {
                    if (event.keyCode === 13) {
                        $saveBtn.trigger('click');
                    }
                };

                ui.onDialogShown(self.$dialog, function () {
                    context.triggerEvent('dialog.shown');

                    $inpTest.val(info.widgetId).on('change', function () {
                        ui.toggleBtn($saveBtn, $inpTest.val());
                    }).trigger('focus').on('keyup', onKeyup);

                    $saveBtn
                        .text(info.node ? lang.widget.edit : lang.widget.insert)
                        .click(function (event) {
                            event.preventDefault();

                            deferred.resolve({widgetId: $inpTest.val()});
                        });

                    // init save button
                    ui.toggleBtn($saveBtn, $inpTest.val());
                });

                ui.onDialogHidden(self.$dialog, function () {
                    $inpTest.off('input keyup');
                    $saveBtn.off('click');

                    if (deferred.state() === 'pending') {
                        deferred.reject();
                    }
                });

                ui.showDialog(self.$dialog);
            });
        };
    };

    // Extends summernote
    $.extend(true, $.summernote, {
        plugins: {
            widget: WidgetPlugin
        },

        options: {
            popover: {
                widget: [
                    ['widget', ['widgetDialog']]
                ]
            }
        },

        // add localization texts
        lang: {
            'en-US': {
                widget: {
                    name: 'Choose widget',
                    insert: 'Insert widget',
                    edit: 'Edit widget',
                    testLabel: ''
                }
            }
        }

    });

}));
