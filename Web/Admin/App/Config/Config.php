<?php

class Config {
    const maintenanceMode = false;
    const BoxesLimit = 9;
	const debugMode = DEBUG_MODE_ADMIN;
	const useCache = USE_CACHE_ADMIN;

}