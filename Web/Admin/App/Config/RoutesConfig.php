<?php

/**
 * Class RoutesConfig
 */
class RoutesConfig {

	/**
	 * @return RouteDTO[]
	 */
	public static function GetRoutes() {
		$routes = array(
			"home" => new RouteDTO("home", "Home", "Index"),

			// Maintenance
			"install-database" => new RouteDTO("install/database", "Install", "Database"),

			// Security
			"login" => new RouteDTO("login", "Security", "Login"),
			"request-access" => new RouteDTO("request-access", "Security", "RequestAccess"),
			"reset-password" => new RouteDTO("reset-password", "Security", "ResetPassword"),
			"reset-password-successful" => new RouteDTO("reset-password/sent", "Security", "ResetPasswordSuccessful"),
			"reset-password-confirm" => new RouteDTO("reset-password/confirm/{token:string}", "Security", "ResetPasswordConfirm"),
			"change-password" => new RouteDTO("change-password", "Security", "ChangePassword"),
			"logout" => new RouteDTO("logout", "Security", "Logout"),

			// User
			"user-list" => new RouteDTO("users/list", "Users", "List"),
			"user-blocked-list" => new RouteDTO("users/list/blocked", "Users", "BlockedUsers"),
			"not-approved-user-list" => new RouteDTO("not-approved-users/list", "Users", "NotApprovedUsersList"),
			"user-create" => new RouteDTO("users/create", "Users", "Create"),
			"user-delete" => new RouteDTO("users/delete/{username:string}", "Users", "Delete"),
			"user-update-details" => new RouteDTO("user/update/{username:string}", "Users", "UpdateDetails"),
			"logged-user-update-details" => new RouteDTO("logged-user/update", "Users", "UpdateLoggedUserDetails"),
			"current-user-update-details" => new RouteDTO("user/current-user-update-details", "Users", "UpdateCurrentUserDetails"),
			"user-update-picture" => new RouteDTO("user/update-picture", "Users", "UpdatePicture"),
			"registration" => new RouteDTO("registration", "Users", "Register"),
			"registration-confirm" => new RouteDTO("registration/confirm/{token:string}", "Users", "RegistrationConfirmed"),
			"registration-link-sent" => new RouteDTO("registration-link/sent", "Users", "RegistrationLinkSent"),
			"registration-successful" => new RouteDTO("registration-successful", "Users", "RegistrationSuccessful"),
			"registration-unsuccessful" => new RouteDTO("registration-unsuccessful", "Users", "RegistrationUnsuccessful"),


			//Slides
			"slide-list" => new RouteDTO("slide/list", "Slides", "List"),
			"slide-create" => new RouteDTO("slide/create", "Slides", "Create"),
			"slide-delete" => new RouteDTO("slide/delete/{slideId:integer}", "Slides", "Delete"),
			"slide-update" => new RouteDTO("slide/update/{slideId:integer}", "Slides", "UpdateSlide"),
			"update-slide-weight" => new RouteDTO("weight-slides", "Slides", "UpdateWeight"),

			//Ads
			"ad-list" => new RouteDTO("ad/list", "Ads", "Ads"),
			"ad-create" => new RouteDTO("ad/create", "Ads", "Create"),
			"ad-delete" => new RouteDTO("ad/delete/{adId:integer}", "Ads", "Delete"),
			"ad-update" => new RouteDTO("ad/update/{adId:integer}", "Ads", "UpdateAd"),


			//Social Icons
			"social-icon-list" => new RouteDTO("social-icon/list", "SocialIcons", "List"),
			"social-icon-create" => new RouteDTO("social-icon/create", "SocialIcons", "Create"),
			"social-icon-delete" => new RouteDTO("social-icon/delete/{socialIconId:integer}", "SocialIcons", "Delete"),
			"social-icon-update" => new RouteDTO("social-icon/update/{socialIconId:integer}", "SocialIcons", "Update"),
			"update-social-icon-weight" => new RouteDTO("weight-social-icons", "SocialIcons", "UpdateWeight"),


			//Roles
			"role-list" => new RouteDTO("roles/list", "Roles", "List"),
			"role-create" => new RouteDTO("roles/create", "Roles", "Create"),
			"role-update" => new RouteDTO("role/update/{roleId:integer}", "Roles", "Update"),
			"role-delete" => new RouteDTO("role/delete/{roleId:integer}", "Roles", "Delete"),

			//Media
			"media-manager" => new RouteDTO("media/manager", "Media", "MediaManager"),
			"media-upload" => new RouteDTO("media/upload", "Media", "Upload"),
			"media-delete" => new RouteDTO("media/delete", "Media", "Delete"),
			"media-batch-delete" => new RouteDTO("media/delete/batch", "Media", "BatchDelete"),
			"media-edit" => new RouteDTO("media/edit", "Media", "Edit"),
			"media-gallery-widget-browse" => new RouteDTO("media/gallery-widget/browse", "Media", "GalleryWidgetBrowse"),
			"media-people-widget-browse" => new RouteDTO("media/people-widget/browse", "Media", "PeopleWidgetBrowse"),
			"media-gallery-event-browse" => new RouteDTO("media/event-widget/browse", "Media", "EventWidgetBrowse"),
			"media-vimeo-videos" => new RouteDTO("media/vimeo/videos", "Media", "Vimeo"),
			"media-thumbnail-crop" => new RouteDTO("media/crop-thumbnail/{id:integer}/{size:string}", "Media", "ThumbnailCrop"),

			//Menus
			"menus-list" => new RouteDTO("menus/list", "Menus", "List"),
			"menus-create" => new RouteDTO("menus/create", "Menus", "Create"),
			"menus-update" => new RouteDTO("menus/update/{id:integer}", "Menus", "Update"),
			"menus-delete" => new RouteDTO("menus/delete/{id:integer}", "Menus", "Delete"),
			"menus-create-item" => new RouteDTO("menus/create-item/{menuId:integer}", "Menus", "CreateItem"),
			"menus-update-item" => new RouteDTO("menus/update-item/{menuItemId:integer}", "Menus", "UpdateItem"),
			"menus-delete-item" => new RouteDTO("menus/delete-item/{id:integer}", "Menus", "DeleteItem"),
			"menu-item-type-submenu" => new RouteDTO("menus/item-type-submenu", "Menus", "ItemTypeSubmenu"),
			"update-menu-item-weight" => new RouteDTO("weight-menu-items", "Menus", "UpdateWeight"),

			//Pages
			"pages-list" => new RouteDTO("pages/list", "Pages", "List"),
			"pages-create" => new RouteDTO("pages/create", "Pages", "Create"),
			"pages-delete" => new RouteDTO("pages/delete/{id:integer}", "Pages", "Delete"),
			"pages-update" => new RouteDTO("pages/update/{id:integer}", "Pages", "Update"),
			"pages-thumbnail-crop" => new RouteDTO("pages/crop-thumbnail/{id:integer}/{size:string}", "Pages", "ThumbnailCrop"),

			// Builder
			"page-builder-create" => new RouteDTO("builder", "Builder", "Builder"),
			"page-builder" => new RouteDTO("builder/{id:integer}", "Builder", "Builder"),
			"page-builder-layout" => new RouteDTO("builder/layout", "Builder", "Layout"),
			"page-builder-widget" => new RouteDTO("builder/widget", "Builder", "Widget"),
			"page-builder-save" => new RouteDTO("builder/save", "Builder", "Save"),
			"page-builder-get" => new RouteDTO("builder/get", "Builder", "Page"),
			"page-builder-copy" => new RouteDTO("builder/copy", "Builder", "CopyStructure"),
			"page-builder-duplicate" => new RouteDTO("builder/duplicate/{id:integer}", "Builder", "Duplicate"),

			// Widgets
			"layout-options" => new RouteDTO("layouts/options", "Builder", "LayoutOptions"),

			"widget-options" => new RouteDTO("widgets/options", "Pages", "WidgetOptions"),
			"widget-save" => new RouteDTO("widgets/save", "Pages", "WidgetSave"),
			"widget-delete" => new RouteDTO("widgets/delete", "Pages", "WidgetDelete"),
			"widget-reorder" => new RouteDTO("widgets/reorder", "Pages", "WidgetReorder"),


			// Maintenance
			"regenerate-thumbs" => new RouteDTO("maintenance/resize-thumbs", "Maintenance", "RegenerateThumbnails"),
			"regenerate-one-thumb" => new RouteDTO("maintenance/regenerate-one-thumb", "Maintenance", "RegenerateOneThumb"),

			// Coaches
			"coaches-list" => new RouteDTO("coaches/list", "Coaches", "List"),
			"coaches-create" => new RouteDTO("coaches/create", "Coaches", "Create"),
			"coaches-update" => new RouteDTO("coaches/update/{id:integer}", "Coaches", "Update"),
			"coaches-delete" => new RouteDTO("coaches/delete/{id:integer}", "Coaches", "Delete"),
			"coaches-thumbnail-crop" => new RouteDTO("coaches/crop-thumbnail/{id:integer}/{size:string}", "Coaches", "ThumbnailCrop"),

			// Categories
			"categories-list" => new RouteDTO("categories/list", "Categories", "List"),
			"categories-create" => new RouteDTO("categories/create", "Categories", "Create"),
			"categories-update" => new RouteDTO("categories/update/{id:integer}", "Categories", "Update"),
			"categories-delete" => new RouteDTO("categories/delete/{id:integer}", "Categories", "Delete"),
			"categories-thumbnail-crop" => new RouteDTO("categories/crop-thumbnail/{id:integer}/{size:string}", "Categories", "ThumbnailCrop"),

			// BasketballSchools
			"basketballSchools-list" => new RouteDTO("basketballSchools/list", "BasketballSchools", "List"),
			"basketballSchools-create" => new RouteDTO("basketballSchools/create", "BasketballSchools", "Create"),
			"basketballSchools-update" => new RouteDTO("basketballSchools/update/{id:integer}", "BasketballSchools", "Update"),
			"basketballSchools-delete" => new RouteDTO("basketballSchools/delete/{id:integer}", "BasketballSchools", "Delete"),
			"basketballSchools-thumbnail-crop" => new RouteDTO("basketballSchools/crop-thumbnail/{id:integer}/{size:string}", "BasketballSchools", "ThumbnailCrop"),

			// Sponsors
			"sponsors-list" => new RouteDTO("sponsors/list", "Sponsors", "List"),
			"sponsors-create" => new RouteDTO("sponsors/create", "Sponsors", "Create"),
			"sponsors-update" => new RouteDTO("sponsors/update/{id:integer}", "Sponsors", "Update"),
			"sponsors-delete" => new RouteDTO("sponsors/delete/{id:integer}", "Sponsors", "Delete"),
			"sponsors-thumbnail-crop" => new RouteDTO("sponsors/crop-thumbnail/{id:integer}/{size:string}", "Sponsors", "ThumbnailCrop"),

			// Sponsor Groups
			"sponsorGroups-list" => new RouteDTO("sponsorGroups/list", "SponsorGroups", "List"),
			"sponsorGroups-create" => new RouteDTO("sponsorGroups/create", "SponsorGroups", "Create"),
			"sponsorGroups-update" => new RouteDTO("sponsorGroups/update/{id:integer}", "SponsorGroups", "Update"),
			"sponsorGroups-delete" => new RouteDTO("sponsorGroups/delete/{id:integer}", "SponsorGroups", "Delete"),

			// Posts
			"posts" => new RouteDTO("posts", "Posts", "Posts"),
			"create-post" => new RouteDTO("create/posts", "Posts", "Create"),
			"delete-post" => new RouteDTO("delete/post/{id:integer}", "Posts", "Delete"),
			"update-post" => new RouteDTO("update/post/{id:integer}", "Posts", "Update"),
			"posts-thumbnail-crop" => new RouteDTO("posts/crop-thumbnail/{id:integer}/{size:string}", "Posts", "ThumbnailCrop"),
			"single-post" => new RouteDTO("posts/{slug:string}", "Posts", "ThumbnailCrop"),

			// Blog
			"blogs" => new RouteDTO("blogs", "Blog", "Blogs"),
			"create-blog" => new RouteDTO("create/blogs", "Blog", "Create"),
			"delete-blog" => new RouteDTO("delete/blog/{id:integer}", "Blog", "Delete"),
			"update-blog" => new RouteDTO("update/blog/{id:integer}", "Blog", "Update"),
			"blogs-thumbnail-crop" => new RouteDTO("blogs/crop-thumbnail/{id:integer}/{size:string}", "Blog", "ThumbnailCrop"),

			// Bookings
			"bookings" => new RouteDTO("bookings", "Bookings", "Bookings"),

		);
		return $routes;
	}

}