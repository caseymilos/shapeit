$("[data-partial-load]").each(function () {
    var $this = $(this);
    $this.html('<div class="text-center"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>');

    $.ajax({
        url: $this.data('partialLoad'),
        success: function (content) {
            $this.html(content);
        }
    });

});