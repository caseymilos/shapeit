<?php


try {

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    include('../../../GlobalConfig.php');
    include('../../../GlobalAutoload.php');
    include('Components/Autoload.php');

    Session::Start();

    $router = new Router();
    $router->RenderPage();
} catch (MVCException $e) {
    $e->DisplayError();
} catch (Exception $e) {
    var_dump($e); die();
    $handler = new GeneralException();
    $handler->DisplayError();
} finally {
    if (isset($e) && $e instanceof \Exception) {
        // \Business\Helper\LogHelper::Log($e);
    }
}