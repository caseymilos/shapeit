<?php


class AccessDeniedException extends MVCException {
    public function DisplayError() {
        HtmlHelper::RenderView("Errors/AccessDenied");
    }
}