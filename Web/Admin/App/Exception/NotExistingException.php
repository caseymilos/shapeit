<?php

class NotExistingException extends MVCException {

    public function DisplayError() {
        HtmlHelper::RenderView("Errors/NotExisting");
    }
}