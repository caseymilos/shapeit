<?php


class GeneralException extends MVCException {

    public function DisplayError() {
        HtmlHelper::RenderView("Errors/General");
    }
}