<?php

use Business\Enums\LanguageCodesEnum;

class MainHelper {

    public static function GetLanguage() {
        global $router;
        return LanguageCodesEnum::GetConstant($router->Language);
    }

}