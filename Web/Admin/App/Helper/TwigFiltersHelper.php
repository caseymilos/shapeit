<?php

require_once 'Lib/Twig/Autoloader.php';

class TwigFiltersHelper extends Twig_Extension {

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('indexes', array($this, 'GetIndexes')),
        );
    }

    public function GetIndexes($items, $property) {
        $result = [];
        if (count($items) > 0) {
            foreach ($items as $item) {
                $result[] = $item->{$property};
            }
        }

        return $result;
    }

    public function getName() {
        return 'gdev_twig_filters';
    }

}