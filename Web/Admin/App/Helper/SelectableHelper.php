<?php

use Business\ApiControllers\AdsApiController;
use Business\ApiControllers\ArtistsApiController;
use Business\ApiControllers\BlogsApiController;
use Business\ApiControllers\EventsApiController;
use Business\ApiControllers\PostsApiController;
use Business\ApiControllers\PagesApiController;
use Business\Enums\AdPositionsEnum;
use Business\Enums\BuilderLayoutsEnum;
use Business\Enums\ColorsEnum;
use Business\Enums\GendersEnum;
use Business\Enums\LanguagesEnum;
use Business\Enums\MenuItemTypesEnum;
use Business\Enums\MenuPositionsEnum;
use Business\Enums\OfferTypesEnum;
use Business\Enums\PostTypesEnum;
use Business\Enums\TypographyEnum;
use Business\Enums\UserStatusTypesEnum;
use Business\Enums\WidgetsEnum;
use Data\Repositories\AuthorsRepository;
use Gdev\MenuManagement\ApiControllers\MenuItemsApiController;
use Gdev\UserManagement\ApiControllers\PermissionsApiController;
use Gdev\UserManagement\ApiControllers\RolesApiController;

/**
 * Created by PhpStorm.
 * User: Nino
 * Date: 15.6.2016
 * Time: 14:07
 */
class SelectableHelper {

	public static function UserRoles() {
		$items = [];
		$roleDescriptions = RolesApiController::GetRoles();
		foreach ($roleDescriptions as $roleDescription) {
			$items[$roleDescription->RoleId] = $roleDescription->Name;
		}
		return $items;
	}

	public static function Permissions() {
		$items = [];
		$permissions = PermissionsApiController::GetPermissions();
		foreach ($permissions as $permission) {
			$items[$permission->PermissionId] = $permission->Description;
		}
		return $items;
	}

	public static function UserStatusTypes() {
		$items = [];
		foreach (UserStatusTypesEnum::enum() as $key => $value) {
			$items[$value] = UserStatusTypesEnum::Description($value);
		}
		return $items;
	}

	public static function PostTypes() {
		$items = [];
		foreach (PostTypesEnum::enum() as $key => $value) {
			$items[$value] = PostTypesEnum::Description($value);
		}
		return $items;
	}

	public static function BuilderLayouts() {
		$items = [];
		foreach (BuilderLayoutsEnum::enum() as $key => $value) {
			$items[$value] = BuilderLayoutsEnum::Description($value);
		}
		return $items;
	}

	public static function Widgets() {
		$items = [];
		foreach (WidgetsEnum::enum() as $key => $value) {
			$items[$value] = WidgetsEnum::Description($value);
		}
		return $items;
	}

	public static function Typography() {
		$items = [];
		foreach (TypographyEnum::enum() as $key => $value) {
			$items[$value] = TypographyEnum::Description($value);
		}
		return $items;
	}


	public static function MenuPositions() {
		$items = [];
		foreach (MenuPositionsEnum::enum() as $key => $value) {
			$items[$value] = MenuPositionsEnum::Description($value);
		}

		return $items;
	}

	public static function MenuItemTypes() {
		$items = [];
		foreach (MenuItemTypesEnum::enum() as $key => $value) {
			$items[$value] = MenuItemTypesEnum::Description($value);
		}

		return $items;
	}

	public static function MenuItemTargets() {
		$items = [
			"_self" => "Same window",
			"_blank" => "New window"
		];
		return $items;
	}

	public static function Menus() {
		$menuItems = \Gdev\MenuManagement\ApiControllers\MenusApiController::GetMenus();

		$items = [];
		foreach ($menuItems as $value) {
			$items[$value->MenuId] = $value->Alias;
		}

		return $items;
	}

	public static function MenuItems($menuId) {
		$menuItems = MenuItemsApiController::GetMenuItems($menuId);

		$items = [];
		foreach ($menuItems as $value) {
			$items[$value->MenuItemId] = $value->Caption;
		}

		return $items;
	}

	public static function Languages() {
		$items = [];
		foreach (LanguagesEnum::enum() as $key => $value) {
			$items[$value] = LanguagesEnum::Description($value);
		}

		return $items;
	}


	public static function Pages() {
		$items = [];
		$data = PagesApiController::GetPages();
		if (count($data) > 0) {
			foreach ($data as $item) {
				$desc = $item->GetDescription(MainHelper::GetLanguage());
				if (is_null($desc)) {
					$title = "- no title -";
				} else {
					$title = $item->GetDescription(MainHelper::GetLanguage())->Title;
				}
				$items[$item->PageId] = $title;
			}
		}

		return $items;
	}


	public static function Templates() {
		return [
			'default' => 'Default',
		];
	}

	public static function HeaderStyles() {
		return [
			'default' => 'Default',
			'semi-transparent' => 'Semi Transparent',
			'transparent' => 'Transparent',
			'no-header' => 'No Header',
		];
	}


	public static function Categories() {
		$categories = \Business\ApiControllers\CategoriesApiController::GetCategories();
		$result = [];

		foreach ($categories as $category) {
			$result[$category->CategoryId] = $category->GetDetails(MainHelper::GetLanguage())->Name;
		}

		return $result;
	}

	public static function Posts() {
		$posts = PostsApiController::GetPosts();
		$result = [];

		foreach ($posts as $post) {
			$result[$post->PostId] = $post->Title;
		}

		return $result;
	}
	public static function Blogs($excludedBlogs = []) {
		$blogs = BlogsApiController::GetBlogs($excludedBlogs);
		$result = [];

		foreach ($blogs as $blog) {
			$result[$blog->BlogId] = $blog->Title;
		}

		return $result;
	}

	public static function Ads() {
		$ads = AdsApiController::GetAds();
		$result = [];

		foreach ($ads as $ad) {
			$result[$ad->AdId] = $ad->Title;
		}

		return $result;
	}

	public static function AdPositions() {
        $items = [];
        foreach (AdPositionsEnum::enum() as $key => $value) {
            $items[$value] = AdPositionsEnum::Description($value);
        }

        return $items;
	}

	public static function SponsorGroups() {
		$items = [];
		$data = \Business\ApiControllers\SponsorsApiController::GetSponsorGroups();
		if (count($data) > 0) {
			foreach ($data as $item) {
				$items[$item->SponsorGroupId] = $item->GetDetails(MainHelper::GetLanguage())->Name;
			}
		}

		return $items;
	}


}