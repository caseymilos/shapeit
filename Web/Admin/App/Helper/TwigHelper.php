<?php
use Business\Enums\StaticTaskStatusesEnum;

/**
 * Created by PhpStorm.
 * User: nikola
 * Date: 8.5.16.
 * Time: 23.16
 */
class TwigHelper {

    public static function ActiveButton($controller, $action = false, $class = "active") {
        return HtmlHelper::ActiveButton($controller, $action, $class);
    }

    public static function IsLoggedIn() {
        return Security::GetCurrentUser() == false ? false : true;
    }

    public static function GetCurrentUser() {
        return Security::GetCurrentUser();
    }

    public static function Enum($enum, $value) {
        $type = "Business\\Enums" . '\\' . $enum;
        return $type::Description($value);
    }

    public static function EnumCaption($enum, $value) {
        $type = "Business\\Enums" . '\\' . $enum;
        $enum = new $type();
        return $enum->Caption($value);
    }

    public static function EnumConstant($enum, $constant) {
        $type = "Business\\Enums" . '\\' . $enum;
        return $type::GetConstant($constant);
    }

    public static function GetLanguage() {
        return MainHelper::GetLanguage();
    }

    public static function LayoutSubtitle($id) {
        return \Business\Enums\BuilderLayoutsEnum::Subtitle($id);
    }

}