<?php

class HtmlHelper {

    public static function ActiveButton($controller, $action = false, $class = "active") {
        global $router;
        if ($router->Controller === $controller && (!$action || $action === $router->Action)) {
            return $class;
        }

        return "";
    }

    /**
     * @param array $options A list of options. Key is used as value attribute, value is used as label
     * @param string $name
     * @param mixed $selectedValue
     * @param string $label Default (null) value label
     * @param array $attributes A list of attributes. Key is used as attribute name, value is used as attribute value
     * @param string $nullValue
     * @return string
     */
    public static function Select($options, $name, $selectedValue = null, $label = null, $attributes = [], $nullValue = "") {
        if (is_null($label)) {
            $label = "Select";
        }
        if (is_null($name)) {
            $name = "";
        }
        $select = sprintf("<select name='%s' class='form-control' ", $name);
        if (count($attributes) > 0) {
            foreach ($attributes as $key => $attribute) {
                $select .= sprintf('%s = "%s" ', $key, $attribute);
            }
        }
        $select .= ">";
        $select .= sprintf("<option value='%s'>%s</option>", $nullValue, $label);
        foreach ($options as $key => $option) {
            $select .= sprintf("<option value ='%s' %s >%s</option>", $key, ($selectedValue == $key ? 'selected = "selected"' : ''), $option);
        }
        $select .= "</select>";
        echo $select;
    }

    public static function SearchableSelect($options, $name, $selectedValue = null, $label = null, $attributes = [], $nullValue = "", $multiselect = false) {
        $multiple = "";
        $title = "";
        if (is_null($label)) {
            $label = "Select";
        }
        if (is_null($name)) {
            $name = "";
        }
        if ($multiselect) {
            if (substr($name, strlen($name) - 2, 2) !== "[]") {
                $name .= ("[]");
                $multiple = "multiple";
                $title = sprintf("data-placeholder='%s'", $label);
            }
        }
        $select = sprintf("<select name='%s' class='form-control select2' data-live-search='' %s %s ", $name, $multiple, $title);


        if (count($attributes) > 0) {
            foreach ($attributes as $key => $attribute) {
                $select .= sprintf("%s = '%s' ", $key, $attribute);
            }
        }
        $select .= ">";
        if (!$multiselect) {
            $select .= sprintf("<option value='%s'>%s</option>", $nullValue, $label);
        }
        foreach ($options as $key => $option) {

            if (!is_array($selectedValue)) {
                $select .= sprintf("<option value ='%s' %s >%s</option>", $key, ($selectedValue == $key ? 'selected = "selected"' : ''), $option);
            }
            else {
                $select .= sprintf("<option value ='%s' %s >%s</option>", $key, (in_array($key, $selectedValue) ? 'selected = "selected"' : ''), $option);

            }
        }
        $select .= "</select>";
        return $select;
    }

    public static function Radio($options, $name, $selectedValue = null, $attributes = [], $labelClass = null) {
        if (is_null($name)) {
            $name = "";
        }

        $attrStrings = [];
        if (count($attributes) > 0) {
            foreach ($attributes as $key => $attribute) {
                $attrStrings[] = sprintf('%s = "%s"', $key, $attribute);
            }
        }

        $inputs = [];
        foreach ($options as $key => $option) {
            $inputId = sprintf("radio-input-%s-%s", $name, $key);
            $inputs[] = sprintf('<label for="%s" class="rdiobox rdiobox-success %s"><input id="%s" type="radio" value="%s" name="%s" %s %s><span>%s</span></label>', $inputId, $labelClass, $inputId, $key, $name, ($selectedValue == $key ? 'checked' : ''), implode(" ", $attrStrings), $option);
        }

        return implode("\n", $inputs);
    }

    public static function Checkbox($name, $checked, $label = "", $value = 1, $inputClass = null, $labelClass = null, $attributes = []) {
        $attrStrings = [];
        if (count($attributes) > 0) {
            foreach ($attributes as $key => $attribute) {
                $attrStrings[] = sprintf('%s = "%s"', $key, $attribute);
            }
        }

        $id = uniqid();

        $checkbox = sprintf('
                     <label>%s</label>
                     <div>
                         <div class="toggle-wrapper">
                             <div class="toggle toggle-light %s" data-toggle-on="%s" data-toggle-checkbox="#%s" %s>
                             </div>
                             <input class="hidden" type="checkbox" name="%s" value="%s" %s id="%s" />
                         </div>
                     </div>', $label, $labelClass, $checked ? "true" : "false", $id, implode(" ", $attrStrings), $name, $value, $checked ? "checked = 'checked'" : "", $id);

        return $checkbox;
    }


    public static function RenderView($template, $parameters = [], $return = false, $theme = null) {
        require_once 'Lib/Twig/Autoloader.php';
        Twig_Autoloader::register();

        $loader = new Twig_Loader_Filesystem('View');
        $twig = new Twig_Environment($loader, array(
            'cache' => null
        ));

		$loader = new Twig_Loader_Filesystem('View');
		$twig = new Twig_Environment($loader, array(
			'cache' => Config::useCache === true ? "cache" : false,
			'auto_reload' => Config::useCache,
			'debug' => Config::debugMode
		));

		if (Config::debugMode === true) {
			$twig->addExtension(new Twig_Extension_Debug());
		}



		global $router;
        $twig->addGlobal('Router', $router);
        $twig->addGlobal('Config', \Business\Utilities\Config\Config::GetInstance());
        $twig->addGlobal('Helper', new TwigHelper());
        $twig->addGlobal('HtmlHelper', new HtmlHelper());
        $twig->addGlobal('SelectableHelper', new SelectableHelper());

        // custom filters
        $twig->addExtension(new TwigFiltersHelper());

        if (is_null($theme)) {
            $config = \Business\Utilities\Config\Config::GetInstance();
            $theme = $config->admin->theme;
        }

        $template = $twig->loadTemplate($theme . "/" . $template . '.twig');

        if ($return) {
            return $template->render($parameters);
        }

        echo $template->render($parameters);
        return true;

    }

} 