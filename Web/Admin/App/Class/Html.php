<?php


class Html {

    public static function RenderPartial($partial, $vars = array()) {
        require_once 'Lib/Twig/Autoloader.php';
        Twig_Autoloader::register();

        $loader = new Twig_Loader_Filesystem('View/Admin');
        $twig = new Twig_Environment($loader, array(
            'cache' => null
        ));

        global $router;
        $twig->addGlobal('Router', $router);
        $twig->addGlobal('Config', new Config());

        $template = $twig->loadTemplate($partial . '.twig');

        return $template->render($vars);
    }

} 