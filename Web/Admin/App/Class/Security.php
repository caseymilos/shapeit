<?php

class Security {

    public static function CheckPermissions(array $permissions) {
        $currentUser = static::GetCurrentUser();
        if ($currentUser && property_exists($currentUser, "Permissions")) {
            $userPermissions = static::GetCurrentUser()->Permissions;
        }
        else {
            $userPermissions = null;
        }

        if (!is_array($userPermissions)) {
            return false;
        }

        if (count(array_intersect($userPermissions, $permissions))) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * @return Business\DTO\CurrentUserDTO|bool
     */
    public static function GetCurrentUser() {
        return Session::Get("CurrentUser");
    }

    /**
     * @param Business\DTO\CurrentUserDTO $user
     */
    public static function SetCurrentUser($user) {
        Session::Set("CurrentUser", $user);
    }

} 