<?php
define("BASE_PATH", dirname(__FILE__));

if (is_readable(BASE_PATH . "/Composer/Bundles/autoload.php")) {
    include(BASE_PATH . "/Composer/Bundles/autoload.php");
}

spl_autoload_extensions(".php");
spl_autoload_register('autoloader');
spl_autoload_register('libAutoloader');


function autoloader($className) {
    $path = str_replace("\\", "/", BASE_PATH . "/" . $className . '.php');
    if (is_readable($path)) {
        include $path;
    }
}

function libAutoloader($className) {
    $path = str_replace("\\", "/", BASE_PATH . "/" . "Business/Lib/" . $className . '.php');
    if (is_readable($path)) {
        include $path;
    }

}
