<?php

namespace Data\Models;


use Business\Utilities\Config\Config;
use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class Media
 * @package Models
 *
 * @property integer MediaId
 * @property string Source
 * @property string Thumbnail
 * @property string Uri
 * @property string Copyrights
 * @property integer MediaTypeId
 * @property MediaType MediaType
 * @property \DateTime DateCreated
 * @property MediaDescription[] Descriptions
 */
class Media extends Entity {
    // Database Mapping
    protected static $table = "media";

    public static function fields() {
        return [
            "MediaId" => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            "Source" => ['type' => 'string', 'required' => true],
            "Uri" => ['type' => 'string'],
            "Copyrights" => ['type' => 'string'],
            "Thumbnail" => ['type' => 'string'],
            "MediaTypeId" => ['type' => 'integer', 'required' => true],
            "DateCreated" => ['type' => 'datetime', 'required' => true],
        ];
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity) {
        return [
            'Descriptions' => $mapper->hasMany($entity, 'Data\Models\MediaDescription', 'MediaId'),
            'MediaType' => $mapper->belongsTo($entity, 'Data\Models\MediaType', 'MediaTypeId'),
        ];
    }

    // Helper functions

    /**
     * @param integer $language
     * @return MediaDescription
     */
    public function GetDescription($language) {
        if (count($this->Descriptions) > 0) {
            foreach ($this->Descriptions as $desc) {
                if ($desc->LanguageId == $language) {
                    return $desc;
                }
            }
        }

        return null;
    }

    public function PictureSource($thumb = null) {
        $config = Config::GetInstance();

        if (!empty($this->Source)) {
            return sprintf("%sMedia/Media/%s%s", $config->cdn->url, empty($this->Source) ? "" : $thumb . "/", $this->Source);
        }
        return sprintf("%s/Content/media-default.png", $config->cdn->url);
    }

    public function SourceUrl() {
        $config = Config::GetInstance();

        if (!empty($this->Source)) {
            return sprintf("%sMedia/Media/%s", $config->cdn->url, $this->Source);
        }

        return "";
    }

    public function PicturePath($thumb = null) {
        return sprintf("%sMedia/Media/%s%s", CDN_PATH, empty($this->Source) ? "" : $thumb . "/", $this->Source);
    }


}
