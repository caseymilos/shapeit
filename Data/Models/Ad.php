<?php

namespace Data\Models;

use Business\Utilities\Config\Config;
use Gdev\UserManagement\Models\User;
use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class Ad
 * @package Data\Models
 * @property integer AdId
 *
 * @property string Title
 * @property string Code
 * @property integer Position
 */
class Ad extends Entity {

	protected static $table = "ads";

	public static function fields() {
		return [
			"AdId" => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
			"Title" => ['type' => 'text'],
			"Code" => ['type' => 'text'],
			"Position" => ['type' => 'integer'],
		];
	}
}