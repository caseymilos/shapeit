<?php

namespace Data\Models;

use Spot\Entity;

/**
 * Class Permission
 * @package Models
 *
 * @property integer PermissionId
 * @property string Caption
 */
class Permission extends Entity
{
	protected static $table = "permissions";

	public static function fields()
	{
		return [
			"PermissionId" => ['type' => 'integer', 'primary' => true],
			"Caption" => ['type' => 'string', 'required' => true]
		];
	}

}