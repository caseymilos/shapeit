<?php

namespace Data\Models;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class Layout
 * @package Models
 *
 * @property integer LayoutId
 * @property integer LayoutTypeId
 * @property integer Weight
 * @property object Options
 * @property integer PageDetailsId
 * @property Widget[] Widgets
 */
class Layout extends Entity {
    // Database Mapping
    protected static $table = "layouts";

    public static function fields() {
        return [
            "LayoutId" => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            "LayoutTypeId" => ['type' => 'integer'],
            "Weight" => ['type' => 'integer'],
            "PageDetailsId" => ['type' => 'integer'],
            "Options" => ['type' => 'json'],
        ];
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity) {
        return [
            'Widgets' => $mapper->hasMany($entity, 'Data\Models\Widget', 'LayoutId')->order(["Weight" => "ASC"]),
            'PageDetails' => $mapper->belongsTo($entity, 'Data\Models\PageDetails', 'PageDetailsId'),
        ];
    }

}
