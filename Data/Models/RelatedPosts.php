<?php

namespace Data\Models;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class PageDetails
 * @package Models
 *
 * @property integer RelatedPostsId
 * @property integer ParentId
 * @property integer ChildId
 */
class RelatedPosts extends Entity {
	// Database Mapping
	protected static $table = "related_posts";

	public static function fields() {
		return [
			"RelatedPostsId" => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
			"ParentId" => ['type' => 'integer', 'required' => true],
			"ChildId" => ['type' => 'integer', 'required' => true],
		];
	}

	public static function relations(MapperInterface $mapper, EntityInterface $entity) {
		return [
			'Parent' => $mapper->belongsTo($entity, 'Data\Models\Post', 'ParentId'),
			'Child' => $mapper->belongsTo($entity, 'Data\Models\Post', 'ChildId'),
		];
	}

}
