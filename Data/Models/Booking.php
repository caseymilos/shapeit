<?php

namespace Data\Models;

use Business\Utilities\Config\Config;
use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;


/**
 * Class Booking
 * @package Data\Models
 *
 * @property integer BookingId
 * @property Post Offer
 * @property integer OfferId
 * @property string FirstName
 * @property string LastName
 * @property string Email
 * @property integer Adults
 * @property string PhoneNumber
 * @property \DateTime DateFrom
 * @property \DateTime DateTo
 * @property string Message
 *
 */
class Booking extends Entity {

	protected static $table = "bookings";

	public static function fields() {
		return [
			"BookingId" => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
			"OfferId" => ['type' => 'integer'],
			"FirstName" => ['type' => 'string'],
			"LastName" => ['type' => 'string'],
			"Email" => ['type' => 'string'],
			"Adults" => ['type' => 'integer'],
			"PhoneNumber" => ['type' => 'string'],
			"DateFrom" => ['type' => 'date'],
			"DateTo" => ['type' => 'date'],
			"Message" => ['type' => 'string'],
		];
	}

	public static function relations(MapperInterface $mapper, EntityInterface $entity) {
		return [
			'Offer' => $mapper->belongsTo($entity, 'Data\Models\Post', 'OfferId'),
		];
	}

}