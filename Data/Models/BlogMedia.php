<?php

namespace Data\Models;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class PageDetails
 * @package Models
 *
 * @property integer BlogMediaId
 * @property integer BlogId
 * @property integer MediaId
 * @property Blog Blog
 * @property Media Media
 */
class BlogMedia extends Entity {
	// Database Mapping
	protected static $table = "blog_media";

	public static function fields() {
		return [
			"BlogMediaId" => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
			"BlogId" => ['type' => 'integer', 'required' => true],
			"MediaId" => ['type' => 'integer', 'required' => true],
		];
	}

	public static function relations(MapperInterface $mapper, EntityInterface $entity) {
		return [
			'Blog' => $mapper->belongsTo($entity, 'Data\Models\Blog', 'BlogId'),
			'Media' => $mapper->belongsTo($entity, 'Data\Models\Media', 'MediaId'),
		];
	}

}
