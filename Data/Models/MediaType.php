<?php

namespace Data\Models;


use Spot\Entity;

/**
 * Class MediaType
 * @package Models
 *
 * @property integer MediaTypeId
 * @property string Caption
 */
class MediaType extends Entity {
    // Database Mapping
    protected static $table = "media_types";

    public static function fields() {
        return [
            "MediaTypeId" => ['type' => 'integer', 'primary' => true],
            "Caption" => ['type' => 'string', 'required' => true]
        ];
    }
}
