<?php

namespace Data\Models;

use Business\Utilities\Config\Config;
use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class Blog
 * @package Data\Models
 * @property integer CommentId
 * @property integer PostId
 * @property integer UserId
 * @property string Comment
 * @property string Author
 * @property string AuthorEmail
 * @property string Picture
 * @property Post Post
 * @property \DateTime Date
 */
class Comment extends Entity {

	protected static $table = "comments";

	public static function fields() {
		return [
			"CommentId" => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
			"PostId" => ['type' => 'integer'],
			"UserId" => ['type' => 'integer'],
			"Comment" => ['type' => 'text'],
			"Author" => ['type' => 'string'],
			"AuthorEmail" => ['type' => 'string'],
			"Picture" => ['type' => 'string'],
			"Date" => ['type' => 'datetime'],

		];
	}

	public static function relations(MapperInterface $mapper, EntityInterface $entity) {
		return [
			'Post' => $mapper->belongsTo($entity, 'Data\Models\Post', 'PostId'),
		];
	}
}