<?php

namespace Data\Models;

use Business\Utilities\Config\Config;
use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class Page
 * @package Models
 *
 * @property integer PageId
 * @property bool Active
 * @property integer Padding
 * @property bool UseOverlay
 * @property bool ShowTitle
 * @property bool ShowBreadcrumbs
 * @property string Template
 * @property string HeaderStyle
 * @property string Style
 * @property string Picture
 * @property PageDetails[] Details
 */
class Page extends Entity {

    // Database Mapping
    protected static $table = "pages";

    public static function fields() {
        return [
            "PageId" => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            "Active" => ['type' => 'boolean', 'required' => true],
            "Template" => ['type' => 'string', 'required' => true],
            "Style" => ['type' => 'string'],
            "Picture" => ['type' => 'string'],
            "HeaderStyle" => ['type' => 'string'],
            "Padding" => ['type' => 'integer'],
            "UseOverlay" => ['type' => 'boolean'],
            "ShowTitle" => ['type' => 'boolean'],
            "ShowBreadcrumbs" => ['type' => 'boolean'],
        ];
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity) {
        return [
            'Details' => $mapper->hasMany($entity, 'Data\Models\PageDetails', 'PageId'),
        ];
    }

    // Helper functions

    /**
     * @param integer $language
     * @return PageDetails
     */
    public function GetDescription($language) {
        if (count($this->Details) > 0) {
            foreach ($this->Details as $det) {
                if ($det->LanguageId == $language) {
                    return $det;
                }
            }
        }

        return null;
    }


    public function PictureSource($thumb = null) {
        $config = Config::GetInstance();

        if (!empty($this->Picture)) {
            return sprintf("%sMedia/Pages/%s%s", $config->cdn->url, empty($this->Picture) ? "" : $thumb . "/", $this->Picture);
        }
        return sprintf("%s/Content/page-default.png", $config->cdn->url);
    }

    public function PicturePath($thumb = null) {
        return sprintf("%sMedia/Pages/%s%s", CDN_PATH, empty($this->Picture) ? "" : $thumb . "/", $this->Picture);
    }


}
