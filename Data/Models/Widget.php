<?php

namespace Data\Models;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class Widget
 * @package Models
 *
 * @property integer WidgetId
 * @property string Type
 * @property string Options
 * @property string Position
 * @property integer LayoutId
 * @property boolean Active
 * @property integer Weight
 * @property Layout Layout
 */
class Widget extends Entity {
    // Database Mapping
    protected static $table = "widgets";

    public static function fields() {
        return [
            "WidgetId" => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            "Type" => ['type' => 'integer', 'required' => true],
            "Position" => ['type' => 'string', 'required' => true],
            "Options" => ['type' => 'json'],
            "LayoutId" => ['type' => 'integer', 'required' => true],
            "Active" => ['type' => 'boolean'],
            "Weight" => ['type' => 'integer'],
        ];
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity) {
        return [
            'Layout' => $mapper->belongsTo($entity, 'Data\Models\Layout', 'LayoutId')
        ];
    }
}