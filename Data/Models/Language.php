<?php
/**
 * Created by PhpStorm.
 * User: Marko
 * Date: 27/09/2016
 * Time: 13:44
 */

namespace Data\Models;


use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

class Language extends Entity
{
    protected static $table = "languages";

    public static function fields() {
        return [
            "LanguageId" => ['type' => 'integer', 'primary' => true],
            "Caption" => ['type' => 'string', 'required' => true],
            "Code" => ['type' => 'string', 'required' => true]
        ];
    }
    public static function relations(MapperInterface $mapper, EntityInterface $entity) {
        return [

        ];
    }

}
