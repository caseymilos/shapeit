<?php

namespace Data\Models;

use Spot\Entity;

/**
 * Class PostType
 * @package Models
 *
 * @property integer PostTypeId
 * @property string Caption
 */
class PostType extends Entity
{
	protected static $table = "post_types";

	public static function fields()
	{
		return [
			"PostTypeId" => ['type' => 'integer', 'primary' => true],
			"Caption" => ['type' => 'string', 'required' => true]
		];
	}

}