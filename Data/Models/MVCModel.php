<?php
/**
 * Created by PhpStorm.
 * User: Nenad
 * Date: 25.7.2017.
 * Time: 10.02
 */

namespace Data\Models;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class MVCModel
 * @package Models
 * @property \DateTime CreatedAt
 * @property \DateTime UpdatedAt
 */
class MVCModel extends Entity
{
    const PICTURE_PATH = "";
    public static function fields()
    {
        return [
            "CreatedAt"   => ["type" => "datetime", "value" => new \DateTime()],
            "UpdatedAt"   => ["type" => "datetime", "value" => new \DateTime()]
        ];
    }

    public static function getPrimaryKey(){
        $fields = static::fields();
        $primaryKey = "";
        foreach ($fields as $key => $field) {
            if (isset($field["primary"]) && $field["primary"] == true) {
                $primaryKey = $key;
                break;
            }
        }
        return $primaryKey;
    }
    public function PictureUrl($thumb = null) {
        return sprintf("%s%s%s%s",CDN_URL,static::PICTURE_PATH,  empty($this->Image) ? "" : $thumb . "/", $this->Image);
    }

    public function PicturePath($thumb = null) {
        return sprintf("%s%s%s%s",CDN_PATH,static::PICTURE_PATH , empty($this->Image) ? "" : $thumb . "/", $this->Image);
    }
}
