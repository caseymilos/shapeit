<?php

namespace Data\Models;


use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class MediaDescription
 * @package Models
 *
 * @property integer MediaDescriptionId
 * @property integer MediaId
 * @property integer LanguageId
 * @property string Title
 * @property Media Media
 * @property Language Language
 */
class MediaDescription extends Entity {
    // Database Mapping
    protected static $table = "media_descriptions";

    public static function fields() {
        return [
            "MediaDescriptionId" => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            "MediaId" => ['type' => 'integer', 'required' => true, 'unique' => 'media_per_language'],
            "LanguageId" => ['type' => 'integer', 'required' => true, 'unique' => 'media_per_language'],
            "Title" => ['type' => 'string'],
        ];
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity) {
        return [
            'Media' => $mapper->belongsTo($entity, 'Data\Models\Media', 'MediaId'),
            'Language' => $mapper->belongsTo($entity, 'Data\Models\Language', 'LanguageId'),
        ];
    }


}