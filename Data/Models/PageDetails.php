<?php

namespace Data\Models;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class PageDetails
 * @package Models
 *
 * @property integer PageDetailsId
 * @property integer PageId
 * @property integer LanguageId
 * @property string Description
 * @property string Title
 * @property string Subtitle
 * @property string Slug
 * @property string Copyrights
 * @property Page Page
 * @property Language Language
 * @property Layout[] Layouts
 */
class PageDetails extends Entity {
// Database Mapping
    protected static $table = "page_details";

    public static function fields() {
        return [
            "PageDetailsId" => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            "PageId" => ['type' => 'integer', 'required' => true],
            "LanguageId" => ['type' => 'integer', 'required' => true, 'unique' => "slug_per_language"],
            "Description" => ['type' => 'text'],
            "Title" => ['type' => 'string', 'required' => true],
            "Subtitle" => ['type' => 'string'],
            "Copyrights" => ['type' => 'string'],
            "Slug" => ['type' => 'string', 'required' => true, 'unique' => "slug_per_language"],
        ];
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity) {
        return [
            'Page' => $mapper->belongsTo($entity, 'Data\Models\Page', 'PageId'),
            'Language' => $mapper->belongsTo($entity, 'Data\Models\Language', 'LanguageId'),
            'Layouts' => $mapper->hasMany($entity, 'Data\Models\Layout', 'PageDetailsId'),
        ];
    }

}
