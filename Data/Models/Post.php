<?php

namespace Data\Models;

use Business\Utilities\Config\Config;
use Gdev\UserManagement\Models\User;
use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class Post
 * @package Data\Models
 * @property integer PostId
 * @property integer PostTypeId
 *
 * @property string Title
 * @property string Theme
 * @property string Description
 * @property string Excerpt
 * @property integer AuthorId
 * @property User Author
 * @property Comment[] Comments
 * @property string Picture
 * @property string Slug
 * @property \DateTime Date
 */
class Post extends Entity {

	protected static $table = "posts";

	public static function fields() {
		return [
			"PostId" => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
			"Title" => ['type' => 'string'],
			"Theme" => ['type' => 'string'],
			"Description" => ['type' => 'text'],
			"Excerpt" => ['type' => 'text'],
			"AuthorId" => ['type' => 'integer'],
			"PostTypeId" => ['type' => 'integer'],
			"Picture" => ['type' => 'string'],
			"Date" => ['type' => 'datetime'],
			"Slug" => ['type' => 'string']
		];
	}

	public static function relations(MapperInterface $mapper, EntityInterface $entity) {
		return [
			'PostType' => $mapper->belongsTo($entity, 'Data\Models\PostType', 'PostTypeId'),
			'Author' => $mapper->belongsTo($entity, 'Gdev\UserManagement\Models\User', 'AuthorId'),
			'Comments' => $mapper->hasMany($entity, 'Data\Models\Comment', 'PostId'),
			'Children' => $mapper->hasManyThrough($entity, 'Data\Models\Post', '\Data\Models\RelatedPosts', "ChildId", "ParentId"),

		];
	}


	public function PictureSource($thumb = null) {
		$config = Config::GetInstance();

		if (!empty($this->Picture)) {
			return sprintf("%sMedia/Posts/%s%s", $config->cdn->url, empty($this->Picture) ? "" : $thumb . "/", $this->Picture);
		}
		return sprintf("%s/Content/post-default.png", $config->cdn->url);
	}

	public function PicturePath($thumb = null) {
		return sprintf("%sMedia/Posts/%s%s", CDN_PATH, empty($this->Picture) ? "" : $thumb . "/", $this->Picture);
	}

	public function GetShortDescription() {
		if (!empty($this->Excerpt)) {
			return $this->Excerpt;
		} else {
			preg_match("/(?:\w+(?:\W+|$)){0,15}/", strip_tags($this->Description), $matches);
			return $matches[0];
		}
	}

}