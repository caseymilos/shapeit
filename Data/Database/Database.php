<?php

namespace Data\Database;

use Business\Enums\CompletionTypesEnum;
use Business\Enums\DataTypesEnum;
use Business\Enums\GambitCategoriesEnum;
use Business\Enums\LanguageCodesEnum;
use Business\Enums\LanguagesEnum;
use Business\Enums\MediaTypesEnum;
use Business\Enums\MenuItemTypesEnum;
use Business\Enums\MenuPositionsEnum;
use Business\Enums\MessageStatusesEnum;
use Business\Enums\GambitTypesEnum;
use Business\Enums\LeadTypesEnum;
use Business\Enums\OfferTypesEnum;
use Business\Enums\OptionTypesEnum;
use Business\Enums\PermissionsEnum;
use Business\Enums\PostTypesEnum;
use Business\Enums\ThreadTypesEnum;
use Business\Enums\UserStatusTypesEnum;
use Business\Security\Crypt;
use Data\Models\CompletionType;
use Data\Models\DataType;
use Data\Models\GambitCategory;
use Data\Models\GambitType;
use Data\Models\Language;
use Data\Models\LeadType;
use Data\Models\MediaType;
use Data\Models\MessageStatus;
use Data\Models\PostType;
use Data\Models\OptionType;
use Data\Models\ThreadType;
use DateTime;
use Doctrine\DBAL\Types\Type;
use Gdev\MenuManagement\Models\MenuItemType;
use Gdev\MenuManagement\Models\MenuPosition;
use Gdev\UserManagement\ApiControllers\RolesApiController;
use Gdev\UserManagement\ApiControllers\UsersApiController;
use Gdev\UserManagement\ApiControllers\UserStatusesApiController;
use Gdev\UserManagement\Models\Permission;
use Gdev\UserManagement\Models\Role;
use Gdev\UserManagement\Models\User;
use Gdev\UserManagement\Models\RolePermission;
use Gdev\UserManagement\Models\UserDetails;
use Gdev\UserManagement\Models\UserRole;
use Gdev\UserManagement\Models\UserStatus;
use Gdev\UserManagement\Models\UserStatusType;
use Spot\Config;
use Spot\Entity;
use Spot\EntityInterface;
use Spot\Locator;
use stdClass;

class Database {

	private static $_databases = null;

	public static function getInstance($name = 'default') {
		if (static::$_databases == null) {
			static::_construct();
		}

		return static::$_databases[$name];
	}

	private static function _construct() {

		Type::addType('json', 'Data\Types\JsonType');
		// setting up default database
		$config = \Business\Utilities\Config\Config::GetInstance();

		$db = $config->db->default;

		$cfg = new Config();
		$adapter = $cfg->addConnection($db->type, [
			'dbname' => $db->dbname,
			'user' => $db->user,
			'password' => $db->pass,
			'host' => $db->host,
			'driver' => $db->driver
		]);

		/* Log SQL queries. Make sure logger is configured. */
		$logger = new \Doctrine\DBAL\Logging\DebugStack();
		$adapter->getConfiguration()->setSQLLogger($logger);

		static::$_databases['default'] = new Locator($cfg);


	}

	/**
	 * @param string $dir
	 * @param string $namespace
	 * @param \Spot\Locator $locator
	 * @param array $listOfExcludedClassArrays
	 */
	public static function migrateEntities($dir, $namespace, $locator, &$listOfExcludedClassArrays = []) {
		$contents = array_filter(scandir($dir), function ($entry) {
			return !in_array($entry, ['.', '..', "MVCModel.php"]);
		});

		foreach ($contents as $entry) {
			if (is_file($dir . DIRECTORY_SEPARATOR . $entry)) {
				$classname = $namespace . substr($entry, 0, -4);

				// prepare locator
				$reflection = new \ReflectionClass($classname);
				$constants = $reflection->getStaticProperties();
				$database = (array_key_exists("database", $constants)) ? $constants["database"] : "default";
				$locator = self::getInstance($database);


				//check if the class exists and is an Entity
				if (class_exists($classname) && is_subclass_of($classname, \Spot\Entity::class) && !in_array($classname, $listOfExcludedClassArrays)) {
					$parentClass = get_parent_class($classname);
					/*$entity=new $classname();
					$relations=$classname::relations($locator->mapper($classname), $entity);*/
					$locator->mapper($classname)->migrate();
					if ($parentClass != \Spot\Entity::class) {
						$listOfExcludedClassArrays[] = $parentClass;
						$listOfExtendedClasses = $classname;
					}
				}
			} else {
				//must be a subdirectory, so scan that too
				static::migrateEntities(
					$dir . DIRECTORY_SEPARATOR . $entry,
					$namespace . "{$entry}\\",
					$locator
				);
			}
		}
	}

	public static function install() {
		if (static::$_databases == null) {
			static::_construct();
		}

		$dirsWithLocators = [];

		// install default database
		$db = static::getInstance();

		$defaultDB = new stdClass();
		$defaultDB->Locator = $db;
		$defaultDB->Dir = [];
		$modelsDir = new stdClass();
		$modelsDir->Namespace = "Data\Models\\";
		$modelsDir->Path = ROOT_PATH . sprintf("%sData%sModels", DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR);
		$defaultDB->Dir[] = $modelsDir;
		$gdevDir = new stdClass();
		$gdevDir->Namespace = "Gdev\UserManagement\Models\\";
		$gdevDir->Path = ROOT_PATH . sprintf("%sComposer%sBundles%sgdev%suser-management%sModels", DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR);
		$defaultDB->Dir[] = $gdevDir;
		$gdevDir = new stdClass();
		$gdevDir->Namespace = "Gdev\MenuManagement\Models\\";
		$gdevDir->Path = ROOT_PATH . sprintf("%sComposer%sBundles%sgdev%smenu-management%sModels", DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR);
		$defaultDB->Dir[] = $gdevDir;
		$dirsWithLocators[] = $defaultDB;
		foreach ($dirsWithLocators as $dirWithLocator) {
			$listOfExcludedClasses = [];
			foreach ($dirWithLocator->Dir as $dir) {
				self::migrateEntities($dir->Path, $dir->Namespace, $dirWithLocator->Locator, $listOfExcludedClasses);
			}
		}

		// Adding lookup values
		$mapper = $db->mapper('Data\Models\Language');
		foreach (LanguagesEnum::enum() as $key => $value) {
			$model = $mapper->get($value);
			if (false == $model) {
				$model = new Language();
			}
			$model->LanguageId = $value;
			$model->Caption = LanguagesEnum::Description($value);
			$model->Code = LanguageCodesEnum::Description($value);
			$mapper->save($model);
		}
		unset($mapper);

		$mapper = $db->mapper('Data\Models\MediaType');
		foreach (MediaTypesEnum::enum() as $key => $value) {
			$model = $mapper->get($value);
			if (false == $model) {
				$model = new MediaType();
			}
			$model->MediaTypeId = $value;
			$model->Caption = MediaTypesEnum::Description($value);
			$mapper->save($model);
		}
		unset($mapper);

		$mapper = $db->mapper('Gdev\UserManagement\Models\Permission');
		foreach (PermissionsEnum::enum() as $key => $value) {
			$model = $mapper->get($value);
			if (false == $model) {
				$model = new Permission();
			}
			$model->PermissionId = $value;
			$model->Caption = PermissionsEnum::Description($value);
			$model->Description = PermissionsEnum::Description($value);
			$mapper->save($model);
		}
		unset($mapper);

		$mapper = $db->mapper('Gdev\UserManagement\Models\UserStatusType');
		foreach (UserStatusTypesEnum::enum() as $key => $value) {
			$model = $mapper->get($value);
			if (false == $model) {
				$model = new UserStatusType();
			}
			$model->UserStatusTypeId = $value;
			$model->Caption = UserStatusTypesEnum::Description($value);
			$mapper->save($model);
		}
		unset($mapper);

		$mapper = $db->mapper('Gdev\MenuManagement\Models\MenuPosition');
		foreach (MenuPositionsEnum::enum() as $key => $value) {
			$model = $mapper->get($value);
			if (false == $model) {
				$model = new MenuPosition();
			}
			$model->MenuPositionId = $value;
			$model->Caption = MenuPositionsEnum::Description($value);
			$mapper->save($model);
		}
		unset($mapper);

		$mapper = $db->mapper('Gdev\MenuManagement\Models\MenuItemType');
		foreach (MenuItemTypesEnum::enum() as $key => $value) {
			$model = $mapper->get($value);
			if (false == $model) {
				$model = new MenuItemType();
			}
			$model->MenuItemTypeId = $value;
			$model->Caption = MenuItemTypesEnum::Description($value);
			$mapper->save($model);
		}
		unset($mapper);

		$mapper = $db->mapper('Data\Models\PostType');
		foreach (PostTypesEnum::enum() as $key => $value) {
			$model = $mapper->get($value);
			if (false == $model) {
				$model = new PostType();
			}
			$model->PostTypeId = $value;
			$model->Caption = PostTypesEnum::Description($value);
			$mapper->save($model);
		}
		unset($mapper);

		// Create admin user
		$user = UsersApiController::GetUserByUserName("admin");
		if (false == $user) {
			$user = new User();
			$user->UserName = "admin";
			$user->Email = "admin@bizbot.dev";
			$user->RegistrationDate = new DateTime();
			$user->Password = Crypt::HashPassword("123456");
			$user->Approved = true;
			$user->Active = true;
			$userId = UsersApiController::InsertUser($user);

			// add details
			$userDetails = new UserDetails();
			$userDetails->UserId = $userId;
			$userDetails->FirstName = "BizBot";
			$userDetails->LastName = "Admin";
			$userDetails->DateOfBirth = new DateTime("30 years ago");
			UsersApiController::InsertUserDetails($userDetails);

			// add active status for admin
			$status = new UserStatus();
			$status->UserId = $userId;
			$status->UserStatusTypeId = UserStatusTypesEnum::Active;
			$status->DateFrom = new DateTime();
			$status->Message = "New Admin";
			UserStatusesApiController::InsertUserStatus($status);

			// add roles
			$role = new Role();
			$role->Name = "Administrator";
			$role->Active = 1;
			$role->Protected = 1;
			$roleId = RolesApiController::InsertRole($role);

			// add role permissions
			$mapper = $db->mapper('Gdev\UserManagement\Models\RolePermission');
			foreach (PermissionsEnum::enum() as $key => $value) {
				$rolePermission = $mapper->where(["PermissionId" => $value, "RoleId" => $roleId])->first();
				if (false == $rolePermission) {
					$rolePermission = new RolePermission();
				}
				$rolePermission->RoleId = $roleId;
				$rolePermission->PermissionId = $value;
				$rolePermission->Protected = true;
				$mapper->save($rolePermission);
			}
			unset($mapper);

			// add user roles
			$mapper = $db->mapper('Gdev\UserManagement\Models\UserRole');
			$userRole = $mapper->where(["UserId" => $userId, "RoleId" => $roleId])->first();
			if (false == $userRole) {
				$userRole = new UserRole();
			}
			$userRole->RoleId = $roleId;
			$userRole->UserId = $userId;
			$mapper->save($userRole);
			unset($mapper);
		}

	}

}