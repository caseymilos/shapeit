<?php

namespace Data\DataManagers;


use Data\Models\Media;
use Data\Models\MediaDescription;
use Data\Repositories\MediaDescriptionsRepository;
use Data\Repositories\MediaRepository;

class MediaDataManager {
    public static function GetAllMedia() {
        return MediaRepository::getInstance()->all()->with(["Descriptions"])->order(["DateCreated" => "DESC"]);
    }

    public static function GetMedia($mediaId) {
        return MediaRepository::getInstance()->get($mediaId);
    }

    public static function DeleteMedia($mediaId) {
        return MediaRepository::getInstance()->delete(["MediaId" => $mediaId]);
    }

    public static function SaveMedia(Media $media) {
        return MediaRepository::getInstance()->save($media);
    }

    public static function SaveDescription(MediaDescription $description) {
        return MediaDescriptionsRepository::getInstance()->save($description);
    }

    public static function GetGalleryMedia($ids) {
        return MediaRepository::getInstance()->all()->where(["MediaId" => $ids])->with(["Descriptions"])->execute();
    }

    public static function GetOne($id) {
        return MediaRepository::getInstance()->get($id);
    }

    public static function BatchDeleteMedia($ids) {
        return MediaRepository::getInstance()->delete(["MediaId" => $ids]);
    }

}