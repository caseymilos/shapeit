<?php

namespace Data\DataManagers;


use Data\Cache\MemcachedService;
use Data\Models\Page;
use Data\Models\PageDetails;
use Data\Repositories\PageDetailsRepository;
use Data\Repositories\PagesRepository;
use Spot\Entity\Collection;

class PagesDataManager {

    /**
     * @return Page[]
     */
    public static function GetPages() {
        return PagesRepository::getInstance()->all()->execute();
    }

    public static function GetPageBySlug($slug, $language = null) {
        $pageData = MemcachedService::getInstance()->get("PageDetails" . $slug . $language);

        if (is_array($pageData) && count($pageData) > 0) {
            $page = new Page($pageData);
            if (array_key_exists("Details", $pageData) && is_array($pageData["Details"]) && count($pageData["Details"]) > 0) {
                $pd = new Collection();
                foreach ($pageData["Details"] as $pageDetailsData) {
                    $pd->add(new PageDetails($pageDetailsData));
                }
                $page->Details = $pd;
            }
            return $page;

        }
        else {
            $conditions = ['Slug' => $slug];
            if (!is_null($language)) {
                $conditions['LanguageId'] = $language;
            }
            $pageDetails = PageDetailsRepository::getInstance()->where($conditions)->with(["Page"])->first();
            if ($pageDetails instanceof PageDetails) {
                // save to cache
                $page = $pageDetails->Page;
                $pd = new Collection();
                $pd->add($pageDetails);
                $page->Details = $pd;

                MemcachedService::getInstance()->set("PageDetails" . $slug . $language, $page->toArray());
                return $page;
            }
        }

        return false;
    }

    public static function CheckSlug($slug) {
        $query = "SELECT PageDetailsId FROM page_details WHERE Slug = :slug";
        return PageDetailsRepository::getInstance()->query($query, ['slug' => $slug])->first();
    }

    public static function TranslateSlug($slug, $languageId) {
        $query = "SELECT atd.* FROM page_details pd JOIN pages p ON p.PageId = pd.PageId AND pd.Slug = :slug INNER JOIN page_details atd ON atd.PageId = p.PageId WHERE atd.LanguageId = :languageId";
        return PageDetailsRepository::getInstance()->query($query, ['slug' => $slug, 'languageId' => $languageId])->first();
    }

    public static function DeletePage($id) {
        // clear cache
        $page = self::GetPage($id);
        foreach ($page->Details as $details) {
            MemcachedService::getInstance()->delete("PageDetails" . $details->Slug . $details->LanguageId);
        }
        return PagesRepository::getInstance()->delete(["PageId" => $id]);
    }

    public static function GetPage($id) {
        return PagesRepository::getInstance()->get($id);
    }

    public static function SavePage($page) {
        // clear cache
        if (count($page->Details) > 0) {
            foreach ($page->Details as $details) {
                MemcachedService::getInstance()->delete("PageDetails" . $details->Slug . $details->LanguageId);
            }
        }
        return PagesRepository::getInstance()->save($page);
    }

    public static function SaveDetails($details) {
        // clear cache
        MemcachedService::getInstance()->delete("PageDetails" . $details->Slug . $details->LanguageId);
        return PageDetailsRepository::getInstance()->save($details);
    }

    public static function GetPagesByListOfIds($pages) {
        return PagesRepository::getInstance()->all()->where(["PageId" => $pages])->with(["Details"])->execute();
    }
}