<?php

namespace Data\DataManagers;

use Data\Cache\MemcachedService;
use Data\Repositories\SocialIconsRepository;

class SocialIconsDataManager {
    public static function GetSocialIcons() {
        return SocialIconsRepository::getInstance()->all()->order(["Weight" => "ASC"])->execute();
    }

    public static function GetActiveSocialIconsOrderedByWeight() {
        $result = MemcachedService::getInstance()->get("ActiveSocialIcons");

        if ($result) {
            $icons = $result;
        }
        else {
            $icons = SocialIconsRepository::getInstance()->all()->where(["Active" => 1])->order(["Weight" => "ASC"])->execute();
            MemcachedService::getInstance()->set("ActiveSocialIcons", $icons, MemcachedService::Lifetime);
        }

        return $icons;
    }

    public static function GetSocialIcon($socialIconId) {
        return SocialIconsRepository::getInstance()->get($socialIconId);
    }

    public static function DeleteSocialIcon($socialIconId) {
        MemcachedService::getInstance()->delete("ActiveSocialIcons");
        return SocialIconsRepository::getInstance()->delete(["SocialIconId" => $socialIconId]);
    }

    public static function SaveSocialIcon($model) {
        MemcachedService::getInstance()->delete("ActiveSocialIcons");
        return SocialIconsRepository::getInstance()->save($model);
    }

    public static function GetSocialIconByWeight($weight) {
        return SocialIconsRepository::getInstance()->where(["Weight" => $weight])->first();
    }

    public static function UpdateWeight($weights) {
        MemcachedService::getInstance()->delete("ActiveSocialIcons");

        if (count($weights) > 0) {
            foreach ($weights as $weightItem) {
                $icon = SocialIconsRepository::getInstance()->get($weightItem['ItemId']);
                $icon->Weight = $weightItem['Weight'];
                SocialIconsRepository::getInstance()->save($icon);
            }
        }

        return true;
    }

}