<?php

namespace Data\DataManagers;

use Data\Models\Widget;
use Data\Repositories\WidgetsRepository;

class WidgetsDataManager {

    public static function GetWidget($id) {
        return WidgetsRepository::getInstance()->get($id);
    }

    public static function SaveWidget(Widget $widget) {
        return WidgetsRepository::getInstance()->save($widget);
    }

    public static function DeleteWidget($id) {
        return WidgetsRepository::getInstance()->delete(["WidgetId" => $id]);
    }

}