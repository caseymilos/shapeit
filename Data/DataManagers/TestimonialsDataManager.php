<?php

namespace Data\DataManagers;


use Data\Cache\MemcachedService;
use Data\Models\Testimonial;
use Data\Models\TestimonialDescription;
use Data\Repositories\TestimonialDescriptionsRepository;
use Data\Repositories\TestimonialsRepository;
use Spot\Entity\Collection;

class TestimonialsDataManager {
    public static function GetTestimonials() {
        return TestimonialsRepository::getInstance()->all()->with(["Descriptions"])->order(["Weight" => "ASC"])->execute();
    }

    public static function GetActiveTestimonialsOrderedByWeight() {
        // cached data
        $cachedTestimonials = MemcachedService::getInstance()->get("ActiveTestimonials");
        if ($cachedTestimonials) {
            $result = new Collection();
            foreach ($cachedTestimonials as $testimonialData) {
                $testimonial = new Testimonial($testimonialData);
                $testimonialDescriptions = new Collection();
                if (count($testimonialData["Descriptions"]) > 0) {
                    foreach ($testimonialData["Descriptions"] as $descriptionData) {
                        $testimonialDescriptions->add(new TestimonialDescription($descriptionData));
                    }
                }
                $testimonial->Descriptions = $testimonialDescriptions;
                $result->add($testimonial);
            }
        }
        else {
            $result = TestimonialsRepository::getInstance()->all()->with(["Descriptions"])->where(["Active" => 1])->order(["Weight" => "ASC"])->execute();
            MemcachedService::getInstance()->set("ActiveTestimonials", $result->toArray(), MemcachedService::Lifetime);
        }

        return $result;
    }

    public static function GetTestimonial($testimonialId) {
        return TestimonialsRepository::getInstance()->get($testimonialId);
    }

    public static function DeleteTestimonial($testimonialId) {
        MemcachedService::getInstance()->delete("ActiveTestimonials");
        return TestimonialsRepository::getInstance()->delete(["TestimonialId" => $testimonialId]);
    }

    public static function SaveTestimonial($testimonial) {
        MemcachedService::getInstance()->delete("ActiveTestimonials");
        return TestimonialsRepository::getInstance()->save($testimonial);
    }

    public static function GetTestimonialByWeight($weight) {
        return TestimonialsRepository::getInstance()->where(["Weight" => $weight])->with(["Descriptions"])->first();
    }

    public static function UpdateWeight($weights) {
        MemcachedService::getInstance()->delete("ActiveTestimonials");

        if (count($weights) > 0) {
            foreach ($weights as $weightItem) {
                $testimonial = TestimonialsRepository::getInstance()->get($weightItem['ItemId']);
                $testimonial->Weight = $weightItem['Weight'];
                TestimonialsRepository::getInstance()->save($testimonial);
            }
        }

        return true;
    }

    // Testimonials descriptions

    public static function SaveTestimonialDescription($testimonial) {
        MemcachedService::getInstance()->delete("ActiveTestimonials");
        return TestimonialDescriptionsRepository::getInstance()->save($testimonial);
    }

}