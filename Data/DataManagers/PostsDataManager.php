<?php

namespace Data\DataManagers;


use Data\Models\Post;
use Data\Repositories\PostsRepository;
use Data\Repositories\RelatedPostsRepository;

class PostsDataManager {
	/**
	 * @return Post[]
	 */
	public static function GetPosts() {


		return PostsRepository::getInstance()->all()->execute();
	}

	public static function DeletePost($id) {

		return PostsRepository::getInstance()->delete(["PostId" => $id]);
	}

	public static function GetPost($id) {
		return PostsRepository::getInstance()->get($id);
	}

	public static function SavePost($model) {
		return PostsRepository::getInstance()->save($model);
	}

	public static function GetPostsByListOfIds($posts) {
		return PostsRepository::getInstance()->all()->where(["PostId" => $posts]);
	}

	public static function GetPostBySlug($slug) {
		return PostsRepository::getInstance()->first(['Slug' => $slug]);
	}

	public static function RemoveRelation($parentId, $childId) {
		return RelatedPostsRepository::getInstance()->delete(["ParentId" => $parentId, "ChildId" => $childId]);
	}

	public static function AddRelation($relation) {
		return RelatedPostsRepository::getInstance()->save($relation);
	}

	public static function GetPostsByPostTypeId($postTypeId = null, $offset = null, $limit = null, $count = false) {

		$wheres = [];
		if (!is_null($postTypeId)) {
			$wheres["PostTypeId"] = $postTypeId;
		}

		if ($count) {
			return PostsRepository::getInstance()->all()->where($wheres)->count();
		}

		return PostsRepository::getInstance()->all()->where($wheres)->limit($limit, $offset)->order(["Date" => "DESC"]);
	}


}