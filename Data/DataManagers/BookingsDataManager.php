<?php

namespace Data\DataManagers;


use Data\Models\Booking;
use Data\Repositories\BookingsRepository;

class BookingsDataManager {
	/**
	 * @return Booking[]
	 */
	public static function GetBookings() {

		return BookingsRepository::getInstance()->all()->execute();
	}

	public static function DeleteBooking($id) {

		return BookingsRepository::getInstance()->delete(["BookingId" => $id]);
	}

	public static function GetBooking($id) {
		return BookingsRepository::getInstance()->get($id);
	}

	public static function SaveBooking($model) {
		return BookingsRepository::getInstance()->save($model);
	}

}