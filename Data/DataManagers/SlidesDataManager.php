<?php

namespace Data\DataManagers;


use Data\Cache\MemcachedService;
use Data\Models\Slide;
use Data\Models\SlideDescription;
use Data\Repositories\SlideDescriptionsRepository;
use Data\Repositories\SlidesRepository;
use Spot\Entity\Collection;

class SlidesDataManager {
    public static function GetSlides() {
        return SlidesRepository::getInstance()->all()->with(["Descriptions"])->order(["Weight" => "ASC"])->execute();
    }

    public static function GetActiveSlidesOrderedByWeight() {
        // cached data
        $cachedSlides = MemcachedService::getInstance()->get("ActiveSlides");
        if ($cachedSlides) {
            $result = new Collection();
            foreach ($cachedSlides as $slideData) {
                $slide = new Slide($slideData);
                $slideDescriptions = new Collection();
                if (count($slideData["Descriptions"]) > 0) {
                    foreach ($slideData["Descriptions"] as $descriptionData) {
                        $slideDescriptions->add(new SlideDescription($descriptionData));
                    }
                }
                $slide->Descriptions = $slideDescriptions;
                $result->add($slide);
            }
        }
        else {
            $result = SlidesRepository::getInstance()->all()->with(["Descriptions"])->where(["Active" => 1])->order(["Weight" => "ASC"])->execute();
            MemcachedService::getInstance()->set("ActiveSlides", $result->toArray(), MemcachedService::Lifetime);
        }

        return $result;
    }

    public static function GetSlide($slideId) {
        return SlidesRepository::getInstance()->get($slideId);
    }

    public static function DeleteSlide($slideId) {
        MemcachedService::getInstance()->delete("ActiveSlides");
        return SlidesRepository::getInstance()->delete(["SlideId" => $slideId]);
    }

    public static function SaveSlide($slide) {
        MemcachedService::getInstance()->delete("ActiveSlides");
        return SlidesRepository::getInstance()->save($slide);
    }

    public static function GetSlideByWeight($weight) {
        return SlidesRepository::getInstance()->where(["Weight" => $weight])->with(["Descriptions"])->first();
    }

    public static function UpdateWeight($weights) {
        MemcachedService::getInstance()->delete("ActiveSlides");

        if (count($weights) > 0) {
            foreach ($weights as $weightItem) {
                $slide = SlidesRepository::getInstance()->get($weightItem['ItemId']);
                $slide->Weight = $weightItem['Weight'];
                SlidesRepository::getInstance()->save($slide);
            }
        }

        return true;
    }

    // Slides descriptions

    public static function SaveSlideDescription($slide) {
        MemcachedService::getInstance()->delete("ActiveSlides");
        return SlideDescriptionsRepository::getInstance()->save($slide);
    }

}