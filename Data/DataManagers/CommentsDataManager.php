<?php


namespace Data\DataManagers;

use Data\Models\Comment;
use Data\Repositories\CommentsRepository;


class CommentsDataManager {
	/**
	 * @return Comment[]
	 */
	public static function GetCommentsByPostId($postId) {
		return CommentsRepository::getInstance()->get($postId);
	}

	public static function SaveComment($model) {
		return CommentsRepository::getInstance()->save($model);
	}


}