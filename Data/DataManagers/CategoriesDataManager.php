<?php
/**
 * Created by PhpStorm.
 * User: Marko
 * Date: 04/10/2016
 * Time: 14:21
 */

namespace Data\DataManagers;

use Data\Cache\MemcachedService;
use Data\Models\Category;
use Data\Models\CategoryDetails;
use Data\Repositories\CategoryDetailsRepository;
use Data\Repositories\CategoriesRepository;
use Spot\Entity\Collection;


class CategoriesDataManager
{
    public static function GetCategory($categoryId) {
        return CategoriesRepository::getInstance()->get($categoryId);
    }

    public static function DeleteCategory($categoryId) {
        MemcachedService::getInstance()->delete("ActiveCategories");
        return CategoriesRepository::getInstance()->delete(["CategoryId" => $categoryId]);
    }

    public static function SaveCategory($category) {
        MemcachedService::getInstance()->delete("ActiveCategories");
        return CategoriesRepository::getInstance()->save($category);
    }

    // Category details

    public static function SaveCategoryDetails($category) {
        MemcachedService::getInstance()->delete("ActiveDisciplines");
        return CategoryDetailsRepository::getInstance()->save($category);
    }

    public static function GetCategories()
    {
        return CategoriesRepository::getInstance()->all()->with(["CategoryDetails"]);
    }


}