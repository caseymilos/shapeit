<?php

namespace Data\DataManagers;


use Data\Models\Ad;
use Data\Repositories\AdsRepository;

class AdsDataManager {
	/**
	 * @return Ad[]
	 */
	public static function GetAds($position = null) {
	    $wheres = [];
	    if(!empty($position)) {
	        $wheres["Position"] = $position;
        }
		return AdsRepository::getInstance()->all()->where($wheres)->execute();
	}

	public static function DeleteAd($id) {

		return AdsRepository::getInstance()->delete(["AdId" => $id]);
	}

	public static function GetAd($id) {
		return AdsRepository::getInstance()->get($id);
	}

	public static function SaveAd($model) {
		return AdsRepository::getInstance()->save($model);
	}

}