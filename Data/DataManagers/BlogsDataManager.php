<?php

namespace Data\DataManagers;


use Data\Models\Blog;
use Data\Repositories\BlogsRepository;
use Data\Repositories\RelatedBlogsRepository;

class BlogsDataManager {
	/**
	 * @return Blog[]
	 */
	public static function GetBlogs($excludedBlogs = []) {
		$wheres = [];
		if (!empty($excludedBlogs)) {
			$wheres["BlogId NOT IN"] = $excludedBlogs;
		}

		return BlogsRepository::getInstance()->all()->where($wheres)->order(["Date" => "DESC"])->execute();
	}

	/**
	 * @return Blog[]
	 */
	public static function GetLatestBlogs($limit) {
		return BlogsRepository::getInstance()->all()->limit($limit)->order(["Date" => "DESC"])->execute();
	}

	public static function DeleteBlog($id) {

		return BlogsRepository::getInstance()->delete(["BlogId" => $id]);
	}

	public static function GetBlog($id) {
		return BlogsRepository::getInstance()->get($id);
	}

	public static function SaveBlog($model) {
		return BlogsRepository::getInstance()->save($model);
	}

	public static function GetBlogsByListOfIds($offers) {
		return BlogsRepository::getInstance()->all()->where(["BlogId" => $offers]);
	}

	public static function GetBlogBySlug($slug) {
		return BlogsRepository::getInstance()->first(['Slug' => $slug]);
	}

	public static function AddRelation($relation) {
		return RelatedBlogsRepository::getInstance()->save($relation);

	}

	public static function DeleteRelation($parentId, $childId) {
		return RelatedBlogsRepository::getInstance()->delete(["ParentId" => $parentId, "ChildId" => $childId]);
	}


}