<?php

namespace Data\Repositories;

class LogSessionsRepository extends BaseRepository
{

    const ConnectionName = 'logserver';
    const Model = 'Data\Models\LogSession';

}