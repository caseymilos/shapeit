<?php

namespace Data\Repositories;

class LogEntriesRepository extends BaseRepository
{

    const ConnectionName = 'logserver';
    const Model = 'Data\Models\LogEntry';

}