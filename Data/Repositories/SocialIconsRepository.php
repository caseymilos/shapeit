<?php

namespace Data\Repositories;

use Data\Database\MysqliDb;

class SocialIconsRepository extends BaseRepository {

    const Model = 'Data\Models\SocialIcon';

}