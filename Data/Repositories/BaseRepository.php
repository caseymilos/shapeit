<?php
/**
 * Created by PhpStorm.
 * User: nikola
 * Date: 4.6.16.
 * Time: 18.31
 */

namespace Data\Repositories;


use Business\Helpers\PHPHelper;
use Data\Database\Database;
use Business\DTO\DTColumnSearchDTO;
use Business\DTO\DTDataDTO;
use Business\DTO\DTQueryDTO;
use Business\DTO\RequiredConditionDTO;
use Spot\Mapper;

class BaseRepository
{

    const ConnectionName = 'default';
    const Model = null;

    /**
     * @return Mapper
     */
    public static function getInstance()
    {
        $db = Database::getInstance(static::ConnectionName);
        $mapper = $db->mapper(static::Model);
        return $mapper;
    }

    /**
     * @param integer $start
     * @param integer $length
     * @param array $columns
     * @param array $groupableColumns
     * @param array $numericColumns
     * @param string[] $joins
     * @param string[] $groupBy
     * @param RequiredConditionDTO $requiredCondition
     * @param string[] $requiredConditionJoins
     * @param string $countSelector
     * @return DTQueryDTO
     */


    public static function PrepareDataTableQuery($start, $length, $columns = [], $groupableColumns = [],
                                                 $numericColumns = [], $joins = [], $groupBy = null, $requiredCondition = null, $requiredConditionJoins = [], $countSelector = null,
                                                 $selectRequiredJoins = true)
    {
        $tableName = call_user_func(array(static::Model, "table"));
        $primaryKey = call_user_func(array(static::Model, "getPrimaryKey"));


        $selectQuery = "SELECT";
        $selectQuery .= sprintf(' %s.%s ', $tableName, $primaryKey);
        $orderByQuery = "";
        $searchableQuery = "";

        $filteredRecordsCountQuery = "";

        $columnsDTO = [];
        foreach ($columns as $key => $column) {
            if (!empty((trim($column->Name)))) {
                $columnsArray = explode("|", $column->Name);
                $parsedColumnsArray = [];
                foreach ($columnsArray as $columnName) {
                    $singleColumnString = preg_replace("/(?<=[a-zA-Z])(?=[A-Z])/", ".", $columnName, 1);
                    $parsedColumnsArray[] = $singleColumnString;
                }
                //$columnString = preg_replace("/(?<=[a-zA-Z])(?=[A-Z])/", ".", $column->Name, 1);
                $columnString = join("|", $parsedColumnsArray);
                $numeric = in_array($column->Name, $numericColumns);

                $groupable = isset($groupableColumns[$column->Name]) ? $groupableColumns[$column->Name] : false;
                $columnDTO = new DTColumnSearchDTO($column->Name, $columnString, $numeric, $groupable, $column->OrderBy, $column->Searchable);
                $columnsDTO[] = $columnDTO;

                //generate selectString
                if (!empty($selectQuery)) {
                    $selectQuery .= ", ";
                }
                $selectQuery .= $columnDTO->QueryString;

                if (isset($columnDTO->OrderBy)) {
                    if (empty($orderByQuery)) {
                        $orderByQuery .= "ORDER BY ";
                    } else {
                        $orderByQuery .= ", ";
                    }
                    $orderByQuery .= sprintf("%s %s", $columnDTO->Column, $columnDTO->OrderBy);
                }
                if (isset($columnDTO->Searchable)) {
                    if (!empty($searchableQuery)) {
                        $searchableQuery .= " OR ";
                    } else {
                        $searchableQuery .= " (";
                    }
                    //search integer values with = (equals) operator instead of % (like)
                    //if (gettype($columnDTO->Searchable) == "integer") {
                    //ToDo: Prodiskutovati ovo - da li zelimo da ukoliko je bot id 22 a search string 2 on vrati oba bota?
                    if ($columnDTO->Numeric || gettype($columnDTO->Searchable) == "integer") {
                        $searchableQuery .= $columnDTO->Column . " ='" . $columnDTO->Searchable . "'";
                    } else {
                        $searchableQuery .= $columnDTO->Column . ' like "%' . $columnDTO->Searchable . '%"';
                    }
                }
            }
        }
        //close queries and  add where condition if there's required paramter
        if ($requiredCondition && $requiredCondition->Value) {
            //$selectQuery .= ", if (user_businesses.UserId != '', user_businesses.UserId, 'N/A') as UserId";
            $selectQuery .= sprintf(", if (%s != '', %s, 'N/A') as %s", $requiredCondition->QuerySelector(),
                $requiredCondition->QuerySelector(), PHPHelper::RemoveDots($requiredCondition->QuerySelector()));
        }

        //add joins
        $joinsQuery = join(" ", $joins);
        if ($selectRequiredJoins && $requiredCondition && $requiredCondition->Value && $requiredConditionJoins) {
            $joinsQuery = sprintf("%s %s", $joinsQuery, join(" ", $requiredConditionJoins));
        }

        $selectQuery .= sprintf(" FROM %s", $tableName);

        $whereQuery = "";
        if (!empty($searchableQuery)) {
            if ($requiredCondition && $requiredCondition->Value) {
                $searchableQuery = sprintf("(%s)", $searchableQuery);
                $searchableQuery .= sprintf(" AND %s = '%s')", $requiredCondition->QuerySelector(), $requiredCondition->Value);
            } else {
                $searchableQuery .= ")";
            }
            $whereQuery = sprintf("WHERE %s", $searchableQuery);
        } else if ($requiredCondition && $requiredCondition->Value) {
            $whereQuery .= sprintf(" WHERE %s = '%s'", $requiredCondition->QuerySelector(), $requiredCondition->Value);
        }

        $groupByQuery = $groupBy ? sprintf("GROUP BY %s", $groupBy) : "";

        $limitQuery = "";
        if (isset($length)) {
            $limitQuery .= sprintf(" LIMIT %s", $length);
        }
        if (isset($start)) {
            $limitQuery .= sprintf(" OFFSET %s", $start);
        }

        $completeSelectQuery = sprintf("%s %s %s %s %s %s", $selectQuery, $joinsQuery, $whereQuery, $groupByQuery, $orderByQuery, $limitQuery);

        $countSelector = $countSelector ? $countSelector : sprintf("%s.%s", $tableName, $primaryKey);
        $countTableStrArray = explode(".", $countSelector);
        $countTable = count($countTableStrArray) > 1 ? $countTableStrArray[0] : $tableName;

        if (!empty($searchableQuery)) {
            $filteredRecordsCountQuery = sprintf("SELECT COUNT(DISTINCT %s) FilteredRecords from %s %s %s", $countSelector, $countTable, $joinsQuery, $whereQuery);

        }

        $countWhereQuery = "";
        $countJoins = "";

        //if there's required condition (eg get entities by user id - count only entities that are related to condition)
        if ($requiredCondition && $requiredCondition->Value) {
            $countWhereQuery = sprintf(" WHERE %s = '%s'", $requiredCondition->QuerySelector(), $requiredCondition->Value);
            $countJoins = join(" ", array_merge($joins, $requiredConditionJoins));
        }

        $countQuery = sprintf("SELECT COUNT(DISTINCT %s ) AS TotalRecords from %s %s %s ", $countSelector, $countTable, $countJoins, $countWhereQuery);

        return new DTQueryDTO($completeSelectQuery, $countQuery, $columnsDTO, $filteredRecordsCountQuery);
    }

    /**
     * @param DTQueryDTO $queries
     * @return DTDataDTO;
     */
    public static function executeDTQuery(DTQueryDTO $queries)
    {
        $db = static::getInstance();


        $data = $db->query($queries->SelectQuery);
        $count = $db->query($queries->CountQuery);

        $newData = [];
        foreach ($data as $oneData) {
            $newData[] = $oneData->toArray();
        }
        $parsedTotalRecords = ($count[0]->toArray());

        $DTData = new DTDataDTO($newData, $parsedTotalRecords["TotalRecords"]);

        if (!empty($queries->FilteredQuery)) {
            $filteredRecords = $db->query($queries->FilteredQuery);
            $parsedFilteredRecords = $filteredRecords[0]->toArray();
            $DTData->FilteredRecords = $parsedFilteredRecords["FilteredRecords"];
        } else {
            $DTData->FilteredRecords = $DTData->TotalRecords;
        }
        return $DTData;
    }


}