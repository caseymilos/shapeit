<?php

namespace Data\Repositories;

class LogFilesRepository extends BaseRepository
{

    const ConnectionName = 'logserver';
    const Model = 'Data\Models\LogFile';

}