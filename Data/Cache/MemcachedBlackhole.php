<?php

namespace Data\Cache;

class MemcachedBlackhole
{

    public function get($key, callable $cache_cb = null, &$cas_token = null)
    {
        return false;
    }

    public function set($key, $value, $expiration = null)
    {
        return true;
    }

    public function delete($key, $time = 0)
    {
        return true;
    }

}