<?php

namespace Data\Cache;

use Business\Utilities\Config\Config;

class MemcachedService
{

    const Lifetime = 2592000; // 30 days

    private static $instance = null;

    private function __construct()
    {

    }

    /**
     * @return \Memcached|null
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            $config = Config::GetInstance();

            if ($config->memcached->use == false) {
                self::$instance = new MemcachedBlackhole();
            } else {
                self::$instance = new \Memcached();
                self::$instance->addServer($config->memcached->host, $config->memcached->port);
                self::$instance->setOption(\Memcached::OPT_PREFIX_KEY, $config->memcached->prefix);
            }
        }

        return self::$instance;
    }
}